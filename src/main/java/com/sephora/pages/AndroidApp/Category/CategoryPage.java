package com.sephora.pages.AndroidApp.Category;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.sephora.base.pages.SephoraAndroidAppBasePage;
import com.sephora.core.driver.mobile.SephoraMobileDriver;
import com.sephora.pages.AndroidApp.Product.ProductPage;

/**==========================================================================================================
 * CategoryPage is the page which
 * contains Category specific reusable components 
 * 
 ==========================================================================================================*/

public class CategoryPage extends SephoraAndroidAppBasePage{

	public CategoryPage(SephoraMobileDriver driver) {
		super(driver);
	}
	
	private static final Logger logger = LogManager.getLogger(CategoryPage.class.getName());
	
	public static final By FACE_MAKEUP_VIEW_EXPAND = By.xpath("//android.widget.LinearLayout[@index='0']/android.widget.ImageView");
	public static final By EYE_MAKEUP_VIEW_EXPAND = By.xpath("//android.widget.LinearLayout[@index='2']/android.widget.ImageView");
	public static final By NAILS_VIEW_EXPAND = By.xpath("//android.widget.LinearLayout[@index='3']/android.widget.ImageView");
	public static final By LIP_VIEW_EXPAND = By.xpath("//android.widget.LinearLayout[@index='4']/android.widget.ImageView");
	public static final By BRUSHES_AND_APPLICATORS_EXPAND = By.xpath("//android.widget.LinearLayout[@index='5']/android.widget.ImageView");
	public static final By VALUE_AND_GIFT_SET_EXPAND = By.xpath("//android.widget.LinearLayout[@index='6']/android.widget.ImageView");
	public static final By SELECT_CATEGORY= By.xpath("//android.widget.LinearLayout/android.widget.ImageView");
	public static final By FACE_MAKEUP_SUBCATEGORY_FOUNDATION= By.xpath("//android.widget.TextView[@text='Foundation']");
	public static final By CATEGORIES_SUB_SECTION_SALE = By.xpath("//android.widget.ImageView[@index='10']");
	//public static final By SELECT_SUB_CATEGORY= By.id("com.sephora:id/category_item_container");
	public static final By SELECT_SUB_CATEGORY= By.id("com.sephora:id/category_name");
	public static final By SELECT_THIRD_LEVEL_CATEGORY = By.id("com.sephora:id/subcategory");
	
	/**==========================================================================================================
	 * DESCRIPTION: To Select 1st level category
	 * PARAMETERS: int index, String category
	 * RETURN: CategoryPage
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	
	public CategoryPage selectCategory(int index, String category){
		waitForElementPresent(SELECT_CATEGORY);
	    List<WebElement> list_Categories= findElements(SELECT_CATEGORY);
		System.out.println(list_Categories.size());
		list_Categories.get(index).click();
		pauseExecutionFor(2000);
		logger.info("Top level Category selected as: "+ category);
		return new CategoryPage(driver);
	}
	
	/**==========================================================================================================
	 * DESCRIPTION: To click sub category (level 2 category) on index basis
	 * PARAMETERS: int indexSublevelCategory
	 * RETURN: CategoryPage
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	public CategoryPage clickSubCategory(int indexSublevelCategory){
		waitForElementPresent(SELECT_SUB_CATEGORY);
		List<WebElement> list_Categories= findElements(SELECT_SUB_CATEGORY);
		System.out.println(list_Categories.size());
		String subCategory = list_Categories.get(indexSublevelCategory).getText();
		list_Categories.get(indexSublevelCategory).click();
		pauseExecutionFor(2000);
		logger.info("Sub level Category selected as: "+ subCategory);
		return new CategoryPage(driver);
	}
	
	/**==========================================================================================================
	 * DESCRIPTION: To click third level category on index basis
	 * PARAMETERS: int indexThirdLevel
	 * RETURN: CategoryPage
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	public CategoryPage clickThirdLevelCategory(int indexThirdLevel){
		waitForElementPresent(SELECT_THIRD_LEVEL_CATEGORY);
		List<WebElement> list_Categories= findElements(SELECT_THIRD_LEVEL_CATEGORY);
		System.out.println(list_Categories.size());
		String thirdLevelCategory = list_Categories.get(indexThirdLevel).getText();
		list_Categories.get(indexThirdLevel).click();
		logger.info("Third level Category selected as: "+ thirdLevelCategory);
		return new CategoryPage(driver);
	}
	
}
