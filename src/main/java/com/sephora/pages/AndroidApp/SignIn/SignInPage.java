package com.sephora.pages.AndroidApp.SignIn;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;

import com.sephora.base.pages.SephoraAndroidAppBasePage;
import com.sephora.core.driver.mobile.SephoraMobileDriver;
import com.sephora.pages.AndroidApp.Home.HomePage;

public class SignInPage  extends SephoraAndroidAppBasePage{
	public SignInPage(SephoraMobileDriver driver) {
		super(driver);
	}
	
	private static final Logger logger = LogManager.getLogger(SignInPage.class.getName());
	
	public static final By SIGN_IN_EMAIL_TEXTFIELD_LOC = By.xpath("//android.widget.LinearLayout[@index='1']/android.widget.EditText");
	public static final By SIGNIN_BUTTON = By.xpath("//android.widget.Button[@text='SIGN IN']");
	public static final By CREATE_ACCOUNT_BUTTON = By.xpath("//android.widget.Button[@text='CREATE ACCOUNT']");
	public static final By SIGN_IN_PASSOWRD_TEXTFIELD_LOC = By.xpath("//android.widget.LinearLayout[@index='2']/android.widget.EditText");
    public static final By STAY_SIGN_IN_SWITCH = By.id("com.sephora:id/login_ssi");
    public static final String ON_KEYWORD = "ON";
    public static final By SIGN_IN_ERROR_MESSAGE = By.xpath("//android.widget.RelativeLayout[@index='0']/android.widget.TextView");
    public static final By FORGOT_PASSWORD_LINK = By.xpath("//android.widget.LinearLayout[@index='3']/android.widget.TextView[@text='forgot?']");
    public static final By CANCEL_BUTTON=By.xpath("//android.widget.TextView[@text='Cancel']");
    public static final By SIGN_IN_ERROR_OK = By.xpath("//android.widget.TextView[@text='OK']");
    
	 public SignInPage WaitforSignInPagetoLoad(){
		 waitForElementPresent(SIGNIN_BUTTON);
		 waitForElementPresent(CREATE_ACCOUNT_BUTTON);
		 return new SignInPage(driver);
		 }
	 
	 public SignInPage typeEmail(String email) {
			waitForElementPresent(SIGN_IN_EMAIL_TEXTFIELD_LOC);
			clear(SIGN_IN_EMAIL_TEXTFIELD_LOC);
			type(SIGN_IN_EMAIL_TEXTFIELD_LOC, email);
			logger.info(email+" typed in email field");
			return new SignInPage(driver);
		}
	 
	 public SignInPage typePassword(String password) {
			waitForElementPresent(SIGN_IN_PASSOWRD_TEXTFIELD_LOC);
			pauseExecutionFor(6000);
			clear(SIGN_IN_PASSOWRD_TEXTFIELD_LOC);
			type(SIGN_IN_PASSOWRD_TEXTFIELD_LOC, password);
			logger.info(password +" typed in Password field");
			return new SignInPage(driver);
		}
	 
	 public void clickSignInButton() {
			waitForElementPresent(SIGNIN_BUTTON);
			click(SIGNIN_BUTTON);
			logger.info("click on 'Sign In' button from sign in overlay");
			pauseExecutionFor(5000);
		}
	 
	 public void clickCreateAccountButton() {
			waitForElementPresent(CREATE_ACCOUNT_BUTTON);
			click(CREATE_ACCOUNT_BUTTON);
			logger.info("click on 'Sign In' button from sign in overlay");
			pauseExecutionFor(5000);
		}
	 
	 public SignInPage clearAllSignInFields() {
			clear(SIGN_IN_EMAIL_TEXTFIELD_LOC);
			clear(SIGN_IN_PASSOWRD_TEXTFIELD_LOC);
			return new SignInPage(driver);
		}
	 
	 public SignInPage clickStaySignInSwitch(String status) {
		 if(status=="ON"){
			 if(isStaySignInSwitchON()){
				 logger.info("Stay Sign In switch is already in ON state");
			 }
			 else{
				 waitForElementPresent(STAY_SIGN_IN_SWITCH);
					click(STAY_SIGN_IN_SWITCH);
					logger.info("SSI toggle is clicked");
					return this;
			 }
		 }
		 else if(status=="OFF"){
			 if(isStaySignInSwitchON()){
				 waitForElementPresent(STAY_SIGN_IN_SWITCH);
					click(STAY_SIGN_IN_SWITCH);
					logger.info("SSI toggle is clicked");
					return this;
			 }
			 else{
				 logger.info("Stay Sign In switch is already in OFF state");
			 }
		 }
		 return this;
		}
		
		public boolean isStaySignInSwitchPresent() {
			if(waitExplicitly(STAY_SIGN_IN_SWITCH, 10)) {
				return true;
			}
			logger.info("SSI toggel is not present");
			return false;
		}
		
		public boolean isStaySignInSwitchON() {
			if(getText(STAY_SIGN_IN_SWITCH).contains(ON_KEYWORD))
				return true;
			logger.info("SSI toggel is OFF");
			return false;
		}
		
		public boolean isSignInErrorMessageValid(String error){
			waitForElementPresent(SIGN_IN_ERROR_MESSAGE);
			logger.info("Error message contains: " + error);
			String ActualError= getText(SIGN_IN_ERROR_MESSAGE);
			logger.info("Actual Error message is: " +ActualError);
			if(ActualError.contains(error)){
				click(SIGN_IN_ERROR_OK);
				return true;
			}
			return false;
		}
		
		public HomePage signIn(String email, String password, String ssiStatus){
			waitForElementPresent(SIGNIN_BUTTON);
			typeEmail(email).typePassword(password);
			clickStaySignInSwitch(ssiStatus);
			clickSignInButton();
			return new HomePage(driver);
			
		}
		
}
