package com.sephora.pages.AndroidApp.SignIn;

import java.io.IOException;
import org.openqa.selenium.By;

import com.sephora.base.pages.SephoraAndroidAppBasePage;
import com.sephora.core.driver.mobile.SephoraMobileDriver;
import com.sephora.pages.AndroidApp.Home.HomePage;

public class CreateAccountPage extends SephoraAndroidAppBasePage {

	public CreateAccountPage(SephoraMobileDriver driver) {
		super(driver);
	}
	
	public static final By CREATE_ACCOUNT_EMAIL_TEXTFIELD_LOC = By.xpath("//android.widget.LinearLayout[@text='Email Address']/android.widget.EditText");
	public static final By FIRST_NAME_FIELD_LOC = By.xpath("//android.widget.LinearLayout[@text='First Name']/android.widget.EditText");
	public static final By LAST_NAME_FIELD_LOC = By.xpath("//android.widget.LinearLayout[@text='Last Name']/android.widget.EditText");
	public static final By PASSWORD_FIELD_LOC = By.xpath("//android.widget.LinearLayout[@index='3']/android.widget.EditText");
	public static final By CONFIRM_PASSWORD_FIELD_LOC= By.xpath("//android.widget.LinearLayout[@text='Confirm Password']/android.widget.EditText");
	public static final By STAY_SIGN_IN_SWITCH = By.id("com.sephora:id/create_account_ssi");
	public static final By COMPLETE_REGISTRATION_BUTTON_LOC= By.xpath("//android.widget.Button[@text='COMPLETE REGISTRATION']");
			
	public boolean isAccountPage(){
		waitForElementPresent(WINDOW_HEADER);
		if(getText(WINDOW_HEADER).equalsIgnoreCase("CREATE ACCOUNT")){
			logger.info("current page is CREATE ACCOUNT");
			return true;
		}
		return false;
	}
	
	 public CreateAccountPage typeNewUserEmail() {
		 String email = newUser("Testautom");
			waitForElementPresent(CREATE_ACCOUNT_EMAIL_TEXTFIELD_LOC);
			clear(CREATE_ACCOUNT_EMAIL_TEXTFIELD_LOC);
			type(CREATE_ACCOUNT_EMAIL_TEXTFIELD_LOC, email);
			logger.info(email+" : typed in email field");
			return new CreateAccountPage(driver);
		}
	 
	 public CreateAccountPage typeFirstName(String firstName) {
			type(FIRST_NAME_FIELD_LOC, firstName);
			logger.info(firstName+" - type First name in register overlay");
			return new CreateAccountPage(driver);
		}
	 
	 public HomePage registerNewUserNonBI(String FirstName, String lastName, String password, String confirmPassword, String ssiStatus) throws IOException{
		 logger.info("Trying to register a New Non BI user");
//		 typeFirstName(FirstName).typeLastName(LastName).typePassword(Password).typeConfirmPassword(ConfirmPassword).keyboardOperationDone();
		 typeFirstName(FirstName).typeLastName(lastName).typePassword(lastName).typeConfirmPassword(confirmPassword);
		clickStaySignInSwitch(ssiStatus).clickCompleteRegistrationButton();
		 return new HomePage(driver);
	 }
	 
	 public CreateAccountPage clearFirstName() {
			clear(FIRST_NAME_FIELD_LOC);
			return new CreateAccountPage(driver);
		}
	 
	 public CreateAccountPage typeLastName(String lastName) {
			type(LAST_NAME_FIELD_LOC, lastName);
			logger.info(lastName+" - type Last Name in register overlay");
			return new CreateAccountPage(driver);
		}
	 
	 public CreateAccountPage clearLastName() {
			clear(LAST_NAME_FIELD_LOC);
			return new CreateAccountPage(driver);
		}
	 
	 public CreateAccountPage typePassword(String password) {
			type(PASSWORD_FIELD_LOC, password);
			logger.info(password+" - type New Password in register overlay");
			return new CreateAccountPage(driver);
		}
	 
	 public CreateAccountPage typeConfirmPassword(String confirmPassword) {
			type(CONFIRM_PASSWORD_FIELD_LOC, confirmPassword);
			logger.info(confirmPassword+" - type confirm password in register overlay");
			return new CreateAccountPage(driver);
		}
	 
		public void clickCompleteRegistrationButton() {
			waitForElementPresent(COMPLETE_REGISTRATION_BUTTON_LOC);
			click(COMPLETE_REGISTRATION_BUTTON_LOC);
			logger.info("click on 'Complete Registration' button on Create Account Page successful");
			pauseExecutionFor(5000);
		}
		
		public boolean isStaySignInSwitchPresent() {
			if(waitExplicitly(STAY_SIGN_IN_SWITCH, 10)) {
				return true;
			}
			logger.info("SSI toggel is not present");
			return false;
		}
	 
	 public CreateAccountPage clickStaySignInSwitch(String status) {
		 if(status=="ON"){
			 if(isStaySignInSwitchON()){
				 logger.info("Stay Sign In switch is already in ON state");
			 }
			 else{
				 waitForElementPresent(STAY_SIGN_IN_SWITCH);
					click(STAY_SIGN_IN_SWITCH);
					logger.info("SSI toggle is clicked");
					logger.info("SSI is now in ON State");
					return this;
			 }
		 }
		 else if(status=="OFF"){
			 if(isStaySignInSwitchON()){
				 waitForElementPresent(STAY_SIGN_IN_SWITCH);
					click(STAY_SIGN_IN_SWITCH);
					logger.info("SSI toggle is clicked");
					logger.info("SSI is now in OFF State");
					return this;
			 }
			 else{
				 logger.info("Stay Sign In switch is already in OFF state");
			 }
		 }
		 return this;
		}
		
		
		public boolean isStaySignInSwitchON() {
			waitForElementPresent(STAY_SIGN_IN_SWITCH);
			if(getText(STAY_SIGN_IN_SWITCH).contains(ON_KEYWORD)){
				logger.info("SSI toggle is ON");
				return true;
			}
			logger.info("SSI toggle is OFF");
			return false;
		}
}
