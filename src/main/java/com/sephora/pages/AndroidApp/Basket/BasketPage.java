package com.sephora.pages.AndroidApp.Basket;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.sephora.base.pages.SephoraAndroidAppBasePage;
import com.sephora.core.driver.mobile.SephoraMobileDriver;
import com.sephora.pages.AndroidApp.Checkout.CheckoutPage;
import com.sephora.core.driver.mobile.SephoraMobileDriver;

/**==========================================================================================================
 * BasketPage is the page which
 * contains basket specific reusable components 
 * 
 ==========================================================================================================*/

public class BasketPage extends SephoraAndroidAppBasePage{

	public BasketPage(SephoraMobileDriver driver) {
		super(driver);
	}
	
	private static final Logger logger = LogManager.getLogger(BasketPage.class.getName());
	public static final By CHECKOUT_BUTTON = By.id("com.sephora:id/basket_checkout_button");
	public static final By QUANTITY_OF_PRODUCT_ADDED = By.xpath("//android.widget.RelativeLayout[@index='0']/android.widget.Spinner/android.widget.RelativeLayout/android.widget.TextView");
	public static final By REMOVE_BUTTON = By.xpath("//android.widget.RelativeLayout[@index='0']/android.widget.TextView[@index='3']");
	public static final By MOVE_TO_LOVES_BUTTON = By.xpath("//android.widget.RelativeLayout[@index='0']/android.widget.TextView[@index='4']");
	public static final By BASKET_ORDER_TOTAL_ITEMS= By.id("com.sephora:id/order_total_price_label");
	public static final By BASKET_ORDER_TOTAL= By.id("com.sephora:id/order_total_price");
	public static final By PROMO_ADDED_MESSAGE_LOC = By.id("com.sephora:id/basket_promotion_coupon");
	public static final By PROMO_ITEM_NAME = By.id("com.sephora:id/promotion_item_name");
	public static final By PROMO_ITEM_PRICE = By.id("com.sephora:id/cart_item_product_price");
	public static final By PROMO_NAME_LOC = By.id("com.sephora:id/promotion_item_name");
	public static final By PROMO_ITEM_NAME_GWP = By.id("com.sephora:id/cart_item_brand_name");
	public static final By PROMO_ITEM_DESCRIPTION_GWP = By.id("com.sephora:id/cart_item_product_name");
	public static final By PROMO_REMOVE_BUTTON = By.id("com.sephora:id/promotion_item_remove_button");
	public static final By MERCHANDISE_TOTAL_VALUE = By.id("com.sephora:id/price_summary_merch_subtotal_value");
	public static final By DISCOUNT_VALUE = By.id("com.sephora:id/price_summary_discount_value");
	public static final String SPINNER_PRODUCT_QUANTITY= "//android.widget.RelativeLayout[@index='%s']/android.widget.Spinner";
	public static final String SPINNER_PRODUCT_QUANTITY_OVERLAY_SELECTION = "//android.widget.CheckedTextView[contains(@text,'%s')]";
	public static final String REMOVE_PRODUCT_BUTTON= "//android.widget.RelativeLayout[@index='%s']/android.widget.TextView[@text='Remove']";
	public static final By REMOVE_PRODUCT_BUTTON_LOC= By.xpath("//android.widget.RelativeLayout[@index='0']/android.widget.TextView[@text='Remove']");
    public static final By PRODUCT_QUANTITY_LIST_LOC = By.xpath("//android.support.v7.widget.RecyclerView/android.widget.RelativeLayout");
    public static final By PRODUCT_MERGE_OVERLAY_LOC = By.id("com.sephora:id/detailsTextView");
    public static final By PRODUCT_MERGE_OVERLAY_OK_BUTTON = By.id("com.sephora:id/okButton");
    public static final By PRODUCT_BRAND_NAME = By.xpath("//android.widget.RelativeLayout[@index='0']/android.widget.LinearLayout/android.widget.TextView[@index='0']");
    public static final By PRODUCT_CART_PRODUCT_NAME = By.xpath("//android.widget.RelativeLayout[@index='0']/android.widget.LinearLayout/android.widget.TextView[@index='1']");
    public static final By PRODUCT_VARIATION = By.xpath("//android.widget.RelativeLayout[@index='0']/android.widget.LinearLayout/android.widget.TextView[@index='2']");
    public static final By PRODUCT_PRICE = By.xpath("//android.widget.RelativeLayout[@index='0']/android.widget.LinearLayout/android.widget.TextView[@index='3']");
    public static final By PROMOTION_HEADER = By.xpath("//android.widget.LinearLayout/android.widget.TextView[@text='PROMOTIONS']");
    public static final By PROMOTION_APPLY_BUTTON = By.id("com.sephora:id/basket_promotion_apply");
    public static final By PROMOTIONS_TEXT_LOC = By.xpath("//android.widget.TextView[@text='PROMOTIONS']");
	public static final By VIEW_CURRENT_PROMOTION_LOC= By.id("com.sephora:id/basket_promotion_view_current_promotions");
	public static final By ADD_PROMO_INPUT_FIELD = By.id("com.sephora:id/basket_promotion_coupon_input");
	public static final By GIFT_CARD_PURCHASE_VALUE = By.id("com.sephora:id/order_summary_gc_redeem_value");
	public static final By PROMO_APPLY_ERROR_MSG_LOC = By.xpath("//android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView");
	/**==========================================================================================================
	 * DESCRIPTION: To create dynamic locator of item quantity using text
	 * PARAMETERS: String text
	 * RETURN: WebElement
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
    
	private WebElement productQuantitySelect(String text) {
	    return findElement(By.xpath(String.format(SPINNER_PRODUCT_QUANTITY_OVERLAY_SELECTION, text)));
	}

	/**==========================================================================================================
	 * DESCRIPTION: To click the dynamic locator of item quantity
	 * PARAMETERS: String name
	 * RETURN: 
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	public void clickProductQuantity(String name){
		productQuantitySelect(name).click();
		}
	/**==========================================================================================================
	 * DESCRIPTION: To create dynamic locator of spinner contains item quantity using text
	 * PARAMETERS: int num
	 * RETURN: WebElement
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	private WebElement spinnerSelect(int num) {
	    return findElement(By.xpath(String.format(SPINNER_PRODUCT_QUANTITY, num)));
	}
	/**==========================================================================================================
	 * DESCRIPTION: To click the dynamic locator of spinner contains item quantity using name
	 * PARAMETERS: int name
	 * RETURN: 
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	public void clickProductQuantityspinner(int name){
		spinnerSelect(name).click();
		}
	/**==========================================================================================================
	 * DESCRIPTION: To create dynamic locator of remove button of product added to basket
	 * PARAMETERS: int num
	 * RETURN: WebElement
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	private WebElement removeButtonSelect(int num) {
	    return findElement(By.xpath(String.format(REMOVE_PRODUCT_BUTTON, num)));
	}
	/**==========================================================================================================
	 * DESCRIPTION: To click dynamic locator of remove button of product added to basket
	 * PARAMETERS: int num
	 * RETURN: 
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	public void clickRemoveButton(int num){
		removeButtonSelect(num).click();
		}
	/**==========================================================================================================
	 * DESCRIPTION: To validate if the page is basket page
	 * PARAMETERS: 
	 * RETURN: boolean
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	public boolean isBasketPage(){
		if(getText(WINDOW_HEADER).contains("Basket")){
			logger.info("user is on Basket Page");
		return true;
		}
		logger.info("user is not on Basket page");
		return false;
	}
	/**==========================================================================================================
	 * DESCRIPTION: To wait for basket page to load
	 * PARAMETERS: 
	 * RETURN: BasketPage
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	public BasketPage waitForBasketPageToLoad(){
		waitForElementPresent(WINDOW_HEADER);
		getText(WINDOW_HEADER).equalsIgnoreCase("Basket");
		waitForElementPresent(CHECKOUT_BUTTON);
		logger.info("Basket Page is loaded successfully");
		return new BasketPage(driver);
	}
	/**==========================================================================================================
	 * DESCRIPTION: To validate if the products added to the basket
	 * PARAMETERS: 
	 * RETURN: boolean
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	public boolean isProductAddedToBasket(){
		if(isElementPresent(REMOVE_BUTTON)&&isElementPresent(MOVE_TO_LOVES_BUTTON)){
			logger.info("Quantity of product added successfully to basket is: "+getText(BASKET_ORDER_TOTAL_ITEMS).trim().replace("Order Total ", ""));
		return true;
		}
		logger.info("product is not added to basket");
		return false;
	}
	/**==========================================================================================================
	 * DESCRIPTION: To ckick Checkout buton on basket page
	 * PARAMETERS: 
	 * RETURN: CheckoutPage
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	public CheckoutPage clickCheckoutButton(){
		waitForElementPresent(CHECKOUT_BUTTON);
		click(CHECKOUT_BUTTON);
		pauseExecutionFor(2000);
		return new CheckoutPage(driver);
	}
	/**==========================================================================================================
	 * DESCRIPTION: To validate if the promotion added to basket
	 * PARAMETERS: String promoCode
	 * RETURN: boolean
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	public List <Object> promoDetailsBasket = new ArrayList<Object>();
	public boolean isPromoAdded(String promoCode){
		if(promoCode.equals(PROMO_CODE_GWP)){
		scrollToExact("PROMOTIONS");
		}
		else{
			scrollToExact("1 code per order");
		}
		if((isElementPresent(PROMO_ADDED_MESSAGE_LOC) || isElementPresent(PROMO_NAME_LOC)) && getText(PROMO_ADDED_MESSAGE_LOC).equalsIgnoreCase("Promo Applied: "+promoCode)) {
			if(promoCode.equals(PROMO_CODE_GWP)){
				logger.info("promotion added and displayed correctly on Checkout: "+ promoCode + " is having Title: " + getText(PROMO_ITEM_NAME_GWP).toUpperCase()
						+ " and description: " + getText(PROMO_ITEM_DESCRIPTION_GWP));
				promoDetailsBasket.add(getText(PROMO_ITEM_NAME_GWP));
				promoDetailsBasket.add(getText(PROMO_ITEM_DESCRIPTION_GWP));
				promoDetailsBasket.add(1);
				if(isElementPresent(PROMO_ITEM_PRICE)){
				logger.info("the price of promo is: "+ getText(PROMO_ITEM_PRICE));
				promoDetailsBasket.add(getText(PROMO_ITEM_PRICE));
				}
				return true;
			} else{
			logger.info("promotion added and displayed correctly on Checkout: "+ promoCode + " is having description: " + getText(PROMO_ITEM_NAME).toUpperCase());
			scrollToExact("1 code per order");
			if(isElementPresent(PROMO_ITEM_PRICE)){
				logger.info("the price of promo is: "+ getText(PROMO_ITEM_PRICE));
				}
			return true;
		      }
		}
		logger.info(promoCode+"-- is not present");
		return false;
	}
	/**==========================================================================================================
	 * DESCRIPTION: To update quantity of particular item added to basket
	 * PARAMETERS: int productNum, String quantity
	 * RETURN: BasketPage
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	public BasketPage updateItemCountOfProductInBasket(int productNum, String quantity){
		clickProductQuantityspinner(productNum);
		scrollToExact(quantity);
		clickProductQuantity(quantity);
		logger.info("the new quantity of product # "+ (productNum+1) +" in basket is: "+ quantity);
		return new BasketPage(driver);
	}
	/**==========================================================================================================
	 * DESCRIPTION: To remove product from basket using index
	 * PARAMETERS: int index, int productNum
	 * RETURN: BasketPage
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	public BasketPage removeProductFromBasket(int index, int productNum) throws InterruptedException{
		clickRemoveButton(index);
		logger.info("item #: " + (productNum) + " is removed");
		return new BasketPage(driver);
	}
	/**==========================================================================================================
	 * DESCRIPTION: To remove product from basket for pre-condition 
	 * PARAMETERS: int productnum
	 * RETURN: BasketPage
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	public BasketPage removeProductFromBasketforPreCondition(int productnum){
		clickRemoveButton(productnum);
		return new BasketPage(driver);
		}
	/**==========================================================================================================
	 * DESCRIPTION: To get count of item added to basket
	 * PARAMETERS: 
	 * RETURN: items
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	public int getNumberOfItemsFromBasketOrderTotal() {
		int items = 0;
		String text = getText(BASKET_ORDER_TOTAL_ITEMS);
		if (!text.isEmpty()) {
			items = getIntegerValueFromString(text);
		}
		return items;
	}
	/**==========================================================================================================
	 * DESCRIPTION: To get merchandise total of item added to basket
	 * PARAMETERS: 
	 * RETURN: total 
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	public double getMerchandiseTotalFromBasketOrderTotal() {
		double total = 0;
		String text = getText(BASKET_ORDER_TOTAL);
		if (!text.isEmpty() && text.contains("$")) {
			total = Double.parseDouble(text.trim().replace("$", ""));
			logger.info("the current merchandise total is: " + total);
		}
		else{
			total = Double.parseDouble(text.trim().replace("C$", ""));
			logger.info("the current merchandise total is: " + total);
		}
		return total;
	}
	
	/**==========================================================================================================
	 * DESCRIPTION: To click moves to loves button based on index of item
	 * PARAMETERS: int index, int productNum
	 * RETURN: BasketPage
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	public BasketPage clickMovesToLoves(int index, int productNum){
		waitForElementPresent(MOVE_TO_LOVES_BUTTON);
		List<WebElement> list_loveButton= driver.findElements(MOVE_TO_LOVES_BUTTON);
		System.out.println(list_loveButton.size());
		list_loveButton.get(index).click();
		logger.info("product at: #"+productNum + " is moved to Loves");
		return new BasketPage(driver);
	}
	
	/**===========================================================================================================================
	 * DESCRIPTION: To validate if product merge overlay is present when anonymous user login with existing user from basket page
	 * PARAMETERS: 
	 * RETURN: boolean
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	public boolean isProductMergeOverlayPresent(){
		if(isElementPresent(PRODUCT_MERGE_OVERLAY_LOC)){
			logger.info("Product merge overlay is present");
		return true;
		}
		logger.info("Product merge overlay is not present");
		return false;
	}
	
	/**==========================================================================================================
	 * DESCRIPTION: To click OK button on product merge overlay
	 * PARAMETERS: 
	 * RETURN: BasketPage
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	
	public BasketPage clickProductMergeOKButton(){
		if(isElementPresent(PRODUCT_MERGE_OVERLAY_LOC)){
			click(PRODUCT_MERGE_OVERLAY_OK_BUTTON);
		}
		return new BasketPage(driver);
	}
	/**==========================================================================================================
	 * DESCRIPTION: To validate the product info added basket
	 * PARAMETERS: int numOfProduct
	 * RETURN: 
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	
	public List <Object> basketItemDetails = new ArrayList<Object>(); 
	public boolean isProductInfoCorrectWithNoTrucation(int numOfProduct) throws InterruptedException{
		int i;
		
		int numberOfItems =getNumberOfItemsFromBasketOrderTotal();
		logger.info("the number of item currently in basket is: " + numberOfItems);
		basketItemDetails.add(numberOfItems);
		for(i=1; i<=numOfProduct; i++){
			logger.info("the details of product #: " +i);
			String brandName = getText(PRODUCT_BRAND_NAME);
			if(!brandName.contains("...")){
			logger.info("the brand name is: " +brandName);
			basketItemDetails.add(brandName);
			}
			String productName = getText(PRODUCT_CART_PRODUCT_NAME);
			if(!productName.contains("....")){
			logger.info("The product name is: " +productName);
			basketItemDetails.add(productName);
			}
			String productVariation = getText(PRODUCT_VARIATION);
			if(productVariation.contains("$")){
				String priceOfProduct = getText(PRODUCT_VARIATION); 
				logger.info("The Price of product # "+ i +" is: "+ priceOfProduct);
				basketItemDetails.add(priceOfProduct);
			}
			else
				if(!productVariation.contains("....")){
			logger.info("The product variation is: "+(productVariation));
			basketItemDetails.add(productVariation);
				}
			
			int quantityOfProduct = Integer.parseInt(getText(QUANTITY_OF_PRODUCT_ADDED));
			if(isElementPresent(QUANTITY_OF_PRODUCT_ADDED)){
				logger.info("The Quantity of the product "+ i +" is: "+quantityOfProduct);
				basketItemDetails.add(quantityOfProduct);
			}
			
			if(isElementPresent(REMOVE_BUTTON)){
				String removeButton = getText(REMOVE_BUTTON);
				if(removeButton.equalsIgnoreCase("Remove")){
				logger.info("The Remove button is present for product # "+ i +" with the text: "+removeButton);
				}
				}
			if(isElementPresent(MOVE_TO_LOVES_BUTTON)){
				String movesToLovesButton = getText(MOVE_TO_LOVES_BUTTON);
				if(movesToLovesButton.equalsIgnoreCase("Move to Loves")){
					logger.info("The moves To Loves Button is present for product # "+ i +" with the text: "+movesToLovesButton);
					}
					}
			
			if(isElementPresent(PRODUCT_PRICE)){
			String productPrice = getText(PRODUCT_PRICE);
			logger.info("The Price of the product# "+ i +" is: "+productPrice);
			basketItemDetails.add(productPrice);
			}
			
			scrollPageDownForBasket(1);
		}
		System.out.println("size of list of product is: "+basketItemDetails.size());
		for(Object value:basketItemDetails){
			System.out.println(value);
		}
		
		return true;
	}
	
	/**==========================================================================================================
	 * DESCRIPTION: To validate the fields of promotion in basket
	 * PARAMETERS: 
	 * RETURN: 
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	
	public boolean isPromotionFieldValid() throws InterruptedException{
		scrollPageDown(3);
		if(isElementPresent(PROMOTION_HEADER)&& isElementPresent(PROMOTIONS_TEXT_LOC) && isElementPresent(PROMOTION_APPLY_BUTTON)
				&& isElementPresent(VIEW_CURRENT_PROMOTION_LOC)){
			String promoHeader=getText(PROMOTION_HEADER);
			if(promoHeader.equals("PROMOTIONS")){
				logger.info("Promotion section have valid header: "+promoHeader);
			}
			logger.info("Promo input field is present");
			logger.info("Promo Apply Button is present");
			logger.info("View Promotion link is present");
		}
		return true;
	}
	/**==========================================================================================================
	 * DESCRIPTION: To Add promotion in basket
	 * PARAMETERS: String promoCode
	 * RETURN: 
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	public BasketPage addPromotion(String promoCode){
		scrollToExact("1 code per order");
		waitForElementPresent(PROMOTIONS_TEXT_LOC);
		clear(ADD_PROMO_INPUT_FIELD);
		type(ADD_PROMO_INPUT_FIELD, promoCode);
		logger.info("The promocode: "+ promoCode + " is typed in the promo input field");
		click(PROMOTION_APPLY_BUTTON);
		return new BasketPage(driver);
	}
	
	/**==========================================================================================================
	 * DESCRIPTION: To remove promo added in basket
	 * PARAMETERS: 
	 * RETURN: 
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	public BasketPage removePromotion(){
		scrollToExact("1 code per order");
		waitForElementPresent(PROMO_REMOVE_BUTTON);
		click(PROMO_REMOVE_BUTTON);
		logger.info("The promocode is removed");
		return new BasketPage(driver);
	}
	/**==========================================================================================================
	 * DESCRIPTION: To verify if remove promotion button is present
	 * PARAMETERS: 
	 * RETURN: boolean
	        ==========================================================================================================*/
	public boolean isremovePromotionButtonPresent(){
		scrollToExact("1 code per order");
		if(isElementPresent(PROMO_REMOVE_BUTTON)){
		logger.info("The remove promotion button is present");
		return true;
		}
		logger.info("remove promotion button is not present");
		return false;
	}
	/**==========================================================================================================
	 * DESCRIPTION: To validate if Promo Of Percent Type Reflects Correct Discount
	 * PARAMETERS: double promoPercent
	 * RETURN: boolean
	        ==========================================================================================================*/
	public boolean isPromoOfPercentTypeReflectsCorrectDiscount(double promoPercent){
		scrollPageDown(2);
		if(isElementPresent(MERCHANDISE_TOTAL_VALUE) && isElementPresent(DISCOUNT_VALUE)){
			double merchTotal = Double.parseDouble(getText(MERCHANDISE_TOTAL_VALUE).trim().replace("$", ""));
			logger.info("the merch value is :" + merchTotal);
			double discountTotal = Double.parseDouble(getText(DISCOUNT_VALUE).trim().replace("-$", ""));
			logger.info("the Discount value is :" + discountTotal);
			double percent = (discountTotal*100)/merchTotal;
			double percentRoundOff = percent*100;
	        double percentRoundOff1 = Math.round(percentRoundOff);
	        double percentRoundOff2 = percentRoundOff1/100;
			logger.info("discount percent is: "+ percentRoundOff2);
			if (percentRoundOff2==promoPercent){
			logger.info("The promo applied to reflect correct discount value");
			return true;
			}
		
		}
		logger.info("The promo applied fails to reflect correct discount value");
		return false;
	}
	/**==========================================================================================================
	 * DESCRIPTION: To validate if Promo Of $ Type Reflects Correct Discount
	 * PARAMETERS: double dollar
	 * RETURN: boolean
	        ==========================================================================================================*/
	public boolean isPromoOf$TypeReflectsCorrectDiscount(double dollar){
		scrollPageDown(4);
		if(isElementPresent(MERCHANDISE_TOTAL_VALUE) && isElementPresent(DISCOUNT_VALUE)){
			double discountTotal = Double.parseDouble(getText(DISCOUNT_VALUE).trim().replace("-$", ""));
			logger.info("the Discount value is :" + discountTotal);
			if (discountTotal==dollar){
			logger.info("The promo applied to reflect correct discount value");
			return true;
			}
		
		}
		logger.info("The promo applied fails to reflect correct discount value");
		return false;
	}
	/**==========================================================================================================
	 * DESCRIPTION: To validate if Discount Promo Updates Subtotal
	 * PARAMETERS: double beforeTotal, double discount, double afterTotal
	 * RETURN: boolean
	        ==========================================================================================================*/
	public boolean isDiscountPromoUpdatesSubtotal(double beforeTotal, double discount, double afterTotal){
		scrollPageDown(4);
		logger.info("before applying promo the total is: " +beforeTotal);
		logger.info("after applying promo the total is: " +afterTotal);
		logger.info("promo discount total is: " +discount);
		logger.debug("the difference of before and after total is: "+ (beforeTotal-discount));
		double diff = (beforeTotal-discount);
		double diff1 = diff*100;
		double diff2= Math.round(diff1);
		double diff3 = diff2/100;
		if(afterTotal==diff3){
			logger.info("Discount promo updates subtotal in price summary");
			return true;
		}
		logger.info("Discount promo fails to update subtotal in price summary");
		return false;
	}
	/**==========================================================================================================
	 * DESCRIPTION: To get Merchandise SubTotal From Basket Order Total
	 * PARAMETERS: 
	 * RETURN: double
	        ==========================================================================================================*/
	public double getMerchandiseSubTotalFromBasketOrderTotal() {
		double total = 0;
		String text = getText(MERCHANDISE_TOTAL_VALUE);
		if (!text.isEmpty() && text.contains("$")) {
			total = Double.parseDouble(text.trim().replace("$", ""));
			logger.info("the current merchandise sub total is: " + total);
		}
		else{
			total = Double.parseDouble(text.trim().replace("C$", ""));
			logger.info("the current merchandise sub total is: " + total);
		}
		return total;
	}
	/**==========================================================================================================
	 * DESCRIPTION: To get Discount Total From Basket Order Total
	 * PARAMETERS: 
	 * RETURN: double
	        ==========================================================================================================*/
	public double getDiscountTotalFromBasketOrderTotal() {
		scrollToExact("Tax");
		double total = 0;
		String text = getText(DISCOUNT_VALUE);
		if (!text.isEmpty() && text.contains("-$")) {
			total = Double.parseDouble(text.trim().replace("-$", ""));
			logger.info("the current merchandise total is: " + total);
		}
		else{
			total = Double.parseDouble(text.trim().replace("-C$", ""));
			logger.info("the current merchandise total is: " + total);
		}
		return total;
	}
	/**==========================================================================================================
	 * DESCRIPTION: To validate if promo is applied on merchandise only
	 * PARAMETERS: double promoPercent
	 * RETURN: boolean
	        ==========================================================================================================*/
	public boolean isPromoApplyOnMerchOnly(double promoPercent){
		scrollPageDown(2);
		if(isElementPresent(MERCHANDISE_TOTAL_VALUE) && isElementPresent(DISCOUNT_VALUE)){
			double merchTotal = Double.parseDouble(getText(MERCHANDISE_TOTAL_VALUE).trim().replace("$", ""));
			logger.info("the merch value is :" + merchTotal);
			double discountTotal = Double.parseDouble(getText(DISCOUNT_VALUE).trim().replace("-$", ""));
			logger.info("the Discount value is :" + discountTotal);
			double percent = (discountTotal*100)/merchTotal;
			double percentRoundOff = percent*100;
	        double percentRoundOff1 = Math.round(percentRoundOff);
	        double percentRoundOff2 = percentRoundOff1/100;
			logger.info("discount percent is: "+ percentRoundOff2);
			if (percent==promoPercent){
			logger.info("The promo is applied only on merch");
			return true;
			}
		}
		logger.info("The promo is not applied only on merch");
		return false;
	}
	/**==========================================================================================================
	 * DESCRIPTION: To validate if discount line is displayed in totals section
	 * PARAMETERS: 
	 * RETURN: boolean
	        ==========================================================================================================*/
	public boolean isDiscountLineDisplayed(){
		if(isElementPresent(DISCOUNT_VALUE)){
			logger.info("Discount Line is displayed");
		return true;
		}
		logger.info("Discount Line is not displayed");
		return false;
	}
	/**==========================================================================================================
	 * DESCRIPTION: To validate if order price is updated in totals section
	 * PARAMETERS: double beforeTotal, double removedProductPrice, double afterTotal
	 * RETURN: boolean
	        ==========================================================================================================*/
	public boolean isOrderPriceUpdated(double beforeTotal, double removedProductPrice, double afterTotal){
		logger.info("after adding new item the total is: " +beforeTotal);
		logger.info("after removing item the total is: " +afterTotal);
		logger.info("price of removed item is: " +removedProductPrice);
		double diff = (beforeTotal+removedProductPrice);
		double diff1 = diff*100;
		double diff2= Math.round(diff1);
		double diff3 = diff2/100;
		if(afterTotal==diff3){
			logger.info("Discount promo updates subtotal in price summary");
			return true;
		}
		logger.info("Discount promo fails to update subtotal in price summary");
		return false;
	}
	/**==========================================================================================================
	 * DESCRIPTION: To validate if the promo paply error message is valid
	 * PARAMETERS: String errorMessage
	 * RETURN: boolean
	        ==========================================================================================================*/
	public boolean isPromoApplyErrorMessageValid(String errorMessage){
		waitForElementPresent(PROMO_APPLY_ERROR_MSG_LOC);
		String errorMsg = getText(PROMO_APPLY_ERROR_MSG_LOC).trim().replace("?", " ");
		if(errorMsg.equalsIgnoreCase(errorMessage)){
		  logger.info("The error message: "+ errorMsg + " is valid");
		  return true;
		}
		logger.info("The error message: "+ errorMsg + " is invalid");
		return false;
	}
	/**==========================================================================================================
	 * DESCRIPTION: To validate if the product added is more than X amount
	 * PARAMETERS: double amount
	 * RETURN: boolean
	        ==========================================================================================================*/
	public boolean isProductAddedMoreThanXAmount(double amount){
		waitForElementPresent(BASKET_ORDER_TOTAL);
		double orderTotal = Double.parseDouble(getText(BASKET_ORDER_TOTAL).trim().replace("$", ""));
		if(orderTotal>=amount){
		  logger.info("The order total is basket is :" + orderTotal + " and is more than "+amount);
		  return true;
		}
		logger.info("The order total is basket is :" + orderTotal + " and is not more than "+amount);
		return false;
	}
	
}
