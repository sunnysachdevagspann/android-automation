package com.sephora.pages.AndroidApp.Product;

import io.appium.java_client.AppiumDriver;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;

import com.sephora.base.pages.SephoraAndroidAppBasePage;
import com.sephora.core.driver.mobile.SephoraMobileDriver;

public class ProductDetailPage extends SephoraAndroidAppBasePage {

	public ProductDetailPage(SephoraMobileDriver driver) {
		super(driver);
	}
	private static final Logger logger = LogManager.getLogger(ProductDetailPage.class.getName());
	
	public static final By ADD_TO_BASKET_BUTTON = By.id("com.sephora:id/pp_fab");
	
	 public ProductDetailPage  clickAddToBasketButton() {
		 waitForElementPresent(ADD_TO_BASKET_BUTTON);
			click(ADD_TO_BASKET_BUTTON);
			logger.info("Add to basket button is clicked");
			return new ProductDetailPage(driver);
			
	 }
	 
}
