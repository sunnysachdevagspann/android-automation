package com.sephora.pages.AndroidApp.Product;

import io.appium.java_client.AppiumDriver;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.sephora.base.pages.SephoraAndroidAppBasePage;
import com.sephora.core.driver.mobile.SephoraMobileDriver;
import com.sephora.pages.AndroidApp.Basket.BasketPage;
import com.sephora.pages.AndroidApp.Category.CategoryPage;
import com.sephora.pages.AndroidApp.Home.HomePage;

public class ProductPage extends SephoraAndroidAppBasePage {

	public ProductPage(SephoraMobileDriver driver) {
		super(driver);
	}
	protected ProductDetailPage productDetailPage;
	protected HomePage homePage;
	protected CategoryPage categoryPage;
	
	private static final Logger logger = LogManager.getLogger(ProductPage.class.getName());
	
	public static final By PRODUCT_LOC = By.id("com.sephora:id/product_card");
	public static final By ADD_TO_BASKET_BUTTON = By.id("com.sephora:id/pp_fab");
	public static final By BASKET_ORDER_TOTAL= By.id("com.sephora:id/order_total_price");
	
	public ProductDetailPage selectProductbyIndex(int index){
		waitForElementPresent(PRODUCT_LOC);
		List<WebElement> list_Products= findElements(PRODUCT_LOC);
		System.out.println(list_Products.size());
		list_Products.get(index).click();
		logger.info("The product going to be selected is: "+ getText(WINDOW_HEADER));
		return new ProductDetailPage(driver);
	}
	
	public ProductDetailPage selectMultipleproduct(int number) {
		ProductDetailPage productDetailPage = new ProductDetailPage(driver);
		int i;
		int num= number;
		for (i=1; i<=num; i++){
			selectProductbyIndex(i);
			logger.debug("going to add item to basket at index: "+ i);
			productDetailPage.clickAddToBasketButton();
			navigateBack();
			logger.debug("device back button is clicked");
			}
		return new ProductDetailPage(driver);
	}
	
	public boolean isProductSearchResultPage(String searchResultHeader){
		if(getText(WINDOW_HEADER).equalsIgnoreCase(searchResultHeader));
		return true;
	}
	
}
