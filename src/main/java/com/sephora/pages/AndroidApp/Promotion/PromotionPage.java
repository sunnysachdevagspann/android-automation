package com.sephora.pages.AndroidApp.Promotion;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.sephora.base.pages.SephoraAndroidAppBasePage;
import com.sephora.core.driver.mobile.SephoraMobileDriver;

public class PromotionPage extends SephoraAndroidAppBasePage{

	public PromotionPage(SephoraMobileDriver driver) {
		super(driver);
	}
	
	public static final By PROMO_LIST_ADDBUTTON_LOC = By.id("com.sephora:id/promo_sample_selector_add_or_remove_button");
	public static final By ADD_PROMO_BUTTON_LOC = By.id("com.sephora:id/promo_sample_selector_add_promo_button");
	
	public PromotionPage selectPromotion(int index){
	waitForElementPresent(PROMO_LIST_ADDBUTTON_LOC);
    List<WebElement> list_GiftOptions= findElements(PROMO_LIST_ADDBUTTON_LOC);
	System.out.println(list_GiftOptions.size());
	list_GiftOptions.get(index).click();
	click(ADD_PROMO_BUTTON_LOC);
	return new PromotionPage(driver);
	}
}
