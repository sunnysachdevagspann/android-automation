package com.sephora.pages.AndroidApp.Navigation;

import io.appium.java_client.AppiumDriver;

import org.openqa.selenium.By;

import com.sephora.base.pages.SephoraAndroidAppBasePage;
import com.sephora.core.driver.mobile.SephoraMobileDriver;
import com.sephora.pages.AndroidApp.Home.HomePage;

public class LeftNavigationDrawerPage extends SephoraAndroidAppBasePage {

	public LeftNavigationDrawerPage(SephoraMobileDriver driver) {
		super(driver);
	}
	
	public static final By HOME_BUTTON_LOC = By.id("com.sephora:id/navigation_item_home");
	public static final By SHOP_BUTTON_LOC = By.id("com.sephora:id/navigation_item_shop");
	public static final By MYACCOUNT_BUTTON_LOC = By.id("com.sephora:id/navigation_item_my_account");

	
	
	public HomePage clickHomeButton() {
		waitForElementPresent(HOME_BUTTON_LOC);
		click(HOME_BUTTON_LOC);
			logger.info("Home Button on left navigation drawer is clicked");
			return new HomePage(driver);
	 }
	
	public LeftNavigationDrawerPage clickShopButton() {
		waitForElementPresent(SHOP_BUTTON_LOC);
		click(SHOP_BUTTON_LOC);
			logger.info("Shop Button on left navigation drawer is clicked");
			return new LeftNavigationDrawerPage(driver);
	 }
	
	public LeftNavigationDrawerPage clickMyAccoyntButton() {
		waitForElementPresent(MYACCOUNT_BUTTON_LOC);
		click(MYACCOUNT_BUTTON_LOC);
			logger.info("MY ACcount Button on left navigation drawer is clicked");
			return new LeftNavigationDrawerPage(driver);
	 }
}
