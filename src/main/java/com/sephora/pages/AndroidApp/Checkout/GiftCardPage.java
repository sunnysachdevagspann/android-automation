package com.sephora.pages.AndroidApp.Checkout;

import org.openqa.selenium.By;

import com.sephora.base.pages.SephoraAndroidAppBasePage;
import com.sephora.core.driver.mobile.SephoraMobileDriver;

/**==========================================================================================================
 * DESCRIPTION: Gift Card Page contains all the functions for the actions which takes 
 * place on DGift Card Page 
 ==========================================================================================================*/

public class GiftCardPage  extends SephoraAndroidAppBasePage {
	
	public GiftCardPage(SephoraMobileDriver driver) {
		super(driver);
	}
	
	public static final By GC_NUMBER_INPUT_FIELD = By.xpath("//android.widget.LinearLayout[contains(@text,'Gift Card')]/android.widget.EditText");
	public static final By GC_PIN_INPUT_FIELD = By.xpath("//android.widget.LinearLayout[contains(@text,'PIN')]/android.widget.EditText");
	
	/**==========================================================================================================
	 * DESCRIPTION: to type Gift card number
	 * PARAMETERS: String gcNumber
	 * RETURN: GiftCardPage
	 ==========================================================================================================*/
	
	 public GiftCardPage typeGCNumber(String gcNumber) {
			waitForElementPresent(GC_NUMBER_INPUT_FIELD);
			clear(GC_NUMBER_INPUT_FIELD);
			type(GC_NUMBER_INPUT_FIELD, gcNumber);
			logger.info(gcNumber+" typed in GC number field");
			return new GiftCardPage(driver);
		}
	 
	 /**==========================================================================================================
		 * DESCRIPTION: to type Gift card pin
		 * PARAMETERS: String gcPin 
		 * RETURN: GiftCardPage
	 ==========================================================================================================*/
	 
	 public GiftCardPage typeGCPin(String gcPin) {
			waitForElementPresent(GC_PIN_INPUT_FIELD);
			clear(GC_PIN_INPUT_FIELD);
			type(GC_PIN_INPUT_FIELD, gcPin);
			logger.info(gcPin+" typed in GC Pin field");
			return new GiftCardPage(driver);
		}
}
