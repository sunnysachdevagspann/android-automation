package com.sephora.pages.AndroidApp.Checkout;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.sephora.base.pages.SephoraAndroidAppBasePage;
import com.sephora.core.driver.mobile.SephoraMobileDriver;

/**==========================================================================================================
 * DESCRIPTION: Delivery and gift Option page contains all the functions for the actions which takes 
 * place on Delivery and gift option page
  ==========================================================================================================*/

public class DeliveryAndGiftOptionPage extends SephoraAndroidAppBasePage {

	public DeliveryAndGiftOptionPage(SephoraMobileDriver driver) {
		super(driver);
	}
	
	public static final By DELIVERY_OPTION_ITEMS = By.id("com.sephora:id/delivery_option_item");
	public static final By GIFT_OPTION_CHECKBOX = By.id("com.sephora:id/delivery_is_gift_cb");
	public static final By GIFT_WRAP_OPTION = By.id("com.sephora:id/delivery_wrap_item");
	public static final By GIFT_MESSAGE_INPUT = By.id("com.sephora:id/gift_message_input");
	
	/**==========================================================================================================
	 * DESCRIPTION: to verify if the current page is Delivery and Gift Option page
	 * PARAMETERS: 
	 * RETURN: boolean
	 ==========================================================================================================*/
	
	public boolean isDeliveryAndGiftOptionPage(){
		if(getText(WINDOW_HEADER).equals("DELIVERY")){
		return true;
		}
		return false;
	}
	/**==========================================================================================================
	 * DESCRIPTION: to select the delivery option on index basis
	 * PARAMETERS: int index, String deliveryOption
	 * RETURN: CheckoutPage
	 ==========================================================================================================*/
	
	public CheckoutPage selectDeliveryOption(int index, String deliveryOption){
		waitForElementPresent(DELIVERY_OPTION_ITEMS);
	    List<WebElement> list_Categories= findElements(DELIVERY_OPTION_ITEMS);
		System.out.println(list_Categories.size());
		list_Categories.get(index).click();
		pauseExecutionFor(2000);
		clickDoneButton();
		logger.info("Delivery method selected is: "+ deliveryOption);
		return new CheckoutPage(driver);
	}
	/**==========================================================================================================
	 * DESCRIPTION: to verify if the gift option checkbox is checked
	 * PARAMETERS: 
	 * RETURN: boolean
	 ==========================================================================================================*/
	public boolean isGiftOptionCheckboxChecked(){
		System.out.println(getAttribute(GIFT_OPTION_CHECKBOX, "checked"));
		if(getAttribute(GIFT_OPTION_CHECKBOX, "checked").equals("true")){
		return true;
	}
		return false;
	}
	/**==========================================================================================================
	 * DESCRIPTION: to check the gift box after verifying if its already checked
	 * PARAMETERS: 
	 * RETURN: DeliveryAndGiftOptionPage
	 ==========================================================================================================*/
	public DeliveryAndGiftOptionPage ckeckGiftBox(){
		if(isDeliveryAndGiftOptionPage()){
			scrollPageDown(1);
		
		if(isElementPresent(GIFT_OPTION_CHECKBOX) && isGiftOptionCheckboxChecked()){
			logger.info("Gift Option checkbox is already checked");
		}
		else if(isElementPresent(GIFT_OPTION_CHECKBOX) && !isGiftOptionCheckboxChecked()){
			
			click(GIFT_OPTION_CHECKBOX);
			logger.info("Gift Option checkbox is checked");
		}
		}
		return new DeliveryAndGiftOptionPage(driver);
	}
	/**==========================================================================================================
	 * DESCRIPTION: to select giftwrap option based on index and add message
	 * PARAMETERS: int index, String deliveryOption, String message
	 * RETURN: CheckoutPage
	 ==========================================================================================================*/
	public CheckoutPage selectGiftWrapOptionAndAddMessage(int index, String deliveryOption, String message) throws InterruptedException{
		scrollPageDown(1);
		waitForElementPresent(GIFT_WRAP_OPTION);
	    List<WebElement> list_GiftOptions= findElements(GIFT_WRAP_OPTION);
		System.out.println(list_GiftOptions.size());
		list_GiftOptions.get(index).click();
		System.out.println(list_GiftOptions.get(index).isSelected());
		type(GIFT_MESSAGE_INPUT, message);
		clickDoneButton();
		logger.info("Gift Option selected is: "+ deliveryOption);
		return new CheckoutPage(driver);
	}
	
}
