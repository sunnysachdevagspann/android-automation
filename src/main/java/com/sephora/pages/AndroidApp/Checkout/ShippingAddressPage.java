package com.sephora.pages.AndroidApp.Checkout;


import org.openqa.selenium.By;

import com.sephora.base.pages.SephoraAndroidAppBasePage;
import com.sephora.core.driver.mobile.SephoraMobileDriver;
/**==========================================================================================================
 * DESCRIPTION: Shipping Address Page contains all the functions for the actions which takes 
* place on Shipping Address Page
==========================================================================================================*/

public class ShippingAddressPage extends SephoraAndroidAppBasePage {

	public ShippingAddressPage(SephoraMobileDriver driver) {
		super(driver);
	}
	
	public static final By FIRST_NAME_INPUT_FIELD = By.xpath("//android.widget.LinearLayout[@text='First Name']/android.widget.EditText");
	public static final By LAST_NAME_INPUT_FIELD = By.xpath("//android.widget.LinearLayout[@text='Last Name']/android.widget.EditText");
	public static final By ADDRESS_INPUT_FIELD = By.xpath("//android.widget.LinearLayout[@text='Address']/android.widget.EditText");
    public static final By ADDRESS_LINE_2_INPUT_FIELD = By.xpath("//android.widget.LinearLayout[contains(text,'Address Line 2')]/android.widget.EditText");
	public static final By ZIP_POSTAL_INPUT_FIELD = By.xpath("//android.widget.LinearLayout[@text='Zip/Postal Code']/android.widget.EditText");
	public static final By CITY_INPUT_FIELD = By.xpath("//android.widget.LinearLayout[@text='City']/android.widget.EditText");
	public static final By PHONE_INPUT_FIELD = By.xpath("//android.widget.LinearLayout[@text='Phone']/android.widget.EditText");
	public static final By DEFAULT_SHIPPING_ADDRESS_SWITCH = By.xpath("//android.widget.Switch[@index='2']");
	public static final By EDIT_SHIP_TO_OVERLAY = By.id("com.sephora:id/address_edit_btn");
	public static final By EDIT_ADD_SHIPPING_ADDRESS = By.xpath("//android.widget.Button[@text='Add Shipping Address']");
	public static final By EDIT_SHIP_TO_CANCEL= By.id("com.sephora:id/addresses_cancel_btn");

	/**==========================================================================================================
	 * DESCRIPTION: to wait for Shipping address page to load
	 * PARAMETERS: 
	 * RETURN: ShippingAddressPage
	 ==========================================================================================================*/
	public ShippingAddressPage waitForShippingAddressPageToLoad(){
		waitForElementPresent(WINDOW_HEADER);
		if(getText(WINDOW_HEADER).equalsIgnoreCase("SHIPPING ADDRESS")){
		logger.info("the user is on Shipping Address page");
		}
		return new ShippingAddressPage(driver);
	}
	/**==========================================================================================================
	 * DESCRIPTION: to type Address First Line
	 * PARAMETERS: String address
	 * RETURN: ShippingAddressPage
	 ==========================================================================================================*/
	public ShippingAddressPage typeAddressFirstLine(String address){
		clear(ADDRESS_INPUT_FIELD);
		type(ADDRESS_INPUT_FIELD, address);
		logger.info("Typed Address First Line :"+address);
		return new ShippingAddressPage(driver);
	}
	/**==========================================================================================================
	 * DESCRIPTION: to type address second line in input field
	 * PARAMETERS: String address
	 * RETURN: ShippingAddressPage
	 ==========================================================================================================*/
	public ShippingAddressPage typeAddressSecondLine(String address){
		clear(ADDRESS_LINE_2_INPUT_FIELD);
		type(ADDRESS_LINE_2_INPUT_FIELD, address);
		logger.info("Typed Address Second Line :"+address);
		return new ShippingAddressPage(driver);
	}
	/**==========================================================================================================
	 * DESCRIPTION: to type Postal Code in input field
	 * PARAMETERS: String postal
	 * RETURN: ShippingAddressPage
	 ==========================================================================================================*/
	public ShippingAddressPage typePostalCode(String postal){
		clear(ZIP_POSTAL_INPUT_FIELD);
		type(ZIP_POSTAL_INPUT_FIELD, postal);
		logger.info("Typed Postal Code :"+postal);
		return new ShippingAddressPage(driver);
	}
	/**==========================================================================================================
	 * DESCRIPTION: to type City in input field
	 * PARAMETERS: String city
	 * RETURN: ShippingAddressPage
	 ==========================================================================================================*/
	public ShippingAddressPage typeCity(String city){
		clear(CITY_INPUT_FIELD);
		type(CITY_INPUT_FIELD, city);
		logger.info("Typed City :"+city);
		return new ShippingAddressPage(driver);
	}
	/**==========================================================================================================
	 * DESCRIPTION: to type Phone number in input field
	 * PARAMETERS: 
	 * RETURN: ShippingAddressPage
	 ==========================================================================================================*/
	public ShippingAddressPage typePhone(String phone){
		scrollToExact("Phone");
		clear(PHONE_INPUT_FIELD);
		type(PHONE_INPUT_FIELD, phone);
		logger.info("Typed Phone Number :"+phone);
		return new ShippingAddressPage(driver);
	}
	/**==========================================================================================================
	 * DESCRIPTION: to click default shipping address switch
	 * PARAMETERS: String status
	 * RETURN: ShippingAddressPage
	 ==========================================================================================================*/
	 public ShippingAddressPage clickDefaultShippingAddressSwitch(String status) {
		 if(status=="ON"){
			 if(isdefaultShippingAddressSwitchON()){
				 logger.info("default Shipping Address switch is already in ON state");
			 }
			 else{
				 waitForElementPresent(DEFAULT_SHIPPING_ADDRESS_SWITCH);
					click(DEFAULT_SHIPPING_ADDRESS_SWITCH);
					logger.info("default Shipping Address toggle is clicked");
					logger.info("default Shipping Address switch is now in ON State");
					return this;
			 }
		 }
		 else if(status=="OFF"){
			 if(isdefaultShippingAddressSwitchON()){
				 waitForElementPresent(DEFAULT_SHIPPING_ADDRESS_SWITCH);
					click(DEFAULT_SHIPPING_ADDRESS_SWITCH);
					logger.info("default Shipping Address toggle is clicked");
					logger.info("default Shipping Address switch is now in OFF State");
					return this;
			 }
			 else{
				 logger.info("default Shipping Address switch is already in OFF state");
			 }
		 }
		 return this;
		}
		
	 /**==========================================================================================================
	  * DESCRIPTION: to verify if default Shipping Address Switch is ON
	  * PARAMETERS: 
	  * RETURN: boolean
	  ==========================================================================================================*/
		public boolean isdefaultShippingAddressSwitchON() {
			waitForElementPresent(DEFAULT_SHIPPING_ADDRESS_SWITCH);
			if(getText(DEFAULT_SHIPPING_ADDRESS_SWITCH).contains(ON_KEYWORD)){
				logger.info("default Shipping Address toggle is ON");
				return true;
			}
			logger.info("default Shipping Address is OFF");
			return false;
		}
		/**==========================================================================================================
		 * DESCRIPTION: to fill Shipping Address Data
		 * PARAMETERS: String address1, String postal, String city, String phone
		 * RETURN: CheckoutPage
		 ==========================================================================================================*/
		public CheckoutPage fillShippingAddressData(String address1, String postal, String city, String phone){
			scrollToExact("Phone");
			typeAddressFirstLine(address1).typePostalCode(postal).typeCity(city).typePhone(phone);
			logger.info("Address data filled in successfully");
			return new CheckoutPage(driver);
		}
		/**==========================================================================================================
		 * DESCRIPTION: to edit Ship To Address
		 * PARAMETERS: 
		 * RETURN: ShippingAddressPage
		 ==========================================================================================================*/
		public ShippingAddressPage editShipToAddress(){
			waitForElementPresent(EDIT_SHIP_TO_OVERLAY);
			click(EDIT_SHIP_TO_OVERLAY);
			logger.info("Edit link on Ship To Overlay is clicked");
			return new ShippingAddressPage(driver);
		}
		
}
