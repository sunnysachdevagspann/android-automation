package com.sephora.pages.AndroidApp.Checkout;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.sephora.base.pages.SephoraAndroidAppBasePage;
import com.sephora.core.driver.mobile.SephoraMobileDriver;

/**==========================================================================================================
 * DESCRIPTION: Payment Page contains all the functions for the actions which takes 
* place on Payment Page
==========================================================================================================*/

public class PaymentPage extends SephoraAndroidAppBasePage {

	public PaymentPage(SephoraMobileDriver driver) {
		super(driver);
		
	}
	
	public static final By ADD_PAYMENT_OVERLAY_CC = By.id("com.sephora:id/payment_selection_add_credit_card");
	public static final By EDIT_PAYMENT_OVERLAY_CC = By.id("com.sephora:id/payment_selection_secondary_button");
	public static final By ADD_PAYMENT_OVERLAY_GC = By.id("com.sephora:id/payment_selection_add_gift_card");
	public static final By ADD_PAYMENT_OVERLAY_CANCEL = By.id("com.sephora:id/payment_selection_cancel");
	public static final By SCAN_CARD_LOC= By.id("com.sephora:id/scan_credit_card");
	public static final By CARD_NUMBER_INPUT_FIELD = By.id("com.sephora:id/credit_card_number");
	public static final By SECURITY_CODE_LOC= By.id("com.sephora:id/security_code");
	public static final By FIRST_NAME_INPUT_FIELD = By.id("com.sephora:id/first_name");
	public static final By LAST_NAME_INPUT_FIELD = By.id("com.sephora:id/last_name");
	public static final By SAVE_TO_MY_ACCOUNT_SWITCH= By.id("com.sephora:id/save_to_my_account_switch");
	public static final By MAKE_MY_CARD_DEFAULT_SWITCH= By.id("com.sephora:id/make_my_default_card_switch");
	public static final By BILLING_ADDRESS_CHECKBOX = By.xpath("//android.widget.CheckBox[contains(@text,'Billing address')]");
	public static final By MONTH_SPINNER_LOC = By.xpath("//android.widget.TextView[@text='Month']");
	public static final By YEAR_SPINNER_LOC = By.id("com.sephora:id/year_spinner");
	public static final String SPINNER_MONTH_YEAR_OVERLAY_SELECTION= "//android.widget.TextView[contains(@text,'%s')]";
	public static final By STATE_SPINNER_LOC= By.id("com.sephora:id/add_shipping_state");
	public static final By COUNTRY_SPINNER_LOC = By.id("com.sephora:id/add_shipping_country");
	public static final String SPINNER_COUNTRY_STATE_OVERLAY_SELECTION = "//android.widget.CheckedTextView[contains(@text,'%s')]";
	public static final By SPINNER_STATE_AND_COUNTRY_OVERLAY_SELECTION= By.xpath("//android.widget.CheckedTextView");
	public static final By ZIP_POSTAL_INPUT_FIELD = By.xpath("//android.widget.LinearLayout[@text='Postal Code']/android.widget.EditText");
	public static final By PHONE_INPUT_FIELD = By.xpath("//android.widget.LinearLayout[@text='Phone']/android.widget.EditText");
	public static final By CITY_INPUT_FIELD = By.xpath("//android.widget.LinearLayout[@text='City']/android.widget.EditText");
	
	/**==========================================================================================================
	 * DESCRIPTION: to create dynamic xpath for locators of month and year dropdown
	 * PARAMETERS: 
	 * RETURN: WebElement
	 ==========================================================================================================*/
	private WebElement MonthandYearSelect(String text) {
	    return findElement(By.xpath(String.format(SPINNER_MONTH_YEAR_OVERLAY_SELECTION, text)));
	}
	/**==========================================================================================================
	 * DESCRIPTION: to click month or year value from the drop-down using dynamic xpath
	 * PARAMETERS: 
	 * RETURN: 
	 ==========================================================================================================*/
	public void clickMonthandYear(String name){
		MonthandYearSelect(name).click();
		}
	/**==========================================================================================================
	 * DESCRIPTION: o create dynamic xpath for locators of country and state dropdown
	 * PARAMETERS: 
	 * RETURN: WebElement
	 ==========================================================================================================*/
	private WebElement CountryandStateSelect(String text) {
	    return findElement(By.xpath(String.format(SPINNER_COUNTRY_STATE_OVERLAY_SELECTION, text)));
	}
	/**==========================================================================================================
	 * DESCRIPTION: to click country and state value from the drop-down using dynamic xpath
	 * PARAMETERS: 
	 * RETURN: boolean
	 ==========================================================================================================*/
	public void clickCountryandState(String name){
		CountryandStateSelect(name).click();
		}
	/**==========================================================================================================
	 * DESCRIPTION: to click Add Credit Card Overlay on Payment page
	 * PARAMETERS: 
	 * RETURN: PaymentPage
	 ==========================================================================================================*/
	public PaymentPage clickAddCreditCardOverlay(){
		waitForElementPresent(ADD_PAYMENT_OVERLAY_CC);
		click(ADD_PAYMENT_OVERLAY_CC);
		logger.info("Add Credit Card on overlay is clicked");
		return new PaymentPage(driver);
	}
	/**==========================================================================================================
	 * DESCRIPTION: to click Edit Credit Card Overlay on payment page
	 * PARAMETERS: 
	 * RETURN: PaymentPage
	 ==========================================================================================================*/
	public PaymentPage clickEditCreditCardOverlay(){
		waitForElementPresent(EDIT_PAYMENT_OVERLAY_CC);
		click(EDIT_PAYMENT_OVERLAY_CC);
		logger.info("Edit Credit Card on overlay is clicked");
		return new PaymentPage(driver);
	}
	/**==========================================================================================================
	 * DESCRIPTION: to check Billing Address Checkbox
	 * PARAMETERS: 
	 * RETURN: PaymentPage
	 ==========================================================================================================*/
	public PaymentPage checkBillingAddressCheckbox(String status){
		waitForElementPresent(BILLING_ADDRESS_CHECKBOX);
		if(status=="ON"){
		if(getAttribute(BILLING_ADDRESS_CHECKBOX, "checked").equals("true")){
			logger.info("'billing address same as shipping address' checkbox is already checked");
		}
		else{
		click(BILLING_ADDRESS_CHECKBOX);
		logger.info("Billing Address checkBox is clicked");
		}
		}
		if(status=="OFF"){
			if(getAttribute(BILLING_ADDRESS_CHECKBOX, "checked").equals("true")){
				click(BILLING_ADDRESS_CHECKBOX);
				logger.info("Billing Address checkBox is clicked");
				
			}
			else{
				logger.info("'billing address same as shipping address' checkbox is already unchecked");
			}
			}
		return new PaymentPage(driver);
	}
	/**==========================================================================================================
	 * DESCRIPTION: to type Credit Card Last Name
	 * PARAMETERS: 
	 * RETURN: PaymentPage
	 ==========================================================================================================*/
	 public PaymentPage typeCCLastName(String lastName) {
			type(LAST_NAME_INPUT_FIELD, lastName);
			logger.info(lastName+" - type Last Name in Add Credit Card Page");
			return new PaymentPage(driver);
		}
	 /**==========================================================================================================
	  * DESCRIPTION: to type Credit Card First Name
	  * PARAMETERS: 
	  * RETURN: PaymentPage
	  ==========================================================================================================*/
	 public PaymentPage typeCCFirstName(String firstName) {
			type(FIRST_NAME_INPUT_FIELD, firstName);
			logger.info(firstName+" - type first Name in Add Credit Card Page");
			return new PaymentPage(driver);
		}
	 /**==========================================================================================================
	  * DESCRIPTION: to type Credit card Security Code
	  * PARAMETERS: 
	  * RETURN: PaymentPage
	  ==========================================================================================================*/
	 public PaymentPage typeCCSecurityCode(String securityCode) {
			type(SECURITY_CODE_LOC, securityCode);
			logger.info(securityCode+" - type security code in Add Credit Card Page");
			return new PaymentPage(driver);
		}
	 /**==========================================================================================================
	  * DESCRIPTION: to type credit card number 
	  * PARAMETERS: 
	  * RETURN: PaymentPage
	  ==========================================================================================================*/
	 public PaymentPage typeCreditCardNumber(String ccNumber) {
			type(CARD_NUMBER_INPUT_FIELD, ccNumber);
			logger.info(ccNumber+" - type Credit Card number in Add Credit Card Page");
			return new PaymentPage(driver);
		}
	 /**==========================================================================================================
	  * DESCRIPTION: to verify if Save To My Account Switch is Present
	  * PARAMETERS: 
	  * RETURN: boolean
	  ==========================================================================================================*/
	 public boolean isSaveToMyAccountSwitchPresent() {
			if(waitExplicitly(SAVE_TO_MY_ACCOUNT_SWITCH, 10)) {
				return true;
			}
			logger.info("Save To My Account switch is not present");
			return false;
		}
	 /**==========================================================================================================
	  * DESCRIPTION: to click Save To My Account Switch as per status
	  * PARAMETERS: String status
	  * RETURN: PaymentPage
	  ==========================================================================================================*/
	 public PaymentPage clickSaveToMyAccountSwitch(String status) {
		 if(status=="ON"){
			 if(isSaveToMyAccountSwitchON()){
				 logger.info("Save To My Account switch is already in ON state");
			 }
			 else{
				 waitForElementPresent(SAVE_TO_MY_ACCOUNT_SWITCH);
					click(SAVE_TO_MY_ACCOUNT_SWITCH);
					logger.info("Save To My Account switch is clicked");
					logger.info("Save To My Account switch is now in ON State");
					return this;
			 }
		 }
		 else if(status=="OFF"){
			 if(isSaveToMyAccountSwitchON()){
				 waitForElementPresent(SAVE_TO_MY_ACCOUNT_SWITCH);
					click(SAVE_TO_MY_ACCOUNT_SWITCH);
					logger.info("Save To My Account switch is clicked");
					logger.info("Save To My Account switch is now in OFF State");
					return this;
			 }
			 else{
				 logger.info("Save To My Account switch is already in OFF state");
			 }
		 }
		 return this;
		}
		
	 /**==========================================================================================================
	  * DESCRIPTION: to verify if Save To My Account Switch is ON
	  * PARAMETERS: 
	  * RETURN: boolean
	  ==========================================================================================================*/
		public boolean isSaveToMyAccountSwitchON() {
			waitForElementPresent(SAVE_TO_MY_ACCOUNT_SWITCH);
			if(getText(SAVE_TO_MY_ACCOUNT_SWITCH).contains(ON_KEYWORD)){
				logger.info("Save To My Account switch is ON");
				return true;
			}
			logger.info("Save To My Account switch is OFF");
			return false;
		}
		/**==========================================================================================================
		 * DESCRIPTION: to verify if Make My Default Credit Card Switch is Present
		 * PARAMETERS: 
		 * RETURN: boolean
		 ==========================================================================================================*/
		public boolean isMakeMyDefaultCCSwitchPresent() {
			if(waitExplicitly(MAKE_MY_CARD_DEFAULT_SWITCH, 10)) {
				return true;
			}
			logger.info("Make My Default CC Switch is not present");
			return false;
		}
		/**==========================================================================================================
		 * DESCRIPTION: to click Make My Default Credit Card Switch Switch as per statis
		 * PARAMETERS: String status
		 * RETURN: PaymentPage
		 ==========================================================================================================*/
	 public PaymentPage clickMakeMyDefaultCCSwitchSwitch(String status) throws InterruptedException {
		 if(status=="ON"){
			 if(isMakeMyDefaultCCSwitchON()){
				 logger.info("Make My Default CC Switch is already in ON state");
			 }
			 else{
				 waitForElementPresent(MAKE_MY_CARD_DEFAULT_SWITCH);
					click(MAKE_MY_CARD_DEFAULT_SWITCH);
					logger.info("Make My Default CC Switch is clicked");
					logger.info("Make My Default CC Switch is now in ON State");
					return this;
			 }
		 }
		 else if(status=="OFF"){
			 if(isMakeMyDefaultCCSwitchON()){
				 waitForElementPresent(MAKE_MY_CARD_DEFAULT_SWITCH);
					click(MAKE_MY_CARD_DEFAULT_SWITCH);
					logger.info("Make My Default CC Switch is clicked");
					logger.info("Make My Default CC Switch is now in OFF State");
					return this;
			 }
			 else{
				 logger.info("Make My Default CC Switch is already in OFF state");
			 }
		 }
		 return this;
		}
	 /**==========================================================================================================
	  * DESCRIPTION: to verify if Make My Default Credit Card Switch is ON
	  * PARAMETERS: 
	  * RETURN: boolean
	  ==========================================================================================================*/
		public boolean isMakeMyDefaultCCSwitchON(){
			scrollToExact("Make my default credit card");
			waitForElementPresent(MAKE_MY_CARD_DEFAULT_SWITCH);
			if(getText(MAKE_MY_CARD_DEFAULT_SWITCH).contains(ON_KEYWORD)){
				logger.info("Make My Default CC Switch is ON");
				return true;
			}
			logger.info("Make My Default CC Switch is OFF");
			return false;
		}
		/**==========================================================================================================
		 *  DESCRIPTION: to select Credit Card Expiry Month
		 * PARAMETERS: String month
		 * RETURN: PaymentPage
		 ==========================================================================================================*/
		public PaymentPage selectCCExpiryMonth(String month){
			waitForElementPresent(MONTH_SPINNER_LOC);
			logger.info("Selecting CC Expiry MOnth");
			click(MONTH_SPINNER_LOC);
			scrollToExact(month);
			clickMonthandYear(month);
			return new PaymentPage(driver);
		}
		/**==========================================================================================================
		 * DESCRIPTION: to select Credit Card Expiry Year
		 * PARAMETERS: String year
		 * RETURN: PaymentPage
		 ==========================================================================================================*/
		public PaymentPage selectCCExpiryYear(String year){
			waitForElementPresent(YEAR_SPINNER_LOC);
			logger.info("Selecting CC Expiry Year");
			click(YEAR_SPINNER_LOC);
			scrollToExact(year);
			clickMonthandYear(year);
			return new PaymentPage(driver);
		}
		/**==========================================================================================================
		 * DESCRIPTION: to Type Postal Code on edit payment page
		 * PARAMETERS: String postal
		 * RETURN: PaymentPage
		 ==========================================================================================================*/
		public PaymentPage TypePostalCode(String postal){
			clear(ZIP_POSTAL_INPUT_FIELD);
			type(ZIP_POSTAL_INPUT_FIELD, postal);
			logger.info("Typed Postal Code :"+postal);
			return new PaymentPage(driver);
		}
		
		
		/**==========================================================================================================
		 * DESCRIPTION: to fill credit card Details
		 * PARAMETERS: String ccNumber, String firstName, String lastName, String securityCode, String month, String year
		 * RETURN: CheckoutPage
		 ==========================================================================================================*/
		public CheckoutPage fillCCDetails(String ccNumber, String firstName, String lastName, String securityCode, String month, String year){
			typeCreditCardNumber(ccNumber).selectCCExpiryMonth(month).selectCCExpiryYear(year).typeCCSecurityCode(securityCode)
			.typeCCFirstName(firstName).typeCCLastName(lastName);
		return new CheckoutPage(driver);
		}
		/**==========================================================================================================
		 * DESCRIPTION: to fill JC Penny credit card Details
		 * PARAMETERS: String ccNumber, String firstName, String lastName
		 * RETURN: CheckoutPage
		 ==========================================================================================================*/
		public CheckoutPage fillJCPDetails(String ccNumber, String firstName, String lastName){
			typeCreditCardNumber(ccNumber).typeCCFirstName(firstName).typeCCLastName(lastName);
		return new CheckoutPage(driver);
		}
		/**==========================================================================================================
		 * DESCRIPTION: to select state from dropdown on edit payment page
		 * PARAMETERS: String state
		 * RETURN: PaymentPage
		 ==========================================================================================================*/
		public PaymentPage selectState(String state){
			waitForElementPresent(STATE_SPINNER_LOC);
			logger.info("Selecting State");
			click(STATE_SPINNER_LOC);
			scrollToExact(state);
			clickCountryandState(state);
			return new PaymentPage(driver);
		}
		/**==========================================================================================================
		 * DESCRIPTION: to select Country from dropdown on edit payment page
		 * PARAMETERS: String country
		 * RETURN: PaymentPage
		 ==========================================================================================================*/
		public PaymentPage selectCountry(String country){
			scrollToExact("Phone");
			waitForElementPresent(COUNTRY_SPINNER_LOC);
			logger.info("Selecting Country");
			click(COUNTRY_SPINNER_LOC);
			scrollToExact(country);
			clickCountryandState(country);
			return new PaymentPage(driver);
		}
		/**==========================================================================================================
		 * DESCRIPTION: to click Add Gift Card Overlay on payment page
		 * PARAMETERS: 
		 * RETURN: GiftCardPage
		 ==========================================================================================================*/
		public GiftCardPage clickAddGiftCardOverlay(){
			waitForElementPresent(ADD_PAYMENT_OVERLAY_GC);
			click(ADD_PAYMENT_OVERLAY_GC);
			return new GiftCardPage(driver);
		}
		/**==========================================================================================================
		 * DESCRIPTION: to type Phone in edit payment page
		 * PARAMETERS: String phone
		 * RETURN: PaymentPage
		 ==========================================================================================================*/
		public PaymentPage typePhone(String phone) throws InterruptedException{
			scrollPageDown(4);
			clear(PHONE_INPUT_FIELD);
			type(PHONE_INPUT_FIELD, phone);
			logger.info("Typed Phone Number :"+phone);
			return new PaymentPage(driver);
		}
		/**==========================================================================================================
		 * DESCRIPTION: to type City in edit payment page
		 * PARAMETERS: String city
		 * RETURN: PaymentPage
		 ==========================================================================================================*/
		public PaymentPage typeCity(String city){
			scrollToExact("Phone");
			clear(CITY_INPUT_FIELD);
			type(CITY_INPUT_FIELD, city);
			logger.info("Typed City :"+city);
			return new PaymentPage(driver);
		}
		
}
