package com.sephora.pages.AndroidApp.Checkout;

import org.openqa.selenium.By;

import com.sephora.base.pages.SephoraAndroidAppBasePage;
import com.sephora.core.driver.mobile.SephoraMobileDriver;

/**==========================================================================================================
 * DESCRIPTION: Gift Card Shipping Page contains all the functions for the actions which takes 
 * place on Gift Card Shipping Page
 ==========================================================================================================*/

public class GiftCardShippingPage extends SephoraAndroidAppBasePage {
	
	public GiftCardShippingPage(SephoraMobileDriver driver) {
		super(driver);
	}
	
	public static final By GC_ADD_NEW_ADDRESS = By.id("com.sephora:id/add_gc_shipping_add_new_btn");
	public static final By GC_ADD_MESSAGE_INPUT_FIELD = By.id("com.sephora:id/add_gc_shipping_gift_message_input");
	public static final By NEXT_BUTTON_ON_HEADER = By.id("com.sephora:id/editor_action_next");
	public static final String GC_HEADER_TITLE = "GIFT CARD SHIPPING";
	public static final By EDIT_GC_SHIPPING_ADDRESS = By.id("com.sephora:id/add_gc_shipping_address_edit_btn");
	
	/**==========================================================================================================
	 * DESCRIPTION: to click new Address button on gift card shipping page
	 * PARAMETERS: 
	 * RETURN: GiftCardShippingPage
	 ==========================================================================================================*/
	
	public GiftCardShippingPage clickAddNewAddressGC(){
		if(isElementPresent(GC_ADD_NEW_ADDRESS));
		click(GC_ADD_NEW_ADDRESS);
		return new GiftCardShippingPage(driver);
	}
	
	/**==========================================================================================================
	 * DESCRIPTION: to type message on gift card shipping page
	 * PARAMETERS: 
	 * RETURN: GiftCardShippingPage
	 ==========================================================================================================*/
	public GiftCardShippingPage typeMessageGC(String message){
		if(isElementPresent(GC_ADD_MESSAGE_INPUT_FIELD));
		type(GC_ADD_MESSAGE_INPUT_FIELD, message);
		return new GiftCardShippingPage(driver);
	}
	/**==========================================================================================================
	 * DESCRIPTION: to click on nest button on header of gift card shipping page
	 * PARAMETERS: 
	 * RETURN: GiftCardShippingPage
	 ==========================================================================================================*/
	public GiftCardShippingPage clickNextButtonOnHeaderGC(){
		scrollToExact("NEXT");
		if(isElementPresent(NEXT_BUTTON_ON_HEADER));
		click(NEXT_BUTTON_ON_HEADER);
		return new GiftCardShippingPage(driver);
	}
	
	}
	
