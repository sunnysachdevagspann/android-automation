package com.sephora.pages.AndroidApp.Checkout;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;

import com.sephora.base.pages.SephoraAndroidAppBasePage;
import com.sephora.core.driver.mobile.SephoraMobileDriver;
import com.sephora.core.driver.mobile.SephoraMobileDriver;

/**==========================================================================================================
 * CheckoutPage is the page which
 * contains Checkout specific reusable components 
 * 
 ==========================================================================================================*/

public class CheckoutPage extends SephoraAndroidAppBasePage{

	public CheckoutPage(SephoraMobileDriver driver) {
		super(driver);
	}
	
	private static final Logger logger = LogManager.getLogger(CheckoutPage.class.getName());
	
	public static final By PROMOTIONS_TEXT_LOC = By.xpath("//android.widget.TextView[@text='PROMOTIONS']");
	public static final By PROMOTION_APPLY_BUTTON = By.id("com.sephora:id/basket_promotion_apply");
	public static final By PROMO_INPUT_FIELD = By.id("com.sephora:id/basket_promotion_coupon_input");
	public static final By CODE_PER_ORDER = By.id("com.sephora:id/basket_promotion_one_code_per_order");
	public static final By VIEW_CURRENT_PROMOTION = By.xpath("//android.widget.TextView[@text='View current promotions']");
	public static final By PLACE_ORDER_BUTTON = By.id("com.sephora:id/checkout_place_order_button");
	public static final By ADD_SHIPPING_ADDRESS = By.xpath("//android.widget.TextView[@text='Add Shipping Address']");
	public static final By ADD_DELIVERY_METHOD = By.xpath("//android.widget.TextView[contains(@text,'Delivery')]");
	public static final By ADD_PAYMENT = By.xpath("//android.widget.TextView[@text='Add Payment']");
	public static final By ITEM_TOTAL = By.id("com.sephora:id/order_items");
	public static final By ORDER_TOTAL_LABEL = By.id("com.sephora:id/order_total_price_label");
	public static final By ORDER_TOTAL_VALUE = By.id("com.sephora:id/order_total_price");
	public static final By MERCHANDISE_SUB_TOTAL_LABEL = By.id("com.sephora:id/order_summary_merch_total_label");
	public static final By MERCHANDISE_TOTAL_VALUE = By.id("com.sephora:id/order_summary_merch_total_value");
	public static final By DISCOUNT_VALUE = By.id("com.sephora:id/order_summary_discount_value");
	public static final By MERCHANDISE_SHIPPING_VALUE = By.id("com.sephora:id/order_summary_shipping_n_handling_value");
	public static final By GIFT_CARD_REDEEMED_VALUE = By.id("com.sephora:id/order_summary_gc_redeem_value");
	public static final By TAX_LABEL = By.id("com.sephora:id/order_summary_tax_label");
	public static final By TAX_TOTAL_VALUE = By.id("com.sephora:id/order_summary_tax_value");
	public static final By SHIPPING_ADDRESS_DETAIL = By.id("com.sephora:id/address_details");
	public static final By SHIP_TO_ACCORDION = By.xpath("//android.widget.TextView[@text='Ship to']");
	public static final By DELIVERY_METHOD_DETAIL= By.id("com.sephora:id/delivery_method_details");
	public static final By PAYMENT_DETAIL= By.id("com.sephora:id/payment_card_info");
	public static final By PAYMENT_DETAIL_GC= By.xpath("//android.widget.LinearLayout/android.widget.TextView[contains(@text,'Gift Card Redeemed')]");
	public static final By ADD_PROMO_INPUT_FIELD = By.id("com.sephora:id/basket_promotion_coupon_input");
	public static final By PROMO_ADDED_MESSAGE_LOC = By.id("com.sephora:id/basket_promotion_coupon");
	public static final By PROMO_ITEM_NAME = By.id("com.sephora:id/cart_item_product_name");
	public static final By PROMO_ITEM_NAME_GWP = By.id("com.sephora:id/cart_item_brand_name");
	public static final By PROMO_REMOVE_BUTTON = By.id("com.sephora:id/promotion_item_remove_button");
	public static final By STORE_CREDIT_TOTAL=By.id("com.sephora:id/order_summary_store_credit_value");
	public static final By GIFT_WRAP_OPTION_DETAIL =By.id("com.sephora:id/wrap_options_details");
	public static final By GIFT_WRAP_TOTAL = By.id("com.sephora:id/order_summary_gift_wrap_value");
	public static final By GC_SHIPPING_AND_DELIVERY_ACCORDION = By.id("com.sephora:id/order_item_title");
	public static final By GC_SHIPPING_ADDRESS_DETAIL = By.id("com.sephora:id/gc_shipping_address");
	public static final By GC_DELIVERY_ADDRESS_DETAIL = By.id("com.sephora:id/gc_shipping_delivery");
	public static final By GC_MESSAGE_DETAIL= By.id("com.sephora:id/gc_shipping_gift_message");
	public static final By NEXT_BUTTON_ON_HEADER = By.id("com.sephora:id/editor_action_next");
    public static final By PRODUCT_BRAND_NAME = By.xpath("//android.widget.LinearLayout[@index='4']/android.widget.LinearLayout/android.widget.TextView[@index='0']");
    public static final By PRODUCT_PRODUCT_NAME = By.xpath("//android.widget.LinearLayout[@index='4']/android.widget.LinearLayout/android.widget.TextView[@index='1']");
    public static final By PRODUCT_VARIATION = By.xpath("//android.widget.LinearLayout[@index='4']/android.widget.LinearLayout/android.widget.TextView[@index='2']");
    public static final By PRODUCT_QUANTITY = By.xpath("//android.widget.LinearLayout[@index='4']/android.widget.LinearLayout/android.widget.TextView[@index='3']"); 
    public static final By PRODUCT_PRICE = By.xpath("//android.widget.LinearLayout[@index='4']/android.widget.LinearLayout/android.widget.TextView[@index='4']"); 
	
	/**==========================================================================================================
	 * DESCRIPTION: To wait for checkout page to load
	 * PARAMETERS: 
	 * RETURN: CheckoutPage
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	public CheckoutPage waitforCheckOutPageToLoad(){
		waitForElementPresent(WINDOW_HEADER);
		if(getText(WINDOW_HEADER).equalsIgnoreCase("Checkout")){
		logger.info("user is on Checkout page");
			}
		return new CheckoutPage(driver);
	}
	/**==========================================================================================================
	 * DESCRIPTION: To verify if its Checkout page
	 * PARAMETERS: 
	 * RETURN: CheckoutPage
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	public boolean isCheckoutPage(){
		if(getText(WINDOW_HEADER).contains("Checkout") && isElementPresent(ORDER_TOTAL_LABEL)){
			scrollPageDown(4);
			if(isElementPresent(MERCHANDISE_TOTAL_VALUE)){
			logger.info("User is on Checkout Page");
			}
		return true;
	}
		logger.info("User is on: "+ getText(WINDOW_HEADER) +" page instead of Checkout Page");
		return false;
	}
	/**==========================================================================================================
	 * DESCRIPTION: To verify if Place order button is enabled
	 * PARAMETERS: 
	 * RETURN: boolean
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	public boolean isPlaceOrderEnabled(){
		if(IsLocatorEnable(PLACE_ORDER_BUTTON)){
			logger.info("Place Order button is enabled");
		return true;
	}
		return false;
	}
	/**==========================================================================================================
	 * DESCRIPTION: To Click Place order button at the bottom
	 * PARAMETERS: 
	 * RETURN: OrderConfirmationPage
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	public OrderConfirmationPage clickPlaceOrderButton(){
		waitForElementPresent(PLACE_ORDER_BUTTON);
		if(IsLocatorEnable(PLACE_ORDER_BUTTON) && IsLocatorVisible(PLACE_ORDER_BUTTON));
			click(PLACE_ORDER_BUTTON);
		return new OrderConfirmationPage(driver);
		
	}
	/**==========================================================================================================
	 * DESCRIPTION: To click Delivery and Gift Option Accordion
	 * PARAMETERS: 
	 * RETURN: DeliveryAndGiftOptionPage
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	public DeliveryAndGiftOptionPage clickDeliveryAndGiftOptionAccordion(){
		waitForElementPresent(ADD_DELIVERY_METHOD);
		click(ADD_DELIVERY_METHOD);
		logger.info("Delivery Method & Gift Wrap Option is clicked");
		return new DeliveryAndGiftOptionPage(driver);
	}
	/**==========================================================================================================
	 * DESCRIPTION: To click add Shipping Address Accordion
	 * PARAMETERS: 
	 * RETURN: ShippingAddressPage
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	public ShippingAddressPage clickAddShippingAddressAccordion(){
		waitForElementPresent(ADD_SHIPPING_ADDRESS);
		click(ADD_SHIPPING_ADDRESS);
		logger.info("Add Shipping Address accordion is clicked");
		return new ShippingAddressPage(driver);
	}
	/**==========================================================================================================
	 * DESCRIPTION: To click Ship To Accordion if Address if pre-filled
	 * PARAMETERS: 
	 * RETURN: ShippingAddressPage
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	public ShippingAddressPage clickShipToAccordion(){
		waitForElementPresent(SHIP_TO_ACCORDION);
		click(SHIP_TO_ACCORDION);
		logger.info("Ship To accordion is clicked");
		return new ShippingAddressPage(driver);
	}
	/**==========================================================================================================
	 * DESCRIPTION: To click add Payment Accordion
	 * PARAMETERS: 
	 * RETURN: PaymentPage
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	public PaymentPage clickAddPaymentAccordion(){
		waitForElementPresent(ADD_PAYMENT);
		click(ADD_PAYMENT);
		logger.info("Add Payment accordion is clicked");
		return new PaymentPage(driver);
	}
	/**==========================================================================================================
	 * DESCRIPTION: To verify if Shipping Address is added
	 * PARAMETERS: String address
	 * RETURN: boolean
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	public boolean isShippingAddressAdded(String address){
		if(getText(SHIPPING_ADDRESS_DETAIL).contains(address)){
		return true;
	}
		return false;
	}
	/**==========================================================================================================
	 * DESCRIPTION: To verify if delivery option updated
	 * PARAMETERS: String deliveryMethod
	 * RETURN: boolean
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	public boolean isDeliveryOptionUpdated(String deliveryMethod){
		if(getText(DELIVERY_METHOD_DETAIL).contains(deliveryMethod)){
		return true;
	}
		return false;
	}
	/**==========================================================================================================
	 * DESCRIPTION: to verify if credit card added
	 * PARAMETERS: String cardNumber
	 * RETURN: boolean
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	public boolean isCreditCardAdded(String cardNumber){
		scrollPageDown(2);
		scrollToExact("PAYMENT");
		String lastDigit = cardNumber.substring(cardNumber.length()-4, cardNumber.length()).trim();
		if(isElementPresent(PAYMENT_DETAIL)) {
		if(getText(PAYMENT_DETAIL).contains(lastDigit)) {
			return true;
		}else {
			logger.info(cardNumber+"-- is not present");
			return false;
		}
	}else {
		logger.info("none of card is selected");
		return false;
	}
	}
	/**==========================================================================================================
	 * DESCRIPTION: to verify if gift card added
	 * PARAMETERS: String giftCardNumber
	 * RETURN: boolean
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	public boolean isGiftCardAdded(String giftCardNumber){
		scrollToExact("PAYMENT");
		String lastDigit = giftCardNumber.substring(giftCardNumber.length()-4, giftCardNumber.length()).trim();
		if(isElementPresent(PAYMENT_DETAIL_GC)) {
		if(getText(PAYMENT_DETAIL_GC).contains(lastDigit)) {
			return true;
		}else {
			logger.info(giftCardNumber+"-- is not present");
			return false;
		}
	}else {
		logger.info("none of card is selected");
		return false;
	}
	}
	/**==========================================================================================================
	 * DESCRIPTION: to click Edit payment accordion
	 * PARAMETERS: 
	 * RETURN: PaymentPage
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	public PaymentPage clickEditPaymentAccordion(){
		scrollToExact("PAYMENT");
		waitForElementPresent(PAYMENT_DETAIL);
		click(PAYMENT_DETAIL);
		logger.info("Edit Payment accordion is clicked");
		return new PaymentPage(driver);
	}
	/**==========================================================================================================
	 * DESCRIPTION: to verify if promotion is added
	 * PARAMETERS: String promoCode
	 * RETURN: boolean
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	public boolean isPromoAdded(String promoCode){
		if(promoCode.equals(PROMO_CODE_GWP)){
		scrollToExact("PROMOTIONS");
		}
		else{
			scrollToExact("1 code per order");
		}
		if(isElementPresent(PROMO_ADDED_MESSAGE_LOC) && getText(PROMO_ADDED_MESSAGE_LOC).equalsIgnoreCase("Promo Applied: "+promoCode)) {
			if(promoCode.equals(PROMO_CODE_GWP)){
				logger.info("promotion added and displayed correctly on Checkout: "+ promoCode + " is having details: " + getText(PROMO_ITEM_NAME_GWP).toUpperCase()
						+ "/n info: " + getText(PROMO_ITEM_NAME));
				return true;
			} else{
			logger.info("promotion added and displayed correctly on Checkout: "+ promoCode + " is having details: " + getText(PROMO_ITEM_NAME).toUpperCase());
			return true;
		      }
		}
		logger.info(promoCode+"-- is not present");
		return false;
	}
	/**==========================================================================================================
	 * DESCRIPTION: To add promotion on checkout page
	 * PARAMETERS: String promoCode
	 * RETURN: CheckoutPage
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	public CheckoutPage addPromotion(String promoCode){
		scrollToExact("1 code per order");
		waitForElementPresent(PROMOTIONS_TEXT_LOC);
		type(PROMO_INPUT_FIELD, promoCode);
		logger.info("The promocode: "+ promoCode + " is typed in the promo input field");
		click(PROMOTION_APPLY_BUTTON);
		return new CheckoutPage(driver);
	}
	/**==========================================================================================================
	 * DESCRIPTION: to verify the total section of checkout page display correct info
	 * PARAMETERS: 
	 * RETURN: boolean
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	public boolean isTotalsSectionDisplayCorrectInfoforSanity() {
		scrollPageDown(4);
		if(isElementPresent(MERCHANDISE_SHIPPING_VALUE) && isElementPresent(MERCHANDISE_TOTAL_VALUE)
				&& isElementPresent(TAX_TOTAL_VALUE)){
			logger.info("Total items is: " + getText(ITEM_TOTAL));
			logger.info("Merchandise SubTotal is: " + getText(MERCHANDISE_TOTAL_VALUE));
			if(isElementPresent(DISCOUNT_VALUE)){
			logger.info("Discount is: " + getText(DISCOUNT_VALUE));
			}
			logger.info("Merchandise Shipping value is: " + getText(MERCHANDISE_SHIPPING_VALUE));
			logger.info("Tax value is: " + getText(TAX_TOTAL_VALUE));
			if(isElementPresent(GIFT_CARD_REDEEMED_VALUE)){
			logger.info("Gift Card Redeemed value is: " + getText(GIFT_CARD_REDEEMED_VALUE));
			}
			if(isElementPresent(GIFT_WRAP_TOTAL)){
				logger.info("Gift Wrap value is: " + getText(GIFT_WRAP_TOTAL));
			}
			if(isElementPresent(STORE_CREDIT_TOTAL)){
				logger.info("Store Credit Total is: " + getText(STORE_CREDIT_TOTAL));
			}
			logger.info("Order Total is: " + getText(ORDER_TOTAL_VALUE));
			logger.info("Checkout Totals section reflects the correct info");
			return true;
		}
		logger.info("Checkout Totals section fails to reflect the correct info");
		return false;
	}
	/**==========================================================================================================
	 * DESCRIPTION: to verify if Shipping address is pre-filled
	 * PARAMETERS: 
	 * RETURN: boolean
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	public boolean isShippingAddressPrefilled(){
		if(isElementPresent(SHIP_TO_ACCORDION) && isElementPresent(SHIPPING_ADDRESS_DETAIL) && !isElementPresent(ADD_SHIPPING_ADDRESS)){
			String address = getText(SHIPPING_ADDRESS_DETAIL).trim().replace("?", ", ");
			logger.info("The Shipping Address is Prefilled with the address: " + address);
		return true;
		}
		logger.info("The Shipping Address is not prefilled");
		return false;
	}
	/**==========================================================================================================
	 * DESCRIPTION: to verify if Delivery method is pre-filled
	 * PARAMETERS: String deliveryMethod
	 * RETURN: boolean
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	public boolean isDeliveryMethodPrefilled(String deliveryMethod){
		if(isElementPresent(DELIVERY_METHOD_DETAIL) ){
			String method = getText(DELIVERY_METHOD_DETAIL).trim().replace("?", ", ");
			System.out.println(method);
			if(method.contains(deliveryMethod)){
				logger.info("The Delivery Method is Prefilled with the details: " + method);
			}
			
		return true;
		}
		logger.info("The Delivery Method is not prefilled");
		return false;
	}
	/**==========================================================================================================
	 * DESCRIPTION: to verify if Credit card is pre-filled
	 * PARAMETERS: 
	 * RETURN: boolean
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	public boolean isCCPrefilledInPaymentSection(){
		scrollToExact("PAYMENT");
		if(isElementPresent(PAYMENT_DETAIL) && !isElementPresent(ADD_PAYMENT)){
			String paymentdetail = getText(PAYMENT_DETAIL);
			String lastDigit = paymentdetail.substring(paymentdetail.length()-4, paymentdetail.length()).trim();
			if(paymentdetail.contains(lastDigit)){
				logger.info("The Payment section is Prefilled with the CC details: " + paymentdetail);
			}
			
		return true;
		}
		logger.info("The Payment section is not prefilled");
		return false;
	}
	/**==========================================================================================================
	 * DESCRIPTION: to get number of items from checkout page
	 * PARAMETERS: 
	 * RETURN: items
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	public int getNumberOfItemsFromCheckoutOrderTotal() {
		int items = 0;
		String text = getText(ITEM_TOTAL);
		if (!text.isEmpty()) {
			items = getIntegerValueFromString(text);
			logger.info("the number of products currently in Checkout page is: " + items);
		}
		return items;
	}
	/**==========================================================================================================
	 * DESCRIPTION: to verify if Gift Wrap Option Added
	 * PARAMETERS: 
	 * RETURN: boolean
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	public boolean isGiftWrapOptionAdded(String giftWrap, String message){
		String giftWrapDetail = getText(GIFT_WRAP_OPTION_DETAIL).trim().replace("?", "/n");
		if(isElementPresent(GIFT_WRAP_OPTION_DETAIL)){
		if(giftWrapDetail.contains(giftWrap)){
			logger.info("Gift Wrap Option is added successfully");
		}
		if(giftWrapDetail.contains(message)){
			logger.info("Gift Wrap message is added successfully");
		}
		return true;
		}
		logger.info("Gift Wrap option and message is not added successfully");
		return false;
	}
	/**==========================================================================================================
	 * DESCRIPTION: to add Gift Card Shipping and Delivery
	 * PARAMETERS: 
	 * RETURN: CheckoutPage
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	public CheckoutPage addGCShippingAndDelivery(){
		scrollPageDown(3);
		scrollToExact("Select Gift Card Shipping & Delivery");
		if(isElementPresent(GC_SHIPPING_AND_DELIVERY_ACCORDION));
		click(GC_SHIPPING_AND_DELIVERY_ACCORDION);
		return new CheckoutPage(driver);
	}
	/**==========================================================================================================
	 * DESCRIPTION: to click Next Button On Header on add Gift Card Shipping address page
	 * PARAMETERS: 
	 * RETURN: GiftCardShippingPage
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	public GiftCardShippingPage clickNextButtonOnHeaderGC(){
		scrollToExact("NEXT");
		if(isElementPresent(NEXT_BUTTON_ON_HEADER));
		click(NEXT_BUTTON_ON_HEADER);
		return new GiftCardShippingPage(driver);
	}
	/**==========================================================================================================
	 * DESCRIPTION: to verify if Gift card Address is added
	 * PARAMETERS: String gcAddress
	 * RETURN: boolean
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	public boolean isGCAddressAddedSuccssfully(String gcAddress){
		scrollPageDown(1);
		String address = getText(GC_DELIVERY_ADDRESS_DETAIL).trim().replace("?", " ");
		if(address.contains(gcAddress)){
			logger.info("GC Address Added successfully");
		return true;
		}
		logger.info("GC Address is not added");
		return false;
	}
	/**==========================================================================================================
	 * DESCRIPTION: to verify if Gift Card Message Added 
	 * PARAMETERS: String gcMessage
	 * RETURN: boolean
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	public boolean isGCMessageAddedSuccssfully(String gcMessage){
		String message = getText(GC_MESSAGE_DETAIL).trim().replace("?", " ");
		if(message.equalsIgnoreCase(gcMessage)){
			logger.info("GC Message Added successfully");
		return true;
		}
		logger.info("GC Message is not added");
		return false;
	}
	/**==========================================================================================================
	 * DESCRIPTION: To verify if Gift Card Delivery Method Added 
	 * PARAMETERS: String gcDeliveryMethod
	 * RETURN: boolean
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	public boolean isGCDeliveryMethodAddedSuccssfully(String gcDeliveryMethod){
		String deliverymethod = getText(GC_DELIVERY_ADDRESS_DETAIL).trim().replace("?", " ");
		if(deliverymethod.contains(gcDeliveryMethod)){
			logger.info("GC Delivery Method Added successfully");
		return true;
		}
		logger.info("GC Delivery Method is not added");
		return false;
	}
	/**==========================================================================================================
	 * DESCRIPTION: to remove promotion from checkout page
	 * PARAMETERS: 
	 * RETURN: CheckoutPage
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	public CheckoutPage removePromotion(){
		scrollToExact("1 code per order");
		waitForElementPresent(PROMO_REMOVE_BUTTON);
		click(PROMO_REMOVE_BUTTON);
		logger.info("The promocode is removed");
		return new CheckoutPage(driver);
	}
	
	/**==========================================================================================================
	 * DESCRIPTION: to click Item total section on checkout page
	 * PARAMETERS: 
	 * RETURN: CheckoutPage
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	
	public CheckoutPage clickItemTotalDropList(){
		if(isElementPresent(ITEM_TOTAL)){
			click(ITEM_TOTAL);
		}
		return new CheckoutPage(driver);
	}
	
	/**==========================================================================================================
	 * DESCRIPTION: to verify if the product info is correct on checkout page and to store data is arraylist
	 * PARAMETERS: 
	 * RETURN: boolean
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	
	public List <Object> checkoutItemDetails = new ArrayList<Object>(); 
	public boolean isCheckoutProductInfoCorrect(int numOfProduct) throws InterruptedException{
		int i;
		
		int numberOfItems =getNumberOfItemsFromCheckoutOrderTotal();
		logger.info("the number of item currently on checkout page is: " + numberOfItems);
		checkoutItemDetails.add(numberOfItems);
		for(i=1; i<=numOfProduct; i++){
			logger.info("the details of product #: " +i);
			String brandName = getText(PRODUCT_BRAND_NAME);
			if(!brandName.contains("...")){
			logger.info("the brand name is: " +brandName);
			checkoutItemDetails.add(brandName);
			}
			String productName = getText(PRODUCT_PRODUCT_NAME);
			if(!productName.contains("....")){
			logger.info("The product name is: " +productName);
			checkoutItemDetails.add(productName);
			}
			
			String productVariation = getText(PRODUCT_VARIATION);
			if(productVariation.contains("Qty")){
				int qtyOfProduct = getIntegerValueFromString(getText(PRODUCT_VARIATION)); 
				logger.info("The quantity of product # "+ i +" is: "+ qtyOfProduct);
				checkoutItemDetails.add(qtyOfProduct);
			}
			else{
				if(!productVariation.contains("...")){
			logger.info("The product variation is: "+(productVariation));
			checkoutItemDetails.add(productVariation);
				}
			}
			
			String quantityOfProduct = getText(PRODUCT_QUANTITY);
			if(quantityOfProduct.contains("$")||quantityOfProduct.contains("C$")){
				String price = getText(PRODUCT_QUANTITY); 
				logger.info("The price of product # "+ i +" is: "+ price);
				checkoutItemDetails.add(price);
			}
			else if(quantityOfProduct.contains("Qty")){
				int quantity = getIntegerValueFromString(quantityOfProduct);
				logger.info("The Quantity of the product "+ i +" is: "+quantity);
				checkoutItemDetails.add(quantity);
			}
			else{
				logger.info("The price of the product "+ i +" is: "+quantityOfProduct);
				checkoutItemDetails.add(quantityOfProduct);
			}
			if(isElementPresent(PRODUCT_PRICE)){
			String productPrice = getText(PRODUCT_PRICE);
			logger.info("The Price of the product# "+ i +" is: "+productPrice);
			checkoutItemDetails.add(productPrice);
			}
			
			scrollPageDownForCheckout(1);
		}
		System.out.println("size of list of product is: "+checkoutItemDetails.size());
		for(Object value:checkoutItemDetails){
			System.out.println(value);
		}
		
		return true;
	}
	
	
}
