package com.sephora.pages.AndroidApp.Checkout;

import java.util.Iterator;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.sephora.base.pages.SephoraAndroidAppBasePage;
import com.sephora.core.driver.mobile.SephoraMobileDriver;

/**==========================================================================================================
  * DESCRIPTION: Order Confirmation Page contains all the functions for the actions which takes 
 * place on Order Confirmation Page
 ==========================================================================================================*/
public class OrderConfirmationPage extends SephoraAndroidAppBasePage {

	public OrderConfirmationPage(SephoraMobileDriver driver) {
		super(driver);
	}
	
	public static final By THANKYOU_TEXT_LOC = By.xpath("//android.widget.TextView[contains(@text,'Thank you')]");
	public static final By ORDER_ID_LOC = By.id("com.sephora:id/order_conf_number");
	public static final By HEADER_SHOP_BUTTON = By.id("com.sephora:id/order_conf_menu_shop");
	public static final By KEEP_SHOPPING_BUTTON = By.xpath("//android.widget.Button[@text='Keep shopping']");
	public static final By ITEM_LIST= By.id("com.sephora:id/checkout_item_brand_name");
	public static final By ORDER_TOTAL_VALUE = By.id("com.sephora:id/order_total_price");
	public static final By MERCHANDISE_SUB_TOTAL_LABEL = By.id("com.sephora:id/order_summary_merch_total_label");
	public static final By MERCHANDISE_TOTAL_VALUE = By.id("com.sephora:id/order_summary_merch_total_value");
	public static final By DISCOUNT_VALUE = By.id("com.sephora:id/order_summary_discount_value");
	public static final By MERCHANDISE_SHIPPING_VALUE = By.id("com.sephora:id/order_summary_shipping_n_handling_value");
	public static final By GIFT_CARD_REDEEMED_VALUE = By.id("com.sephora:id/order_summary_gc_redeem_value");
	public static final By STORE_CREDIT_VALUE= By.id("com.sephora:id/order_summary_store_credit_value");
	public static final By TAX_LABEL = By.id("com.sephora:id/order_summary_tax_label");
	public static final By TAX_TOTAL_VALUE = By.id("com.sephora:id/order_summary_tax_value");
	public static final By BI_INFO_LABEL_LOC= By.xpath("//android.widget.TextView[@text='Sephora Beauty Insider']");
	public static final By BI_PREVIOUS_BALANCE_LABEL_LOC= By.xpath("//android.widget.TextView[@text='Previous Balance']");
	public static final By BI_POINTS_EARNED_LABEL_LOC= By.xpath("//android.widget.TextView[@text='Points Earned Today']");
	public static final By BI_POINTS_REDEEMED_LABEL_LOC= By.xpath("//android.widget.TextView[@text='Points Redeemed']");
	public static final By BI_NEW_BALANCE_LABEL_LOC= By.xpath("//android.widget.TextView[@text='New beauty bank balance']");
	public static final By BI_PREVIOUS_BALANCE= By.id("com.sephora:id/order_bi_prev_balance");
	public static final By BI_POINTS_EARNED= By.id("com.sephora:id/order_bi_earned_today");
	public static final By BI_POINTS_REDEEMED= By.id("com.sephora:id/order_bi_redeemed");
	public static final By BI_NEW_BALANCE= By.id("com.sephora:id/order_bi_new_balance");
	public static final By GIFT_WRAP_TOTAL = By.id("com.sephora:id/order_summary_gift_wrap_value");
	
	protected CheckoutPage checkoutPage;
	
	/**==========================================================================================================
	 * DESCRIPTION: to verify if the current page is Order confirmation page
	 * PARAMETERS: 
	 * RETURN: boolean
	 ==========================================================================================================*/
	
	public boolean isOrderConfirmationPage(){
		waitForElementPresent(WINDOW_HEADER);
		if(getText(WINDOW_HEADER).equalsIgnoreCase("ORDER CONFIRMATION")){
			isElementPresent(ORDER_ID_LOC);
			isElementPresent(HEADER_SHOP_BUTTON);
			logger.info("User is on Order Confirmation Page");
		return true;
		}
		return false;
		
	}
	/**==========================================================================================================
	 * DESCRIPTION: to verify if the order is placed successfully
	 * PARAMETERS: 
	 * RETURN: boolean
	 ==========================================================================================================*/
	public boolean isOrderSuccessfullyPlaced() throws InterruptedException{
		if(isOrderConfirmationPage()){
			logger.info("order id of successful order is: "+ getText(ORDER_ID_LOC));
			List<WebElement> items_list = findElements(ITEM_LIST);
			Iterator<WebElement> itr = items_list.iterator();
			while(itr.hasNext()) {
				String Items = itr.next().getText();
			   logger.info("Items ordered: " + Items);	
		    }
			if(isTotalsSectionDisplayCorrectInfoForSanity()){
				logger.info("Order Confirmation section reflects the correct info");
			}
			scrollToExact("Keep shopping");
			waitForElementPresent(KEEP_SHOPPING_BUTTON);
			logger.info("Keep Shopping Button is present at the bottom");
			logger.info("Order is placed successfully");
			return true;
		}
	
		logger.info("order is not placed successfully");
		return false;
}
	/**==========================================================================================================
	 * DESCRIPTION: to verify if total section display correct info on place order page
	 * PARAMETERS: 
	 * RETURN: boolean
	 ==========================================================================================================*/
	public boolean isTotalsSectionDisplayCorrectInfoForSanity() throws InterruptedException{
		scrollPageDown(4);
		if(isElementPresent(MERCHANDISE_SHIPPING_VALUE) && isElementPresent(MERCHANDISE_TOTAL_VALUE)&& isElementPresent(TAX_TOTAL_VALUE) ){
			logger.info("Merchandise SubTotal is: " + getText(MERCHANDISE_TOTAL_VALUE));
			if(isElementPresent(DISCOUNT_VALUE)){
			logger.info("Discount is: " + getText(DISCOUNT_VALUE));
			}
			logger.info("Merchandise Shipping value is: " + getText(MERCHANDISE_SHIPPING_VALUE));
			logger.info("Tax value is: " + getText(TAX_TOTAL_VALUE));
			if(isElementPresent(GIFT_CARD_REDEEMED_VALUE)){
			logger.info("Gift Card Redeemed value is: " + getText(GIFT_CARD_REDEEMED_VALUE));
     		}
			if(isElementPresent(STORE_CREDIT_VALUE)){
				logger.info("Store Credit value is: " + getText(STORE_CREDIT_VALUE));
	     		}
			if(isElementPresent(GIFT_WRAP_TOTAL)){
				logger.info("Gift Wrap value is: " + getText(GIFT_WRAP_TOTAL));
			}
			logger.info("Order Total is: " + getText(ORDER_TOTAL_VALUE));
			scrollToExact("Thank you!");
			if(isElementPresent(BI_INFO_LABEL_LOC)){
				logger.info("The Title of BI section is: " + getText(BI_INFO_LABEL_LOC));
				logger.info("BI info:"+"/n Previous Balance is: " + getText(BI_PREVIOUS_BALANCE));
				logger.info("Points Earned Today is: " + getText(BI_POINTS_EARNED));
				logger.info("Points Redeemed: " + getText(BI_POINTS_REDEEMED));
				logger.info("New beauty bank balance is: " + getText(BI_NEW_BALANCE));
				logger.info("BI points info is displayed correctly");
			}
			return true;
		}
		logger.info("Order Confirmation sections fails to reflect the correct info");
		return false;
	}
}
