package com.sephora.pages.AndroidApp.Search;

import io.appium.java_client.MobileElement;

import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import com.sephora.base.pages.SephoraAndroidAppBasePage;
import com.sephora.core.driver.mobile.SephoraMobileDriver;

public class SearchPage extends SephoraAndroidAppBasePage {

	public SearchPage(SephoraMobileDriver driver) {
		super(driver);
	}
	
	private static final Logger logger = LogManager.getLogger(SearchPage.class.getName());
	public static final By SEARCH_TEXT_INPUT_FIELD = By.id("com.sephora:id/search_src_text");
	public static final By SEARCH_SUGGESTION = By.id("com.sephora:id/suggestion");
	public static final By SEARCH_CLOSE_BUTTON_LOC= By.id("com.sephora:id/search_close_btn");
	
	
	public SearchPage searchKeywordbyIndex(String keyword, int index) throws IOException{
		waitForElementPresent(SEARCH_TEXT_INPUT_FIELD);
		clear(SEARCH_TEXT_INPUT_FIELD);
		type(SEARCH_TEXT_INPUT_FIELD, keyword);
		keyboardOperationSearch();
		List<MobileElement> search_list = findMobileElements(SEARCH_SUGGESTION);
		logger.info("The suggestion is going to be clicked: "+search_list.get(index).getText());
		search_list.get(index).click();
		return new SearchPage(driver);
	}
	
	
	
	public boolean isSearhPage(){
		if(IsLocatorVisible(SEARCH_CLOSE_BUTTON_LOC)){
			logger.info("Search page loaded successfully");
			return true;
		}
		return false;
	}
	
	public SearchPage typeSearchText(String searchText) {
			waitForElementPresent(SEARCH_TEXT_INPUT_FIELD);
			type(SEARCH_TEXT_INPUT_FIELD, searchText);
			logger.info(searchText +" typed in search text");
			return new SearchPage(driver);
	}
	 
	 

}

