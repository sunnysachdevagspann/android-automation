package com.sephora.pages.AndroidApp.Home;

import io.appium.java_client.AppiumDriver;

import org.openqa.selenium.By;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sephora.base.pages.SephoraAndroidAppBasePage;
import com.sephora.core.driver.mobile.SephoraMobileDriver;
import com.sephora.pages.AndroidApp.Basket.BasketPage;
import com.sephora.pages.AndroidApp.Checkout.CheckoutPage;
import com.sephora.pages.AndroidApp.Navigation.LeftNavigationDrawerPage;

public class HomePage extends SephoraAndroidAppBasePage {

	/**
	 * @param driver
	 * 
	 * Constructor for HomePage expects the driver object instance to
	 * instantiate the Page object
	 */
	public HomePage(SephoraMobileDriver driver) {
		super(driver);
	}
	
	private static final Logger logger = LogManager.getLogger(HomePage.class.getName());
	
	
	public static final By SIGNIN_LINK = By.xpath("//android.widget.TextView[contains(@text,'SIGN IN')]");
	public static final By ACCOUNT_TEXT = By.id("com.sephora:id/account_text");
	public static final By SEARCH_BUTTON = By.id("com.sephora:id/header_item_search");
	public static final By BASKET_BUTTON = By.id("com.sephora:id/header_item_basket");
	public static final By HOME_SECTION = By.xpath("//android.widget.TextView[@text='HOME']");
	public static final By CATEGORIES_SECTION = By.xpath("//android.widget.TextView[@text='CATEGORIES']");
	public static final By CATEGORIES_SUB_SECTION_MAKEUP = By.xpath("//android.widget.ImageView[@index='1']");
	public static final By CATEGORIES_SUB_SECTION_SALE = By.xpath("//android.widget.ImageView[@index='10']");
	public static final By LOGOUT_BUTTON = By.id("com.sephora:id/main_logout_button");
	public static final By PPAGE_COLOR_LOC = By.id("com.sephora:id/main_ppage_button_color");
	public static final By PPAGE_SIZE_LOC = By.id("com.sephora:id/main_ppage_button_size");
	public static final By PPAGE_SINGLE_SKU_LOC = By.id("com.sephora:id/main_ppage_single_sku");
	public static final By PPAGE_TYPE_LOC = By.id("com.sephora:id/main_ppage_button_type");
	public static final By PPAGE_GC_LOC = By.id("com.sephora:id/main_ppage_gc");
	public static final By BCC_TC_LOC = By.id("com.sephora:id/main_bcc_tc");
	public static final By BCC_PRIVACY_POLICY_LOC = By.id("com.sephora:id/main_bcc_privacy_policy");
	
	
	BasketPage basketPage = new BasketPage(driver);
	 LeftNavigationDrawerPage leftNavigationDrawerPage = new LeftNavigationDrawerPage(driver);
	 CheckoutPage checkoutPage = new CheckoutPage(driver);
	
	
	 public void WaitforHomePagetoLoad(){
		 waitForElementPresent(SEARCH_BUTTON);
		 waitForElementPresent(BASKET_BUTTON);
		 waitForElementPresent(WINDOW_HEADER);
		 waitForElementPresent(LEFTNAVIGATION_DRAWER);
		 waitForElementPresent(HOME_SECTION);
		 logger.info("User is on Home Page");
		 }
		 	 
	 public HomePage navigateToHomePage(){
		/* int i;
		 int j=4;
		 for(i=1; i<=j; i++){
			 navigateBack();
			 if (checkoutPage.isCheckoutPage()) break;
		 }*/
		 
		 if(basketPage.isBasketPage()){
			 navigateBack();
			 leftNavigationDrawerPage.clickLeftNavigationButton().clickHomeButton().WaitforHomePagetoLoad();
		 }
		 else if(checkoutPage.isCheckoutPage()){
			 navigateBack();
			 if(basketPage.isBasketPage()){
			 navigateBack();
			 }
			 leftNavigationDrawerPage.clickLeftNavigationButton().clickHomeButton().WaitforHomePagetoLoad();
		 }
		 else{
			 leftNavigationDrawerPage.clickLeftNavigationButton().clickHomeButton().WaitforHomePagetoLoad();
		 }
		 return new HomePage(driver);
	 }
	 
		 public void clickSignInLink() {
			 if(IsLocatorEnable(LOGOUT_BUTTON)){
				 click(LOGOUT_BUTTON);
			 }
			waitForElementPresent(SIGNIN_LINK);
				click(SIGNIN_LINK);
				logger.info("Sign In link is clicked");
				
		 }
		 
		 public void clickSearchIcon() {
			waitForElementPresent(SEARCH_BUTTON);
				click(SEARCH_BUTTON);
				logger.info("Search Icon on header is clicked");
		 }
		 
		 public void clickCategoriesSection() {
			 waitForElementPresent(CATEGORIES_SECTION);
				click(CATEGORIES_SECTION);
				logger.info("Categories Section is clicked");
				pauseExecutionFor(4000);
		 }
		 
		 public void clickCategoriesSubSectionMakeup() {
			 waitForElementPresent(CATEGORIES_SUB_SECTION_MAKEUP);
				click(CATEGORIES_SUB_SECTION_MAKEUP);
				logger.info("Categories Sub Section Makeup is clicked");
		 }
		 
		 public void clickCategoriesSubSectionSale() {
				click(CATEGORIES_SUB_SECTION_SALE);
				logger.info("Categories Sub Section Sale is clicked");
		 }
		
		 
		 public boolean isSignInSuccessful(String userName){
			 logger.info("waiting for SIGN IN Link to disappear");
				if(!isElementPresent(SIGNIN_LINK) && isElementPresent(ACCOUNT_TEXT)){
					if(getText(ACCOUNT_TEXT).contains(userName));
					String usrName = getText(ACCOUNT_TEXT).trim().replace("Hello,", "");
					logger.info("UserName is: "+ usrName);
					return true;
				}
				return false;
		}

		 public void clickPPageColor() {
				waitForElementPresent(PPAGE_COLOR_LOC);
					click(PPAGE_COLOR_LOC);
					logger.info("PPage Color button is clicked");
			 }
		 
		 public void clickPPageSize() {
				waitForElementPresent(PPAGE_SIZE_LOC);
					click(PPAGE_SIZE_LOC);
					logger.info("PPage Size button is clicked");
			 }
		 public void clickPPageType() {
				waitForElementPresent(PPAGE_TYPE_LOC);
					click(PPAGE_TYPE_LOC);
					logger.info("PPage Type button is clicked");
			 }
		 public void clickPPageSingleSKU() {
				waitForElementPresent(PPAGE_SINGLE_SKU_LOC);
					click(PPAGE_SINGLE_SKU_LOC);
					logger.info("PPage Single SKU button is clicked");
			 }
		 public void clickPPageGC() {
				waitForElementPresent(PPAGE_GC_LOC);
					click(PPAGE_GC_LOC);
					logger.info("PPage GC button is clicked");
			 }
		 
		 
}
