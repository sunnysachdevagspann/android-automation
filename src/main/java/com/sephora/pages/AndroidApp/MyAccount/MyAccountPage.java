package com.sephora.pages.AndroidApp.MyAccount;

import org.openqa.selenium.By;

import com.sephora.base.pages.SephoraAndroidAppBasePage;
import com.sephora.core.driver.mobile.SephoraMobileDriver;
import com.sephora.pages.AndroidApp.Home.HomePage;

public class MyAccountPage extends SephoraAndroidAppBasePage {

	public MyAccountPage(SephoraMobileDriver driver) {
		super(driver);
	}
	
	public static final By SIGNOUT_BUTTON = By.id("com.sephora:id/my_account_sign_out");
	
	public HomePage clickSignOutButtton(){
		scrollToExact("Sign Out");
		waitForElementPresent(SIGNOUT_BUTTON);
		click(SIGNOUT_BUTTON);
	    logger.debug("SignOut button is clicked");
		return new HomePage(driver);
	}


}
