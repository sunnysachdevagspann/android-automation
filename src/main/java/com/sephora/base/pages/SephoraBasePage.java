package com.sephora.base.pages;

import io.appium.java_client.AppiumDriver;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.sephora.core.driver.mobile.SephoraMobileDriver;

/**
 * SephoraBasePage is the super class for all the page classes
 * 
 * @author AmarnathRayudu 
 * 
 */
public class SephoraBasePage extends SephoraMobileDriver {
	public WebDriver driver;
	private static final Logger logger = LogManager
			.getLogger(SephoraBasePage.class.getName());

	public SephoraBasePage(SephoraMobileDriver driver) {
		super(null);
	}
	
	public WebDriver getWebdriver(){
		return driver;
	}

}
