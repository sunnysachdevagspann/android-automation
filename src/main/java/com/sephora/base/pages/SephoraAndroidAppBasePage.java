package com.sephora.base.pages;

import io.appium.java_client.TouchAction;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import com.sephora.core.controls.ByT;
import com.sephora.core.driver.mobile.SephoraMobileDriver;
import com.sephora.pages.AndroidApp.Basket.BasketPage;
import com.sephora.pages.AndroidApp.Checkout.CheckoutPage;
import com.sephora.pages.AndroidApp.Home.HomePage;
import com.sephora.pages.AndroidApp.MyAccount.MyAccountPage;
import com.sephora.pages.AndroidApp.Navigation.LeftNavigationDrawerPage;
import com.sephora.core.mobile.constants.TestConstantsI;

/**
 * SephoraAndroidAppBasePage is the base page for all Mobile pages and 
 * contains mobile specific reusable components 
 * 
 */
public class SephoraAndroidAppBasePage extends SephoraBasePage implements TestConstantsI {
	public static final By WINDOW_HEADER = By.id("com.sephora:id/toolbar_title");
	protected final Logger logger;
	protected SephoraAndroidAppBasePage container;
	protected By locator;
	protected ByT byTlocator;
	protected String frame;
	protected String name;
	protected MyAccountPage myAccountPage;
	public static final int SCREEN_HEIGHT_5_7INCH = 2450;
	public static final int SCREEN_HEIGHT_4_7INCH = 667;
	public static final By BASKET_BUTTON = By.id("com.sephora:id/header_item_basket");
	public static final By DONE_BUTTON = By.id("com.sephora:id/editor_action_done");
	public static final By LEFTNAVIGATION_DRAWER = By.xpath("//android.view.View/android.widget.ImageButton[@index='0']");
	
	protected static SephoraMobileDriver driver;
	
	public SephoraAndroidAppBasePage(SephoraMobileDriver driver){
		super(driver);
		this.driver=driver; 
		logger = LogManager.getLogger(this.getClass().getName());
	}
	
	/**==========================================================================================================
	 * DESCRIPTION: To wait explicitly for a particular element 
	 * PARAMETERS: By locator, int time
	 * RETURN: 
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/

	public boolean waitExplicitly(By locator, int time) {
		pauseExecutionFor(5000);
		int fg = 1;
		boolean flag = false;
		try {
			turnOffImplicitWaits();
			for(int count=1; count<=time; count++) {
				pauseExecutionFor(1000);
				if(isElementPresent(locator)) {
					if(findElement(locator).isDisplayed()) {
						fg++;
						flag = true;
						if(fg == 6)
							break;
					}
				}
			}
		} finally {
			turnOnImplicitWaits();
		}
		return flag;
	}
	
	/**==========================================================================================================
	 * DESCRIPTION: To get unique email id for registering new user
	 * PARAMETERS: String firstName
	 * RETURN: email
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	
	public String newUser(String firstName){
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("ddMhm");
		String formattedDate = sdf.format(date);
		String email = firstName+"Test"+""+formattedDate+"@yopmail.com";
		System.out.println("NewUser"+formattedDate);
		return email;
		
	}
	
	/**==========================================================================================================
	 * DESCRIPTION: To press Done button on soft keyboard (OEM Keyboard)
	 * PARAMETERS: 
	 * RETURN: 
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	
	public void keyboardOperationDone() throws IOException{
		String command = "adb -s input keyevent KEYCODE_ENTER"; 
		Process result = Runtime.getRuntime().exec(command);
	}
	
	/**==========================================================================================================
	 * DESCRIPTION: To press Search button on soft keyboard (OEM Keyboard)
	 * PARAMETERS: --
	 * RETURN: 
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	
	public void keyboardOperationSearch() throws IOException{
		String command = "adb -s input keyevent KEYCODE_SEARCH"; 
		Process result = Runtime.getRuntime().exec(command);
	}
	
	/**
	 * click by coordinates
	 * @param x - x coordinate
	 * @param y - y coordinate
	 */
	public void clickByCoordinates(Double x, Double y) {
		Integer height = driver.manage().window().getSize().getHeight();
		double coefficient = height.equals(SCREEN_HEIGHT_5_7INCH) ? 1.29 : height.equals(SCREEN_HEIGHT_4_7INCH) ? 1.2 : 1;
		clickByCoordinates(x, y, coefficient);
	}
	
	/**
	 * click by coordinates
	 * @param x - x coordinate
	 * @param y - y coordinate
	 * @param coefficient - coefficient depending on IOS/device version
	 */
	public void clickByCoordinates(Double x, Double y, Double coefficient) {
		try {
			TouchAction tap = new TouchAction(driver);
			tap.press(new Double(x * coefficient).intValue(), new Double(y * coefficient).intValue()).release().perform();
			logger.debug("Element was taped in x:'" + x * coefficient + "'; y:'" + y * coefficient + "'");
		} catch (Exception e) {
			logger.error("Element was taped in x:'" + x + "'; y:'" + y + "' -> ERROR!");
			
		}
		
	}
	/**==========================================================================================================
	 * DESCRIPTION: To press device back button
	 * PARAMETERS: -- 
	 * RETURN: 
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	
	public void pressDeviceBackButton(){
		navigateBack();
	}
	
	/**==========================================================================================================
	 * DESCRIPTION: To Click Basket button on Header
	 * PARAMETERS: -- 
	 * RETURN: BasketPage
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	
	public BasketPage clickBasketButton() {
	waitForElementPresent(BASKET_BUTTON);
		click(BASKET_BUTTON);
			logger.info("Basket Button is clicked");
			return new BasketPage(driver);
	 }
	
	/**==========================================================================================================
	 * DESCRIPTION:To click leftNavigation drawer (hamburger menu) 
	 * PARAMETERS: -- 
	 * RETURN: LeftNavigationDrawerPage
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	
	public LeftNavigationDrawerPage clickLeftNavigationButton() {
		waitForElementPresent(LEFTNAVIGATION_DRAWER);
		click(LEFTNAVIGATION_DRAWER);
			logger.info("Left Navigation Button is clicked");
			return new LeftNavigationDrawerPage(driver);
	 }
	
	/**==========================================================================================================
	 * DESCRIPTION:To Click Done button on Header
	 * PARAMETERS: 
	 * RETURN:
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	
	public void clickDoneButton() {
		scrollToExact("DONE");
		waitForElementPresent(DONE_BUTTON);
		click(DONE_BUTTON);
		logger.info("click on 'Done' button on Header successful");
		pauseExecutionFor(5000);
	}

	/**==========================================================================================================
	 * DESCRIPTION:To Scroll Page down n number of times 
	 * PARAMETERS: -- int num
	 * RETURN:
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	public void scrollPageDown( int num) {
		 Dimension dimensions = driver.manage().window().getSize();
		 Double screenHeightStart = dimensions.getHeight() * 0.5;
		 int scrollStart = screenHeightStart.intValue();
		 Double screenHeightEnd = dimensions.getHeight() * 0.1;
		 int scrollEnd = screenHeightEnd.intValue();
		 
		 for (int i=0;i<num;i++){
		 driver.swipe(0,scrollStart,0,scrollEnd,1500);
		 try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 }
	}
	
	/**==========================================================================================================
	 * DESCRIPTION:To Scroll Page down specifically for basket page n number of times 
	 * PARAMETERS: -- int num
	 * RETURN:
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	public void scrollPageDownForBasket( int num){
		 Dimension dimensions = driver.manage().window().getSize();
		 Double screenHeightStart = dimensions.getHeight() * 0.5;
		 int scrollStart = screenHeightStart.intValue();
		 Double screenHeightEnd = dimensions.getHeight() * 0.11;
		 int scrollEnd = screenHeightEnd.intValue();
		 
		 for (int i=0;i<num;i++){
		 driver.swipe(0,scrollStart,0,scrollEnd,5000);
		 try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 }
	}
	
	/**==========================================================================================================
	 * DESCRIPTION:To Scroll Page down specifically for Checkout page n number of times 
	 * PARAMETERS: -- int num
	 * RETURN:
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	
	public void scrollPageDownForCheckout( int num) {
		 Dimension dimensions = driver.manage().window().getSize();
		 Double screenHeightStart = dimensions.getHeight() * 0.5;
		 int scrollStart = screenHeightStart.intValue();
		 Double screenHeightEnd = dimensions.getHeight() * 0.2;
		 int scrollEnd = screenHeightEnd.intValue();
		 
		 for (int i=0;i<num;i++){
		 driver.swipe(0,scrollStart,0,scrollEnd,5000);
		 try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 }
	}
	/**==========================================================================================================
	 * DESCRIPTION:To Scroll Page up n number of times 
	 * PARAMETERS: -- int num
	 * RETURN:
	 *------------------------------------------------------------
	 * REVISION HISTORY:
	 * DATE:            Author:          REASON:
	 * 07-04-2016                        Orginal
	        ==========================================================================================================*/
	public void scrollPageUP(int num){
		 Dimension dimensions = driver.manage().window().getSize();
		 Double screenHeightStart = dimensions.getHeight() * 0.5;
		 int scrollStart = screenHeightStart.intValue();
		 Double screenHeightEnd = dimensions.getHeight() * 0.9;
		 int scrollEnd = screenHeightEnd.intValue();
		 
		 for (int i=0;i<num;i++){
		 driver.swipe(0,scrollStart,0,scrollEnd,5000);
		 try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 }
	}
	
	
}
