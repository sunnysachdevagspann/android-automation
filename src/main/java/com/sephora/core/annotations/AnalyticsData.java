package com.sephora.core.annotations;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ METHOD, TYPE })
public @interface AnalyticsData {

	public String PRODUCT_ID() default "";
	public String PRODUCT_NAME() default "";
}
