package com.sephora.core.listeners;

import java.lang.reflect.Method;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.ITestListener;
import org.testng.ITestNGMethod;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;
import com.sephora.core.annotations.TestRailID;
import com.sephora.core.utils.TestRail;

/**
 * TestListner is the listener class that listens to test
 * case execution and allows Engineer to complete post test operations.
 * 
 * @author AmarnathRayudu 
 *
 */
public class TestListner extends TestListenerAdapter implements ITestListener {
	private static final Logger logger = LogManager.getLogger(TestListner.class.getName());
	private TestRail testRail = new TestRail();
	String[] testIds;
	String filePath = "E:\\SCREENSHOTS";
	WebDriver driver=null;

	private String[] qmetryID(ITestResult tr) {
		ITestNGMethod testNGMethod = tr.getMethod();
		Method method = testNGMethod.getConstructorOrMethod().getMethod();
		testNGMethod.getDescription();
		TestRailID testAnnotation = (TestRailID) method.getAnnotation(TestRailID.class);
		if (testAnnotation != null)
			return testAnnotation.id();
		else
			return null;
	}

	@Override
	public void onTestStart(ITestResult tr) {
		// testIds = qmetryID(tr);
		testIds = qmetryID(tr);
		for (String test : testIds)
			logger.info("[TC-" + test + " ------- Executing " + tr.getMethod().getMethodName() + "]" + "<br>DESCRIPTION :"
					+ tr.getMethod().getDescription());
	}

	@Override
	public void onTestSuccess(ITestResult tr) {
		testIds = qmetryID(tr);
		testIds = qmetryID(tr);
		for (String test : testIds) {
			logger.info("[TEST IS SUCCESSFUL -------- Test case " + test + " passed]");
			try {
				testRail.testRail(1, test.replace("C", ""));
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onTestFailure(ITestResult tr) {
		testIds = qmetryID(tr);
		Throwable cause = tr.getThrowable();
		for (String test : testIds) {
			try {
//				testRail.testRail(5, test.replace("C", ""));
			} catch (Throwable e) {
				e.printStackTrace();
			}
			if (null != cause) {
				logger.info("[TEST FAILED -------- Test case " + test + " failed " + cause.getMessage() + "]");
			} else
				logger.info("[TEST FAILED -------- Test case " + test + " failed]");
		}
	}

	@Override
	public void onTestSkipped(ITestResult tr) {
		testIds = qmetryID(tr);
		for (String test : testIds)
			logger.info("[TEST IS SKIPPED -------- Test case " + test + " skipped]");
	}
	    
	   
}
