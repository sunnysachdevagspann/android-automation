package com.sephora.core.utils;
//package com.sephora.core;
//
//import org.openqa.selenium.By;
//import org.openqa.selenium.Keys;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
//import org.openqa.selenium.interactions.Actions;
//import org.openqa.selenium.support.pagefactory.ByChained;
//import org.openqa.selenium.support.ui.ExpectedConditions;
//import org.openqa.selenium.support.ui.Select;
//import org.openqa.selenium.support.ui.WebDriverWait;
//import org.testng.Assert;
//
///**
// * The class allows configuring the web element at any point, regardless of the
// * element existing on the page. The locator is saved, and the web element will
// * be searched during runtime each time that it is used. Additionally, the class
// * provides utility functions for the web element.
// * 
// * @author GSPANN
// * 
// */
//public class UIElement {
//
//	protected WebDriver driver = WebDriverFactory.getWebDriver();
//	protected WebDriverWait wait = WebDriverFactory.getWebDriverWait();
//	protected Actions actions = WebDriverFactory.getWebDriverActions();
//	protected By by;
//	protected String description;
//
//	// protected boolean isTranslated;
//
//	/**
//	 * Class constructor The description is not specified, so it will be set
//	 * with the locator string
//	 * 
//	 * @param by
//	 *            the locator of the web element
//	 */
//	public UIElement(By by) {
//		this(by, by.toString());
//	}
//
//	public UIElement(By by, String description) {
//		this.by = by;
//		this.description = description;
//	}
//
//	// public UIElement(By by, boolean isTranslated, String description) {
//	// this.by = by;
//	// this.description = description;
//	// this.isTranslated = isTranslated;
//	// }
//
//	public By getLocator() {
//		return by;
//	}
//
//	public String getDescription() {
//		// TODO Auto-generated method stub
//		return description;
//	}
//
//	public String getSelector() {
//		// TODO: method doesn't match all cases
//		String selector = "";
//		String strLocator = by.toString();
//		String[] bys = strLocator.replace("By.chained({", "").replace("})", "")
//				.split(",");
//		boolean flag = false;
//		int index;
//		for (String by : bys) {
//			by = by.replace("By.", "");
//			if (by.startsWith("id")) {
//				selector += "#";
//			} else if (by.startsWith("class")) {
//				selector += ".";
//			} else if (by.startsWith("name")) {
//				selector += "[name=";
//				flag = true;
//			} else if (by.startsWith("xpath")) {
//				return null;
//			}
//			index = by.indexOf(": ");
//			selector += by.substring(index + 2);
//			if (flag) {
//				selector += "]";
//			}
//			selector += " ";
//		}
//		return selector;
//	}
//
//	/**
//	 * Gets the failure message. Adds to the Throwable exception the description
//	 * of the failed element.
//	 * 
//	 * @param e
//	 *            the exception that was caught
//	 * @return String - the failure message
//	 */
//	protected String getFailureMsg(Throwable e) {
//		String method = new RuntimeException().getStackTrace()[1]
//				.getMethodName();
//		return method + " failed for \"" + description + "\":\n" + e;
//	}
//
//	public void scrollToElement(WebElement we) {
//		WebDriverFactory.getJSExecutor().executeScript(
//				"arguments[0].scrollIntoView(true);", we);
//	}
//
//	public void scrollToElement() {
//		scrollToElement(getElement());
//	}
//
//	public void scrollToElementIfFireFox(WebElement we) {
//		if (Properties.browserType.equals("firefox") && we != null) {
//			scrollToElement();
//		}
//	}
//
//	public void scrollToElementIfIEOrFF(WebElement we) {
//		if ((Properties.browserType.equals("ie") && we != null)
//				|| (Properties.browserType.equals("firefox") && we != null)) {
//			scrollToElement();
//		}
//	}
//
//	// /**
//	// * click enter to focus before clicking on element - IE
//	// */
//	// public void clickEnterIfIE() {
//	// if (Properties.browserType.equals("ie") && getElement() != null) {
//	// getElement().sendKeys("/n");
//	// }
//	// }
//
//	/**
//	 * Finds the web element according to the locator that was set by the
//	 * constructor.
//	 * 
//	 * @return the web element.
//	 */
//	public WebElement getElement() {
//		try {
//			return driver.findElement(by);
//		} catch (Throwable e) {
//			Assert.fail(getFailureMsg(e));
//			return null;
//		}
//	}
//
//	/**
//	 * Finds the web element according to the locator that was set by the
//	 * constructor.
//	 * 
//	 * @return the web element.
//	 */
//	// public WebElement getElementNoFail() {
//	// // int currTimeOut = WebDriverFactory.setTimeoutSession(1);
//	// WebElement we = driver.findElement(by);
//	// // WebDriverFactory.setTimeoutSession(currTimeOut);
//	// return we;
//	// }
//
//	/**
//	 * Tries to find the web element, and check if it exists.
//	 * 
//	 * @return true if the web element exists, false otherwise.
//	 */
//	public boolean isExist() {
//		try {
//			// getElementNoFail();
//			getElement();
//			return true;
//		} catch (Throwable e) {
//			return false;
//		}
//	}
//
//	/**
//	 * Finds the web element & clicks on it.
//	 */
//	public void click() {
//		UIElement platformBusy = new UIElement(
//				By.id("capbDynamicContentLoaderContainer"));
//		try {
//			scrollToElementIfFireFox(getElement());
//			wait.until(ExpectedConditions.elementToBeClickable(by)).click();
//		} catch (Throwable e) {
//			if (e.getMessage().contains("Element is not clickable")) {
//				try {
//					platformBusy.waitForDisplayed(false);
//					clickByPosition(5, 5);
//					if (ExpectedConditions.elementToBeClickable(by).apply(
//							driver) != null) {
//						getElement().click();
//					}
//				} catch (Throwable e2) {
//					Assert.fail(getFailureMsg(e2));
//				}
//			} else {
//				Assert.fail(getFailureMsg(e));
//			}
//		}
//	}
//
//	/**
//	 * Finds the web element & performs the js click event.
//	 */
//	public void jsClick() {
//		try {
//			WebDriverFactory.executeJS("arguments[0].click();", getElement());
//		} catch (Throwable e) {
//			Assert.fail(getFailureMsg(e));
//		}
//	}
//
//	/**
//	 * Sets a new value in the specified web element. (clears the previous
//	 * value)
//	 */
//	protected void setVal(WebElement we, CharSequence val) {
//		if (val != null) {
//			we.clear();
//			we.clear();
//			we.sendKeys(Keys.chord(Keys.CONTROL, "a") + val);
//		}
//	}
//
//	/**
//	 * Finds the web element & sets a new value in it. (clears the previous
//	 * value)
//	 * 
//	 * @param val
//	 *            the new value to set
//	 */
//	public void setVal(CharSequence val) {
//		if (val != null) {
//			waitForEnabled(true);
//			WebElement we = getElement();
//			try {
//				setVal(we, val);
//			} catch (Throwable e) {
//				Assert.fail(getFailureMsg(e));
//			}
//		}
//	}
//
//	/**
//	 * Finds the web element & returns its displayed text. The function is
//	 * relevant for web elements that are NOT input fields.
//	 * 
//	 * @return the displayed text of the web element
//	 */
//	public String getText() {
//		try {
//			WebElement we = getElement();
//			scrollToElement(we);
//			return we.getText();
//		} catch (Throwable e) {
//			Assert.fail(getFailureMsg(e));
//			return null;
//		}
//	}
//
//	/**
//	 * Finds the web element & returns its input value. The function is relevant
//	 * only for input fields.
//	 * 
//	 * @return the input value of the web element
//	 */
//	public String getInputVal() {
//		try {
//			return getElement().getAttribute("value");
//		} catch (Throwable e) {
//			Assert.fail(getFailureMsg(e));
//			return null;
//		}
//	}
//
//	/**
//	 * Finds the web element & clicks on it in the given position. The
//	 * coordinates are relative to the web element.
//	 * 
//	 * @param x
//	 *            the x-coordinate, measured in pixels
//	 * @param y
//	 *            the y-coordinate, measured in pixels
//	 */
//	public void clickByPosition(int x, int y) {
//		WebElement we = getElement();
//		try {
//			actions.moveToElement(we, x, y).click().perform();
//		} catch (Throwable e) {
//			Assert.fail(getFailureMsg(e));
//		}
//	}
//
//	/**
//	 * Finds the parent of the web element.
//	 * 
//	 * @return the parent web element
//	 */
//	public WebElement getParent() {
//		return getElement().findElement(By.xpath("..")); // TODO - need to try
//	}
//
//	public void waitForText(String value) {
//		if (value != null)
//			wait.until(ExpectedConditions.textToBePresentInElementValue(by,
//					value));
//	}
//
//	public void waitForAttribute(final String attribute, final String value,
//			final boolean isExist) {
//		wait.until(new SephoraExpectedCondition() {
//			public Boolean apply(WebDriver wd) {
//				try {
//					return getElement().getAttribute(attribute).contains(value) == isExist;
//				} catch (Throwable e) {
//					String isFound = isExist ? "not " : "";
//					setMessage("\nValue " + value + " was " + isFound
//							+ "found for Attribute " + attribute + " in "
//							+ description + ".");
//					return false;
//				}
//			}
//		});
//	}
//
//	public void waitForEnabled(final boolean isEnabled) {
//		wait.until(new SephoraExpectedCondition() {
//
//			public Boolean apply(WebDriver wd) {
//				try {
//					return getElement().isEnabled() == isEnabled;
//				} catch (Throwable e) {
//					setMessage(e);
//					return false;
//				}
//			}
//		});
//	}
//
//	public void waitForDisplayed(final boolean isDisplayed) {
//		wait.until(new SephoraExpectedCondition() {
//
//			public Boolean apply(WebDriver wd) {
//				try {
//					return getElement().isDisplayed() == isDisplayed;
//				} catch (Throwable e) {
//					setMessage(e);
//					return false;
//				}
//			}
//		});
//	}
//
//	/**
//	 * Converts the UIElement to a Select element (a selenium object). Relevant
//	 * for input web elements that are selectable. (e.g. combobox)
//	 * 
//	 * @return the Select object of the web element
//	 */
//	public Select toSelect() {
//		try {
//			return new Select(getElement());
//		} catch (Throwable e) {
//			Assert.fail(getFailureMsg(e));
//			return null;
//		}
//	}
//
//	public void selectByVisibleText(String val) {
//		if (val != null) {
//			toSelect().selectByVisibleText(val.toString());
//		}
//	}
//
//	public UIElementList getSelectOptions() {
//		return new UIElementList(new ByChained(by, By.tagName("option")));
//	}
//
//	/**
//	 * Verifies that the displayed text of the element is as expected. <br>
//	 * If the expected value is NULL, no verification will take place. <br>
//	 * If the expected value is an empty string, it verifies that the behavior
//	 * is as one of the 2: <br>
//	 * * The web element doesn't exist OR <br>
//	 * * The value of the web element is empty.
//	 * 
//	 * @param expectedVal
//	 *            The value that is expected in the displayed text of the web
//	 *            element.
//	 */
//	public void verifyText(String expectedVal) {
//		if (expectedVal == null) {
//			return;
//		}
//		if (!expectedVal.equals("")) {
//			Assert.assertEquals(getText(), expectedVal, description);
//		} else {
//			Assert.assertTrue(!isExist() || getText().equals(""), description);
//		}
//	}
//
//	/**
//	 * Verifies that the attribute of the element is as expected.
//	 * 
//	 * @param attribute
//	 * @param value
//	 * @param expectedExists
//	 */
//	public void verifyAttribute(String attribute, String value,
//			boolean expectedExists) {
//		String isFound = expectedExists ? "not " : "";
//		Assert.assertEquals(getElement().getAttribute(attribute)
//				.contains(value), expectedExists, "Value " + value + " was "
//				+ isFound + "found for Attribute " + attribute + " in "
//				+ description);
//	}
//
//	public void verifyInputValue(String expectedVal) {
//		if (expectedVal != null) {
//			Assert.assertEquals(getInputVal(), expectedVal, description);
//		}
//	}
//
//	public void verifyInputLength(Integer length) {
//		if (length != null) {
//			Assert.assertEquals(getInputVal().length(), length.intValue(),
//					"Wrong length in field: " + description + "\n");
//		}
//	}
//
//	public void verifyDisplayed(Boolean isDisplayed) {
//		if (isDisplayed != null) {
//			Assert.assertEquals(getElement().isDisplayed(),
//					isDisplayed.booleanValue(), description);
//		}
//	}
//
//	public void verifyEnabled(Boolean isEnabled) {
//		if (isEnabled != null) {
//			Assert.assertEquals(getElement().isEnabled(),
//					isEnabled.booleanValue(), description);
//		}
//	}
//
//	public void verifySelectedValue(String expectedValue) {
//		if (expectedValue != null) {
//			Assert.assertEquals(toSelect().getFirstSelectedOption().getText(),
//					expectedValue, description);
//		}
//	}
//
//	public void verifySelectOptions(String... expectedValues) {
//		UIElementList options = new UIElementList(new ByChained(by,
//				By.tagName("option")));
//		options.verifyAllTextValues(expectedValues);
//	}
//
//	public void doubleClick() {
//		actions.doubleClick(getElement()).perform();
//	}
//
//	public void dragAndDrop(int xClick, int yClick, int xOffset, int yOffset) {
//		actions.moveToElement(getElement(), xClick, yClick).clickAndHold()
//				.moveByOffset(xOffset, yOffset).release().perform();
//	}
//}
