package com.sephora.core.utils;
//package com.sephora.core;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import org.apache.commons.lang3.ArrayUtils;
//import org.openqa.selenium.By;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
//import org.openqa.selenium.support.ui.ExpectedCondition;
//import org.testng.Assert;
//
///**
// * The class allows configuring a list of web elements at any point, regardless
// * of the elements existing on the page. The locator is saved, and the list of
// * web elements will be searched during runtime each time that it is used. The
// * class is useful especially for dynamic lists. Additionally, the class
// * provides utility functions for the web element list. <br>
// * The class extends the UIElement class. Functions that are called from the
// * parent class will refer to the first element in the list.
// * 
// * @author GSPANN
// * 
// */
//public class UIElementList extends UIElement {
//
//	/**
//	 * Class constructor
//	 * 
//	 * @param by
//	 *            the locator of the web element
//	 */
//	public UIElementList(By by) {
//		super(by);
//	}
//
//	/**
//	 * Class constructor
//	 * 
//	 * @param by
//	 * @param description
//	 */
//	public UIElementList(By by, String description) {
//		super(by, description);
//	}
//
//	/**
//	 * Gets the failure message. Adds to the Throwable exception the description
//	 * of the failed element.
//	 * 
//	 * @param e
//	 *            the exception that was caught
//	 * @param index
//	 *            the index of the failed element
//	 * @return String - the failure message
//	 */
//	protected String getFailureMsg(int index, Throwable e) {
//		String method = new RuntimeException().getStackTrace()[1]
//				.getMethodName();
//		return method + " failed for \"" + description + "\" with index: "
//				+ index + ":\n" + e;
//	}
//
//	/**
//	 * Gets the failure message. Adds to the Throwable exception the description
//	 * of the failed element.
//	 * 
//	 * @param e
//	 *            the exception that was caught
//	 * @param val
//	 *            the value of the failed element
//	 * @return String - the failure message
//	 */
//	protected String getFailureMsg(String val, Throwable e) {
//		String method = new RuntimeException().getStackTrace()[1]
//				.getMethodName();
//		return method + " failed for \"" + description + "\" with value: "
//				+ val + ":\n" + e;
//	}
//
//	/**
//	 * Finds the list of web elements according to the locator that was set by
//	 * the constructor.
//	 * 
//	 * @return List<WebElement>
//	 */
//	public List<WebElement> getElements() {
//		return driver.findElements(by);
//	}
//
//	/**
//	 * Finds the list of web elements, and returns the element at the specified
//	 * position in this list.
//	 * 
//	 * @param index
//	 *            index of the element to return.
//	 * @return the element at the specified position in this list.
//	 */
//	public WebElement getElement(int index) {
//		List<WebElement> elements = getElements();
//		return elements.get(index);
//	}
//
//	public void scrollToElement(int index) {
//		scrollToElement(getElement(index));
//	}
//
//	/**
//	 * Finds the list of web elements, and returns the index of the element that
//	 * its displayed text is as specified. The function is relevant for web
//	 * elements that are NOT input fields.
//	 * 
//	 * @param val
//	 *            the displayed text to search for
//	 * @return the index of the element that its displayed text is as specified,
//	 *         or -1 if none of the web elements text match the specified.
//	 */
//	public int getIndexByVal(String val) {
//		List<WebElement> elements = getElements();
//		for (int i = 0; i < elements.size(); i++) {
//			scrollToElementIfIEOrFF(elements.get(i));
//			if (elements.get(i).getText().equals(val)) {
//				return i;
//			}
//		}
//		Assert.fail("Value: " + val + " doesn't exists in:\n" + description);
//		return -1;
//	}
//
//	public Integer[] getIndexesByValues(String... val0) {
//		List<Integer> indexes = new ArrayList<Integer>();
//		int listSize = getElements().size();
//		for (int i = 0; i < listSize; i++) {
//			if (ArrayUtils.contains(val0, getText(i))) {
//				indexes.add(i);
//			}
//		}
//		return indexes.toArray(new Integer[indexes.size()]);
//	}
//
//	/**
//	 * Finds the list of web elements, and returns the index of the element that
//	 * its input value is as specified. The function is relevant only for input
//	 * fields.
//	 * 
//	 * @param val
//	 *            the input value to search for
//	 * @return the index of the element that its input value is as specified, or
//	 *         -1 if none of the web elements input values match the specified.
//	 */
//	public int getIndexByInputVal(String val) {
//		List<WebElement> elements = getElements();
//		for (int i = 0; i < elements.size(); i++) {
//			if (elements.get(i).getAttribute("value").equals(val)) {
//				return i;
//			}
//		}
//		Assert.fail("Input value: " + val + "doesn't exists in:\n"
//				+ description);
//		return -1;
//	}
//
//	/**
//	 * Finds the list of web elements, and returns the web element that its
//	 * displayed text is as specified. The function is relevant for web elements
//	 * that are NOT input fields.
//	 * 
//	 * @param val
//	 *            the displayed text to search for
//	 * @return the web element that its displayed text is as specified, or null
//	 *         if none of the web elements text match the specified.
//	 */
//	public WebElement getElementByVal(String val) {
//		List<WebElement> elements = getElements();
//		for (int i = 0; i < elements.size(); i++) {
//			scrollToElementIfFireFox(elements.get(i));
//			if (elements.get(i).getText().equals(val)) {
//				return elements.get(i);
//			}
//		}
//		Assert.fail("Value: " + val + "\ndoesn't exists in: " + description);
//		return null;
//	}
//
//	/**
//	 * Finds the list of web elements, and returns web element that its input
//	 * value is as specified. The function is relevant only for input fields.
//	 * 
//	 * @param val
//	 *            the input value to search for
//	 * @return the web element that its input value is as specified, or null if
//	 *         none of the web elements input values match the specified.
//	 */
//	public WebElement getElementByInputVal(String val) {
//		return getElementByAttribue("value", val);
//	}
//
//	/**
//	 * Finds the list of web elements, and returns web element that its
//	 * attribute value is as specified.
//	 * 
//	 * @param attribute
//	 * @param val
//	 *            the input value to search for
//	 * @return the web element that its input value is as specified, or null if
//	 *         none of the web elements input values match the specified.
//	 */
//	public WebElement getElementByAttribue(String attribute, String val) {
//		List<WebElement> list = getElements();
//		for (WebElement we : list) {
//			if (attribute.equals("text")) {
//				scrollToElementIfFireFox(we);
//			}
//			String attributeVal = we.getAttribute(attribute);
//			if (attributeVal != null && attributeVal.equals(val)) {
//				return we;
//			}
//		}
//		Assert.fail("No " + description + " exists with attribute: "
//				+ attribute + ", and value: " + val);
//		return null;
//	}
//
//	public List<WebElement> getElementsByAttribue(String attribute, String val) {
//		List<WebElement> list = getElements();
//		List<WebElement> matchList = new ArrayList<WebElement>();
//		for (WebElement we : list) {
//			if (we.getAttribute(attribute).contains(val)) {
//				matchList.add(we);
//			}
//		}
//		Assert.fail("No " + description + " exists with attribute: "
//				+ attribute + ", and value: " + val);
//		return matchList;
//	}
//
//	public int getIndexByAttribue(String attribute, String val, boolean isExist) {
//		List<WebElement> list = getElements();
//		for (int i = 0; i < list.size(); i++) {
//			if (list.get(i).getAttribute(attribute).contains(val) == isExist) {
//				return i;
//			}
//		}
//		Assert.fail("No " + description + " exists with attribute: "
//				+ attribute + ", and value: " + val);
//		return -1;
//	}
//
//	/**
//	 * Finds the list of web elements, and clicks the web element at the
//	 * specified position in this list.
//	 * 
//	 * @param index
//	 *            index of the element to click.
//	 */
//	public void click(final int index) {
//		wait.until(new ExpectedCondition<Boolean>() {
//			Throwable _e;
//
//			public Boolean apply(WebDriver arg0) {
//				try {
//					getElement(index).click();
//					return true;
//				} catch (Throwable e) {
//					_e = e;
//					return false;
//				}
//			}
//
//			@Override
//			public String toString() {
//				return description + "\n" + _e;
//			}
//		});
//	}
//
//	/**
//	 * Finds the list of web elements, and clicks the web element at the
//	 * specified position in this list.
//	 * 
//	 * @param index
//	 *            index of the element to click.
//	 */
//	public void clickByPosition(int index, int x, int y) {
//		WebElement we = getElement(index);
//		try {
//			scrollToElement(we);
//			actions.moveToElement(we, x, y).click().perform();
//		} catch (Exception e) {
//			Assert.fail(getFailureMsg(index, e));
//		}
//	}
//
//	/**
//	 * Finds the list of web elements, and performs the js click event on the
//	 * web element with the specified position in this list.
//	 * 
//	 * @param index
//	 */
//	public void jsClick(int index) {
//		try {
//			WebDriverFactory.executeJS("arguments[0].click();",
//					getElement(index));
//		} catch (Exception e) {
//			Assert.fail(getFailureMsg(index, e));
//		}
//	}
//
//	/**
//	 * Finds the list of web elements, and clicks the web element witch has
//	 * displayed text as specified .
//	 * 
//	 * @param val
//	 *            the displayed text of the web element to click.
//	 */
//	public void clickByVal(final String val) {
//		wait.until(new ExpectedCondition<Boolean>() {
//			Throwable _e;
//
//			public Boolean apply(WebDriver arg0) {
//				try {
//					getElementByVal(val).click();
//					return true;
//				} catch (Throwable e) {
//					_e = e;
//					return false;
//				}
//			}
//
//			@Override
//			public String toString() {
//				return description + "\n" + _e;
//			}
//		});
//	}
//
//	/**
//	 * Finds the list of web elements, and set a new value in the web element at
//	 * the specified position in this list. (clears the previous value)
//	 * 
//	 * @param index
//	 *            index of the element to set the value.
//	 * @param val
//	 *            the new value to set.
//	 */
//	public void setVal(int index, CharSequence val) {
//		WebElement we = getElement(index);
//		try {
//			super.setVal(we, val);
//		} catch (Exception e) {
//			Assert.fail(getFailureMsg(index, e));
//		}
//	}
//
//	/**
//	 * Finds the list of web elements, and returns the displayed text of the web
//	 * element at the specified position in this list.
//	 * 
//	 * @param index
//	 *            index of the element to set the value.
//	 * @return the displayed text of the web element at the specified position
//	 *         in this list.
//	 */
//	public String getText(int index) {
//		try {
//			WebElement we = getElement(index);
//			scrollToElementIfFireFox(we);
//			return we.getText();
//		} catch (Exception e) {
//			Assert.fail(getFailureMsg(index, e));
//			return null;
//		}
//	}
//
//	/**
//	 * Finds the list of web elements, and returns the input value of the web
//	 * element at the specified position in this list. The function is relevant
//	 * only for input fields.
//	 * 
//	 * @return the input value of the web element
//	 */
//	public String getInputVal(int index) {
//		try {
//			return getElement(index).getAttribute("value");
//		} catch (Exception e) {
//			Assert.fail(getFailureMsg(index, e));
//			return null;
//		}
//	}
//
//	/**
//	 * Finds the list of web elements, and returns a list of the displayed text
//	 * of all the web elements in the list.
//	 * 
//	 * @return the list of the displayed text of the web elements list.
//	 */
//	public List<String> getWebElementsText() {
//		List<String> values = new ArrayList<String>();
//		List<WebElement> elements = getElements();
//		for (WebElement we : elements) {
//			scrollToElementIfFireFox(we);
//			values.add(we.getText());
//		}
//		return values;
//	}
//
//	/**
//	 * Finds the list of web elements, and returns a list of the input values of
//	 * all the web elements in the list.
//	 * 
//	 * @return the list of the input values of the web elements list.
//	 */
//	public List<String> getWebElementsInputVal() {
//		List<WebElement> list = getElements();
//		List<String> values = new ArrayList<String>();
//		for (WebElement we : list) {
//			values.add(we.getAttribute("value"));
//		}
//		return values;
//	}
//
//	/**
//	 * Drags a specified element from the list to a
//	 * 
//	 * @param idx1
//	 * @param idx2
//	 */
//	public void dragAndDrop(int idx1, int idx2) {
//		try {
//			actions.dragAndDrop(getElement(idx1), getElement(idx2)).perform();
//		} catch (Exception e) {
//			Assert.fail("dragAndDrop failed for dragging index: " + idx1
//					+ " to index: " + idx2 + "\n" + e);
//		}
//	}
//
//	public void dragAndDropby(int sourceIndx, int xOffset, int yOffset) {
//		try {
//			actions.dragAndDropBy(getElement(sourceIndx), xOffset, yOffset)
//					.perform();
//		} catch (Exception e) {
//			Assert.fail("dragAndDropBy failed for dragging element: "
//					+ getElement(sourceIndx).getText() + e);
//		}
//	}
//
//	public void waitForList() {
//		wait.until(new ExpectedCondition<Boolean>() {
//			public Boolean apply(WebDriver arg0) {
//				return getElements().size() > 0;
//			}
//		});
//
//	}
//
//	public void waitForListSize(final int size) {
//		wait.until(new ExpectedCondition<Boolean>() {
//			public Boolean apply(WebDriver arg0) {
//				return getElements().size() == size;
//			}
//		});
//
//	}
//
//	public void waitForElementWithVal(final String expectedVal,
//			final boolean isExist) {
//		wait.until(new SephoraExpectedCondition() {
//			public Boolean apply(WebDriver arg0) {
//				List<WebElement> elements = getElements();
//				for (WebElement we : elements) {
//					try {
//						scrollToElementIfFireFox(we);
//						if (we.getText().equals(expectedVal)) {
//							return isExist;
//						}
//					} catch (Throwable e) {
//						return false;
//					}
//				}
//				return !isExist;
//			}
//		});
//	}
//
//	public void waitForEnabled(final int index, final boolean isEnabled) {
//		wait.until(new ExpectedCondition<Boolean>() {
//			public Boolean apply(WebDriver wd) {
//				try {
//					return getElement(index).isEnabled() == isEnabled;
//				} catch (Throwable e) {
//					return false;
//				}
//			}
//		});
//	}
//
//	/**
//	 * Verifies that the displayed text of the element at the specified position
//	 * is as expected.
//	 * 
//	 * @param index
//	 *            index of the element to set the value.
//	 * @param expectedVal
//	 *            the value that is expected in the displayed text of the web
//	 *            element.
//	 */
//	public void verifyText(int index, String expectedVal) {
//		if (expectedVal != null) {
//			Assert.assertEquals(getText(index), expectedVal, description);
//		}
//	}
//
//	public void verifyTexts(Integer[] indexs, String[] expectedVals) {
//		if (expectedVals != null) {
//			List<String> vals = getWebElementsText();
//			for (int i = 0; i < indexs.length; i++) {
//				Assert.assertEquals(vals.get(i), expectedVals[i], description);
//			}
//		}
//	}
//
//	/**
//	 * Verifies that the input value of the element at the specified position is
//	 * as expected.
//	 * 
//	 * @param index
//	 *            index of the element to set the value.
//	 * @param expectedVal
//	 *            the value that is expected in the input value of the web
//	 *            element.
//	 */
//	public void verifyInputValue(int index, String expectedVal) {
//		if (expectedVal != null) {
//			Assert.assertEquals(getInputVal(index), expectedVal, description);
//		}
//	}
//
//	public void verifyInputLength(int index, Integer length) {
//		if (length != null) {
//			Assert.assertEquals(getInputVal(index).length(), length.intValue(),
//					"Wrong length in field: " + description + "\n");
//		}
//	}
//
//	public void verifyDisplayed(int index, Boolean isDisplayed) {
//		if (isDisplayed != null) {
//			Assert.assertEquals(getElement(index).isDisplayed(),
//					isDisplayed.booleanValue(), description);
//		}
//	}
//
//	/**
//	 * Verifies that the input value of the element at the specified position is
//	 * as expected.
//	 * 
//	 * @param index
//	 *            index of the element to set the value.
//	 * @param expectedVal
//	 *            the value that is expected in the input value of the web
//	 *            element.
//	 */
//	public void verifyEnabled(int index, Boolean isEnabled) {
//		if (isEnabled != null) {
//			Assert.assertEquals(getElement(index).isEnabled(),
//					isEnabled.booleanValue(), description);
//		}
//	}
//
//	/**
//	 * Verifies that the attribute of the element is as expected.
//	 * 
//	 * @param attribute
//	 * @param value
//	 * @param expectedExists
//	 */
//	public void verifyAttribute(int index, String attribute, String value,
//			Boolean expectedExists) {
//		if (expectedExists != null) {
//			String isFound = expectedExists ? "not " : "";
//			Assert.assertEquals(getElement(index).getAttribute(attribute)
//					.contains(value), expectedExists.booleanValue(), "Value "
//					+ value + " was " + isFound + "found for Attribute "
//					+ attribute + " in " + description);
//		}
//	}
//
//	/**
//	 * Verifies that the displayed text of all elements in the list is as
//	 * expected.
//	 * 
//	 * @param expectedValue
//	 *            the expected value of all web elements displayed text.
//	 */
//	public void verifyAllTextValues(String expectedValue) {
//		if (expectedValue != null) {
//			List<String> actualValues = getWebElementsText();
//			Assert.assertFalse(actualValues.size() == 0);
//			for (int i = 0; i < actualValues.size(); i++) {
//				Assert.assertEquals(actualValues.get(i), expectedValue);
//			}
//		}
//	}
//
//	/**
//	 * Verifies that the displayed text of all elements in the list contains the
//	 * expected value.
//	 * 
//	 * @param expectedContainedValue
//	 *            the expected value to be contained in all of the web elements
//	 *            displayed text.
//	 */
//	public void verifyDisplayedTextValuesContains(String expectedContainedValue) {
//		verifyDisplayedTextValuesContains(expectedContainedValue, 0);
//	}
//
//	public void verifyDisplayedTextValuesContains(
//			String expectedContainedValue, int StartIndex) {
//		List<WebElement> elements = getElements();
//		List<String> actualValues = getWebElementsText();
//		for (int i = StartIndex; i < actualValues.size(); i++) {
//			if (elements.get(i).isDisplayed()) {
//				Assert.assertTrue(actualValues.get(i).toLowerCase()
//						.contains(expectedContainedValue.toLowerCase()));
//			}
//		}
//	}
//
//	/**
//	 * Verifies that the displayed text of all elements in the list is as
//	 * expected.
//	 * 
//	 * @param expectedValues
//	 *            array of values that are expected in the displayed text of the
//	 *            web elements.
//	 */
//	public void verifyAllTextValues(String... expectedValues) {
//		List<String> actualValues = getWebElementsText();
//		Assert.assertEquals(actualValues.size(), expectedValues.length);
//		for (int i = 0; i < expectedValues.length; i++) {
//			Assert.assertEquals(actualValues.get(i), expectedValues[i],
//					description);
//		}
//	}
//
//	public void verifyAllInputValues(String expectedValue) {
//		if (expectedValue != null) {
//			List<String> actualValues = getWebElementsInputVal();
//			for (int i = 0; i < actualValues.size(); i++) {
//				Assert.assertEquals(actualValues.get(i), expectedValue,
//						description);
//			}
//		}
//	}
//
//	public void verifyAllInputValues(String... expectedValues) {
//		List<String> actualValues = getWebElementsInputVal();
//		Assert.assertEquals(actualValues.size(), expectedValues.length);
//		for (int i = 0; i < expectedValues.length; i++) {
//			Assert.assertEquals(actualValues.get(i), expectedValues[i],
//					description);
//		}
//	}
//
//	/**
//	 * Verifies that the expected values exist in the elements displayed text.
//	 * 
//	 * @param expectedValues
//	 *            array of values that are expected in the displayed text of the
//	 *            web elements.
//	 */
//	public void verifyAllTextsExists(String... expectedValues) {
//		List<String> actualValues = getWebElementsText();
//		for (int i = 0; i < expectedValues.length; i++) {
//			Assert.assertTrue(actualValues.contains(expectedValues[i]),
//					description + "\n" + expectedValues[i]
//							+ " does not exist in the list");
//			actualValues.remove(expectedValues[i]);
//		}
//		Assert.assertEquals(actualValues.size(), 0);
//	}
//
//	/**
//	 * Verifies that the expected values exist in the elements displayed text.
//	 * 
//	 * @param expectedValues
//	 *            array of values that are expected in the displayed text of the
//	 *            web elements.
//	 */
//	public void verifyTextsExist(boolean expectedExists,
//			String... expectedValues) {
//		List<String> actualValues = getWebElementsText();
//		for (int i = 0; i < expectedValues.length; i++) {
//			Assert.assertEquals(actualValues.contains(expectedValues[i]),
//					expectedExists);
//		}
//	}
//
//	public void verifySize(int expectedSize) {
//		Assert.assertEquals(getElements().size(), expectedSize, description);
//	}
//
//	public void doubleClick(int index) {
//		actions.doubleClick(getElement(index)).perform();
//	}
//
//	public void rightClick(int index) {
//		actions.contextClick(getElement(index)).perform();
//	}
//}
