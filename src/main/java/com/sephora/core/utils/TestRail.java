package com.sephora.core.utils;

import com.sephora.test.androidApp.Base.SephoraAndroidAppBaseTest;
import com.sephora.test.androidApp.Base.SephoraBaseTest;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Map;
import java.util.HashMap;

import org.json.simple.JSONObject;

public class TestRail extends SephoraAndroidAppBaseTest {
	APIClient client = new APIClient("https://sephoraus.testrail.net/");
	SephoraBaseTest sephoraAndroidAppBase = new SephoraBaseTest();

	public void testRail(int Status, String testCaseID) throws MalformedURLException, IOException, APIException {
		Map<String, Comparable> data = new HashMap<String, Comparable>();
		client.setUser("anshu.khandelwal@sephora.com");
		client.setPassword("DevTester@02");
		data.put("status_id", new Integer(Status));
		data.put("comment", "Automation Script Execution");
		if (!sephoraAndroidAppBase.testRailRunID.isEmpty()) {
			JSONObject r = (JSONObject) client.sendPost("add_result_for_case/" + sephoraAndroidAppBase.testRailRunID + "/"
					+ testCaseID, data);
		}
	}

}