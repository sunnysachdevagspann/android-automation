package com.sephora.core.utils;

import java.io.IOException;

import org.testng.Reporter;
import org.testng.asserts.Assertion;
import org.testng.asserts.IAssert;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.sephora.core.driver.mobile.SephoraMobileDriver;


public class HardAssert extends Assertion {
	private  SephoraMobileDriver driver;
	 private ExtentTest test;
	 
	 public HardAssert(SephoraMobileDriver driver,ExtentTest test){
	  this.test=test;
	 }
	 
	 public void onAutomationFailure(Exception e) {
			String sScreenshotPath = null;
			try {
				sScreenshotPath = driver.getScreenshot();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			test.log(LogStatus.FATAL, "<pre><b><font color='red'>" +"AUTOMATION FAILURE ::"+e.getMessage()+"."+ "</font></b></pre>"+ test.addScreenCapture(sScreenshotPath));
			throw new AssertionError(e.getMessage());
		}
	 
	 @Override
	 public void onAssertFailure(IAssert a, AssertionError ex)
	 {
	  try {
	   String sScreenshotPath= driver.getScreenshot();
	   Reporter.log("Test Assertion >>>"+a.getMessage()+">>> Failed");
	   test.log(LogStatus.FAIL, "<pre><b><font color='red'>" +"FUNCTIONAL FAILURE ::"+ex.getMessage()+"."+ "</font></b></pre>"+ test.addScreenCapture(sScreenshotPath));
	  } catch (Exception e) {
	   // TODO Auto-generated catch block
	   e.printStackTrace();
	  }
	 }

	 @Override
	 public void onAssertSuccess(IAssert a){
	  try{
	   Reporter.log("Test Assertion >>>"+a.getMessage()+" >>>PASSED");
	   test.log(LogStatus.PASS,"<b><font color='green'>"+a.getMessage()+"</font></b>");
	  }catch(Exception e){

	  }
	 }
	 
	 
	 }