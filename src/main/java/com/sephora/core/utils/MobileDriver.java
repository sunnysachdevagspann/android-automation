package com.sephora.core.utils;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Utility class that takes care of initiating the selenium web driver. <br>
 * The web driver is initiated once, after that getting the web driver will
 * return the existing web driver. <br>
 * <br>
 * The following parameters should be configured in the
 * test/java/env.test.properties file:
 * <ul>
 * <li>automation.browserType - the type of browser to run
 * (chrome/firefox/ie/safari)
 * <li>automation.runOnRemote - 'true' if the selenium should run on a remote
 * machine, 'false' if locally.
 * <li>automation.host - the remote host to run the selenium on. relevant only
 * if runOnRemote is 'true'.
 * <ul>
 * 
 * @author GSPANN
 * 
 */
public class MobileDriver {

	private static WebDriver driver = null;
	private static WebDriverWait wait;
//	private static Actions s_actions;
	private static DesiredCapabilities capabilities;
	private Config config;

	/**
	 * class constructor
	 */
	public MobileDriver(Config config) {
		this.config = config;
	}

	private URL getUrl() {
		String host = config.getProperty("host");
		String url = "http://" + host + ":4723/wd/hub";
		try {
			return new URL(url);
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Initiates the web driver according to the browser type that is set in the
	 * 'test.properties' file
	 * 
	 * @return the web driver that was initialized.
	 * @throws MalformedURLException
	 */
	private WebDriver initWebDriver() {

		if (config.getProperty("deviceName").equals("Android Emulator"))
			capabilities = DesiredCapabilities.android();
		capabilities.setCapability("device", config.getProperty("device"));
		capabilities.setCapability("platformName",
				config.getProperty("platformName"));
		capabilities.setCapability("platformVersion",
				config.getProperty("platformVersion"));
		capabilities.setCapability("deviceName",
				config.getProperty("deviceName"));
		capabilities.setCapability("browserName",
				config.getProperty("browserName"));
		driver = new RemoteWebDriver(
				getUrl(), capabilities);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		return driver;
	}

	/**
	 * Gets the initiated web driver, and initiates it if it was't yet.
	 * 
	 * @return the initiated web driver.
	 * @throws MalformedURLException
	 */
	public WebDriver getWebDriver() throws MalformedURLException {
		if (driver == null) {
			return initWebDriver();
		}
		return driver;
	}

	/**
	 * Gets the selenium Wait object that was initiated according to the default
	 * timeout session value.
	 * 
	 * @return the Wait object
	 */
	public static WebDriverWait getWebDriverWait() {
		return wait;
	}

	/**
	 * Gets a selenium Wait object, setting its timeout session as specified.
	 * 
	 * @param timeoutSession
	 *            the timeout to set for the Wait object
	 * @return the new Wait object that was set
	 */
	public static WebDriverWait getWebDriverWait(int timeoutSession) {
		return new WebDriverWait(driver, timeoutSession);
	}

//	public static Actions getWebDriverActions() {
//		return s_actions;
//	}

	/**
	 * Closes the web driver & kills the driver execution;
	 */
	public static void closeWebDriver() {
		if (driver != null) {
			driver.quit();
		}
		driver = null;
	}

	public Object runJavaScriptCommand(String javaScript, Object... arguments) {
		try {
			getWebDriver();
		} catch (Exception e) {
		}
		Object resultObject;
		try {
			resultObject = getJSExecutor().executeScript(javaScript, arguments);
		} catch (Exception e) {
			resultObject = null;
		}
		return resultObject;
	}

	public static JavascriptExecutor getJSExecutor() {
		return ((JavascriptExecutor) driver);
	}

	public static void executeJS(String script, Object... arg1) {
		((JavascriptExecutor) driver).executeScript(script, arg1);
	}

	public static void waitForLoad() {
		ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript(
						"return document.readyState").equals("complete");
			}
		};
		wait.until(pageLoadCondition);

	}
}
