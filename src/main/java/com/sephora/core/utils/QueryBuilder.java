package com.sephora.core.utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class QueryBuilder {
	public String hazmatProductQueryString = "select p.sku_id as HAZMATSKUID,COUNTRY_CODE as countryCode from ATGCORE.dcs_price p, ATGCORE.dcs_inventory i, ATGCATA.SEPH_SKU_FLAGS f where p.price_list = 'plist32120019' and p.list_price > 0 and p.list_price < 35 and p.sku_id = i.catalog_ref_id and i.stock_level > 10 and i.country_code = 'US' and p.sku_id = f.sku_id and f.IS_HAZMAT = 1 and rownum < 4";
	public static String orderQueryString = "select u.ID user_id, u.email email ,u.password password, o.order_id orderNumber , ci.CATALOG_REF_ID sample_sku_id, s.FULL_SIZE_SKU_LINK from ATGCORE.DPS_USER u"
			+ "join ATGCORE.DCSPP_ORDER o on o.PROFILE_ID = u.ID join ATGCORE.DCSPP_ORDER_ITEM ci_o on ci_o.ORDER_ID = o.ORDER_ID"
			+ "join ATGCORE.DCSPP_ITEM ci on ci.COMMERCE_ITEM_ID = ci_o.COMMERCE_ITEMS join ATGCATA.SEPH_SKU s on s.SKU_ID = ci.CATALOG_REF_IDwhere s.SKU_TYPE = 2 and s.FULL_SIZE_SKU_LINK is not null ;";
	public static String multipleOrderQueryString = "select u.id,u.EMAIL email ,u.password password, count(o.ORDER_ID) order_count from ATGCORE.dps_user u "
			+ "join ATGCORE.dcspp_order o on o.PROFILE_ID = u.ID having count(o.ORDER_ID) >= 10 group by u.id, u.EMAIL order by order_count desc ;";
	public static String multipleShipmentQueryString = "select o.order_id, x.sg_count, u.email, u.id user_id from ATGCORE.dps_user u"
			+ "join ATGCORE.dcspp_order o on o.PROFILE_ID = u.ID join( select o.order_id, count(s.SHIPPING_GROUPS) sg_count "
			+ "from ATGCORE.dcspp_order o join ATGCORE.dcspp_order_sg s on o.ORDER_ID = s.ORDER_ID where o.SEPH_ORDER_STATUS in ('180')"
			+ "having count(s.SHIPPING_GROUPS) > 1 group by o.order_id)x on o.order_id = x.order_id order by x.sg_count desc ;";
	public static String biBirthdayRewardQueryString = "select s.sku_id, (case when s.BI_TYPE = 2 and s.SKU_TYPE = 2 then 'Sample'"
			+ "when s.BI_TYPE = 2 and s.SKU_TYPE = 5 then 'GWP' when s.BI_TYPE = 3 then 'Birthday Gift' when s.BI_TYPE != 2 then 'BI Reward'"
			+ "else '' end) sku_type from ATGCATA.SEPH_SKU s where (s.BI_TYPE = 2 and s.SKU_TYPE in (2, 5)) or s.BI_TYPE != 2;";
	public static String getOutOfStockProductQueryString = "select s.sku_id sku_id from ATGCATA.SEPH_SKU s left outer join ATGCORE.dcs_price p on p.sku_id = s.sku_id"
			+ "join ATGCATA.DCS_SKU_CATALOGS sc on sc.SKU_ID = s.SKU_ID join ATGCATA.SEPH_SKU_FLAGS sf on sf.SKU_ID = s.SKU_ID"
			+ "join ATGCATA.DCS_SKU ds on ds.SKU_ID = s.SKU_ID join ATGCORE.dcs_inventory i on s.sku_id = i.catalog_ref_id"
			+ "where p.price_list is not null and p.LIST_PRICE > 0 and sf.IS_ENABLED != 0 and s.SKU_STATE != 1 and sf.IS_STORE_ONLY != 1 "
			+ "and (ds.END_DATE is null or SYSDATE between ds.START_DATE and ds.END_DATE) and (s.TEMP_DEACTIVATION is null or SYSDATE < s.TEMP_DEACTIVATION)"
			+ "and sf.OUT_OF_STOCK_TREATMENT != 1 and s.SKU_TYPE = 0 and i.STOCK_LEVEL = 0;";
	public static String getOrderWithDiscountQueryString = "select u.ID user_id, u.email, o.order_id orderID, padj.ADJ_DESCRIPTION discount_type, padj.ADJUSTMENT discount from ATGCORE.DPS_USER u"
			+ "join ATGCORE.DCSPP_ORDER o on o.PROFILE_ID = u.ID join ATGCORE.DCSPP_AMTINFO_ADJ adj on adj.AMOUNT_INFO_ID = o.PRICE_INFO"
			+ "join ATGCORE.DCSPP_PRICE_ADJUST padj on padj.ADJUSTMENT_ID = adj.ADJUSTMENTS where padj.ADJ_DESCRIPTION in ('Order Discount', 'Item Discount') and padj.ADJUSTMENT < 0;";
	public static String getStoreCreditQueryString = "select u.ID user_id, u.email, o.order_id, pg.PAYMENT_METHOD from ATGCORE.DPS_USER u"
			+ "join ATGCORE.DCSPP_ORDER o on o.PROFILE_ID = u.ID join ATGCORE.DCSPP_ORDER_PG pg_o on pg_o.ORDER_ID = o.ORDER_ID"
			+ "join ATGCORE.DCSPP_PAY_GROUP pg on pg.PAYMENT_GROUP_ID = pg_o.PAYMENT_GROUPS where pg.TYPE = 3;";
	public static String getGiftCardQueryString = "select u.ID user_id, u.email, o.order_id, pg.PAYMENT_METHOD from ATGCORE.DPS_USER u"
			+ "join ATGCORE.DCSPP_ORDER o on o.PROFILE_ID = u.ID join ATGCORE.DCSPP_ORDER_PG pg_o on pg_o.ORDER_ID = o.ORDER_ID"
			+ "join ATGCORE.DCSPP_PAY_GROUP pg on pg.PAYMENT_GROUP_ID = pg_o.PAYMENT_GROUPS where pg.TYPE = 7;";
	public static String getCreditCardQueryString = "select u.ID user_id, u.email, o.order_id, pg.PAYMENT_METHOD from ATGCORE.DPS_USER u"
			+ "join ATGCORE.DCSPP_ORDER o on o.PROFILE_ID = u.ID join ATGCORE.DCSPP_ORDER_PG pg_o on pg_o.ORDER_ID = o.ORDER_ID"
			+ "join ATGCORE.DCSPP_PAY_GROUP pg on pg.PAYMENT_GROUP_ID = pg_o.PAYMENT_GROUPS where pg.TYPE = 1 ;";
	public static String getUserDetailsHavingSampleAndNotFullSizeProductQueryString = "select u.ID user_id, u.email email u.password password, o.order_id, ci.CATALOG_REF_ID sample_sku_id, s.FULL_SIZE_SKU_LINK from ATGCORE.DPS_USER u"
			+ "join ATGCORE.DCSPP_ORDER o on o.PROFILE_ID = u.ID join ATGCORE.DCSPP_ORDER_ITEM ci_o on ci_o.ORDER_ID = o.ORDER_ID"
			+ " "
			+ "join ATGCORE.DCSPP_ITEM ci on ci.COMMERCE_ITEM_ID = ci_o.COMMERCE_ITEMS join ATGCATA.SEPH_SKU s on s.SKU_ID = ci.CATALOG_REF_ID"
			+ " " + "where s.SKU_TYPE = 2 and s.FULL_SIZE_SKU_LINK is null ";
	public static String getUserDetailsHavingNoOrderAndPurchaseQueryString = "select u.ID user_ID, u.LOGIN email,u.PASSWORD password from ATGCORE.DPS_USER u"
			+ " "
			+ "where u.ID not in(select u.ID from ATGCORE.DPS_USER u join ATGCORE.DCSPP_ORDER o on o.PROFILE_ID = u.ID"
			+ " "
			+ "join ATGCORE.DCSPP_PAY_GROUP pg on pg.ORDER_REF = o.ORDER_ID where pg.AMOUNT_AUTHORIZED != 0 and pg.STATE in ('SETTLED', 'AUTHORIZED'))and rownum <= 1";
	public static String getUserDetailsHavingSampleOneSizeMultipleSizeNoFullSizeQueryString = "select u.ID, u.LOGIN ,u.password from ATGCORE.DPS_USER u join ATGCORE.DCSPP_ORDER o on o.PROFILE_ID = u.ID"
			+ " "
			+ "join ATGCORE.DCSPP_PAY_GROUP pg on pg.ORDER_REF = o.ORDER_ID join ATGCORE.DCSPP_ORDER_ITEM oi on oi.ORDER_ID = o.ORDER_ID join ATGCORE.DCSPP_ITEM ci on ci.COMMERCE_ITEM_ID = oi.COMMERCE_ITEMS"
			+ " "
			+ "join ATGCATA.SEPH_SKU s on s.SKU_ID = ci.CATALOG_REF_ID join ATGCATA.DCS_PRD_CHLDSKU ps on ps.SKU_ID = s.FULL_SIZE_SKU_LINK where pg.AMOUNT_AUTHORIZED != 0 and pg.STATE in ('SETTLED', 'AUTHORIZED')"
			+ " "
			+ "and s.SKU_TYPE = 2 and ps.PRODUCT_ID in (select p.PRODUCT_ID from ATGCATA.DCS_PRD_CHLDSKU p having count(*) = 1 group by p.PRODUCT_ID)group by u.ID, u.LOGIN ,u.password "
			+ " "
			+ "intersect"
			+ " "
			+ "select u.ID, u.LOGIN ,u.password from ATGCORE.DPS_USER u join ATGCORE.DCSPP_ORDER o on o.PROFILE_ID = u.ID join ATGCORE.DCSPP_PAY_GROUP pg on pg.ORDER_REF = o.ORDER_ID"
			+ " "
			+ "join ATGCORE.DCSPP_ORDER_ITEM oi on oi.ORDER_ID = o.ORDER_ID join ATGCORE.DCSPP_ITEM ci on ci.COMMERCE_ITEM_ID = oi.COMMERCE_ITEMS join ATGCATA.SEPH_SKU s on s.SKU_ID = ci.CATALOG_REF_ID join ATGCATA.DCS_PRD_CHLDSKU ps on ps.SKU_ID = s.FULL_SIZE_SKU_LINK"
			+ " "
			+ "where pg.AMOUNT_AUTHORIZED != 0 and pg.STATE in ('SETTLED', 'AUTHORIZED') and s.SKU_TYPE = 2 and ps.PRODUCT_ID in  (select p.PRODUCT_ID from ATGCATA.DCS_PRD_CHLDSKU p having count(*) > 1 group by p.PRODUCT_ID) group by u.ID, u.LOGIN,u.password"
			+ " "
			+ "INTERSECT"
			+ " "
			+ "select u.ID, u.LOGIN ,u.password from ATGCORE.DPS_USER u join ATGCORE.DCSPP_ORDER o on o.PROFILE_ID = u.ID join ATGCORE.DCSPP_PAY_GROUP pg on pg.ORDER_REF = o.ORDER_ID join ATGCORE.DCSPP_ORDER_ITEM oi on oi.ORDER_ID = o.ORDER_ID join ATGCORE.DCSPP_ITEM ci on ci.COMMERCE_ITEM_ID = oi.COMMERCE_ITEMS"
			+ " "
			+ "join ATGCATA.SEPH_SKU s on s.SKU_ID = ci.CATALOG_REF_ID where pg.AMOUNT_AUTHORIZED != 0 and pg.STATE in ('SETTLED', 'AUTHORIZED') and s.SKU_TYPE = 2 and s.FULL_SIZE_SKU_LINK is null group by u.ID, u.LOGIN,u.password ";
	public static String getUserDetailsHavingMultipleAddressesQueryString = "select * from ( select  u.ID, u.LOGIN , u.password from ATGCORE.DPS_USER u  join ATGCORE.DPS_OTHER_ADDR a on a.USER_ID = u.ID having count(a.ADDRESS_ID) > 1"
			+ " " + "group by u.id, u.Login ,u.password)where ROWNUM<=1";

	public Map<String, String> getProductDetails(String productType) throws SQLException {
		Map<String, String> map = new HashMap<String, String>();
		ResultSet rs = null;
		if (productType.equalsIgnoreCase("hazmat")) {
			rs = DBUtil.performSelectQuery(hazmatProductQueryString);
			while (rs.next()) {
				map.put("ProductID", rs.getString("HAZMATSKUID"));
				map.put("countryCode", rs.getString("countryCode"));
			}
		}
		return map;
	}

	public static Map<String, String> getOrderDetails() throws SQLException {
		Map<String, String> map = new HashMap<String, String>();
		ResultSet rs = null;
		rs = DBUtil.performSelectQuery(orderQueryString);
		while (rs.next()) {
			map.put("sampleSkuId", rs.getString("sample_sku_id"));
			map.put("biRewardSKUID", rs.getString("biRewardSKUID"));
			map.put("orderNumber", rs.getString("orderNumber"));
			map.put("email", rs.getString("email"));
			map.put("password", rs.getString("password"));
		}
		return map;
	}

	public static Map<String, String> getUserHavingMultipleOrderHistory() throws SQLException {
		Map<String, String> map = new HashMap<String, String>();
		ResultSet rs = null;
		rs = DBUtil.performSelectQuery(multipleOrderQueryString);
		while (rs.next()) {
			map.put("email", rs.getString("email"));
			map.put("password", rs.getString("password"));
		}
		return map;
	}

	public static Map<String, String> getUserHavingMultipleShipments() throws SQLException {
		Map<String, String> map = new HashMap<String, String>();
		ResultSet rs = null;
		rs = DBUtil.performSelectQuery(multipleShipmentQueryString);
		while (rs.next()) {
			map.put("email", rs.getString("email"));
			map.put("password", rs.getString("password"));
		}
		return map;
	}

	public static Map<String, String> getBIBirthReward() throws SQLException {
		Map<String, String> map = new HashMap<String, String>();
		ResultSet rs = null;
		rs = DBUtil.performSelectQuery(biBirthdayRewardQueryString);
		while (rs.next()) {
			map.put("email", rs.getString("email"));
			map.put("password", rs.getString("password"));
		}
		return map;
	}

	public static Map<String, String> getOOSSkuid() throws SQLException {
		Map<String, String> map = new HashMap<String, String>();
		ResultSet rs = null;
		rs = DBUtil.performSelectQuery(getOutOfStockProductQueryString);
		while (rs.next()) {
			map.put("sku_id", rs.getString("sku_id"));
		}
		return map;
	}

	public static Map<String, String> getUserDetailsWithStoreCreditCard() throws SQLException {
		Map<String, String> map = new HashMap<String, String>();
		ResultSet rs = null;
		rs = DBUtil.performSelectQuery(getOrderWithDiscountQueryString);
		while (rs.next()) {
			map.put("orderID", rs.getString("orderID"));
		}
		rs = DBUtil.performSelectQuery(getStoreCreditQueryString);
		while (rs.next()) {
			map.put("storeCreditCard", rs.getString("storeCreditCard"));
		}
		rs = DBUtil.performSelectQuery(getGiftCardQueryString);
		while (rs.next()) {
			map.put("giftCard", rs.getString("giftCard"));
		}
		rs = DBUtil.performSelectQuery(getCreditCardQueryString);
		while (rs.next()) {
			map.put("creditCard", rs.getString("creditCard"));
		}
		return map;
	}

	public static Map<String, String> getUserDetailsHavingSampleAndNoFullSizeProduct() throws SQLException {
		Map<String, String> map = new HashMap<String, String>();
		ResultSet rs = null;
		rs = DBUtil.performSelectQuery(getUserDetailsHavingSampleAndNotFullSizeProductQueryString);
		while (rs.next()) {
			map.put("email", rs.getString("email"));
			map.put("password", rs.getString("password"));
		}
		return map;
	}

	public static Map<String, String> getUserDetailsHavingNoOrderandPurchase() throws SQLException {
		Map<String, String> map = new HashMap<String, String>();
		ResultSet rs = null;
		rs = DBUtil.performSelectQuery(getUserDetailsHavingNoOrderAndPurchaseQueryString);
		while (rs.next()) {
			map.put("email", rs.getString("email"));
			map.put("password", rs.getString("password"));
		}
		return map;
	}

	public static Map<String, String> getUserDetailsHavingSampleWithONEMultipleNoEquivalents() throws SQLException {
		Map<String, String> map = new HashMap<String, String>();
		ResultSet rs = null;
		rs = DBUtil.performSelectQuery(getUserDetailsHavingSampleOneSizeMultipleSizeNoFullSizeQueryString);
		while (rs.next()) {
			map.put("email", rs.getString("email"));
			map.put("password", rs.getString("password"));
		}
		return map;
	}

	public static Map<String, String> getUserDetailsHavingMultipleAddress() throws SQLException {
		Map<String, String> map = new HashMap<String, String>();
		ResultSet rs = null;
		rs = DBUtil.performSelectQuery(getUserDetailsHavingMultipleAddressesQueryString);
		while (rs.next()) {
			map.put("email", rs.getString("email"));
			map.put("password", rs.getString("password"));
		}
		return map;
	}
}
