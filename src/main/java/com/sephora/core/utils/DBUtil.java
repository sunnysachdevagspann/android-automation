/**
 * 
 */
package com.sephora.core.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DBUtil {
	private static final Logger logger = LogManager.getLogger(DBUtil.class.getName());
	// private Config config;
	// To do - Put in config file
	private static Connection con;
	private static final String Driver = "oracle.jdbc.driver.OracleDriver";
	private static final String ConnectionString = "jdbc:oracle:thin:QA_DSHYMBALIOVA/change!now@10.105.36.139:1525:SEPQA01";
	static {
		try {
			Class.forName(Driver);
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * 
	 * Purpose: To Make connection to DB
	 * 
	 * @author: Sunny Sachdeva
	 * @date:13-Feb-2015
	 * @returnType: Connection
	 * @return
	 * @throws SQLException
	 */
	public static Connection initDatabaseProvider() throws SQLException {
		if (con == null || con.isClosed()) {
			con = DriverManager.getConnection(ConnectionString);
		}
		return con;
	}

	/**
	 * 
	 * Purpose: Checks the state of Connection
	 * 
	 * @author: Sunny Sachdeva
	 * @date:13-Feb-2015
	 * @returnType: boolean
	 */
	public static boolean isConnectedToDB() throws SQLException {
		boolean IsConncted = false;
		if (!con.isClosed()) {
			IsConncted = true;
		}
		return IsConncted;
	}

	/**
	 * 
	 * Purpose: Get ResultSet from DB
	 * 
	 * @author: Sunny Sachdeva
	 * @date:13-Feb-2015
	 * @returnType: ResultSet
	 */
	public static ResultSet performSelectQuery(String sQuery) throws SQLException {
		Connection con = initDatabaseProvider();
		ResultSet rs;
		logger.info("Query ===>" + sQuery);
		PreparedStatement st = con.prepareStatement(sQuery);
		rs = st.executeQuery();
		logger.info("Query Executed :");
		System.out.println(rs);
		return rs;
	}

	/**
	 * 
	 * Purpose: Perform Update context in DB
	 * 
	 * @author: Sunny Sachdeva
	 * @date:13-Feb-2015
	 * @returnType: void
	 */
	public static void performUpdateQuery(String query) throws SQLException {
		Connection con = initDatabaseProvider();
		// ResultSet rs;
		PreparedStatement st = con.prepareStatement(query);
		st.executeUpdate();
	}

	/**
	 * 
	 * Purpose: Close the Connection
	 * 
	 * @author: Sunny Sachdeva
	 * @date:13-Feb-2015
	 * @returnType: void
	 */
	public static void closeConnection() {
		if (con != null) {
			try {
				con.close();
			} catch (SQLException e) {
			}
		}
	}
}
