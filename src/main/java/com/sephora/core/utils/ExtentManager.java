package com.sephora.core.utils;

import com.relevantcodes.extentreports.ExtentReports;

public class ExtentManager {
	
	
	private static ExtentReports extent;
	public static ExtentReports getInstance() {
        if (extent == null) {
            extent = new ExtentReports(System.getProperty("user.dir")+"\\test-output\\Sephora_AutomationReport.html",true);
            //extent = new SephoraExtentReports(System.getProperty("user.dir")+"\\test-output\\Sephora_AutomationReport.html",true);
        }
        return extent;
    }

}
