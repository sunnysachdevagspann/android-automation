package com.sephora.core.mobile.constants;

public interface TestConstantsI {

	/*
	 * URL Links
	 */
	public static final String ENVIRONMENT = "m-qa"; 
	public static final String URL="https://"+ENVIRONMENT+".sephora.com/";
	public static final String HOMEURL_WITHOUTLOGIN="http://"+ENVIRONMENT+".sephora.com/";
	public static final String HOME_URL = ENVIRONMENT+".sephora.com/";
	public static final String ABOUT_BI_URL = "https://"+ENVIRONMENT+".sephora.com/account/bi/about";
	public static final String TOP_LEVEL_CATEGORY_URL="http://"+ENVIRONMENT+".sephora.com/fragrance";
	public static final String SECOND_LEVEL_CATEGORY_URL="http://"+ENVIRONMENT+".sephora.com/face-makeup";
	public static final String CONTENT_NO_AVAILABLE = "http://"+ENVIRONMENT+".sephora.com/Mobile/Mobile%20Web/Regression/Content%20Not%20Available/";
	public static final String SKEDGE_ME_LINKS_URL = "http://"+ENVIRONMENT+".sephora.com/stores/chalkyitsik";
	public static final String RESERVATIION_SKEDGE_ME_WIDGET_URL = "http://"+ENVIRONMENT+".sephora.com/stores/santa-barbara";
	public static final String WITHOUT_RESERVATIION_SKEDGE_ME_WIDGET_URL = "http://"+ENVIRONMENT+".sephora.com/stores/victorville-jcpenney-at-mall-of-victor-valley";
	public static final String UPSSITE="http://wwwapps.ups.com";
	public static final String HOMEURL = ""+ENVIRONMENT+".sephora";
	public static final String ACCOUNTDETAILLINK="account/orderdetail";
	public static final String NICK_NAME_TERM_OF_USE_URL = "communityTermsOfUse";
	public static final String SKEDGE_ME_WIDGET_URL = "skedge.me/widget";
	public static final String TRACKINGURL = "wwwapps.ups.com";
	public static final String BI_ABOUT_PAGE_URL = "bi/about";
	public static final String SEARCH_ENGINE_URL = "http://www.google.com";
	public static final String SEARCH_ENGINE_SEARCH_TERM = "sephora";
	public static final String NAVAR_TRACING = "narvar";
	public static final String CATEGORY_URL="http://"+ENVIRONMENT+".sephora.com/nails-makeup";
	public static final String SERVERINFO="http://"+ENVIRONMENT+".sephora.com/serverInfo";
	public static final String BICARDPAGE="http://"+ENVIRONMENT+".sephora.com/account/bi/card";
	public static final String BIREWARDS="http://"+ENVIRONMENT+".sephora.com/account/bi/rewards";
	public static final String BIABOUT="http://"+ENVIRONMENT+".sephora.com/account/bi/about";
	public static final String ONLINE_ORDER_LINK = "https://"+ENVIRONMENT+".sephora.com/account/orders";
	public static final String BI_TERMS_CONDITION = "BITermsAndConditions";

	/*
	 * SKU-ID's
	 */
	public static final String SKU_ID = "1444777";
	public static final String CANADA_RESTRICTED_PRODUCT = "HERMES";
	public static final String CANADA_RESTRICTED_SKU = "1104215";
	public static final String CANADA_SKU_ID_LESS_50 = "1605641";
	public static final String HAZMAT_SKUID = "1545797";
	public static final String DEFAULT_SKUID="1573336";
	public static final String SKU_ID2 = "1574896";
	public static final String SKU_ID_MORETHAN_200_PRICE  = "P375968";
	public static final String SKU_ID_250 = "1513431";
	public static final String SKU_ID_BW_25_49 = "P384505";
	public static final String SKU_ID_2_BW_25_49 = "1719244";
	public static final String SKU_ID_MORE_600 = "1707009";
	public static final String SKU_ID_VIB_EXCLUSIVE = "1158336";
	public static final String SKU_ID_VIB_EXCLUSIVE_CANADA = "1556208";
	public static final String SKU_ID_BI_EXCLUSIVE = "1000140";
	public static final String SKU_ID_OOS = "1270370";
	public static final String SKU_ID_REVIEWS = "P262061";
	public static final String SKU_ID_WITH_20REVIEWS = "137471";
	public static final String FLASH_SKU = "p379518";
	public static final String SKU_ID_FLASH = "1530070";
	public static final String SKU_ID_GIFTCARD = "00010";
	public static final String SKU_ID_BEAUTYSTUDIO_GIFTCARD = "0051";
	public static final String BLACK_GC_ID = "00050";
	public static final String SKU_ID_COLOR[] = {"1547397", "paper white"};
	public static final String GWP_PROMO_BRAND_NAME = "bareMinerals";
	public static final String PROMOCODE_15_OFF = "15dollars";
	public static final String PROMOCODE = "HOTHAIR";	 
	public static final String PROMO_GWP_SKU_ID = "1348218";
	public static final String PRODUCTID = "P395417";
	//public static final String PRODUCTID2 = "P392403";
	public static final String PRODUCTID2 = "P386289";
	public static final String SKU_ID_VIBROUGUE_EXCLUSIVE = "1000546";
	public static final String SKUID_WITH_STORES = "692467";
	public static final String SKU_ID_BIRTHDAYREWARDS = "1671254";
	public static final String PROMO_CODE_EXPIRED="skinsampler";
	public static final String PROMO_CODE_10_OFF="10%off";
	public static final String PROMO_CODE_CA15_OFF="ca15%off";
	public static final String PROMO_CODE_YOUR_GIFT="yourgift";
	public static final String HAZMAT_SKUID_2 = "1270768";
	//public static final String HAZMAT_SKUID_2 = "1270750";
	public static final String PROMO_CODE_25FREESHIP="25FreeShip";
	public static final String PROMO_CODE_MULTIPLE_ITEM="MSG";
	public static final String PROMO_CODE_MULTIPLE_ITEM_VIB="vibmsg";
	public static final String PROMO_CODE_BIGWP="bigwp";
	public static final String PROMO_CODE_MOBILE="mobile";
	public static final String SKU_ID_SALE_PRD="P217805";
	public static final String SKU_ID_SALE="P391300";
	public static final String SKU_ID_SALE_2="1562297";
	public static final String PROMO_CODE_SALE="fbextra";
	public static final String EMPLOYEE_PROMO="sunshine13";
	public static final String SKU_ID_RELATED_PROMO="1746841";
	public static final String SKU_ID_MORE_THAN_100="1445196";
	public static final String SKU_ID_SEPHORA_COLLECTION="1489665";
	public static final String PROMO_CODE_GWP="GWP";
	public static final String PROMO_CODE_CANADA_GWP="canadagwp";
	public static final String PROMO_CODE_NON_EXISTING="fff";
	public static final String SKU_ID_CA_BTW_50_AND_100="P379559";
	public static final String SKU_ID_CA_MORE_THAN_100="P309208";
	public static final String SKU_ID_CA="1293620";
	public static final String SKU_ID_CA_2="1605641";
	public static final String PROMO_CODE_CA_15_PERCENT_OFF="	";
	public static final String NON_HAZMAT_PRODUCT_UNDER_50_DOLLAR="P84818";
	//public static final String SKU_ID_MORE_THAN_100="P375849";
	public static final String SKU_ID_ONLINE_ONLY = "1007731";
	public static final String SKU_ID_MORE_THAN_50="1496496";
	public static final String SKU_ID_HAZMAT_SAMPLE="1745686";
	
	/*
	 * Messages -- error messages
	 */
	public static final String PREVIOUSONLINEORDERS_MESSAGE="You have no previous online orders.";
	public static final String FLASH_SKU_PRICE = "FREE FOR VIB ROUGE";
	public static final String PDP_BUTTON_FOR_ADDED_FLASH_PRODUCT = "YOU HAVE ADDED FLASH TO BASKET";
	public static final String TERMS_CONDITIONS_TEXT = "TERMS & CONDITIONS";
	public static final String MESSAGE_NEXT_TO_TERMSCONDITION_CHECKBOX = "Join and agree to Terms & Conditions";
	public static final String CANADA_PDP_FREE_SHIP_MESSAGE = "Spend C$50 for FREE SHIPPING";
	public static String FREE_SHIPPING_TEXT_LESS_AMOUNT = "Spend C$%s.00 or more for free shipping!";
	public static String BASKET_INLINE_FREE_SHIPPING_TEXT_LESS_AMOUNT = "You're only C$%s.00 away from Free Shipping.";
	public static final String INVALID_POSTAL_ERROR_MESSAGE_FROM_ADDRESS_PAGE = "Please enter a valid zipcode";
	public static final String FLASH_ENROLLED_MESSAGE = "You are enrolled in Flash";
	public static final String REALTIMEMESSAGEBEFOREVIB ="become a Very Important Beauty Insider (VIB).";
	public static final String EXISTING_NICK_NAME_ERROR_MESSAGE = "This nickname is taken, please select another one.";
	public static final String EXPIRED_CREDIT_CARD = "";
	public static final String PRODUCT_NOT_CARRIED_ERROR_MESSAGE = "We do not currently carry the product you requested. Please try another search";
	public static final String EMPTY_REVIEW_ERROR_MESSAGE = "This field is Required.";
	public static final String LESS_CHAR_REVIEW_ERROR_MESSAGE = "You must enter at least 20 characters for Your review.";
	public static final String REALTIMEMESSAGEVIB="unlock VIB Rouge, our new premium membership status";
	public static final String POST_CONFIRMATION_HEARED = "THANKS GORGEOUS!";
	public static final String POST_CONFIRMATION_TITLE = "THANKS FOR SUBMITTING YOUR REVIEW!";
	public static final String POST_CONFIRMATION_MESSAGE = "Reviews are typically posted within 72 hours of the time you submitted them, so stay tuned.";
	public static final String BI_CARD_REAL_TIME_MESSAGE_VIB_ROUGE[] = {"enjoy VIB status until", "and spend", "to unlock VIB Rouge, our new premium membership status"};
	public static final String BI_REWARD_REAL_TIME_MESSAAGE_AFTER_VIB = "enjoy VIB status until";
	public static final String exceptedFreeShippingMessage="Free Shipping. VIB Rouge, Enjoy FREE FLASH SHIPPING on every order!";
	public static final String UNCHECK_TERMS_CONDITION_ERROR_FOR_BI_USER = "You need to agree to Terms & Conditions for BI";
	public static final String INVALID_DATE_MESSAGE_REGISTER_OVERLAY = "Please provide a valid birth date";
	public static final String NOPAYMENTERRORMESSAGE = "A payment method has not been selected. Please select a credit card";
	public static final String FREE_SHIPPING_TEXT_MORE_AMOUNT = "Your order qualifies for FREE 3-day shipping";
	public static final String BASKET_INLINE_FREE_SHIPPING_TEXT_MORE_AMOUNT = "You now qualify for free shipping!";
	public static final String GIFT_CARD_WITHOUT_GOODS_ERROR_MESSAGE = "Merchandise, excluding gift cards and eGift certificates, must be included in your order to qualify for free samples.";
	public static final String WITHOUT_MERCHANDISE_CHECKOUT_MESSAGE = "You must add merchandise before you can proceed to checkout.";
	public static final String STOCK_OVERFLOW_MESSAGE = "Sorry, there are not enough items in stock to fulfill your order. We have updated the quantity in your basket to reflect the number we have available.";
	public static final String checkEmptyBasketExceptedMessage="YOUR BASKET IS CURRENTLY EMPTY";
	public static final String checkSignBasketExceptedMessage="Please sign in if you are trying to retrieve a basket created in the past";
	public static final String warningBasketExceptedMessage="Items in your last shopping session have been merged with your existing Basket.";
	public static final String PROMO_ADDED_HEADER = "Promotion Added!";
	public static final String errorExceptedMessage="Sorry, the promotion you entered has expired. Please select another promotion code.";
	public static final String errorNonExistPromoExceptedMessage="The coupon fff does not exist. Please check instructions carefully. The promotion code may include letters and numbers.";
	public static final String errorDuplicateExceptedMessage="Sorry, you can only purchase 1 Gift Card per order. We have corrected item quantity in your basket.";
	public static final String VIBSIGNMESSAGE = "You must be a VIB to qualify for this product, sign in now.";
	public static final String VIB_REGISTER_MESSAGE = "You must be a VIB to qualify for this product, register now.";
	public static final String VIBROUGE_SIGNIN_MESSAGE = "You must be a VIB Rouge to qualify for this product, sign in now.";
	public static final String VIBROUGE_LEARN_MESSAGE = "You must be a VIB Rouge to qualify for this product, learn more.";
	public static final String SHIPPING_MESSAGE = "Spend $50 for FREE SHIPPING";
	public static final String VIB_LEARN_MESSAGE = "You must be a VIB to qualify for this product, learn more.";
	public static final String STAR_REVIEW_ERROR_MESSAGE = "You must give the product a star rating to finish reviewing.";
	public static final String ROUGE_SHIPPING_MESSAGE = "Rouge Free Shipping";
	public static final String FLASH_FREE_SHIPPING_MESSAGE = "Free Flash Shipping";
	public static final String FREE_SHIPPING_MESSAGE = "free shipping";
	public static final String BIRTHDAY_GIFT_PRODUCTNAME = "Happy Birthday!";
	public static final String WELCOME_KIT_PRODUCTNAME = "Your Complimentary Welcome Kit";
	public static final String ROUGE_WELCOME_KIT_PRODUCTNAME = "Your Complimentary Welcome Kit";
	public static final String GIFT_CARD_PLEASE_NOTE_MESSAGE = "Gift cards entered in this order are NOT saved in your account for future orders.";
	public static final String CANADA_CHECKOUT_MODEL_TEXT = "The Sephora Mobile Web Site does not support Canadian checkout. Please continue to the desktop site to checkout for all purchases shipped to Canada.";
	public static final String CANADA_CHECKOUT_MODEL_TITLE = "CANADA CHECKOUT";
	public static final String FREE_SHIPPING_QUALIFY_MESSAGE = "You now qualify for free shipping!";
	public static final String SWITCH_TO_CANADA_MESSAGE = "You are about to switch to our Canadian shopping experience. If there are any Canada-restricted items in your basket, they will be removed. Do you want to continue?";
	public static final String SWITCH_TO_US_TITLE = "SWITCH TO UNITED STATES";
	public static final String SWITCH_TO_US_MESSAGE = "You are about to switch to our United States shopping experience. If there are any US-restricted items in your basket, they will be removed. Do you want to continue?";
	public static final String SEND_EMAIL_CONFIRMATION_MESSAGE = "An email has been sent with a link to reset your password.";
	public static final String RESET_PASSWORD_AND_SECURITY_TITLE = "RESET PASSWORD AND SECURITY QUESTION";
	public static final String EMPTY_NEW_PASSWORD_ERROR_MESSAGE = "Please enter a password between 6-12 characters (no spaces).";
	public static final String EMPTY_CONFIRM_PASSWORD_ERROR_MESSAGE = "Please enter a password between 6-12 characters (no spaces).";
	public static final String NOT_MATCHING_PASSWORD_ALERT_MESSAGE = "Your new password and confirm password don't match.";
	public static final String FIRST_NAME_REQUIRED_MESSAGE = "First Name Required. Please enter your first name.";
	public static final String LAST_NAME_REQUIRED_MESSAGE = "Last Name Required. Please enter your last name.";
	public static final String FIRST_NAME_INVALID_MESSAGE = "Invalid First Name. Name cannot include numbers.";
	public static final String LAST_NAME_INVALID_MESSAGE = "Invalid Last Name. Name cannot include numbers.";
	public static final String CONFIRM_PASSWORD_ERROR_MESSAGE = "Your new password and confirm password don't match.";
	public static final String PASSWORD_LIMIT_MESSAGE = "Please enter a password between 6-12 characters (no spaces).";
	public static final String IMPOSSIBLE_TO_ADD_GC_ERROR_MESSAGE = "eGift Certificates and Gift Cards can only be used to purchase merchandise and may not be used to purchase other eGift Certificates or Gift Cards. Please place your order for the eGift Certificate(s) and Gift Card separately.";
	public static final String CHOOSE_UPTO_SAMPLE_TEXT = "Choose up to 5";
	public static final String PROMO_CODE_PER_OFFER_TEXT = "1 code per offer";
	public static final String RESET_PASSWORD_AND_SECURITY_MESSAGE[] = {"To have your password and security question reset, click the", "send e-mail",  "button below. We will send an", "email to your address containing a link to change your information"};
	public static String ALREADY_EXISTED_GIFT_CARD_MESSAGE = "The giftcard: %s already exists";
	public static String INVALID_GIFT_CARD_MESSAGE = "The gift card:%s is invalid.";
	public static final String EMPTY_CARD_NUMBER_ERROR_MESSAGE = "Please enter a 16 digit card number.";
	public static final String InvalidZipCodeMessage = "Invalid ZIP Code. Please enter a ZIP Code with 5 or 9 digits.";
	public static final String INVALID_EMPTY_ZIP_CODE_MESSAGE = "ZIP Code Required. Please enter a 5 digit ZIP Code.";
	public static final String EMPTY_POSTAL_CODE_MESSAGE = "Postal Code Required. Please enter a 6 character Postal Code.";
	public static final String INVALID_EMPTY_POSTAL_CODE_MESSAGE = "Postal Code Required. Please enter a 6 character Postal Code.";
	public static final String EMPTY_PIN_NUMBER_ERROR_MESSAGE = "Please enter an 8 digit PIN";
	public static final String INVALID_ZIP_MESSAGE = "Invalid ZIP Code. Please enter a ZIP Code with 5 or 9 digits.";
	public static final String INVALID_POSTAL_MESSAGE = "Were sorry. This item is not available within a 50.0 miles radius of your selected zip code. Please try your search again.";
	public static final String SWITCH_TO_CANADA_TITLE = "SWITCH TO CANADA";
	public static final String NOSTORE_ERRORMESSAGE = "No results for the given location could be found"; 
	public static final String CAPTCHA_ERROR_MESSAGE = "You did not enter the characters. Please enter the correct code.";
	public static final String EXPECTEDFREESHIPPINGMESSAGE="Free Shipping. VIB Rouge, Enjoy FREE FLASH SHIPPING on every order!";
	public static final String EXPECTEDGIFTCARDALERTMESSAGE="you can only apply 2 Gift Cards per order.";
	public static final String BECOME_BEAUTY_INSIDER_TEXT = "BECOME A BEAUTY INSIDER!";
	public static final String SIGNUP_BI_MESSAGE = "Join our free rewards program for free deluxe samples, birthday gifts, and more.";
	public static final String[] SIGNUP_BI_TERMS_CONDITION = {"Join and agree to", "Terms & Conditions", "By joining, you will automatically receive Beauty Insider offers via email."};
	public static final String EMPTY_BIRTHDATE_ERROR_MESSAGE = "Please select your birth date";
	public static final String NON_BI_USER_CARD_NAME_TEXT = "YOUR NAME HERE";
	public static final String VIB_ROUGE_USER_STATUS = "VIB ROUGE";
	public static final String VIB_USER_STATUS = "VIB";
	public static final String BIRTHDAY_CARD_LINK_TEXT = "HAPPY BIRTHDAY! TAP FOR INFO";
	public static final String BI_MESSAGE_TERMS_CONDITION = "By joining, you will automatically receive Beauty Insider Offers via email";
	public static final String EMPTY_PASSWORD_MESSAGE_SIGNIN_OVERLAY = "Please enter your password.";
	public static final String WRONG_PASSWORD_OR_EMAIL_MESSAGE = "We're sorry, there is an error with your email and/or password. Remember passwords are 4 to 12 characters (letters or numbers) long. Please try again.";
	public static final String BASKET_ERROR = "Merchandise, excluding gift cards and eGift certificates, must be included in your order to qualify for Beauty Insider offers.";
	public static final String WELCOME_CANADA_TITLE = "WELCOME TO SEPHORA.CA";
	public static final String DEFAULT_MESSAGE= "Message for Testing is added";
	public static final String GIFT_CARD_AT_ALL_STORE_MESSAGE= "Gift cards available at all stores";
	public static final String PROMO_PER_ORDER_LIMIT = "1 promo per order limit.";
	public static final String VIB_CONGRATULATIONS_MESSAGE = "Congratulations on becoming a Very Important Beauty Insider.";
	public static final String FLASH_TWO_DAY_SHIPPING_MESSAGE = "FREE 2-DAY SHIPPING FOR ONE YEAR";
	public static final String TERM_CONDITION_MESSAGE = "By selecting 'Add to Basket', you accept Sephora FLASH ";
	public static final String SSIT_COOKIE_MESSAGE_SIGN_IN_PAGE = "You previously opted to stay signed in, but we need to re-verify your information";
	public static final String EXISTING_EMAIL_ERROR_MESSAGE = "An account already exists for"; 
	public static final String CAPTCHA_INVALID_ERROR_MESSAGE = "The characters weren't entered correctly. Please enter the correct code";
	public static final String INVALID_SECURITY_CODE_ERROR_MESSAGE = "Invalid Security Code. Please enter valid security code.";
	public static final String EMPTY_SECURITY_CODE_ERROR_MESSAGE = "Security Code Required. Please enter your security code.";
	public static final String BI_EXLUSIVE_MESSAGE_ANONYMOUS_USER = "You must be a Beauty Insider to qualify for this product, sign in now";
	public static final String ZIP_VALIDATION_ERROR_MESSAGE = "Were sorry. This item is not available within a 50.0 miles radius of your selected zip code. Please try your search again.";
	public static final String PHOTO_GALLERY_EMAIL_TITLE = "1. What is your email address?";
	public static final String PHOTO_GALLERY_PASSWORD_TITLE = "2. Do you have a sephora.com password?";
	public static final String HAZMAT_ALERT_MSG="This item contains some hazardous materials.";
	public static final String NONBI_BI_PROMO_MSG="Must be a Beauty Insider and purchase merchandise to qualify.";
	public static final String SALE_PRD_REMOVAL_MSG="Your promotion has been removed. Must purchase merchandise from the sale category to qualify. Available to US clients only.";
	public static final String NEED_ASSISTANCE_MSG="1-877-SEPHORA (1-877-737-4672)";
	public static final String NEED_ASSISTANCE_LABEL="Need Assistance?";
	public static final String BEAUTY_INSIDER_POINTS_MSG="You Have %d Beauty Insider Points";
	public static final String EMTPY_BASKET_VALID_PROMO_MSG="Buy merchandise to qualify. Availble to US clients only.";
	public static final String ITEMS_NOT_IN_STOCK_BASKET_MSG="Sorry, there are not enough items in stock to fulfill your order. We have updated the quantity in your basket to reflect the number we have available.";
	public static final String INVALID_PROVINCE_CITY_MESSAGE = "The City, State, and Zip combination provided does not match. Please correct address to continue.";
	public static final String ADDRESS_REQUIRED_MESSAGE = "Address Required. Please enter your street address.";
	public static final String CITY_REQUIRED_MESSAGE = "City Required. Please enter your City.";
	public static final String NON_SELECTED_ADDRESS_MESSAGE_SHIPPING_ADDRESS = "A shipping address has not been selected. Please select a shipping address.";
	public static final String PROVINCE_REQUIRED_ERROR_MESSAGE = "Province Required. Please select your province.";
	public static final String INVALID_POSTAL_ERROR_MESSAGE = "Invalid Postal Code. Please enter a Postal Code in format A1A1A1 or A1A 1A1";
	public static final String INVALID_PHONE_ERROR_MESSAGE = "Invalid Phone Number. Please enter valid phone number.";
	public static final String NON_BI_INSIDER_POINTS_MSG="Earn %d POINTS Today";
	public static final String PREVIOUSLY_USED_PROMO_MESSAGE = "Sorry, the promotion is no longer valid since you have already redeemed with a previous order.";
	public static final String PROMO_ERROR_MESSAGE_FOR_LESS_THAN_100$_CART= "Buy $100 of merchandise to qualify. Available to US clients only.";
	/*
	 * Character constans
	 */
	public static final String DOT = ".";
	public static final String FORWARD_SLASH = "//";
	public static final String DOLLAR = "$";
	public static final String BACK_SLASH = "\\";
	public static final String CANADIAN_PRICE = "C$";
	public static final String US_PRICE = "$";
	public static final String EMPTY_STRING = "";
	public static final String SPACE = " ";
	public static final String MINUS_DOLLAR = "-$";
	public static final String MINUS_CANADIAN_PRICE = "-C$";
	public static final String WEEK_DAYS[] = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};
	public static final String HYPHEN = "\\-";

	/*
	 * env related Keywords
	 */
	public static final String PDPPAGETYPE="PDP";
	public static final String BIPAGETYPE="BI";
	public static final String SKUIDTYPE="SKUID";
	public static final String BIABOUTPAGE="BIABOUT";
	public static final String BICARD="BICARD";
	public static final String HOMEPAGETYPE="HOME";
	public static final String QUICKLINKS="QuickLink";
	public static final String DISCOUNT = "DISCOUNT";
	public static final String SALE = "SALE";
	public static final String REWARDSAVAILABLE = "Available";
	public static final String REWARDSSELECTED = "Selected";
	public static final String ZERO_AVAILABLE_REWARDS = "0 Available";
	public static final String PROMOOPTIONAL = "Optional";
	public static final String SHIPPINGBREADCRUMB = "SHIPPING";
	public static final String PAYMENTBREADCRUMB = "PAYMENT";
	public static final String OVERVIEWBREADCRUMB = "OVERVIEW";
	public static final String REWARDS_AVAILABLE = "Available";
	public static final String REWARDS_SELECTED = "Selected";
	public static final String BREADCRUMBREDERROR = "complete error";
	public static final String BREADCRUMBCORRECT = "complete";
	public static final String BREADCRUMBCURRENT = "complete current";
	public static final String BREADCRUMB_DISABLED = "disabled";
	public static final String BREADCRUMB_CURRENT = "current";
	public static final String US_COUNTRY_NAME = "United States";
	public static final String CA_COUNTRY_NAME = "Canada";
	public static final String CANADA_FIND_STORE_FIELD_HINT = "Postal Code";
	public static final String POINT100 = "100";
	public static final String POINT250 = "250";
	public static final String POINT500 = "500";
	public static final String IOS = "iOS";
	public static final String ANDROID = "Android";
	public static final String STORE_SEARCH_KEYWORD="Newyork";
	public static final String DELIVERY_2DAYSHIPPING = "2 Day Shipping";
	public static final int MINIMUM_AMOUNT_FREE_SHIPPING = 50;
	public static final int MINIMUM_AMOUNT_FREE_SHIPPING_CANADA = 50;
	public static final String SEPHORA_GIFT_CARDS = "SEPHORA GIFT CARDS";
	public static final String SORT_NEW	="NEW";
	public static final String SORT_TOP	="TOP RATED";
	public static final String SORT_RELEVANCY	="RELEVANCY";
	public static final String SORT_BEST_SELLING= "BEST SELLING";
	public static final String SORT_EXCLUSIVE	="EXCLUSIVE";
	public static final String SORT_PRICE_LOW_TO_HIGH	= "PRICE LOW TO HIGH";
	public static final String SORT_BRAND_NAME	="BRAND NAME";
	public static final String SORT_PRICE_HIGH_TO_LOW = "PRICE HIGH TO LOW";
	public static final String REQUIRED = "Required";
	public static final String VIB_EXCLUSIVE = "VIB exclusive";
	public static final String VIB_ROUGE_EXCLUSIVE = "VIB Rouge exclusive";
	public static final String BODY_KEYWORD = "BODY";
	public static final String VALUESETS_KEYWORD = "VALUE";
	public static final String CANDLES_KEYWORD = "CANDLES";
	public static final String DESCRIPTION = "DESCRIPTION";
	public static final String DETAILS = "DETAILS";
	public static final String FAQ = "FAQ";
	public static final String SELECTED_KEYWORD = "selected";
	public static final String ON_KEYWORD = "ON";
	public static final String SSIT_COOKIE = "SSIT";
	public static final String DID_COOKIE = "DID";
	public static final String JSESSIONID_COOKIE = "JSESSIONID";
	public static final String RECOGNISE_USER = "recognise";
	public static final String ANONYMOUS_USER = "anonymous";
	public static final int NEW_USER_BDAY_LIES_WITHIN=7;
	/*
	 * env related keywords list
	 */
	public static final String GIFT_CARD_SHIPPING_OPTION[] = {"FREE  USPS First Class", "$10.95  2 Day Shipping", "$16.95  1 Day Shipping"};
	public static final String HAZMAT_SHIPPING_OPTION[] = {"$5.95  Standard Ground", "$5.95  USPS Ground"};
	public static final String CANADA_GIFTWRAP_OPTION[] = {"No Gift Packaging  FREE", "Add Gift Satchel  FREE"};
	public static final String HAZMAT_SHIPPING_OPTION_AK[] = {"$5.95  USPS Ground"};
	public static final String CANADA_SHIPPING_OPTION[] = {"C$7.95  Standard Shipping","C$11.95  Express Shipping"};
	public static final String CANADA_HAZAMAT_OPTION[]= {"FREE  Standard Ground"};
	public static final String SHIPPING_OPTION[] = {"$5.95  Standard 3 Day", "$10.95  2 Day Shipping", "$16.95  1 Day Shipping","$6.95  USPS Priority"};
	public static final String SHIPPING_PO_ADDRESS_LINE1 = "P.O. Box";
	public static final String SHIPPING_PO_OPTION[] = {"$5.95  USPS Ground"};
	public static final String GIFT_CARD_SHIPPING_APO_OPTION[] = {"FREE  USPS First Class"};
	public static final String GIFT_CARD_SHIPPING_AK_OPTION[] = {"FREE  USPS First Class", "$10.95  2 Day Shipping", "$16.95  1 Day Shipping - HI and AK"};
	public static final String SHIPPING_AK_OPTION[] = {"FREE  USPS Priority", "$10.95  2 Day Shipping", "$16.95  1 Day Shipping - HI and AK"};
	public static final String REVIEW_SORTING_LIST[] = {"newest first", "oldest first", "ratings high to low", "ratings low to high", 
			"helpfulness high to low", "helpfulness low to high"};
	public static final String LOVE_LIST_SORT_OPTION_LOWTOHIGH="price low to high";
	public static final String LOVE_LIST_SORT_OPTION_HIGHTOLOW="price high to low";
	public static final String DELIVERY_STANDARDS = "Standard 3 Day";
	public static final String DELIVERY_METHODS_HAZMAT[] = {"STANDARD GROUND", "USPS GROUND"};
	public static final String DELIVERY_METHODS_USPS_PRIORITY[]= {"USPS PRIORITY"};
	public static final String DEIVERY_METHOD_FLASH ="Flash - USPS Ground: FREE";
	public static final String DELIVERY_METHODS_FLASH[] = {"FLASH 2 DAY: FREE", "FLASH 1 DAY:", "FLASH - STANDARD GROUND: FREE", "FLASH - USPS PRIORITY: FREE"};
	public static final String CANADA_ROUGE_SHIPPING_OPTION[] = {"FREE - Rouge Free Shipping (3-6 business days. Delivery Monday-Friday","C$11,95 (strikethrough) C$7.95 - Rouge Express Shipping (1-3 business days. Delivery Monday-Friday)"};

	//public static final String DELIVERY_METHODS[] = {"FREE  Standard 3 Day","$10.95  2 Day Shipping","$16.95  1 Day Shipping","$6.95  USPS Priority"};
	public static final String SHIPPING_METHOD_OPTION[]={"Standard Ground","USPS Ground"};
	public static final String DELIVERY_METHODS[] = {"Standard 3 Day","2 Day Shipping","1 Day Shipping","USPS Priority"};
	public static final String GIFTWRAP_OPTION[] = {"Standard - FREE", "Gift Satchel - $2.00","Gift Box - $4.00"};
	public static final String GIFT_WRAP_MESSAGE = "This is Gift wrap message is added only for Testing purpose.";
	
	public static final String DELIVERY_METHODS_VIB_AK[] = {"USPS PRIORITY", "2 DAY SHIPPING", "1 DAY SHIPPING"};
	public static final String DELIVERY_METHODS_VIB_ROUGE_DPO[] = {"USPS PRIORITY"};
	public static final String DELIVERY_METHODS_VIB_ROUGE[] = {"USPS PRIORITY"};
	public static final String STORE_CREDIT = "Store Credit";
	public static final String GIFT_WRAP_STANDARDS = "No Gift Packaging";
	public static final String GIFT_CARD_MESSAGE = "This is Gift card added only for Testing purpose.";
	public static final String GIFT_METHODS[] = {"No Gift Packaging", "Add Gift Satchel", "Add Gift Box"};
	public static final String TOTAL_AREA[] = {"Merchandise Total","Discount","Merchandise Shipping", 
			"Tax","Gift Card Redeemed","Credit Card Payment", "Order Total"};
	public static final String TOTAL_AREA1[] = {"Merchandise Total","Merchandise Shipping", "Tax","Store Credit Redeemed","Gift Card Redeemed","Order Total"};

	public static final String TOTAL_AREA_GIFTCARD[] = {"Merchandise Total","Gift Card Purchase", 
			"Gift Card Shipping","Merchandise Shipping","Gift Wrap","Tax","Order Total"};
	public static final String NEW_ARRIVAL[] = {"Today", "New Arrivals", "new-beauty-products"};
	public static final String DAILY_OBSESSION[] = {"Today", "Daily Obsession", "todaysobsession"};
	public static final String MOBILE_OFFERS[] = {"Today", "Mobile Offers", "mobile-offers"};
	public static final String TODAY_FREE_SAMPLES[] = {"Today", "Free Samples", "samples"};
	public static final String SHOP[] = {"Shop", "", "shop"};
	public static final String My_Beauty_Bag_love_list[] = {"My Beauty Bag", "My Loves List", ""};
	public static final String My_Beauty_Bag_Purchase_History[] = {"My Beauty Bag", "Purchase History", ""};
	public static final String Beauty_Insider_Card[] = {"Beauty Insider", "Beauty Insider Card", "card"};
	public static final String About_Beauty_Insider[] = {"Beauty Insider", "About Beauty Insider", "about"};
	public static final String Rewards[] = {"Beauty Insider", "Rewards", "rewards"};
	public static final String Account_Details[] = {"My Account", "Account Details", "account"};
	public static final String Give_Feedback[] = {"Give Feedback", "", "submit-new-idea"};
	public static final String Customer_Service_Help[] = {"About Sephora", "Customer Service Help", "customerServiceHelp"};
	public static final String Sign_IN[] = {"My Account", "Sign in", ""};
	public static final String Register[] = {"My Account", "Register", ""};
	public static final String REGISTER_FIELDS_ASTERICKS[] = {"EMAIL", "FIRST NAME", "LAST NAME", "PASSWORD", "CONFIRM PASSWORD"};
	public static final String CANADA_PROVINCES[] = {"AB - Alberta", "BC - British Columbia", "MB - Manitoba", "NB - New Brunswick", "NL - Newfoundland and Labrador", "NS - Nova Scotia", "NT - Northwest Territories", "NU - Nunavut", "ON - Ontario","PE - Prince Edward Island","QC - Quebec","SK - Saskatchewan","YT - Yukon"};
    public static final String COUNTRY[] = {"US - United States","CA - Canada"};
	/*
	 * Users data
	 */
	public static final String INTERNATIONAL_USER[] = {"inter1@yopmail.com", "123456"};
	public static final String US_USER[] = {"test1.test1@gspann.com",	"123456"};
	public static final String US_USER1[] = {"test1@gspann.com",	"1"};
	public static final String CANADA_USER_ORDER[] = {"iryna.pardon@gmail.com", "123456"};
	public static final String US_USER_ODERS[] = {"address@default.com","123456"};
	public static final String EMPLOYEE_USER[] = {"m@qa.com","qwerty"};
	public static final String NEW_BI_USER[] = {"Testbiuserandroidapp@yopmail.com","123456", "TestBI"};
	public static final String NEW_BI_USER2[] = {"TestBIUsersanity@yopmail.com","123456", "TestBI"};
	public static final String US_USER_GIFTCARD[] = {"giftuser@yopmail.com","123456"};
	public static final String US_USER_GIFTCARD_AK[] = {"giftUser1@yopmail.com","123456"};
	public static final String USER_WITH_STORE_CREDIT[] = {"for@automation.com", "123456"};
	public static final String ANALYTICS_USER_US_BI[] = {"analytics_us_bi@yopmail.com", "123456"};
	public static final String ANALYTICS_USER_US_NON_BI[] = {"analytics_us_nonbi@yopmail.com", "123456"};
	public static final String ANALYTICS_USER_CA_ROUGE[] = {"analytics_ca_rouge@yopmail.com", "123456"};
	public static final String ANALYTICS_USER_FLASH[] = {"analytics_flash@yopmail.com", "123456"};
	public static final String NEW_USER_REGISTER[] = {"Tester", "usr", "123456", "The name of your childhood best friend?", "me", "October", "17", "1991"};
	public static final String NEW_USER_TWO[] = {"secd", "tie", "123456", "The name of your childhood best friend?", "me", "October", "17", "1991"};
	public static final String FLASH_USER[] = {"test1661119@yopmail.com","1234567890"};
	public static final String VIB_ROUGE_USER_NOT_ENROLL_IN_FLASH[] = {"testvibrouge5@yopmail.com","123456"};
	public static final String VIB_ROUGE_USER_ENROLL_IN_FLASH[] = {"testvibrouge2@yopmail.com","123456"};
	public static final String NON_VIB_ROUGE_USER[] = {"testnonvibrouge@yopmail.com","123456"};
	public static final String ROUGE_USER_CANADA[] = {"test12@yopmail.com","123456"};
	
	public static final String SECURITY_QUESTONS[] = {"Your grandmother's first name?", "Your mother's maiden name?", "The name of your elementary school?", "The name of your childhood best friend?", "The street name of your childhood home?"}; 
	public static final String INVALID_DATE[] = {"February", "31", "1991"};
	public static final String VALID_DATE[] = {"October", "17", "1991"};
//	public static final String CAPTCHA="123b93f75c"; Old value
	public static final String CAPTCHA="ff819cdc9f";
	public static final String INVALID_CAPTCHA="fBBBB";
	public static final String INVALIDCAPTCHA="QSEDD";
	public static final String PASSWORD = "123456";
	public static final String INVALID_PASSWORD = "123@1";
	public static final String WRONG_PASSWORD = "123rr4";
	public static final String MORETHAN33FIRSTNAME = "dshfhdhfhdfhdsdcxxsswewedsdwesdsfdfgfdfddsxcvcfgvfvcddsxzxsxzxdsssds";
	public static final String MORETHAN33LASTNAME = "dshfhdhfhdfhdsdcddsdsdsxxsswewedsdwesdsfdfgfdfddsxcvcfgvfvcddsxzxsxzxdsssds";
	public static final String MORE_THAN_33_SYMBOLS = "#$$#%#%#%#%#%#%#%#%#%#%#%#%#%#%#%!$";
	public static final String MORE_THAN_24_SYMBOLS = "@(#$$#%#%#%#%#%#%#%#%$$$*";
	public static final String SEARCH_KEYWORD[] = {"Lips", "Face" ,"Eye" , "Free" , "Gift" , "Tool","SKINS","Gift Card"};
	/*
	 * Address
	 */
	public static final String DUMMY_ADDRESS_ONE[] = {"7575 Gateway", "94560", "Newark", "5105059876", "CA"};
	public static final String DUMMY_ADDRESS_TWO[] = {"7577 Gateway", "10080", "5105059877"};
	public static final String DUMMY_ADDRESS_THREE[] = {"6352 Gateway", "15146", "6165053827"};
	public static final String DUMMY_ADDRESS_FOUR[] = {"6353 Gateway", "94561", "Oakley", "6165053828"};
	public static final String DUMMY_ADDRESS_FIVE[] = {"818 North Market Street", "19801", "1111111111", "DE - Delaware", "Wilmington"};
	public static final String ZIP_WITH_NOSTORES = "34567";
	public static final String ZIP_WITH_STORES = "94105";

	public static final String DUMMY_INTERNATIONAL_ADDRESS[] = {"Eberhard Wellhausen Wittekindshof Schulstrasse 4", "32547", "5105059876"};

	public static final String DEFAULTAKHICODE = "34099";
	public static final String AK_ZIPCODE = "96704";
	public static final String MULTICITY_ZIPCODE = "99505-9999";
	public static final String INVALID_POSTAL_CODE = "123456";
	public static final String VALID_POSTAL_CODE = "15146";
	public static final String UNITED_STATES = "United States";

	/*
	 * Credit Cards / Gift Cards
	 */
	public static final String DUMMY_CREDIT_CARD[] = {"5454545454545454", "123", "10", "2017"};
	public static final String DUMMY_CREDIT_CARD_TWO[] ={"4111111111111111", "123", "10", "2017"};
	public static final String DUMMY_CREDIT_CARD_THREE[] ={"5555555555554444", "123", "October", "2017"};
	public static final String DUMMY_CREDIT_CARD_FOUR[] ={"346059196238462", "1234", "October", "2017"};
	public static final String VISACARD = "4929101494308626";
	public static final String INVALID_SECURITY_CODE = "12";

	public static final String JCCARDTYPE = "98990006251";
	public static final String AMERICAN_EXPRESS = "346059196238462";
	public static final String DISCOVER_CARD =   "6011555926602498";
	public static final String MASTERCARD = "5454545454545454";
	public static final String MASTER_CARD = "MasterCard";
	public static final String GIFT_CARD_ONE[] ={"7777007552700612","44253746"};
	public static final String GIFT_CARD_TWO[] ={"7777007552699603","03737241"};
	public static final String INVALID_EXPIRE_DATE[] = {"01", "2015"};
	public static final String SECUIRTY_CREDITCARD = "222";
	
	public static final String CC_DETAILS[]={"3782 8224 6310 005", "TEST", "CREDITCARD", "1234"};

	/*
	 * Categories list
	 */
	public static final String TOP_LEVEL_CATEGORY[] = { "MAKEUP", "SKIN CARE", "FRAGRANCE", "BATH & BODY", "NAILS","HAIR",
			"TOOLS & BRUSHES", "MEN", "GIFTS", "BRANDS"};
	public static final String TOP_LEVEL_CATEGORY_NAME="FRAGRANCE";

	public static final String SECOND_LEVEL_CATEGORY_NAME="FACE MAKEUP";

	public static final String CATEGORY_MAKEUP = "MAKEUP";

	public static final String SUBCATEGORY_MAKEUP[] = { "Palettes & Value Sets","Face Makeup","Body & Camouflage Makeup","Eye Makeup"
			,"Lips","Nails"};

	public static final String 	SUBCATEGORY_MAKEUP_PALETTES[]={ "Makeup Palettes","Value Sets","Makeup Brush Sets"};

	public static final String SUBCATEGORY_MAKEUP_FACEMAKEUP[] = { "Foundation Sets","Foundation","Tinted Moisturizer","BB Cream & CC Cream", 
			"Face Primer","Face Powder","Concealer","Blush","Contour","Bronzer","Luminizer","Oil Control & Blotting Paper","Makeup Remover",
			"Makeup Sponges & Applicators","Face Brushes"};

	public static final String SUBCATEGORY_MAKEUP_EYEMAKEUP[] = { "Eyeshadow Palettes & Eye Sets","Mascara","Eyeliner","Eyeshadow" 
			,"Eyeshadow Primer & Base","Under Eye Concealer","Eyebrows","Eyelash Enhancers & Primers","Eyelash Curlers",
			"False Eyelashes","Eye Makeup Remover","Eye Brushes"};

	public static final String SUBCATEGORY_MAKEUP_LIPS[] = { "Lip Sets","Lipstick","Lip Gloss","Lip Plumper"
			,"Lip Stain","Lip Liner","Lip Balm & Treatments","Lip Brushes"};

	public static final String SUBCATEGORY_MAKEUP_NAILS[] = { "Nail Polish","Nail Kits","Nail Effects" 
			,"Nail Base Coats & Top Coats","Nail Treatment & Remover","Nail Stickers & Appliques","Gel Nails at Home",
	"Manicure & Pedicure Tools" };

	public static final String SUBCATEGORY_SKINCARE[] = { "Value Sets","Cleanse","Moisturize",
			"Treat","Masks","Sunscreen & Sun Protection","Self-Tanning","Other Needs" };

	public static final String SUBCATEGORY_SKINCARE_CLEANSE[] = { "Face Wash & Cleanser","Exfoliators","Toners",
			"Makeup Remover","Cleansing Brushes & Tools"};

	public static final String SUBCATEGORY_SKINCARE_MOISTURIZE[] = { "Moisturizer","Night Cream","Tinted Moisturizer",
			"Face Oil","Face Mist","Eye Cream","Decollete & Neck Cream","Lip Balm & Treatments"};

	public static final String SUBCATEGORY_SKINCARE_TREAT[] = {"Face Serum & Treatments","Blemish & Acne Treatments",
			"Face Masks","Facial Peels","Eye Treatments"};
	public static final String SUBCATEGORY_SKINCARE_SUNSCREENANDSUN[] = { "For Face","For Body" };

	public static final String SUBCATEGORY_SKINCARE_TANNING[] = {"Self-Tanner for Face","Self-Tanner for Body"};

	public static final String SUBCATEGORY_SKINCARE_OTHERS[] = { "Cellulite & Stretch Marks","Hair Removal","Teeth Whitening",
			"Mother & Baby","Supplements","Oil Control & Blotting Paper" };

	public static final String SUBCATEGORY_FRAGRANCE[] = { "Gift & Value Sets","Women","Men","Candles & Home Scents" };

	public static final String SUBCATEGORY_FRAGRANCE_GIFTANDVALUE[] = { "Cologne Gift Sets","Perfume Gift Sets"};

	public static final String SUBCATEGORY_FRAGRANCE_WOMEN[] = { "Perfume","Rollerballs & Travel Size","Lotions & Oils","Body Mist & Hair Mist",
	"Bath & Shower"};

	public static final String SUBCATEGORY_FRAGRANCE_MEN[] = {"Cologne","Body Sprays & Deodorant","Bath & Shower"};

	public static final String SUBCATEGORY_BATHANDBODY[] ={"Gift & Value Sets","Lotions & Creams","Bath & Shower",
			"Sun","Other Needs"};

	public static final String SUBCATEGORY_BATHANDBODY_LOTIONS[] = {"Body Lotions & Body Oils","Hand Cream & Foot Cream"};

	public static final String SUBCATEGORY_BATHANDBODY_BATHANDSHOWER[] = { "Body Wash & Shower Gel","Body Scrub & Exfoliants","Bath Soaks & Bubble Bath" };

	public static final String SUBCATEGORY_BATHANDBODY_SUN[] = {"Sunscreen","Self Tanner"};

	public static final String SUBCATEGORY_BATHANDBODY_OTHERS[] = {"Cellulite & Stretch Marks","Hair Removal & Shaving","Mother & Baby","Deodorant & Antiperspirant"};

	public static final String SUBCATEGORY_HAIR[]={"View All Hair","Value Sets","Styling Products & Tools","Treatment",
			"Shampoo & Conditioner","Best For","Other Needs"};

	public static final String SUBCATEGORY_HAIR_STYLINGPRODUCTS[]= {"View All Styling Products & Tools","Hairspray & Styling Products","Hair Dryers","Flat Irons, Curling Irons & Stylers" };

	public static final String SUBCATEGORY_HAIR_TREATMENTS[]={ "View All Treatment","Scalp & Hair Treatment","Hair Oil","Hair Thinning & Hair Loss" };

	public static final String SUBCATEGORY_HAIR_SHAMPOOANDCONDITION[] = { "View All Shampoo & Conditioner","Shampoo","Dry Shampoo","Conditioner"}; 

	public static final String SUBCATEGORY_HAIR_BESTFOR[] = { "View All Best For","Curls","Frizz","Shine","Texture","Volume"};

	public static final String SUBCATEGORY_HAIR_OTHERNEEDS[] = { "View All Other Needs","Hair Color & Root Touch Up","Hair Brushes & Combs","Hair Accessories","Shower Filters" };

	public static final String SUBCATEGORY_TOOLSANDBRUSHES[] = {"View All Tools & Brushes","Gifts & Value Sets","Professional Devices","Hair Styling Tools",
			"Makeup Brushes & Applicators","Small Tools","Makeup Bags & Travel Cases"};

	public static final String SUBCATEGORY_TOOLSANDBRUSHES_PROFESSIONALDEVICES[] = {"View All Professional Devices","Cleansing Brushes","Spa Tools","Hair Removal Tools",
	"Teeth Whitening"};

	public static final String SUBCATEGORY_TOOLSANDBRUSHES_HAIRSTYLING[] = {"View All Hair Styling Tools","Hair Dryers","Flat Irons, Curling Irons & Stylers","Shower Filters",
	"Hair Brushes & Combs"};

	public static final String SUBCATEGORY_TOOLSANDBRUSHES_MAKEUPBRSHES[] = {"View All Makeup Brushes & Applicators","Makeup Brush Sets","Face Brushes","Eye Brushes",
			"Lip Brushes","Makeup Sponges & Applicators","Makeup Brush Cleaners"};

	public static final String SUBCATEGORY_TOOLSANDBRUSHES_SMALLTOOLS[] = {"View All Small Tools","Manicure & Pedicure Tools","Tweezers & Eyebrow Tools","Eyelash Curlers",
			"Pencil Sharpeners","Mirrors"};

	public static final String SUBCATEGORY_MEN[] = {"Gift & Value Sets","Fragrance","Skin Care","Shaving","Hair","Other Needs"};

	public static final String SUBCATEGORY_MEN_GIFT[] = {"Fragrance Sets","Skincare Sets"};

	public static final String SUBCATEGORY_MEN_FRAGRANCE[] = { "Cologne","Deodorant for Men" };

	public static final String SUBCATEGORY_MEN_SKINCARE[] = { "Face Wash for Men","Moisturizer & Treatments","Sunscreen","Eye Cream" };

	public static final String SUBCATEGORY_MEN_SHAVING[] = { "Men's Grooming & Shaving","Aftershave" };

	public static final String SUBCATEGORY_MEN_HAIR[] = { "Shampoo & Conditioner","Hair Products for Men" };

	public static final String SUBCATEGORY_MEN_OTHERNEEDS[] = { "Body Products","Cleansing Brushes","Spa Tools","Hair Removal Tools","Teeth Whitening","Makeup for Men" };

	public static final String SUBCATEGORY_GIFTS[] = {"By Price","By Category","By Recipient","TOP-RATED GIFTS","EDITORS' PICKS","SEPHORA EXCLUSIVES","TOP TIER EXCLUSIVES"};

	public static final String SUBCATEGORY_GIFTS_PRICE[] ={"Value Sets","$10 and Under","$25 and Under","$50 and Under","$100 and Under"};

	public static final String SUBCATEGORY_GIFTS_CATEGORY[] ={"Makeup Palettes & Sets","Tools & Devices","Fragrance","Lip","Skin Care","Nail","Hair","Bath & Body","Brush Sets & Accessories"};

	public static final String SUBCATEGORY_GIFTS_RECIPIENT[] ={"For Her","For Him","For Teenagers"};

	public static final String SUBCATEGORY_GIFTS_MOREGIFTS[] = {"Top-Rated Gifts","Editors' Picks","Exclusives"};
 


	public static final String CATEGORY_FRAGNANCE = "FRAGNANCE";
	public static final String CATEGORY_TONERS = "TONERS";
	public static final String CATEGORY_EYELINER = "EYELINER";
	public static final String CATEGORY_BAREMINERALS = "BAREMINERALS";
	public static final String CATEGORY_TARTE = "TARTE";
	public static final String CATEGORY_HERM = "HERM";
	public static final String CATEGORY_VIEWALL = "VIEW ALL MAKEUP";
	public static final String CATEGORY_VIEWALL_FACEMAKEUP = "VIEW ALL FACE MAKEUP";
	public static final String SUBCATEGORY_BODYCAMOUFLAGE_MAKEUP = "Body & Camouflage Makeup";
	public static final String INLINESTRING = "meta name=\"robots\" content=\"noindex\"";
	public static final String BRAND_AERIN = "AERIN";
	public static final String PAGES[] = {"Home Page","PDP","My Account","BI","Store locator",
		"Beauty Bag","Search results page" , "Category","Love"};
	public static final String FIRSTNAMEWITHSPECIALCHAR = "Test@!@#$%#$";
	public static final String LASTNAMEWITHSPECIALCHAR = "Test@!@#$%#$";
	public static final String EMAILADDRESSLEGEND[] ={ "1. What is your email address?","2. Do you have a sephora.com password?"};
	public static final String SIGN_IN_EMAIL_HINT = "Email";
	public static final String SIGN_IN_PASSWORD_HINT = "Password";
	public static final String SIGN_IN_EMAIL_ALERT = "Please enter your email address.";
	public static final String SIGN_IN_PASSWORD_ALERT = "Please enter your password.";
	public static final String SIGN_IN_ALERT_MESSAGE = "We're sorry, there is an error with your email and/or password. Remember passwords are 4 to 12 characters (letters or numbers) long. Please try again.";
	public static final String SIGN_IN_INVALID_EMAIL_ALERT = "Please enter an e-mail address in the format username@domain.com.";
	public static final String SIGN_IN_EXISTING_EMAIL_ALERT = "An account already exists for";
	public static final String HAZMAT_NOT_SHIPPED_ERROR_MESSAGE = "Some items in your order cannot be shipped to your location due to hazardous material shipping restrictions. Please remove them from your basket or ship to a different address.";
	
	/*
	 * Colors
	 */
	public static final String GREEN_COLOR = "2, 252, 2";
	public static final String BLACK_COLOR = "0, 0, 0, 1";
	public static final String WHITE_COLOR = "f, f, f";
	public static final String BLACK_ZERO_OPACITY = "0, 0, 0, 0";
	public static final String CHECKBOX_GREEN_COLOR = "rgb(2, 221, 45)";
	public static final String CHECKBOX_GRAY_COLOR = "rgb(220, 220, 220)";
	/*
	 * Breadcrummb circle view
	 */
	
	public static final String BLACKCIRCLE_WITH_CHECKMARK = "Black circle with ckeck mark";
	public static final String BLACKCIRCLE_WITHOUT_CHECKMARK = "Black circle without check mark";
	public static final String HOLLOW_CIRCLE = "Hollow circle";
	public static final String CANADA_ALBERTA_STATE = "ON - Ontario";
	public static final String CANADA_DUMMY_ADDRESS[]={"CA - Canada","1234 CANADA", "M5B 2H1", "ON", "TORONTO", "5105059876"};
	public static final String CANADA_CITY = "Toronto";
	public static final String CANADA_POSTAl_CODE = "M5B 2H1";
	public static final String PO_ZIP_CODE = "02201";
	public static final String APO_ZIP_CODE = "94560";
	public static final String HUWAII_ZIP_CODE = "96814";
	public static final String MILITARY_ZIP_CODE = "09011";
	public static final String ALASKA_ZIP_CODE = "99612";
	public static final String RED_COLOR_RGB_CODE_FOR_ERROR = "rgba(254, 0, 0, 1)";
	public static final String RED_COLOR_RGB_CODE_FOR_SHIPPING_METHOD_TEXT = "rgba(204, 0, 0, 1)";
	public static final String HAZMAT_PRODUCT_PRICED_OVER_50_DOLLAR = "P391707";
	public static final String HAZMAT_PRODUCT_PRICED_OVER_50_DOLLAR_2 = "1643287";
	public static final String NON_HAZMAT_UNDER_50_DOLLAR = "1038454";
	public static final String FREE_USPS_PRIORITY = "FREE  USPS Priority\n(All states: 2-4 business days. Delivery Monday-Saturday)";
	public static final String FREE_USPS_PRIORITY_UPTO = "FREE  USPS Priority\n(Up to 2-4 business days. Delivery Monday-Saturday)";
	public static final String FREE_FLASH_2_DAY = "FREE  Flash 2 Day\n(2-3 business days. Delivery Monday-Friday)";

	public static final String FLASH_1_DAY = "$16.95 $5.95  Flash 1 Day\n(1-2 business days. Delivery Monday-Friday)";
	public static final String FREE_FLASH_STANDARD_GROUND = "FREE  Flash - Standard Ground\n(East Coast: 1-4 business days. West coast: 1-6 business days. Delivery Monday-Friday)";
	public static final String FREE_FLASH_STANDARD_GROUND_4_6_WEST_COAST = "FREE  Flash - Standard Ground\n(East Coast: 1-4 business days. West coast: 4-6 business days. Delivery Monday-Friday)";
	public static final String FREE_FLASH_USPS_PRIORITY = "FREE  Flash - USPS Priority\n(All states: 2-4 business days. Delivery Monday-Saturday)";
	public static final String FREE_FLASH_USPS_GROUND = "FREE  Flash - USPS Ground\n(All states: 2-4 business days. Delivery Monday-Saturday)";
	public static final String CANADA_STANDARD_SHIPPING = "C$7.95  Standard Shipping\n(3-6 business days. Delivery Monday-Friday)";
	public static final String CANADA_EXPRESS_SHIPPING = "C$11.95  Express Shipping\n(1-3 business days. Delivery Monday-Friday)";
	public static final String STANDARD_GROUND ="$5.95  Standard Ground\n(East Coast: 1-4 business days. West coast: 1-6 business days. Delivery Monday-Friday)";
	public static final String STANDARD_GROUND_4_6_WEST_COST_SHIPPING = "$5.95  Standard Ground\n(East Coast: 1-4 business days. West coast: 4-6 business days. Delivery Monday-Friday)";
	public static final String USPS_GROUND = "$5.95  USPS Ground\n(All states: 3-8 business days. Delivery Monday-Saturday)";
	public static final String CANADA_FREE_STANDARD_GROUNDD = "FREE  Standard Ground\n(2-10 business days. Delivery Monday-Friday)";
	public static final String STANDARD_3_DAY = "$5.95  Standard 3 Day\n(3-4 business days. Delivery Monday-Friday)";
	public static final String SHIPPING_2_DAY  ="$10.95  2 Day Shipping\n(2-3 business days. Delivery Monday-Friday)";
	public static final String SHIPPING_2_DAY_ALL_STATES  ="$10.95  2 Day Shipping\n(All states: 2-4 business days. Delivery Monday-Saturday)";
	public static final String SHIPPING_1_DAY  ="$16.95  1 Day Shipping\n(1-2 business days. Delivery Monday-Friday)";
	public static final String SHIPPING_1_DAY_HI_AK =  "$16.95  1 Day Shipping - HI and AK\n(1-2 business days. Delivery Monday-Friday)";
	public static final String USPS_PRIORITY  ="$6.95  USPS Priority\n(All states: 2-4 business days. Delivery Monday-Saturday)";
	public static final String FREE_STANDARD_3_DAY = "FREE  Standard 3 Day\n(3-4 business days. Delivery Monday-Friday)";
	public static final String FREE_USPS_GROUND = "FREE  USPS Ground\n(All states: 3-8 business days. Delivery Monday-Saturday)";
	public static final String ITEMALERT_MESSAGE = "Some items in your order cannot be shipped to your location due to hazardous material shipping restrictions. Please remove them from your basket or ship to a different address.";
	public static final String CANADA_HAZMAT_PRODUCT_OVER_50_DOLLAR = "787010";
	public static final String CANADA_HAZMAT_PRODUCT_LESS_50_DOLLAR = "1397900";

	public static final String FLASH_TERM_CONDITION ="By selecting 'Enroll to Flash', you accept Sephora FLASH Terms & Conditions";
	//for new checkout page
	public static final String INVALID_ZIP_CODE_MESSAGE = "Invalid ZIP code. Please enter a ZIP Code with 5 or 9 digits";
	public static final String ALASKA_ZIP_CODE_9_DIGIT = "995059999";
	
	public static final String VALID_5_DIGIT_ZIP_CITY_STATE[] = {"10080","NEW YORK", "NY"};
	public static final String VALID_9_DIGIT_ZIP_CITY_STATE[] = {"995059999","ANCHORAGE", "AK"};
	public static final String ZIP_CITY_STATE_NOT_MATCHED_ERROR_MESSAGE = "The City, State, and ZIP combination provided doesn't match. Please correct address to continue";
	public static final String CITY_FORT_RICHARDSON = "Fort Richardson";
	public static final String PO_BOX = "P.O. Box";
	public static final String CANADA_PROVINCE_LIST[] = {"Select","AB - Alberta","BC - British Columbia","MB - Manitoba","NB - New Brunswick","NL - Newfoundland and Labrador","NS - Nova Scotia","NT - Northwest Territories","NU - Nunavut"
		,"ON - Ontario","PE - Prince Edward Island","QC - Quebec","SK - Saskatchewan","YT - Yukon"};
	public static final String DUMMY_JAPAN_INTERNATIONAL_ADDRESS[] = {"Eberhard Wellhausen Wittekindshof Schulstrasse 4", "236-1", "5689411234"};
	public static final String COUNTRY_JAPAN = "Japan";
	public static final String CITY_JAPAN = "Tsunekuni";
	public static final String COUNTRY_RUSSIA = "Russia";
	public static final String DUMMY_RUSSIA_INTERNATIONAL_ADDRESS[] = {"14 Grafskiy Str", "129626", "74956680842"};
	public static final String CITY_RUSSIA = "Moscow";
	public static final String INVALID_ZIP_CODE = "19999";
	public static final String CANADA_INVALID_POSTAL_MESSAGE = "Invalid Postal Code. Please enter a Postal Code in format A1A1A1 or A1A 1A1";
	public static final String CANADA_INVALID_POSTAL_CODES[] ={"A","A1B43FV","AAAAAA","111111","1A1A1A","AAA111","AA11AA","A3D2E3"};
	public static final String INVALID_JCP_CARD = "9899955223";
	public static final String INVALID_DISCOVER_CARD = "6056756";
	
	
	/* Keyboard Constants*/
	
	public static final String DONE_KEY = "DONE";
	public static final String NEXT_KEY = "NEXT";
	public static final String METASTATE_DONE_NEXT = "16";
	
	public static final String SSIStatusOFF= "OFF";
	public static final String SSIStatusON= "ON";
	
	/* Scroll Contstants */
	
	public final static String UP = "up";
	public final static String DOWN = "down";
	public final static String LEFT = "left";
	public final static String RIGHT = "right";
	
	public final static int ERROR_CODE_404 = 404;
	public final static int ERROR_CODE_500 = 500;
	public final static int OK_CODE_200 = 200;
	
	public final static String PRICE_FREE = "FREE";
	
	public final static String dateFormat_MM_DD = "MM/dd";
	public final static String dateFormat_DD = "dd";
	public static final String dateFormat_yyyy_MM_dd = "yyyy-MM-dd";
	public static final String dateFormat_yyyyMMdd = "yyyyMMdd";
	public static final String dateFormat_MM_DD_YYYY = "MM/dd/yyyy";
	public static final String dateFormat_MM_DD_YY = "MM/dd/yy";
	public static final String dateFormat_MMdotDDdotYYYY = "MM.dd.yyyy";
	public static final String dateFormat_MMMM_d_yyyy = "MMMM d, yyyy";
	public static final String dateFormat_MMMM_dd_yyyy = "MMMM dd, yyyy";
	public static final String dateFormat_MMMM_d_time = "MMMM d, H:mm:ss";
	public static final String dateFormat_yyyy_MM_dd_HH_mm = "yyyy-MM-dd HH:mm";
	public static final String dateFormat_dd_MM_yyyy = "dd/MM/yyy";
	public static final String dateFormat_dd_MMM_yy = "dd-MMM-yy";
	public static final String dateFormat_YYYY = "yyyy";
	public static final String dateFormat_yyyy_line_MM_line_dd = "yyyy|MM|dd";
	public static final String dateFormat_h_mm_a = "h:mm a";
	public static final String dateFormat_day_in_week = "EEEE";
	public static final String dateFormat_yyyy_line_M_line_d = "yyyy|M|d";
	
	public static final Integer ZERO = 0;
	public static final Integer FIRST = 1;
	public static final Integer SECOND = 2;
	
}


