package com.sephora.core.driver.mobile;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.html5.Location;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.Response;
import org.openqa.selenium.security.UserAndPassword;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.applitools.eyes.Eyes;
import com.sephora.core.driver.SephoraDriver;
import com.sephora.core.utils.Config;
import com.sephora.test.androidApp.Base.SephoraBaseTest;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.MultiTouchAction;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDeviceActionShortcuts;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;

/**
 * SephoraMobileDriver extends implements Webdriver and
 * uses RemoteWebdriver for implementation, customized reusable
 * functions are added along with regular functions
 * 
 * @author AmarnathRayudu 
 */
public class SephoraMobileDriver  extends SephoraBaseTest implements SephoraDriver,WebDriver, MobileDriver {
	public static AppiumDriver driver;
	private Config config;
	private static int DEFAULT_TIMEOUT = 30;
	private static int MIN_DEFAULT_TIMEOUT=5;
	private static String platformUsed;	
	public Eyes eyes;
	private static AppiumDriverLocalService service=null;

	public SephoraMobileDriver(Config config) {
		this.config = config;
	}

	
	public AppiumDriver getDriver(){
		return driver;
	}
	private static final Logger logger = LogManager
			.getLogger(SephoraMobileDriver.class.getName());
	static DesiredCapabilities capabilities = new DesiredCapabilities();
	
	SephoraBaseTest sephoraAndroidAppBase = new SephoraBaseTest();
	/**
	 * @throws MalformedURLException
	 *             Prepares the environment that tests to be run on
	 */
	
	public void loadApplication() throws MalformedURLException {
	capabilities.setCapability("device", sephoraAndroidAppBase.device);
	capabilities.setCapability("platformName",sephoraAndroidAppBase.platformName);
	capabilities.setCapability("platformVersion",sephoraAndroidAppBase.platformVersion);
	capabilities.setCapability("deviceName",sephoraAndroidAppBase.deviceName);
	capabilities.setCapability("browserName",sephoraAndroidAppBase.browserName);
	capabilities.setCapability("autoAcceptAlerts", "true");
	capabilities.setCapability("newCommandTimeout", 120);
	capabilities.setCapability("resetKeyboard", true);
	capabilities.setCapability("unicodeKeyboard", true);
	
	URL remoteUrl = new URL("http://" + sephoraAndroidAppBase.Appium_Host
			+ "/wd/hub");
	logger.debug("Remote URL is " + remoteUrl);
	
	
	if(sephoraAndroidAppBase.platformName.contains("Android_App")){
		capabilities.setCapability("unicodeKeyboard", true);
		capabilities.setCapability("app", sephoraAndroidAppBase.appPath);
		capabilities.setCapability("app-package", sephoraAndroidAppBase.Package);
		capabilities.setCapability("app-activity", sephoraAndroidAppBase.AppActivity);
		driver = new AndroidDriver(remoteUrl, capabilities);
		
	}
	else if (sephoraAndroidAppBase.platformName.contains("iOS")){
		capabilities.setCapability("browserName",sephoraAndroidAppBase.browserName);
		//driver = new IOSDriver(remoteUrl, capabilities);
		driver.get(sephoraAndroidAppBase.baseUrl);
	}
	else{
		capabilities.setCapability("browserName",sephoraAndroidAppBase.browserName);
		driver = new AndroidDriver<WebElement>(remoteUrl, capabilities);
		driver.get(sephoraAndroidAppBase.baseUrl);
	}
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	
	platformUsed = sephoraAndroidAppBase.platformName;
}
	

	public String getURL() {
		return sephoraAndroidAppBase.baseUrl;
	}

	public void eyeOpen(String method) {
		eyes.setApiKey("6BWmnEYnUoEWYAqGlDdWSe6105sYc7C48L5TCwtOG106nNk110");
		eyes.open(driver, "Android test application", method);
	}
	public void eyeAbort() {
		eyes.abortIfNotClosed();
	}

	public void eyeClose() {
		eyes.close();
	}
	public void eyeCheckWindow() {
		eyes.checkWindow();
	}
	
	/**
	 * @param locator
	 * @return
	 */
	public boolean isElementPresent(By locator) {
		boolean IsPresent=false;
		turnOffImplicitWaits();
		if (driver.findElements(locator).size() > 0) {
			logger.debug("Element " + locator + " is present");
			IsPresent=true;
		} else{
			logger.debug("Element " + locator + " is not present");
			IsPresent= false;
		}
		turnOnImplicitWaits();
		return IsPresent;
		
	}

	public void waitForElementPresent(By locator) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, DEFAULT_TIMEOUT, 15);
			logger.debug("waiting for locator " + locator);
			wait.until(ExpectedConditions.presenceOfElementLocated(locator));
			logger.debug("Found locator "+ locator);
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	/**
	 * @param locator
	 *            Waits
	 */
	public void moveToELement(By locator) {
		Actions build = new Actions(driver);
		build.moveToElement(driver.findElement(locator)).build().perform();
	}

	public void get(String Url) {
		driver.get(Url);
	}

	public void click(By locator) {
		waitForElementToBeVisible(locator);
		logger.debug("trying to click: " + locator.toString());
		findElement(locator).click();
		logger.debug("clicked successfully: " + locator.toString());
		
	}

	public void type(By locator, String input) {
		findElement(locator).sendKeys(input);
	}


	public void type(By locator, Keys input) {
		findElement(locator).sendKeys(input);
	}


	public void quit() {
		driver.quit();
	}

	public String getCurrentUrl() {
		return driver.getCurrentUrl();
	}

	public String getTitle() {
		return driver.getTitle();
	}

	public List<WebElement> findElements(By by) {
		//		if(getPlatformUsed().equalsIgnoreCase(TestConstants.IOS))
		//		movetToElementJavascript(by);
		return driver.findElements(by);
	}
	public List<MobileElement> findMobileElements(By by) {
		//		if(getPlatformUsed().equalsIgnoreCase(TestConstants.IOS))
		//		movetToElementJavascript(by);
		return driver.findElements(by);
	}

	public WebElement findElement(By by) {
//		if(getPlatformUsed().equalsIgnoreCase(TestConstants.IOS))
//			movetToElementJavascript(by);
		return driver.findElement(by);
	}

	public WebElement findElementWithoutMove(By by) {
		// movetToElementJavascript(by);
		return driver.findElement(by);
	}

	public String getPageSource() {
		return driver.getPageSource();
	}

	public void close() {
		driver.close();
	}

	public Set<String> getWindowHandles() {
		return driver.getWindowHandles();
	}

	public String getWindowHandle() {
		return driver.getWindowHandle();
	}

	public TargetLocator switchTo() {
		return driver.switchTo();
	}

	public Navigation navigate() {
		return driver.navigate();

	}

	public Options manage() {
		return driver.manage();
	}

	public Select getSelect(By by) {
		return new Select(findElement(by));
	}

	public void scrollToBottomJS() throws InterruptedException {
		((JavascriptExecutor) driver)
		.executeScript("window.scrollTo(0,Math.max(document.documentElement.scrollHeight,document.body.scrollHeight,document.documentElement.clientHeight));");
	}

	public void clear(By by) {
		findElement(by).clear();
	}

	public void movetToElementJavascript(By locator) {
		((JavascriptExecutor) driver).executeScript("window.scrollTo(0,"+findElement(locator).getLocation().y+")");
	}

	public void movetToElementJavascript(WebElement webElement) {
		((JavascriptExecutor) driver).executeScript(
				"arguments[0].scrollIntoView(true);",
				webElement);
	}

	public String getAttribute(By by, String name) {
		String text = "";
		try{
			 text=findElement(by).getAttribute(name);
		}catch(Exception e){
			
		}
		return text;
	}

	/**
	 * 
	 * Purpose:This Method return text value of Element.
	 * 
	 * @author: Amitabh Rai
	 * @date:18-Feb-2015
	 * @returnType: String
	 * @param by
	 * @return
	 */
	public String getText(By by) {
		return findElement(by).getText().trim();
	}

	public void fnDragAndDrop(By by) throws InterruptedException {
		WebElement Slider = driver.findElement(by);
		Actions moveSlider = new Actions(driver);
		moveSlider.clickAndHold(Slider);
		moveSlider.dragAndDropBy(Slider, 3, 0).build().perform();
	}

	public Actions action() {
		return new Actions(driver);
	}

	public void pauseExecutionFor(long lTimeInMilliSeconds) {
		try {
			Thread.sleep(lTimeInMilliSeconds);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Checks if element is visible Purpose:
	 * 
	 * @author: Sunny Sachdeva
	 * @date:16-Feb-2015
	 * @returnType: WebElement
	 */
	public boolean IsElementVisible(WebElement element) {
		return element.isDisplayed() ? true : false;
	}

	/**
	 * 
	 * Purpose:Waits for element to be visible
	 * 
	 * @author: Sunny Sachdeva
	 * @date:16-Feb-2015
	 * @returnType: WebElement
	 */
	public boolean waitForElementToBeVisible(By locator, int timeOut) {
		WebDriverWait wait = new WebDriverWait(driver, timeOut);
		WebElement element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(locator));
		return IsElementVisible(element);
	}

	/**
	 * 
	 * Purpose: Waits and matches the exact title
	 * 
	 * @author: Sunny Sachdeva
	 * @date:17-Feb-2015
	 * @returnType: boolean
	 */
	public boolean IsTitleEqual(By locator, int timeOut, String title) {
		WebDriverWait wait = new WebDriverWait(driver, timeOut);
		return wait.until(ExpectedConditions.titleIs(title));
	}

	/**
	 * Waits and matches if title contains the text Purpose:
	 * 
	 * @author: Sunny Sachdeva
	 * @date:17-Feb-2015
	 * @returnType: boolean
	 */
	public boolean IsTitleContains(By locator, int timeOut, String title) {
		WebDriverWait wait = new WebDriverWait(driver, timeOut);
		return wait.until(ExpectedConditions.titleContains(title));
	}

	/**
	 * Wait for element to be clickable Purpose:
	 * 
	 * @author: Sunny Sachdeva
	 * @date:16-Feb-2015
	 * @returnType: WebElement
	 */
	public WebElement waitForElementToBeClickable(By locator, int timeOut) {
		WebDriverWait wait = new WebDriverWait(driver, timeOut);
		WebElement element = wait.until(ExpectedConditions
				.elementToBeClickable(locator));
		return element;
	}

	/**
	 * 
	 * Purpose:Wait for element to disappear
	 * 
	 * @author: Sunny Sachdeva
	 * @date:16-Feb-2015
	 * @returnType: boolean
	 */
	public boolean waitForElementToBeInVisible(By locator, int timeOut) {
		WebDriverWait wait = new WebDriverWait(driver, timeOut);
		return wait.until(ExpectedConditions
				.invisibilityOfElementLocated(locator));
	}

	public boolean waitForPageLoad() {
		boolean isLoaded = false;
		try {
			logger.debug("Waiting For Page load via JS");
			ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {
				public Boolean apply(WebDriver driver) {
					return ((JavascriptExecutor) driver).executeScript(
							"return document.readyState").equals("complete");
				}
			};
			WebDriverWait wait = new WebDriverWait(driver, DEFAULT_TIMEOUT);
			wait.until(pageLoadCondition);
			isLoaded = true;
		} catch (Exception e) {
			logger.error("Error Occured waiting for Page Load "
					+ driver.getCurrentUrl());
		}
		return isLoaded;
	}

	public void selectFromList(List<WebElement> lstElementList,
			String sValueToBeSelected) throws Exception {
		logger.debug("START OF FUNCTION->selectFromList");
		boolean flag = false;
		logger.debug("Total element found --> " + lstElementList.size());
		logger.debug("Value to be selected " + sValueToBeSelected + " from list"
				+ lstElementList);
		for (WebElement e : lstElementList) {
			logger.debug(">>>>>>>>>>>>>" + e.getText() + "<<<<<<<<<<<<<<<");
			if (e.getText().equalsIgnoreCase(sValueToBeSelected)) {
				logger.debug("Value matched in list as->" + e.getText());
				e.click();

				flag = true;
				break;
			}
		}
		if (flag == false) {
			throw new Exception("No match found in the list for value->"
					+ sValueToBeSelected);
		}
		logger.debug("END OF FUNCTION->selectFromList");
	}

	public WebElement waitForElementToBeClickable(WebElement element,
			int timeOut) {
		WebDriverWait wait = new WebDriverWait(driver, timeOut);
		wait.until(ExpectedConditions.elementToBeClickable(element));
		return element;
	}

	public WebElement waitForElementToBeClickable(WebElement element) {
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_TIMEOUT);
		wait.until(ExpectedConditions.elementToBeClickable(element));
		return element;
	}

	/**
	 * 
	 * Purpose: Helps in case of Stalement Exception
	 * 
	 * @author: Sunny Sachdeva
	 * @date:02-Mar-2015
	 * @returnType: boolean
	 */
	public boolean reTryClick(By by) {
		boolean result = false;
		int attempts = 0;
		while (attempts < 3) {
			try {
				driver.findElement(by).click();
				result = true;
				break;
			} catch (Exception e) {
			}
			attempts++;
		}
		return result;
	}

	public boolean deleteAllCookies(WebDriver driver) {
		boolean IsDeleted = false;
		try {
			driver.manage().deleteAllCookies();
			IsDeleted = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return IsDeleted;
	}


	public void turnOffImplicitWaits() {
		driver.manage().timeouts().implicitlyWait(MIN_DEFAULT_TIMEOUT, TimeUnit.SECONDS);
	}

	public void turnOnImplicitWaits() {
		driver.manage().timeouts().implicitlyWait(MIN_DEFAULT_TIMEOUT, TimeUnit.SECONDS);
	}

	public void clickByJS(By by) {
		moveToELement(by);
		WebElement element = driver.findElement(by);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", element);
	}

	public void clickByJS(WebElement element) {
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", element);
	}

	public String takeSnapShotAndRetPath(WebDriver driver) throws Exception {
		logger.debug("INTO METHOD->Fn_TakeSnapShotAndRetPath");
		String FullSnapShotFilePath = "";

		try {
			logger.debug("Take Screen shot started");
			File scrFile = ((TakesScreenshot) driver)
					.getScreenshotAs(OutputType.FILE);
			String sFilename = null;
			sFilename = "Screenshot-" + getDateTime() + ".png";
			FullSnapShotFilePath = System.getProperty("user.dir")
					+ "\\Output\\ScreenShots\\" + sFilename;
			FileUtils.copyFile(scrFile, new File(FullSnapShotFilePath));
		} catch (Exception e) {

		}

		return FullSnapShotFilePath;
	}

	/**
	 * Returns current Date Time
	 * 
	 * @return
	 */
	public void swipe(int startx, int starty, int endx, int endy, int duration) {
		driver.swipe(startx, starty, endx, endy, duration);
	}

	/**
	 * Returns current Date Time
	 * 
	 * @return
	 */
	public static String getDateTime() {
		String sDateTime = "";
		try {
			SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy");
			SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm:ss");
			Date now = new Date();
			String strDate = sdfDate.format(now);
			String strTime = sdfTime.format(now);
			strTime = strTime.replace(":", "-");
			sDateTime = "D" + strDate + "_T" + strTime;
		} catch (Exception e) {
			System.err.println(e);
		}
		return sDateTime;
	}

	/**
	 * 
	 * Purpose: Verified if click operation is successful by verifying after click Locator is present
	 * @author: Sunny Sachdeva 
	 * @date:10-Mar-2015
	 * @returnType: void
	 */

	public void verifyClick(By clickBy, By afterClickBy){
		int iAttempts=8;
		int iCount=0;
		do{
			clickByJS(clickBy);
			pauseExecutionFor(2000);
			iCount++;
		}
		while(iCount < iAttempts && (!isElementPresent(afterClickBy)) );
	}

	public void clickElement(WebElement element){
		waitForElementToBeClickable(element, DEFAULT_TIMEOUT).click();
		waitForPageLoad();
	}

	public boolean waitForElementToBeVisible(By locator) {
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_TIMEOUT);
		WebElement element = null;
		try {
			element = wait.ignoring(NoSuchElementException.class).ignoring(TimeoutException.class).until(ExpectedConditions
					.visibilityOfElementLocated(locator));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return IsLocatorVisible(locator);
	}

	public void navigateBack(){
		driver.navigate().back();
		/*waitForPageLoad();*/
	}

	public boolean quickCheckForElementPresent(By locator) {
		turnOffImplicitWaits();
		boolean IsElementPresent=false;
		if (driver.findElements(locator).size() > 0) {
			IsElementPresent=true;
		} 
		turnOnImplicitWaits();
		return IsElementPresent;
	}

	public List<WebElement> quickFindElements(By by) {
		turnOffImplicitWaits();
		List<WebElement> list=driver.findElements(by);
		turnOnImplicitWaits();
		return list;
	}

	public boolean waitForElementToBeVisible(WebElement element) {
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_TIMEOUT);
		wait.until(ExpectedConditions
				.visibilityOf(element));
		return IsElementVisible(element);
	}

	public void refreshBrowser(){
		driver.navigate().refresh();
		waitForPageLoad();
	}

	public boolean IsLocatorVisible(By by) {
		boolean IsVisible=false;
		try{
			turnOffImplicitWaits();
			IsVisible= findElement(by).isDisplayed();
		}catch(Exception e){

		}finally{
			turnOnImplicitWaits();
		}

		return IsVisible;
	}

	public void scrollToTopJS() throws InterruptedException {
		((JavascriptExecutor) driver)
		.executeScript("window.scrollTo(0,0);");
	}

	public void takeScreenShot(String fileName) throws Exception{
		String folder = "logs/screen/";
		WebDriver driver1 = new Augmenter().augment(driver);
		File file  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(file, new File(folder+fileName+".jpg"));
	}

	public boolean isElementClickable(WebElement element) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, MIN_DEFAULT_TIMEOUT);
			wait.until(ExpectedConditions.elementToBeClickable(element));
			return false;
		} catch(Exception e) {
			return false;
		}
	}

	public String getPlatformUsed() {
		return platformUsed;
	}

	public void fnDragAndDrop(By by,int x,int y) throws InterruptedException {
		WebElement Slider = driver.findElement(by);
		Actions moveSlider = new Actions(driver);
		moveSlider.clickAndHold(Slider);
		moveSlider.dragAndDropBy(Slider, x, y).build().perform();
	}

	public String getCSSValue(By locator, String propertyName) {
		return driver.findElement(locator).getCssValue(propertyName);
	}

	public void killChrome() throws Exception{
		Runtime.getRuntime().exec("cmd /c adb shell am force-stop com.android.chrome");
	}

	public void openChrome() throws Exception{
		Runtime.getRuntime().exec("cmd /c adb adb shell am start -n com.android.chrome/com.google.android.apps.chrome.Main");
	}

	public void authenticateUsing(String username, String password) {
		driver.switchTo().alert().authenticateUsing(new UserAndPassword(username, password));
	}

	public boolean IsLocatorEnable(By by){
		return driver.findElement(by).isEnabled();
	}

	public int getIntegerValueFromString(String value){
		int returnValue=Integer.parseInt(value.replaceAll("[^0-9.]", ""));
		return returnValue;
	}

	public String getTextFromElement(WebElement element){
		String text=element.getText().trim();
		return text;
	}

	public boolean waitForElementToStale(WebElement element, int timeOut) {
		WebDriverWait wait = new WebDriverWait(driver, timeOut);
		boolean IsStale=wait.until(ExpectedConditions.stalenessOf(element));
		return IsStale;
	}

	public void selectByValue(WebElement element,String value){
		Select select = new Select(element);
		select.selectByValue(value);
	}

	public String getSelectedValue(WebElement element){
		Select select = new Select(element);
		String text=getTextFromElement(select.getFirstSelectedOption());
		return text;
	}

	public boolean waitForElementToBeRemove(By by) {
		try {
			turnOffImplicitWaits();
			WebDriverWait wait = new WebDriverWait(driver,DEFAULT_TIMEOUT);
			boolean present = wait
					.ignoring(NoSuchElementException.class)
					.until(ExpectedConditions.invisibilityOfElementLocated(by));
			return present;
		} catch (Exception e) {
			return false;
		} finally {
			turnOnImplicitWaits();
		}
	}


	public void moveToElementAndClick(By by){
		WebElement element =findElement(by);
		Actions action = new Actions(driver);
		action.moveToElement(element).click().perform();
	}

	public String getAttribute(WebElement element, String name) {
		return element.getAttribute(name);
	}

	public int getCurrentYear(){
		Calendar now = Calendar.getInstance();   // Gets the current date and time
		int year = now.get(Calendar.YEAR);
		return year;
	}


	public void ClickByAction(By by){
		WebElement element=findElement(by);
		Actions action = new Actions(driver);
		action.moveToElement(element).click(element).build().perform();
		
	}


	@Override
	public void performMultiTouchAction(MultiTouchAction arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public TouchAction performTouchAction(TouchAction arg0) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public WebDriver context(String name) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Set<String> getContextHandles() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String getContext() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void rotate(ScreenOrientation orientation) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public ScreenOrientation getOrientation() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public WebElement findElementByAccessibilityId(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public List<WebElement> findElementsByAccessibilityId(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Location location() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void setLocation(Location location) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void hideKeyboard() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void pinch(WebElement arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void pinch(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void tap(int arg0, WebElement arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void tap(int arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void zoom(WebElement arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void zoom(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public byte[] pullFile(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public byte[] pullFolder(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void closeApp() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void installApp(String arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public boolean isAppInstalled(String arg0) {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public void launchApp() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void removeApp(String arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void resetApp() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void runAppInBackground(int arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public WebElement scrollTo(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public WebElement scrollToExact(String arg0) {
		scrollToExactText(arg0);
		logger.debug("Scroll to: "+ arg0 + " is done");
		return null;
	}


	public WebElement scrollToExactText(String str) {
		((AndroidDriver) driver).findElementByAndroidUIAutomator(
	            "new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().text(\""
	                    + str + "\").instance(0))");
		return null;
	}
	
	
	public void scrollTopToBottom(String start, String End){
		TouchAction tAction=new TouchAction(driver);
		int startx = driver.findElement(By.name(start)).getLocation().getX();
		int starty = driver.findElement(By.name(start)).getLocation().getY();
		int endx = driver.findElement(By.name(End)).getLocation().getX();
		int endy = driver.findElement(By.name(End)).getLocation().getY();
		System.out.println(startx + " ::::::: " + starty + " ::::::: " + endx +  " ::::::: " +	endy);

		//First tap on the screen and swipe it right using moveTo function
		tAction.press(startx+20,starty+20).moveTo(endx+20,endy+20).release().perform(); 
	}


	@Override
	public String getAppStrings() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String getAppStrings(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Response execute(String arg0, Map<String, ?> arg1) {
		// TODO Auto-generated method stub
		return null;
	}


	 public void swipe()
	    {  
	    JavascriptExecutor js = (JavascriptExecutor) driver;
	    HashMap<String, Double> swipeObject = new HashMap<String, Double>();
	    swipeObject.put("startX", 0.95);
	    swipeObject.put("startY", 0.5);
	    swipeObject.put("endX", 0.05);
	    swipeObject.put("endY", 0.5);
	    swipeObject.put("duration", 1.8);
	    js.executeScript("mobile: swipe", swipeObject);
	     }


	@Override
	public String getDeviceTime() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Map<String, String> getAppStringMap() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Map<String, String> getAppStringMap(String language) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Map<String, String> getAppStringMap(String language,
			String stringFile) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public String getScreenshot() throws IOException{
		String FullSnapShotFilePath = "";
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		String sFilename = "";
		sFilename = "Screenshot-" + getDateTime() + ".png";
		FullSnapShotFilePath = System.getProperty("user.dir")+ "\\Output\\ScreenShots\\" + sFilename;
		FileUtils.copyFile(scrFile, new File(FullSnapShotFilePath));
		return FullSnapShotFilePath;
	}
	
	public static void launchAppiumServer(int port){
		service = AppiumDriverLocalService
				.buildService(new AppiumServiceBuilder()
						//.usingDriverExecutable(new File("/Applications/Appium.app/Contents/Resources/node/bin/node"))
						.usingDriverExecutable(new File("c:\\Program Files\\nodejs\\node.exe"))
						//.withAppiumJS(new File("/Users/User/Documents/AppiumCode/appium/bin/appium.js"))
						.withAppiumJS(new File("C:\\Program Files (x86)\\Appium\\node_modules\\appium\\bin\\Appium.js"))
						//.usingAnyFreePort()
						.usingPort(port)
						.withIPAddress("127.0.0.1"));
		service.start();
	}
	
	public static void stopAppiumServer(){
		if(service!=null){
			service.stop();
		}
	}
	
}
