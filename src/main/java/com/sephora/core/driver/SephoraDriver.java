package com.sephora.core.driver;

import java.net.MalformedURLException;

public interface SephoraDriver {

	/**
	 * SephoraDriver enforces subclasses to implement
	 * loadApplicaiton method
	 * @throws MalformedURLException 
	 */
	
	public void loadApplication() throws MalformedURLException;
	
}
 