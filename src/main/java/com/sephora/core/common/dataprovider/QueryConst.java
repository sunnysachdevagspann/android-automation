package com.sephora.core.common.dataprovider;

public interface QueryConst {

	public static final String QA_TEMPLATE = "200006";
	public static final String QA_COLL_TEMPLATE = "200002";
	public static final String QA_VCOLL_TEMPLATE = "9100022";
	public static final String QA_PLIST = "plist32120019";
	public static final String QA_CA_PLIST = "plist38860029";
	public static final String PROD_TEMPLATE = "200006";
	public static final String PROD_PLIST = "plist32120019";
	public static final String PROD_COLL_TEMPLATE = "200002";
	public static final String PROD_VCOLL_TEMPLATE = "9100022";
	public static final String PROD_CA_PLIST = "plist38860029";
	public static final String SCHEMA_PARAM_CORE_DEV = "DEVE2E_ATGCORE";
	public static final String SCHEMA_PARAM_CATA_DEV = "DEVE2E_ATGCATA";
	public static final String SCHEMA_PARAM_CORE_QA = "ATGCORE";
	public static final String SCHEMA_PARAM_CATA_QA = "ATGCATA";	
	public static final String SCHEMA_PARAM_CORE_PROD = "ATGCORE";
	public static final String SCHEMA_PARAM_CATA_PROD = "ATGCATA";	
	public static final String SCHEMA_PARAM_CORE_EBF = "STG_ATGCORE";
	public static final String SCHEMA_PARAM_CATA_EBF = "STG_ATGCATA";	
	//SKU_TYPE
	public static final String SKU_TYPE_STANDART = "0";
	public static final String SKU_TYPE_GIFT_CARD = "1"; 
	public static final String SKU_TYPE_SAMPLE = "2";
	public static final String SKU_TYPE_E_CERTIFICATE = "3"; 
	public static final String SKU_TYPE_GIFT_WRAP_OPTION = "4"; 
	public static final String SKU_TYPE_GWP = "5" ;
	public static final String SKU_TYPE_SHIPPING_METHOD_SKU = "6"; 
	//BI_TYPE
	public static final String BI_TYPE_NONE = "2";
	public static final String BI_TYPE_POINTS_100 = "0"; 
	public static final String BI_TYPE_POINTS_500 = "1"; 
	public static final String BI_TYPE_BIRTHDAY_GIFT = "3"; 
	public static final String BI_TYPE_ROUGE_WELCOME_KIT = "8"; 

}
