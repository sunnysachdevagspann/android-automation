package com.sephora.core.common.dataprovider;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sephora.core.utils.Utils;


public class Query implements QueryConst{
	
	 private static final Logger logger = LogManager.getLogger(Query.class.getName());
	private static final String ADDITIONAL_INNER_JOIN = "ADDITIONAL_INNER_JOIN";
	private static final String ADDITIONAL_DEFAULT_SKU_JOIN = " INNER JOIN SCHEMA_PARAM_CATA.SEPH_PRODUCT PR ON  pr.DEF_SKU_ID = p_sku.sku_id";
	private static final String ADDITIONAL_ANY_SKU_JOIN = " INNER JOIN SCHEMA_PARAM_CATA.SEPH_PRODUCT PR ON  pr.product_id = dp.product_id";
	private static final String PRODUCT_SQL_STRING = " FROM SCHEMA_PARAM_CATA.DCS_SKU DS" +
			" INNER JOIN SCHEMA_PARAM_CATA.SEPH_SKU_MCS B ON DS.SKU_ID = B.SKU_ID" +
			" INNER JOIN SCHEMA_PARAM_CORE.DCS_INVENTORY ST ON DS.SKU_ID = ST.CATALOG_REF_ID" +
			" INNER JOIN SCHEMA_PARAM_CATA.SEPH_SKU P_SKU ON DS.SKU_ID = P_SKU.SKU_ID" +
			ADDITIONAL_DEFAULT_SKU_JOIN +
			" INNER JOIN SCHEMA_PARAM_CATA.SEPH_PRODUCT PR ON  pr.DEF_SKU_ID = p_sku.sku_id" +
			" INNER JOIN SCHEMA_PARAM_CATA.SEPH_SKU_FLAGS F ON DS.SKU_ID = F.SKU_ID " +
			" inner join SCHEMA_PARAM_CATA.dcs_product dp on p_sku.primary_product_id = dp.product_id" +
			ADDITIONAL_ANY_SKU_JOIN+
			ADDITIONAL_INNER_JOIN +
			" inner join SCHEMA_PARAM_CATA.seph_brand sb on PR.BRAND_ID = sb.brand_id" +
			" WHERE PR.is_enabled = 1 and F.is_enabled = 1 " +
			" and (ds.end_date is null or ds.end_date >= sysdate) " +
			" and p_sku.temp_deactivation is null " +
			" and st.on_hold = 0 and (st.stock_level > 3 and st.country_code = 'US')" +
			" and st.avail_status = 1004 " +
			" and P_SKU.SKU_STATE = 0 " +
			" and p_sku.primary_product_id is not null " +
			" and pr.temporary_deactive_dt is null " +
			" and (dp.end_date is null or dp.end_date >= sysdate) " +
			" and sb.enabled = 1 " +
			" and (sb.end_date is null or sb.end_date >= sysdate) " +
			" AND P_SKU.sku_type = " + SKU_TYPE_STANDART +
			" AND P_SKU.bi_type= " + BI_TYPE_NONE + " ";
	private static final String EGC_SQL_STRING = " FROM SCHEMA_PARAM_CORE.DCSPP_GIFTCERT"
			+ " WHERE LENGTH(GIFTCERTIFICATE_ID) = 6"
			+ " AND cancel_date is null"
			+ " AND amount_authorized = 0 ";
	public static final String EGC_SQL_RECIPIENT_EMAIL_STRING = "SELECT giftcertificate_id FROM SCHEMA_PARAM_CORE.DCSPP_GIFTCERT"
			+ " WHERE LENGTH(GIFTCERTIFICATE_ID) = 6"
			+ " AND cancel_date is null"
			+ " AND purchaser_id is not null "
			+ " AND amount = amount_remaining AND amount_remaining != 0 "
			+ " AND amount = 100 ";
	private static final String NOT_BI_EXCLUSIVE_PRODUCT = " and f.is_bi_only = 0";
	private static final String NOT_IN_STORE_ONLY_PRODUCT = " and f.is_store_only = '0'";
	public static final String QUERY_FOR_ORDERS = "select ao.order_id from SCHEMA_PARAM_CORE.seph_active_order ao, SCHEMA_PARAM_CORE.dcspp_order so where so.seph_order_status = %s and (so.SEPH_NEEDFRAUDCHECK='0' or so.SEPH_TOTAL_GIFTWRAP_PRICE = '0') and so.order_id = ao.order_id and so.SEPH_FLASH_ORDER_TYPE is null and so.SEPH_LOCALE = '%s' order by ao.date_ordered desc";
	//query for C-file check
	public static final String QUERY_FOR_SET_ORDER_STATUS = "update SCHEMA_PARAM_CORE.seph_active_order set status = '%s' where ORDER_ID='%s'";
	public static final String QUERY_FOR_GET_C_FILE_NAME = "select FILE_NAME from SCHEMA_PARAM_CORE.seph_active_order where ORDER_ID='%s'";
	public static final String QUERY_FOR_GET_DATA_FROM_SEPH_ACTIVE_ORDER = "select id,ship_to_city,ship_to_state,payment_type, giftwrap_type,gift_msg,giftcard_msg,carrier, tax, bill_to_first_name, bill_to_last_name from SCHEMA_PARAM_CORE.seph_active_order where order_id ='%s'";
	public static final String QUERY_FOR_GET_TAX_FROM_SEPH_ACTIVE_ORDER = "select sum(tax) as tax_value from SCHEMA_PARAM_CORE.seph_active_order where order_id ='%s'";
	public static final String QUERY_FOR_GET_DATA_FROM_SEPH_ACTIVE_ORDER_BY_ID = "select id,ship_to_city,ship_to_state,payment_type, giftwrap_type,gift_msg,giftcard_msg,carrier,tax,bill_to_first_name, bill_to_last_name, rel_to_code from SCHEMA_PARAM_CORE.seph_active_order where id ='%s'";
	public static final String QUERY_FOR_API = "select order_id, profile_id, state, creation_date,last_modified_date from SCHEMA_PARAM_CORE.dcspp_order where profile_id=(select id from SCHEMA_PARAM_CORE.dps_user where login='USER_EMAIL_FOR_API') order by creation_date desc";
	//query for get atg user id
	public static final String QUERY_FOR_ATG_USER_ID = "select ID from SCHEMA_PARAM_CORE.DPS_USER where EMAIL = '[email]'";
	public static final String QUERY_FOR_SEPH_USER_TABLE = "select [param] from ATGCORE.seph_user where user_id in (select id from ATGCORE.dps_user where email='[email]')";
	public static final String QUERY_FOR_BI_ACCOUNT_TABLE = "select ACT_TYPE, ACT_CARD_NUMBER, ACT_STATUS_CD, ACT_USA_ID from [DATA_BASE].[dbo].[BI_ACCOUNT] where act_usa_id = (select act_usa_id from [DATA_BASE].[dbo].[BI_ACCOUNT] where act_card_number = '[email]')";
	public static final String QUERY_FOR_BI_ACCOUNT_TABLE_WITH_PARAMETERS = "select [param] from [DATA_BASE].[dbo].[BI_ACCOUNT] where act_usa_id = (select act_usa_id from [DATA_BASE].[dbo].[BI_ACCOUNT] where act_card_number = '[email]')";
	public static final String QUERY_FOR_BI_ACCOUNT_TABLE_ACT_STATUS_CD = "select [param] from [DATA_BASE].[dbo].[BI_ACCOUNT] where act_usa_id = '[user_id]'";
	public static final String QUERY_FOR_CHECK_BI_ACCOUNT_ACTIVITY_TABLE_BY_ACA_USA_ID = "select [param] from [DATA_BASE].[dbo].[bi_account_activity] where aca_usa_id = '[aca_usa_id]'";
	public static final String QUERY_FOR_GET_ACCOUNT_WITH_ACTIVITY_FROM_BI_ACCOUNT_ACTIVITY_TABLE = "select top(1) [param] from [DATA_BASE].[dbo].[bi_account_activity] where aca_type_cd = '[aca_type_cd]'";
	public static final String QUERY_FOR_BI_ACCOUNT_TABLE_ACTIVE_CARD_NUMBER = "select ACT_CARD_NUMBER from [DATA_BASE].[dbo].[BI_ACCOUNT] where act_card_number = '[email]' and ACT_STATUS_CD = 'A'";
	public static final String QUERY_COMMON_FOR_BI_ACCOUNT_ACTIVITY_TABLE = "select [param] from [DATA_BASE].[dbo].[bi_account_activity] where [condition]";
	public static final String QUERY_FOR_ACT_ID = "select ACT_ID from [DATA_BASE].[dbo].[BI_ACCOUNT] where act_usa_id = (select act_usa_id from [DATA_BASE].[dbo].[BI_ACCOUNT] where act_card_number = '[email]')";
	public static final String QUERY_FOR_DPS_USER = "select [param] from SCHEMA_PARAM_CORE.DPS_USER where EMAIL = '[email]'";
	public static final String QUERY_FOR_BI_USER_ACCOUNT = "select [param] from [DATA_BASE].[dbo].[BI_USER_ACCOUNT] where [condition]";
	public static final String QUERY_FOR_SET_DEFAULT_PASSWORD = "update SCHEMA_PARAM_CORE.dps_user set password = 'e10adc3949ba59abbe56e057f20f883e' where email='[email]'";
	public static final String QUERY_FOR_USA_ID = "select act_usa_id from [DATA_BASE].[dbo].[BI_ACCOUNT] where act_card_number = '[email]'";
	public static final String QUERY_FOR_BI_PREFERENCE = "select PRF_NAME, PRF_VALUE from [DATA_BASE].[dbo].[BI_PREFERENCE] where prf_usa_id = (select act_usa_id from [DATA_BASE].[dbo].[BI_ACCOUNT] where act_card_number = '[email]')";
	public static final String QUERY_FOR_DATE_IN_BI_PREFERENCE = "select PRF_UPDATE_DATE from [DATA_BASE].[dbo].[BI_PREFERENCE] where prf_usa_id = (select act_usa_id from [DATA_BASE].[dbo].[BI_ACCOUNT] where act_card_number = '[email]')";
	public static final String QUERY_FOR_SEPH_SUBSCRIPTION = "select STORE_ZIPCODE, EMAIL_ADDRESS, SUBSCRIBE_DATE from SCHEMA_PARAM_CORE.SEPH_SUBSCRIPTION where EMAIL_ADDRESS= '[email]'";
	public static final String QUERY_FOR_SEPH_SUBSCRIPTION_DATE = "select to_char(SUBSCRIBE_DATE, 'yyyy-mm-dd hh24:mi:ss') my_date from SEPH_SUBSCRIPTION where EMAIL_ADDRESS= '[email]'";
	public static final String QUERY_FOR_SEPH_UNSUBSCRIPTION_DATE = "select to_char(SUBSCRIPTION_DATE, 'yyyy-mm-dd hh24:mi:ss') my_date from SCHEMA_PARAM_CORE.seph_unsubscription where EMAIL_ADDRESS = '[email]'";
	public static final String QUERY_FOR_SEPH_UNSUBSCRIPTION = "select EMAIL_ADDRESS,SUBSCRIPTION_DATE from SCHEMA_PARAM_CORE.seph_unsubscription where EMAIL_ADDRESS = '[email]'";
	public static final String QUERY_FOR_TBL_PROFILE_SUBSCRIPTION = "select [param] from [DATA_BASE].[dbo].[tbl_profile_subscription] with (nolock) where profile_email_id in (select profile_email_id from [DATA_BASE].[dbo].[tbl_profile_email] where email='[email]')";
	public static final String QUERY_FOR_TBL_PROFILE_PREFERENCES = "select [param] from [DATA_BASE].[dbo].[tbl_profile_preference] with (nolock) where profile_id in (select profile_id from [DATA_BASE].[dbo].[tbl_profile_email] where email='[email]')";
	public static final String QUERY_FOR_TBL_PROFILE = "select [param] from [DATA_BASE].[dbo].[tbl_profile] with (nolock) where profile_id in (select profile_id from [DATA_BASE].[dbo].[tbl_profile_email] where email='[email]')";
	public static final String QUERY_FOR_TBL_PROFILE_ACCOUNT = "select [param] from [DATA_BASE].[dbo].[tbl_profile_account] with (nolock) where profile_id in (select profile_id from [DATA_BASE].[dbo].[tbl_profile_email] where email='[email]') and acct_type_id=2";
	public static final String QUERY_FOR_TBL_PROFILE_SUMMARY = "select [param] from [DATA_BASE].[dbo].[tbl_profile_bi_summary] with (nolock) where profile_id in (select profile_id from [DATA_BASE].[dbo].[tbl_profile_email] where email='[email]')";
	public static final String QUERY_FOR_TBL_PROFILE_BI_TIER = "select [param] from [DATA_BASE].[dbo].[tbl_profile_bi_tier] with (nolock) where profile_id in (select profile_id from [DATA_BASE].[dbo].[tbl_profile_email] where email='[email]')";
	public static final String QUERY_FOR_TBL_PROFILE_PHONE = "select [param] from [DATA_BASE].[dbo].[tbl_profile_phone] with (nolock) where profile_id in (select profile_id from [DATA_BASE].[dbo].[tbl_profile_email] where email='[email]')";
	public static final String QUERY_FOR_TBL_PROFILE_ADDRESS = "select [param] from [DATA_BASE].[dbo].[tbl_profile_address] with (nolock) where profile_id in (select profile_id from [DATA_BASE].[dbo].[tbl_profile_email] where email='[email]')";
	public static final String QUERY_FOR_TBL_STATE = "select [param] from [DATA_BASE].[dbo].[tbl_state] with (nolock) where state_id in (select state_id from [DATA_BASE].[dbo].[tbl_profile_address] where profile_id in (select profile_id from [DATA_BASE].[dbo].[tbl_profile_email] where email='[email]'))";
	public static final String QUERY_FOR_CRM_ID = "select CRM_ID from SCHEMA_PARAM_CORE.SEPH_USER where USER_ID = (select id from SCHEMA_PARAM_CORE.DPS_USER where EMAIL = '[email]')";
	public static final String QUERY_FOR_SELECT_VIB_ACCOUNTS = "select top(10) scd.curr_qualify_date, ua.usa_search_email from [QA_RM_OND].[dbo].[SEG_CLIENT_DETAIL] scd join [QA_RM_OND].[dbo].[bi_user_account] ua on scd.usa_id=ua.usa_id where scd.curr_qualify_date >= '%s' and scd.curr_qualify_date < '%s'and scd.seg_id = 1 and scd.usa_id not in (select scd.usa_id from [QA_RM_OND].[dbo].[SEG_CLIENT_DETAIL] scd INNER JOIN [QA_RM_OND].[dbo].[CLIENT_SUMMARY] cs ON scd.usa_id=cs.usa_id where scd.seg_id = 2 and cs.amt_ytd >= 350)";
	public static final String QUERY_FOR_PHONE_NUMBER = "SELECT dcu.daytime_phone_num FROM SCHEMA_PARAM_CORE.dcs_user dcu INNER JOIN SCHEMA_PARAM_CORE.dps_user dpu ON dcu.user_id=dpu.ID WHERE dpu.email = '[email]'";
	public static final String QUERY_FOR_BI_PD_REGISTRATION_WITH_PARAM = "select statuscode from [DATA_BASE].[dbo].[bi_pd_registration] where email='[email]'";
	public static final String QUERY_FOR_BI_ADDRESS = "select UAD_ADDR1, UAD_ADDR2, UAD_STATE_CD, UAD_CITY, UAD_PCODE, UAD_CTC_FIRST_NAME, UAD_CTC_LAST_NAME from [DATA_BASE].[dbo].[bi_address] where UAD_USA_ID = (select act_usa_id from [DATA_BASE].[dbo].[BI_ACCOUNT] where act_card_number like '[email]')";
	public static final String QUERY_FOR_ACCOUNT_INFO_IN_ATG = "select DU.LOGIN, DU.FIRST_NAME, DU.LAST_NAME, DECODE (DU.GENDER, '0', 'Unknown','2', 'Male','1', 'Female') as GENDER,SU.ZIP_CODE as EMAIL_POSTACODE from SCHEMA_PARAM_CORE.DPS_USER DU,  SCHEMA_PARAM_CORE.SEPH_USER SU where SU.USER_ID=DU.id and DU.EMAIL='[email]'";
	public static final String QUERY_FOR_SEPH_EMAILCATALOG = "select ec.ADDRESS_LINE1, ec.ADDRESS_LINE2,  ec.CITY, ec.STATE, ec.ZIP_CODE from ATGCORE.SEPH_EMAILCATALOG EC where EC.id in (select SU.SEPHORACATALOG_ID from ATGCORE.SEPH_USER SU, DPS_USER DU  where SU.USER_ID=DU.id and EMAIL='[email]')";
	public static final String QUERY_SITE_DATE = "SELECT CONVERT(VARCHAR(16), DATEADD(HOUR, 7, GETDATE()), 120)";
	public static final String QUERY_US_DATE = "SELECT CONVERT(VARCHAR(16), DATEADD(HOUR, 0, GETDATE()), 120)";
	public static final String QUERY_FOR_BI_ACCOUNT_ATG_ID = "select ACT_CARD_NUMBER from [DATA_BASE].[dbo].[BI_ACCOUNT] where act_usa_id in (select act_usa_id from [DATA_BASE].[dbo].[BI_ACCOUNT] with (nolock) where act_card_number = '[email]') and (ACT_TYPE = 'W' or ACT_TYPE = 'B' )";
	public static final String SELECT_PARAM = "[param]";
	public static final String CONDITION = "[condition]";
	public static final String GROUP_BY = "group by %s";
	public static final String ORDER_BY = "order by %s";
	public static final String HAVING_COUNT = "having count (%s)";
	public static final String TOP = "top(%s)";
	public static final String SELECT_ALL_FIELDS_FOR_DPS_USER = "LOGIN, FIRST_NAME, MIDDLE_NAME, LAST_NAME, EMAIL";
	public static final String SELECT_ALL_FIELDS_FOR_BI_USER_ACCOUNT = "USA_CREATE_DT, USA_USERID, USA_FIRST_NAME, USA_MIDDLE_NAME, USA_LAST_NAME, USA_STATUS_CD, USA_BIRTH_DAY, USA_BIRTH_MONTH, USA_BIRTH_YEAR, USA_AGE_RANGE, USA_GENDER, USA_PHONE, USA_POSTAL_CODE, USA_SIGNUP_CHANNEL, USA_SEARCH_EMAIL, USA_SIGNUP_STORE_NUMBER";
	public static final String SELECT_ALL_FIELDS_FOR_TBL_PROFILE = "first_name, last_name, CASE gender_id WHEN '2' then 'Male' WHEN '1' then 'Female' END as gender, birth_day, birth_month, birth_year";
	public static final String QUERY_FOR_BI_ACCOUNT_ACTIVITY_TABLE = "select ACA_PRODUCT_SK from [DATA_BASE].[dbo].[bi_account_activity] act with (nolock) inner join [DATA_BASE].[dbo].[bi_account] acc on act.aca_act_id = acc.act_id where act.ACA_TYPE_CD = 'I' and  acc.act_usa_id in (select act_usa_id from [DATA_BASE].[dbo].[bi_account] with (nolock) where act_card_number = '[email]')";
	public static final String QUERY_FOR_BI_ACCOUNT_EVENT_TABLE_BY_CARD = "select USA_ID, NEW_VALUE, LOCATION_SK, TYPE from [DATA_BASE].[dbo].[BI_ACCOUNT_EVENT] where NEW_VALUE='[user_id]'";
	public static final String QUERY_FOR_CHECK_BI_ACCOUNT_ACTIVITY_TABLE = "select [param] from [DATA_BASE].[dbo].[bi_account_activity] act with (nolock) inner join [DATA_BASE].[dbo].[bi_account] acc on act.aca_act_id = acc.act_id where acc.act_usa_id in (select act_usa_id from [DATA_BASE].[dbo].[bi_account] with (nolock) where act_card_number = '[email]')";
	public static final String QUERY_FOR_UPDATE_BIRTH_MONTH = "update [DATA_BASE].[dbo].[BI_USER_ACCOUNT] set USA_BIRTH_MONTH = '[birth_month]' where usa_id = (select act_usa_id from [DATA_BASE].[dbo].[BI_ACCOUNT] where act_card_number like '[email]')";
	public static final String QUERY_FOR_UPDATE_QUESTION_VALUE = "update atgcore.seph_user set security_question='[question]' where user_id in (select id from SCHEMA_PARAM_CORE.dps_user where email='[email]')";
	public static final String QUERY_FOR_CHECK_AUDIT_RECORD = "select %s from SEPH_ESB.FEEDS_AUDIT where feed_name='%s' order by %s desc";
	public static final String QUERY_FOR_CHECK_AUDIT_RECORD_TWO_ORDER_PARAMETER = "select %s from SEPH_ESB.FEEDS_AUDIT where feed_name='%s' order by %s desc, %s desc";
	public static final String CONDITION_BY_EMAIL_FOR_BI_USER_ACCOUNT_TABLE = "usa_id = (select act_usa_id from [DATA_BASE].[dbo].[BI_ACCOUNT] where act_card_number like '[email]')";
	public static final String CONDITION_BY_USA_ID_FOR_BI_USER_ACCOUNT_TABLE = "usa_id = '[user_id]'";
	//param in activity table
	public static final String ACA_ID_PARAM =  "ACA_ID";
	public static final String ACA_USA_ID_PARAM = "ACA_USA_ID";
	public static final String ACA_YTD_CURRENT_BASE  = "ACA_YTD_CURRENT_BASE";
	public static final String ACA_PRODUCT_SK_PARAM = "ACA_PRODUCT_SK";
	public static final String ACA_TYPE_CD_PARAM = "ACA_TYPE_CD";
	public static final String ACA_LOCATION_SK_PARAM = "ACA_LOCATION_SK";
	public static final String ACA_NOTE_PARAM = "ACA_NOTE";
	public static final String ACA_CREATE_DT = "ACA_CREATE_DT";
	public static final String ACA_ATG_ORDER_ID = "ACA_ATG_ORDER_ID";
	public static final String ACA_ADJUST_AMT = "ACA_ADJUST_AMT";
	public static final String ACA_POINTS_CURRENT_BASE = "ACA_POINTS_CURRENT_BASE";
	public static final String ALL_PARAM = "*";
	// params for flash
	public static final String IS_FLASH_PARAM = "IS_FLASH";
	public static final String FLASH_EXPIRY_DATE_PARAM = "FLASH_EXPIRY_DATE";
	public static final String FLASH_CANCELED_DATE = "FLASH_CANCELED_DATE";
	//param in bi account table
	public static final String ACT_STATUS_CD_PARAM = "ACT_STATUS_CD";
	public static final String ACT_CARD_NUMBER_PARAM = "ACT_CARD_NUMBER";
	public static final String ACT_TYPE_PARAM = "ACT_TYPE";
	//param in bi user account table
	public static final String USA_SIGNUP_CHANNEL_PARAM = "USA_SIGNUP_CHANNEL";
	public static final String USA_STATUS_CD_PARAM = "USA_STATUS_CD";
	public static final String USA_CREATE_DT_PARAM = "USA_CREATE_DT";
	public static final String USA_SIGNUP_DATETIME_PARAM = "USA_SIGNUP_DATETIME";
	public static final String USA_MODIFY_DT_PARAM = "USA_MODIFY_DT";
	public static final String USA_SIGNUP_STORE_NUMBER_PARAM = "USA_SIGNUP_STORE_NUMBER";
	public static final String USA_ID_PARAM = "USA_ID";
	public static final String USA_GENDER_PARAM = "USA_GENDER";
	public static final String USA_FIRST_NAME_PARAM = "USA_FIRST_NAME";
	public static final String USA_LAST_NAME_PARAM = "USA_LAST_NAME";
	public static final String USA_BIRTH_DAY_PARAM = "USA_BIRTH_DAY";
	public static final String USA_BIRTH_MONTH_PARAM = "USA_BIRTH_MONTH";
	public static final String USA_BIRTH_YEAR_PARAM = "USA_BIRTH_YEAR";
	public static final String USA_PHONE_PARAM = "USA_PHONE";
	public static final String USA_POSTAL_CODE_PARAM = "USA_POSTAL_CODE";
	public static final String USA_SEARCH_EMAIL_PARAM = "USA_SEARCH_EMAIL";
	//param in bi_user_points table
	public static final String CURRENT_POINTS_PARAM = "CURRENT_POINTS";
	public static final String LIFETIME_POINTS_PARAM = "LIFETIME_POINTS";
	//param in seg_client_detail table
	public static final String CURR_QUALIFY_DATE_PARAM = "curr_qualify_date";
	//param in DPS_USER and SEPH_USER table
	public static final String FIRST_NAME_PARAM = "FIRST_NAME";
	public static final String LAST_NAME_PARAM = "LAST_NAME";
	public static final String ZIP_CODE_PARAM = "ZIP_CODE";
	//param in OBJECT table
	public static final String OBJ_STATUS_CD_PARAM = "OBJ_STATUS_CD";
	public static final String OBJ_MODIFY_DT_PARAM = "OBJ_MODIFY_DT";
	//param in profile database
	public static final String PROFILE_DB_SUBSCRIPT_TYPE_ID_PARAM = "subscript_type_id";
	public static final String PROFILE_DB_SUBSCRIPT_STATUS_ID_PARAM = "subscript_status_id";
	public static final String PROFILE_DB_LOCATION_COUNTRY_ID_PARAM = "location_country_id";
	public static final String PROFILE_DB_LAST_MODIFY_DATE_PARAM = "last_modify_date";
	public static final String PROFILE_DB_CREATE_DATE_PARAM = "create_date";
	public static final String PROFILE_DB_PREF_ID_PARAM = "pref_id";
	public static final String PROFILE_DB_BIRTH_YEAR_PARAM = "birth_year";
	public static final String PROFILE_DB_ACCT_ID_PARAM = "acct_id";
	public static final String PROFILE_DB_CREATE_SYSTEM_ID_PARAM = "create_system_id";
	public static final String PROFILE_DB_BI_TIER_ID_PARAM = "bi_tier_id";
	public static final String PROFILE_DB_PHONE_PARAM = "phone";
	public static final String PROFILE_DB_STATE_CD_PARAM = "state_cd";
	public static final String PROFILE_DB_FIRST_NAME_PARAM = "first_name";
	public static final String PROFILE_DB_LAST_NAME_PARAM = "last_name";
	public static final String PROFILE_DB_BIRTH_DAY_PARAM = "birth_day";
	public static final String PROFILE_DB_BIRTH_MONTH_PARAM = "birth_month";
	public static final String PROFILE_DB_ZIP_PARAM = "zip";
	//param in audit record 
	public static final String AUDIT_START_TIME = "START_TIME";
	public static final String AUDIT_END_TIME = "END_TIME";
	public static final String AUDIT_STATUS_CODE = "STATUS_CODE";
	public static final String AUDIT_DOCUMENT_ID = "document_id";
	//addition to condition for selection query (aca_type_cd_param)
	public static final String QUERY_ADDITION_FOR_CONDITION_ACA_TYPE_CD_PARAM = " and " + ACA_TYPE_CD_PARAM + " = '[TYPE]'";
	//addition to condition for selection query (aca_type_cd_param)
	public static final String QUERY_ADDITION_FOR_CONDITION_ACA_ORDER_ID = " and " + ACA_ATG_ORDER_ID + " = '[ORDER_ID]'";
	// verification for active sku (CSC)
	public static final String QUERY_FOR_VERIFICATION_ACTIVE_SKU = "SELECT DS.sku_id FROM SCHEMA_PARAM_CATA.DCS_SKU DS INNER JOIN SCHEMA_PARAM_CATA.SEPH_SKU_MCS B ON DS.SKU_ID = B.SKU_ID INNER JOIN SCHEMA_PARAM_CORE.DCS_INVENTORY ST ON DS.SKU_ID = ST.CATALOG_REF_ID INNER JOIN SCHEMA_PARAM_CATA.SEPH_SKU P_SKU ON DS.SKU_ID = P_SKU.SKU_ID INNER JOIN SCHEMA_PARAM_CATA.SEPH_PRODUCT PR ON P_SKU.PRIMARY_PRODUCT_ID = PR.PRODUCT_ID INNER JOIN SCHEMA_PARAM_CATA.SEPH_SKU_FLAGS F ON DS.SKU_ID = F.SKU_ID inner join SCHEMA_PARAM_CATA.dcs_product dp on p_sku.primary_product_id = dp.product_id  inner join SCHEMA_PARAM_CATA.seph_brand sb on PR.BRAND_ID = sb.brand_id WHERE PR.is_enabled = 1 and F.is_enabled = 1  and (ds.end_date is null or ds.end_date >= sysdate) and DS.sku_id = '[sku_id]' and p_sku.temp_deactivation is null  and st.on_hold = 0 and st.stock_level > 3  and st.avail_status = 1004 and p_sku.primary_product_id is not null  and pr.temporary_deactive_dt is null  and (dp.end_date is null or dp.end_date >= sysdate)  and sb.enabled = 1  and (sb.end_date is null or sb.end_date >= sysdate)  AND P_SKU.sku_type = 0 AND P_SKU.bi_type= 2 and pr.sephora_product_template = 'TEMPLATE_PARAM' AND DS.SKU_ID in (select sku.sku_id from SCHEMA_PARAM_CATA.seph_sku sku inner join SCHEMA_PARAM_CORE.dcs_price price on price.sku_id=sku.sku_id where  price_list='PLIST_PARAM')";
	//update credit card
	public static final String QUERY_FOR_CREDIT_CARD_ID = "select credit_card_id from SCHEMA_PARAM_CORE.dps_usr_creditcard where user_id in (select id from SCHEMA_PARAM_CORE.dps_user where login='[email]')";
	public static final String QUERY_FOR_UPDATE_CREDIT_CARD_EXP = "update SCHEMA_PARAM_CORE.dps_credit_card set expiration_year='2013' where id = '[user_id]'";
	//passbook check
	public static final String QUERY_FOR_BI_ACCOUNT_EVENT_TABLE = "SELECT [param] FROM [DATA_BASE].[dbo].[BI_ACCOUNT_EVENT] where atg_id =(select ACT_CARD_NUMBER from [DATA_BASE].[dbo].[BI_ACCOUNT] where act_usa_id in (select act_usa_id from [DATA_BASE].[dbo].[BI_ACCOUNT] with (nolock) where act_card_number = '[email]') and (ACT_TYPE = 'W' or ACT_TYPE = 'B' ))";
	public static final String ATG_ID_PARAM = "ATG_ID";
	public static final String CARD_NUMBER_PARAM = "CARD_NUMBER";
	public static final String CHANNEL_PARAM = "CHANNEL";
	public static final String EMAIL_PARAM = "EMAIL";
	public static final String DATE = "[DATE]";
	public static final String CREATE_DT_PARAM = "CREATE_DT";
	public static final String TYPE_PARAM = "TYPE";
	public static final String COMMENT_PARAM = "COMMENT";
	public static final String QUERY_FOR_LOCATION_SK = "select location_sk from [DATA_BASE].[dbo].[SMT_LOCATION] where location_number= 111";
	//fields for priority data check
	public static final String SELECT_PD_FIELDS_FOR_BI_USER_ACCOUNT = "USA_USERID, USA_FIRST_NAME, USA_LAST_NAME, USA_BIRTH_DAY, USA_BIRTH_MONTH, USA_BIRTH_YEAR, USA_AGE_RANGE, USA_GENDER, USA_PHONE, USA_POSTAL_CODE, USA_SIGNUP_CHANNEL, USA_SEARCH_EMAIL, USA_SIGNUP_STORE_NUMBER";
	public static final String SELECT_BIRTHDAY = "USA_BIRTH_DAY";
	public static final String SELECT_BIRTHMONTH = "USA_BIRTH_MONTH";
	public static final String SELECT_BIRTYEAR = "USA_BIRTH_YEAR";
	public static final String EMAIL_IN_QUERY = "[email]";
	public static final String QUESTION_IN_QUERY = "[question]";
	public static final String ID_IN_QUERY = "[user_id]";
	public static final String ACA_TYPE_CD_IN_QUERY = "[aca_type_cd]";
	public static final String ACA_USA_ID_IN_QUERY = "[aca_usa_id]";
	public static final String SKU_ID_IN_QUERY = "[sku_id]";
	public static final String BIRTH_MONTH_IN_QUERY = "[birth_month]";
	public static final String EXPIRATION_DAYS = "[days]";
	public static final String ACTIVITY_TYPE = "[TYPE]";
	public static final String PIN_NUMBER = "[PIN_NUMBER]";
	//fields for pos file check
	public static final String SELECT_CARRIER_ID = "carrier_id";
	public static final String SELECT_SHIPPING_METHOD = "shipping_method";
	public static final String SELECT_SHIPPING_SKU_ID = "sku_id";
	public static final String SELECT_POS_ORDER_ID = "pos_order_id";
	public static final String SELECT_ORDER_ID = "order_id";
	public static final String SELECT_COUNTRY_CODE = "country_code";
	//query for adding store credit.
	//replace 'ANY_UNIQUE_STORE_CREDIT_ID', 'AMOUNT', 'ATG_USER_ID'
	public static final String ANY_UNIQUE_STORE_CREDIT_ID = "ANY_UNIQUE_STORE_CREDIT_ID";
	public static final String AMOUNT = "AMOUNT";
	public static final String AMT_YTD = "Amt_YTD";
	public static final String ATG_USER_ID = "ATG_USER_ID";
	public static final String SEG_ID = "[SEG_ID]";
	public static final String QUERY_FOR_ATG_ADD_STORE_CREDIT_STEP_1 = "insert into SCHEMA_PARAM_CORE.dcspp_claimable values ('ANY_UNIQUE_STORE_CREDIT_ID','1','2','1', to_date ('11/03/" + Utils.getNextYear() + "', 'MM/DD/YYYY'))";
	public static final String QUERY_FOR_ATG_ADD_STORE_CREDIT_STEP_2 = "insert into SCHEMA_PARAM_CORE.dcs_storecred_clm values ('ANY_UNIQUE_STORE_CREDIT_ID','AMOUNT','0','AMOUNT','ATG_USER_ID',sysdate,null,null)";
	public static final String QUERY_SET_NO_PENDING_ACTION_STATE_TO_ORDER = "update SCHEMA_PARAM_CORE.dcspp_ship_group set state = 'NO_PENDING_ACTION' where order_ref = '%s'";
	public static final String QUERY_SET_FULFILLED_STATUS_TO_ORDER = "update SCHEMA_PARAM_CORE.dcspp_order set seph_order_status = '160' where order_id = '%s'";
	public static final String QUERY_SET_PROCESSING_STATE_TO_ORDER = "update SCHEMA_PARAM_CORE.dcspp_order set state = 'PROCESSING' where order_id = '%s'";
	public static final String QUERY_FOR_ORDER_STATUS = "select t.seph_order_status from SCHEMA_PARAM_CORE.dcspp_order t where t.order_id in ('%s')";
	public static final String QUERY_FOR_CANCEL_READY_ORDER = "update dcspp_ship_group set state = 'PENDING_CSR_REMOVE' where order_ref in ('%s') and state in ('PENDING_SHIPMENT')";
	public static final String QUERY_FOR_CANCEL_ONE_SHIPPING = "update dcspp_ship_group set state = 'PENDING_CSR_REMOVE' where shipping_group_id in (select SHIPPING_GROUP_ID from  dcspp_ship_group where order_ref in ('%s') and SHIPGRP_CLASS_TYPE='%s') and state in ('PENDING_SHIPMENT')";
	public static final String HARDGOOD_SHIPPING_GROUP = "hardgoodShippingGroup";
	//query for UPC
	public final static String QUERY_FOR_SEPH_UPC_CODE_TABLE = "select * from atgpub.SEPH_UPC_CODE  where ID = (select ID from atgpub.SEPHORA_IMPORT_UPC where UPUPCD = '[upc_number]')";
	public final static String QUERY_FOR_SEPHORA_IMPORT_UPC_TABLE = "select * from atgpub.SEPHORA_IMPORT_UPC where UPUPCD = '[upc_number]'";
	public final static String QUERY_FOR_SEPHORA_IMPORT_SKU = "select * from atgpub.sephora_import_sku where SKU_ID='[upc_number]'";
	//query for POS file checking
	public final static String QUERY_FOR_POS_ORDER_TABLE = "select [param] from atgcore.pos_order where order_id in('%s')";
	public final static String QUERY_FOR_DCSPP_SHIP_GROUP_TABLE = "select [param] from atgcore.dcspp_ship_group where order_ref in ('%s')";
	public final static String QUERY_FOR_SEPH_SHIPPING_RULE_TABLE = "select [param] from atgcore.SEPH_SHIPPING_RULE where shipping_rule_id in (select shipping_method from atgcore.dcspp_ship_group where order_ref in ('%s'))";
	public final static String QUERY_FOR_POS_ORDER_ITEM_TABLE = "select * from atgcore.pos_order_item where pos_order_id like '[param]%'";
	public final static String QUERY_FOR_POS_ORDER_TABLE_RESHIP = "select [param] from atgcore.pos_order where pos_order_id in ('%s')";
	
	//**query for BE ONLY. Do NOT use the in other application or FE
	@Deprecated
	// used '2014-01-01 23:43:22' instead getdate() because wrong time on bd server
	public static final String QUERY_FOR_MAKE_USER_VIB_SEG_USER_DETAIL = "insert into [DATA_BASE].[dbo].[SEG_CLIENT_DETAIL](usa_id, seg_id, curr_qualify_date) values ([user_id], 1, '[DATE]')";
	@Deprecated
	public static final String QUERY_FOR_MAKE_USER_ROUGE_SEG_USER_DETAIL = "insert into [DATA_BASE].[dbo].[SEG_CLIENT_DETAIL](usa_id, seg_id, curr_qualify_date) values ([user_id], 2, '[DATE]')";
	@Deprecated
	public static final String QUERY_FOR_MAKE_USER_VIB_SEG_USER_DETAIL_EXCEPTION = "insert into [DATA_BASE].[dbo].[seg_client_detail_exception](usa_id, exception_id, create_date) values ([user_id], 1, getdate())";
	@Deprecated
	public static final String QUERY_FOR_UPDATE_POINTS_BI_USER_POINTS_TABLE = "update [DATA_BASE].[dbo].[BI_User_Points] set CURRENT_POINTS = 'AMOUNT', LIFETIME_POINTS = 'AMOUNT' where  USA_ID = '[user_id]'";
	@Deprecated
	public static final String QUERY_FOR_SELECT_POINTS_BI_USER_POINTS_TABLE = "SELECT [param] from [DATA_BASE].[dbo].[BI_User_Points] where  USA_ID = '[user_id]'";
	@Deprecated
	public static final String QUERY_FOR_UPDATE_REDEMPTION_DATE_BI_ACCOUNT_ACTIVITY_TABLE = "update [DATA_BASE].[dbo].[bi_account_activity] set ACA_REDEMPTION_DATE ='[DATE]' where ACA_USA_ID in (select act_usa_id from [DATA_BASE].[dbo].[bi_account] where act_card_number = '[email]') and aca_type_cd='[TYPE]'";
	@Deprecated
	public static final String QUERY_FOR_ADD_USER_TO_BI_USER_POINTS = "insert into [DATA_BASE].[dbo].[BI_User_Points] (USA_ID,CURRENT_POINTS, LIFETIME_POINTS, MODIFY_DT) values ([user_id],AMOUNT,AMOUNT,getdate());";
	@Deprecated
	public static final String QUERY_FOR_UPDATE_POINTS_CLIENT_SUMMARY_TABLE = "update [DATA_BASE].[dbo].[CLIENT_SUMMARY] SET Amt_YTD = AMOUNT where USA_ID = (select ACT_USA_ID from [DATA_BASE].[dbo].[BI_ACCOUNT] where ACT_CARD_NUMBER = '[email]')";
	@Deprecated
	public static final String QUERY_FOR_UPDATE_CURR_QUALIFY_DATE = "update [DATA_BASE].[dbo].[SEG_CLIENT_DETAIL] set LAST_QUALIFY_DATE = null, CURR_QUALIFY_DATE = '[DATE]' where USA_ID = (select ACT_USA_ID from [DATA_BASE].[dbo].[BI_ACCOUNT] where ACT_CARD_NUMBER = '[email]')";
	@Deprecated
	public static final String QUERY_FOR_UPDATE_LAST_QUALIFY_DATE = "update [DATA_BASE].[dbo].[SEG_CLIENT_DETAIL] set LAST_QUALIFY_DATE ='[DATE]' where USA_ID = (select ACT_USA_ID from [DATA_BASE].[dbo].[BI_ACCOUNT] where ACT_CARD_NUMBER = '[email]')";
	@Deprecated
	public static final String QUERY_FOR_ADD_USER_TO_CLIENT_SUMMARY = "insert into [DATA_BASE].[dbo].[CLIENT_SUMMARY] (USA_ID,Amt_YTD, Create_Date, Update_Date) values ([user_id],AMOUNT,getdate(),getdate());";
	@Deprecated
	public static final String QUERY_FOR_DELETE_USER_FROM_SEG_CLIENT_DETAIL_TABLE = "delete [DATA_BASE].[dbo].[SEG_CLIENT_DETAIL] where USA_ID = (select ACT_USA_ID from [DATA_BASE].[dbo].[BI_ACCOUNT] where ACT_CARD_NUMBER = '[email]') AND seg_id = [SEG_ID]";
	public static final String QUERY_FOR_GET_USER_INFO_FROM_CLIENT_SUMMARY = "select PARAMETER from [DATA_BASE].[dbo].[CLIENT_SUMMARY] where USA_ID = '[user_id]'";
	public static final String QUERY_FOR_GET_USER_INFO_FROM_SEG_CLIENT_DETAIL = "select [param] from [DATA_BASE].[dbo].[SEG_CLIENT_DETAIL] where USA_ID = '[user_id]' AND seg_id = [SEG_ID]";
	public static final String QA_DB_NAME = "QA_RM_OND";
	public static final String QA2_DB_NAME = "QA_RM_OND2";
	public static final String EBF_DB_NAME = "PROD_RM";
	public static final String PROD_DB_NAME = "RPTBIDB";
	public static final String DEV_DB_NAME = "QA_RM_OND";
	public static final String QA_PROFILE_DB_NAME = "QA_CCMS";
	public static final String DEV_PROFILE_DB_NAME = "DEV_CCMS";
	public static final String DATA_BASE_IN_QUERY = "DATA_BASE";
	//**query for making flash user
	public static final String QUERY_FOR_MAKE_FLASH_ACCOUNT = "UPDATE SCHEMA_PARAM_CORE.seph_user t  SET t.IS_FLASH = 1, t.FLASH_SIGNUP_EXP_DATE = (current_date+10), t.flash_SIGNUP_DATE =(current_date-10), t.flash_expiry_date= (current_date+[days]), t.flash_user_type=0 WHERE t.user_id in (SELECT id FROM SCHEMA_PARAM_CORE.dps_user WHERE login = '[email]')";
	public static final String QUERY_FOR_GET_FLASH_ACCOUNT_EXP_DATE = "select t.flash_expiry_date FROM SCHEMA_PARAM_CORE.seph_user t WHERE t.user_id in (SELECT id FROM SCHEMA_PARAM_CORE.dps_user WHERE login = '[email]')";
	public static final String QUERY_FOR_SKU_HAZMAT_STATUS = "select is_hazmat from SCHEMA_PARAM_CATA.seph_sku_flags where sku_id='%s'";
	public static final String QUERY_FOR_GET_OUT_OF_STOCK_TREATMENT_STATUS = "select out_of_stock_treatment from SCHEMA_PARAM_CATA.seph_sku_flags where sku_id='%s'";
	public static final String QUERY_FOR_INTERNATIONAL_GIFT_WRAP_PRICE = "select value from SCHEMA_PARAM_CORE.e4x_fx_rates where shopper_currency = '%s'";
	public static final String QUERY_FOR_INTERNATIONAL_GIFT_WRAP_PRICE_ROUND = "select round_method from SCHEMA_PARAM_CORE.e4x_fx_rates where shopper_currency = '%s'";
	public static final String QUERY_FOR_UPDATE_STOCK_LEVEL = "update SCHEMA_PARAM_CORE.dcs_inventory set stock_level = '%s' where catalog_ref_id in ('%s') and country_code = 'US'";
	public static final String QUERY_FOR_UPDATE_STOCK_LEVEL_ON_CANADA = "update SCHEMA_PARAM_CORE.dcs_inventory set stock_level = '%s' where catalog_ref_id in ('%s') and country_code = 'CA' and oms_modify_date is not null";
	public static final String QUERY_FOR_UPDATE_SEPHORA_STOCK = "update seph_store_inventory t set t.quantity = '%s' where sku_id in ('%s')";
	public static final String QUERY_FOR_GET_STOCK_LEVEL = "select stock_level from SCHEMA_PARAM_CORE.dcs_inventory where catalog_ref_id in ('%s') and country_code = '%s'";
	public static final String QUERY_FOR_UPDATE_CREDIT_CARD_EXPIRATION = "update SCHEMA_PARAM_CORE.dps_credit_card set expiration_year='%s' where id in (select CREDIT_CARD_ID from SCHEMA_PARAM_CORE.dps_usr_creditcard where user_id in (select id from SCHEMA_PARAM_CORE.dps_user where login='%s'))";
	public static final String QUERY_FOR_TAX_RATE = "select combstax from SCHEMA_PARAM_CORE.seph_tax_rate t where t.zip = '%s' and t.taxable is not null";
	public static final String QUERY_FOR_SKU_FROM_SKINIQ = "select sku.SKU_ID from SCHEMA_PARAM_CATA.SEPH_SKU sku inner join SCHEMA_PARAM_CATA.SEPH_SKU_SKIN_TYPE skin on (sku.sku_id = skin.sku_id)where sku.IQ_PRIMARY_CONCERN in (select con.CONCERNs_REFINEMENT_ID from SCHEMA_PARAM_CATA.SEPH_CONCERNS_REFINEMENT con where con.VALUE in ('%s')) and skin.skin_type_id in (select skin_type_id from SCHEMA_PARAM_CATA.SEPH_SKIN_TYPE s where s.value in ('%s')) and sku.IQ_APPLICATION_ID in (select iq.IQ_APPLICATION_ID from SCHEMA_PARAM_CATA.SEPH_IQ_APPLICATION iq where (iq.VALUE in ('%s')))group by sku.SKU_ID";
	public static final String QUERY_FOR_PRODUCT_FROM_CATEGORY = "Select product_id from SCHEMA_PARAM_CATA.SEPH_PRODUCT where def_parent_cat_id = '%s'";
	public static final String QUERY_FOR_RESTRICTED_COUNTRY_FOR_SKU = "select sc.country_code, bc.country_code from SCHEMA_PARAM_CATA.dcs_sku s, SCHEMA_PARAM_CATA.seph_sku ss, SCHEMA_PARAM_CATA.Seph_Product sp, SCHEMA_PARAM_CATA.seph_sku_countrycode scr, SCHEMA_PARAM_CATA.Seph_Country_Code sc, SCHEMA_PARAM_CATA.Seph_Brand_Countrycode bcr, SCHEMA_PARAM_CATA.Seph_Country_Code bc where ss.sku_id = s.sku_id and sc.country_code_id (+) = scr.country_code_id and scr.sku_id (+) = s.sku_id and ss.primary_product_id = sp.product_id (+) and bcr.brand_id (+) = sp.brand_id and bc.country_code_id (+) = bcr.country_code_id and s.sku_id = '%s' order by s.sku_id, bcr.sequence_num, scr.sequence_num";
	public static final String QUERY_FOR_GET_SKU_PRICE = "select list_price from SCHEMA_PARAM_CORE.dcs_price where sku_id='%s' and price_list='PLIST_PARAM'";
	public static final String QUERY_GET_ID_BY_SKU = "SELECT PRIMARY_PRODUCT_ID from SCHEMA_PARAM_CATA.SEPH_SKU where SKU_ID ='%s'";
	public static final String QUERY_GET_SKU_BY_ID = "SELECT SKU_ID from SCHEMA_PARAM_CATA.SEPH_SKU where PRIMARY_PRODUCT_ID ='%s'";
	public static final String QUERY_GET_PRIMARY_SKU_BY_ID = "SELECT DEF_SKU_ID from SCHEMA_PARAM_CATA.SEPH_PRODUCT where PRODUCT_ID ='%s'";
	public static final String QUERY_GET_FULL_SIZE_SKU_BY_SKU_ID = "SELECT FULL_SIZE_SKU_LINK from SCHEMA_PARAM_CATA.SEPH_SKU where SKU_ID = '%s'";
	public static final String QUERY_FOR_GET_SKU_DESCRIPTION = "select SKU_DESCRIPTION from [DATA_BASE].[dbo].[SMT_PRODUCT] where sku_number = [sku_id]";
	public static final String QUERY_FOR_CHECK_IF_SKU_ACTIVE = "select [param] from [DATA_BASE].[dbo].[OBJECT] where OBJ_LABEL='[sku_id]' and OBJ_PARENT_ID=(select OBJ_ID from [DATA_BASE].[dbo].[OBJECT] where OBJ_LABEL='%s')";
	public static final String QUERY_FOR_GETTING_SKU_TYPE = "SELECT SKU_TYPE from SCHEMA_PARAM_CATA.SEPH_SKU where SKU_ID ='%s'";
	public static final String QUERY_FOR_GETTING_FLASH_FLAG = "select %s from ATGCORE.seph_user where user_id in (select id from ATGCORE.dps_user where email='%s')";
	public static final String QUERY_FOR_UPDATE_FLASH_ENROLLED_DATE = "update SCHEMA_PARAM_CORE.seph_user set flash_user_type=0, IS_FLASH = 1, FLASH_SIGNUP_EXP_DATE = (current_date+10), FLASH_SIGNUP_DATE='%s', FLASH_EXPIRY_DATE = '%s' where user_id in (select id from SCHEMA_PARAM_CORE.dps_user where email='%s')";
	//Play! queries
	public static final String QUERY_FOR_SEND_PLAY_INVITATION = "INSERT INTO SCHEMA_PARAM_CORE.SEPH_INVITATION values ('%s', '[email]','100001', sysdate + 15, '0',null, 'null', sysdate - 1)";
	public static final String QUERY_FOR_CANCEL_PLAY_SUBSCRIPTION = "UPDATE SCHEMA_PARAM_CORE.DBCPP_SCHED_ORDER set state = 1 where profile_id = (select id from SCHEMA_PARAM_CORE.dps_user where login = '[email]')";
	public static final String QUERY_FOR_CHECK_IF_PLAY_INVITATION_ALREADY_EXIST = "SELECT * FROM SCHEMA_PARAM_CORE.SEPH_INVITATION  WHERE ID = %s";
	public static final String QUERY_FOR_GETTING_IS_CLAIMABLE_FOR_SUBSCRIPTION_PLAY = "SELECT IS_CLAIMED from SCHEMA_PARAM_CORE.SEPH_INVITATION where EMAIL ='%s'";
	public static final String QUERY_FOR_GETTING_PLAYBOX_ORDER_NUMBER = "SELECT ORDER_ID from SCHEMA_PARAM_CORE.dcspp_order WHERE profile_id = (select id from SCHEMA_PARAM_CORE.dps_user where login = '[email]') AND sales_channel = 7 ORDER BY order_id desc";
	public static final String QUERY_FOR_SET_PLAY_STATUS_TO_TEMPLATE_ORDER = "update SCHEMA_PARAM_CORE.dcspp_order set seph_order_status = '300', state = 'TEMPLATE' where order_id = '%s'";
	public static final String QUERY_FOR_UPDATE_SCHEDULED_DATE_TO_TEMPLATE_ORDER = "update SCHEMA_PARAM_CORE.dbcpp_sched_order set state = 0, next_scheduled = SYSDATE - 1 where profile_id = (select id from SCHEMA_PARAM_CORE.dps_user where login = '[email]')";
	public static final String QUERY_FOR_GET_MONTHLY_PLAYBOX_SKY = "select playbox_sku_id from SCHEMA_PARAM_CATA.seph_sku_playbox where playbox_month = '%s'";
	public static final String QUERY_FOR_SET_ORDER_STATUS_AND_TEMPLATE_FOR_PLAY_ORDER = "UPDATE SCHEMA_PARAM_CORE.DCSPP_ORDER SET SEPH_ORDER_STATUS = '132', STATE = 'TEMPLATE', SEPH_NEEDFRAUDCHECK = '0' WHERE ORDER_ID = '%s'";
	public static final String QUERY_FOR_SET_ORDER_STATUS_132 = "UPDATE SCHEMA_PARAM_CORE.DCSPP_ORDER SET SEPH_ORDER_STATUS = '132', SEPH_NEEDFRAUDCHECK = '0' WHERE ORDER_ID = '%s'";
	public static final String QUERY_ORDER_PLAY_BOX = "SELECT * FROM SCHEMA_PARAM_CORE.DBCPP_SCHED_CLONE WHERE SCHEDULED_ORDER_ID IN (SELECT SCHEDULED_ORDER_ID FROM DBCPP_SCHED_ORDER WHERE TEMPLATE_ORDER = '%s')";
	public static final String QUERY_FOR_UPDATE_NEXT_SCHEDULED_DATE_TO_TEMPLATE_ORDER = "update SCHEMA_PARAM_CORE.dbcpp_sched_order set next_scheduled = sysdate where template_order in ('%s')";
	public static final String QUERY_FOR_ORDER_STATE = "select t.state from SCHEMA_PARAM_CORE.dcspp_order t where t.order_id in ('%s')";
	public static final String QUERY_FOR_ORDER_CLONE_STATE = "SELECT DECODE( STATE, 0, 'ACTIVE', 1, 'INACTIVE', 2, 'ERROR') AS CLONED_STATE FROM DBCPP_SCHED_ORDER WHERE TEMPLATE_ORDER = '%s'";
	//Holiday rewards queries
	public static final String QUERY_FOR_DELETE_HOLIDAY_REWARDS_FROM_TABLE = "delete SCHEMA_PARAM_CORE.seph_holiday_reward_card_info where CARD_NUMBER_START_RANGE=[PIN_NUMBER]";
	public static final String QUERY_FOR_GET_GIFT_CARD_FROM_HOLIDAY_REWARDS_TABLE = "select HOLIDAY_REWARD_CARD_INFO_ID from SCHEMA_PARAM_CORE.seph_holiday_reward_card_info where CARD_NUMBER_START_RANGE=[PIN_NUMBER]";
	public static final String QUERY_FOR_GET_SKU_SIZE_LABEL = "select SKU_SIZE from SCHEMA_PARAM_CATA.ACX_SKU where id='%s'";
	public static final String QUERY_FOR_GET_REWARD_SKU = "SELECT DS.sku_id FROM SCHEMA_PARAM_CATA.DCS_SKU DS INNER JOIN SCHEMA_PARAM_CATA.SEPH_SKU_MCS B ON DS.SKU_ID = B.SKU_ID INNER JOIN SCHEMA_PARAM_CORE.DCS_INVENTORY ST ON DS.SKU_ID = ST.CATALOG_REF_ID INNER JOIN SCHEMA_PARAM_CATA.SEPH_SKU P_SKU ON DS.SKU_ID = P_SKU.SKU_ID INNER JOIN SCHEMA_PARAM_CATA.SEPH_PRODUCT PR ON P_SKU.PRIMARY_PRODUCT_ID = PR.PRODUCT_ID INNER JOIN SCHEMA_PARAM_CATA.SEPH_SKU_FLAGS F ON DS.SKU_ID = F.SKU_ID inner join SCHEMA_PARAM_CATA.dcs_product dp on p_sku.primary_product_id = dp.product_id  WHERE PR.is_enabled = 1 and F.is_enabled = 1  and (ds.end_date is null or ds.end_date >= sysdate) and p_sku.temp_deactivation is null  and st.on_hold = 0 and st.stock_level > 3  and st.avail_status = 1004 and p_sku.primary_product_id is not null  and pr.temporary_deactive_dt is null  and (dp.end_date is null or dp.end_date >= sysdate) AND P_SKU.sku_type = 0 AND P_SKU.bi_type='%s' ";
	public static final String QUERY_FOR_GET_VALUE_PRICE_FOR_SKU = "SELECT value_price FROM SCHEMA_PARAM_CATA.seph_sku_value_price where sku_id='%s' and locale='%s'";

	public static Boolean isNegativeStockLevel = false;

	public Query() {}
	
	/**
	 * Method returns random SKU or product ID complaint to query
	 * @param query - Query string for select
	 * @param condition - String for conditions defining
	 * @param environment - String for environment defining
	 * @param isCollection
	 * @param isInStock
	 * @return sku or productId. If product is collection - returns collection productId and products skus separated with '-'
	 * @throws SQLException
	 */
	/*public static String query(String query, String condition, String environment, boolean isCollection, boolean isInStock, boolean isBiExclusive, boolean isNotGetDefaultSku,  String additionalInnerJoin, boolean isProduct) throws SQLException {
		ArrayList<String> allResults = queryALLResults(query, condition, environment, isCollection, isInStock, isBiExclusive, isNotGetDefaultSku, additionalInnerJoin, isProduct, 100);
		String result = allResults.get((int) (Math.random() * allResults.size()));
		logger.info("Query result is: " + result);
		return result;
	}*/
	
	/**
	 * Method returns query result list of test objects
	 * @param productData - information about product
	 * @param size - size of query result list
	 * @return resultList of product SKU
	 * @throws SQLException
	 *//*
	public static ArrayList<TestObject> queryALLResults(String productData, String size) throws SQLException {
		TestObject testObjectForData = new TestObject(productData);
		ArrayList<TestObject> productsTestDataList = new ArrayList<TestObject>();
		boolean isProduct = (testObjectForData.getProperty("SKU") == null) ? false : true;
		ArrayList<String> queryResultsList;
		if(isProduct){
			queryResultsList = queryALLResults(testObjectForData.getProperty("dbQuery"), testObjectForData.getProperty("dbCondition"), FrameworkServicesBase.frameworkThread().properties().environmentName(), Utils.isNull(testObjectForData.getProperty("products")) ? false : true, true, Utils.isNull(testObjectForData.getProperty("isBIExclusive")) ? false : Boolean.valueOf(testObjectForData.getProperty("isBIExclusive")), Utils.isNull(testObjectForData.getProperty("isNotGetDefaultSkuString")) ? false : Boolean.valueOf(testObjectForData.getProperty("isNotGetDefaultSkuString")), Utils.isNull(testObjectForData.getProperty("additionalInnerJoin")) ? "" : testObjectForData.getProperty("additionalInnerJoin"), isProduct, Integer.parseInt(size), true);
		}
		else
		{
			queryResultsList = queryALLResults(testObjectForData.getProperty("dbQuery"), testObjectForData.getProperty("dbCondition"), FrameworkServicesBase.frameworkThread().properties().environmentName(), Utils.isNull(testObjectForData.getProperty("products")) ? false : true, true, Utils.isNull(testObjectForData.getProperty("isBIExclusive")) ? false : Boolean.valueOf(testObjectForData.getProperty("isBIExclusive")), Utils.isNull(testObjectForData.getProperty("isNotGetDefaultSkuString")) ? false : Boolean.valueOf(testObjectForData.getProperty("isNotGetDefaultSkuString")), Utils.isNull(testObjectForData.getProperty("additionalInnerJoin")) ? "" : testObjectForData.getProperty("additionalInnerJoin"), isProduct, Integer.parseInt(size));

		}
		
		for (String idFromQueryResults : queryResultsList) {
			productsTestDataList.add(new TestObject(productData, idFromQueryResults));
		}
		return productsTestDataList;
	}
	*/
	/**
	 * Method returns query result
	 * @param query - query to execute
	 * @param connectionDB - connection
	 * @return resultList of data
	 * @throws SQLException
	 */
	/*public static ArrayList<String> executeQuery(String query, Connection connectionDB) throws SQLException {
		return executeQuery(query, connectionDB, true);
	}
	*/
	/**
	 * Method returns query result
	 * @param query - query to execute
	 * @param connectionDB - connection
	 * @param isAllResult - is get all result
	 * @param isLogResults - logger results
	 * @throws SQLException
	 */
	/*public static ArrayList<String> executeQuery(String query, Connection connectionDB, Boolean isAllResult) throws SQLException {
		return executeQuery(query, connectionDB, isAllResult, true);
	}*/
	
	/**
	 * Method returns query result
	 * @param query - query to execute
	 * @param connectionDB - connection
	 * @param isAllResult - is get all result
	 * @param isLogResults - logger results
	 * @return resultList of data
	 * @throws SQLException
	 */
	/*public static ArrayList<String> executeQuery(String query, Connection connectionDB, Boolean isAllResult, Boolean isLogResults) throws SQLException {
		ArrayList<String> resultList = new ArrayList<String>();
		logger.info("Execute query " + transformQuery(query));
		ResultSet rs = getResultSet(query, connectionDB);
		while (rs.next()) {
			int columnCount = rs.getMetaData().getColumnCount();
			for (int i = 1; i <= columnCount; i++) {
				if (rs.getObject(i) != null && !Utils.isNull(rs.getObject(i).toString())) {
					resultList.add(rs.getObject(i).toString());
				}
				if (!isAllResult)
					break;
			}
			if (!isAllResult)
				break;
		}
		if(isLogResults)
			logger.info("Query result is: " + resultList);
		return resultList;
	}*/
	
	/**
	 * Method returns result set for query
	 * @param query - query to execute
	 * @param connectionDB - connection
	 * @throws SQLException
	 */
	/*public static ResultSet getResultSet(String query, Connection connectionDB) throws SQLException {
		query = transformQuery(query);
		PreparedStatement stmt = connectionDB.prepareStatement(query);
		return stmt.executeQuery();
	}
	*/
	/**
	 * function for update data in bd
	 * @param query - query for execute
	 * @throws SQLException
	 */
	/*public static void editDataInDatabase(String query, Connection connectionDB) {
		query = transformQuery(query);
		logger.info("Execute update query " + query);
		try {
			PreparedStatement stmt = connectionDB.prepareStatement(query);
			stmt.executeUpdate();
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}*/
	
	/**
	 * update flash expire date
	 *//*
	public static List<String> updateFlashEnrolledDate(String email, int daysToExpire) throws SQLException, ParseException{
		List<String> dates = new ArrayList<String>();
		String flashSignUpDate = Utils.getDateInSanFrancisco(TestConstants.dateFormat_dd_MMM_yy);
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(TestConstants.dateFormat_dd_MMM_yy);
		SimpleDateFormat returnFormat = new SimpleDateFormat(TestConstants.dateFormat_MM_DD_YYYY);
		Date date = sdf.parse(flashSignUpDate);
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_MONTH, daysToExpire);
		String flashExpireDate = sdf.format(calendar.getTime());
		dates.add(returnFormat.format(calendar.getTime()));
		calendar.add(Calendar.YEAR, -1);
		dates.add(returnFormat.format(calendar.getTime()));
		flashSignUpDate = sdf.format(calendar.getTime());
		String queryString = STRING_CURRENT_YEAR_SHORT.format(QUERY_FOR_UPDATE_FLASH_ENROLLED_DATE, flashSignUpDate, flashExpireDate, email);
		Query.editDataInDatabase(queryString, TestRunnerBase.oracleServerConnection);
		return dates;
	}*/
	/**
	 * Method returns query result list of product SKU complaint to query
	 * @param query - Query string for select
	 * @param condition - String for conditions defining
	 * @param environment - String for environment defining
	 * @param isCollection
	 * @param isInStock
	 * @param size - size of query result list
	 * @param isOrder - true is need add 'order by at the end'
	 * @return resultList of product SKU
	 * @throws SQLException
	 *//*
	public static ArrayList<String> queryALLResults(String query, String condition, String environment, boolean isCollection, boolean isInStock, boolean isBIExclusive,  boolean isNotGetDefaultSku,String additionalInnerJoin, boolean isProduct, int size) throws SQLException {
		return queryALLResults( query,  condition,  environment,  isCollection,  isInStock,  isBIExclusive,   isNotGetDefaultSku, additionalInnerJoin,  isProduct,size,false);
	}*/
	
	/**
	 * Method returns query result list of product SKU complaint to query
	 * @param query - Query string for select
	 * @param condition - String for conditions defining
	 * @param environment - String for environment defining
	 * @param isCollection
	 * @param isInStock
	 * @param size - size of query result list
	 * @param isGroup - true is need add 'order by at the end'
	 * @return resultList of product SKU
	 * @throws SQLException
	 */
	/*public static ArrayList<String> queryALLResults(String query, String condition, String environment, boolean isCollection, boolean isInStock, boolean isBIExclusive,  boolean isNotGetDefaultSku,String additionalInnerJoin, boolean isProduct, int size, boolean isGroup) throws SQLException {
		String queryStr = getQueryStr(query, condition, environment, isInStock, isBIExclusive, isNotGetDefaultSku, additionalInnerJoin, isProduct);
		if(isGroup){
			String parametr = query.replace("SELECT","");
			queryStr = queryStr.concat(" "+String.format(GROUP_BY,parametr));
		}
		PreparedStatement stmt = TestRunnerBase.oracleServerConnection.prepareStatement(queryStr);
		logger.info("Query is: " + queryStr);
		ResultSet rs = stmt.executeQuery();
		ArrayList<String> resultList = new ArrayList<String>();
		if (isCollection) {
			Map<String, String> map = new HashMap<String, String>();
			String key = null;
			while (rs.next()) {
				key = rs.getObject(1).toString();
				if (map.containsKey(key)) {
					map.put(key, map.get(key).toString() + "~" + rs.getObject(2).toString());
				}
				else
					map.put(key, rs.getObject(2).toString());
			}
			for (Map.Entry<String, String> e : map.entrySet()) {
				resultList.add(e.getKey() + "-" + e.getValue());
			}
		}
		else {
			while (rs.next() && resultList.size() < size) {
				//for swatches
				if (rs.getMetaData().getColumnCount() > 1) {
					resultList.add(rs.getObject(1).toString() + "-" + rs.getObject(2).toString());
				}
				else
					resultList.add(rs.getObject(1).toString());
			}
		}
		rs.close();
		stmt.close();
		return resultList;
	}*/
	
	/**
	 * Method returns empty results search
	 * @param query - Query string for select
	 */
	/*public static boolean emptyQueryForOrder(String query) {
		boolean result = false;
		ArrayList<String> queryResults = queryForOrders(query, TestConstants.FIRST);
		if(queryResults.isEmpty())
			result = true;
		return result;
	}*/
	
	/**
	 * Method returns query result list of order IDs complaint to query
	 * @param query - Query string for select
	 * @return resultList list of order IDs
	 */
	/*public static String queryForOrders(String query) {
		query = transformQuery(query);
		PreparedStatement stmt = null;
		ArrayList<String> resultList = null;
		logger.info("Query is: " + query);
		try {
			stmt = TestRunnerBase.oracleServerConnection.prepareStatement(query);
			ResultSet rs = stmt.executeQuery();
			resultList = new ArrayList<String>();
			while (rs.next() && resultList.size() < 2) {
				resultList.add(rs.getObject(1).toString());
			}
			rs.close();
			return resultList.get(0);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return resultList.get(0);
	}
	*/
	/**
	 * Method returns query result list of order IDs complaint to query
	 * @param query - query for select order
	 * @param size - size of result set
	 * @return list of query result
	 */
	/*public static ArrayList<String> queryForOrders(String query, int size) {
		query = transformQuery(query);
		PreparedStatement stmt = null;
		ArrayList<String> resultList = null;
		logger.info("Query is: " + query);
		try {
			stmt = TestRunnerBase.oracleServerConnection.prepareStatement(query);
			ResultSet rs = stmt.executeQuery();
			resultList = new ArrayList<String>();
			while (rs.next() && resultList.size() <= size) {
				resultList.add(rs.getObject(1).toString());
			}
			rs.close();
			return resultList;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return resultList;
	}
	
	*//**
	 * get product stock level
	 * @param stockLevel - stock level
	 *//*
	public static Boolean isInStock(String sku, boolean isInStock)  {
		// minimum stock number for product, if less update stock level
		int minimumStockLevel = 30;
		boolean isMaxStockLevel = true;
		List <String> result = new ArrayList<String>();
		try{
			result = Query.executeQuery(String.format(Query.QUERY_FOR_GET_STOCK_LEVEL, sku.trim(), COUNTRY_US), TestRunnerBase.oracleServerConnection, true);
		} catch (SQLException e){
			e.printStackTrace();
		}
		for(int i = 0; i < result.size(); i++){
			isMaxStockLevel &= isInStock ? (Integer.parseInt(result.get(i)) > minimumStockLevel) : (Integer.parseInt(result.get(i)) > 0);
			isNegativeStockLevel = Integer.parseInt(result.get(i)) < 0;
		}
		return isMaxStockLevel;		
	}
	
	*//**
	 * get product stock level
	 * @param sku - sku
	 * @param country - country
	 *//*
	public static int getStockLevel(String sku, String country)  {
		List <String> result = new ArrayList<String>();
		try{
			result = Query.executeQuery(String.format(Query.QUERY_FOR_GET_STOCK_LEVEL, sku.trim(), country.toUpperCase()), TestRunnerBase.oracleServerConnection, true);
		} catch (SQLException e){
			e.printStackTrace();
		}
		return Integer.parseInt(result.get(ZERO));
	}
	
	*//**
	 * Method returns query result map of order IDs and its product IDs complaint to query
	 * @param query - Query string for select
	 * @return resultMap list of order IDs and product IDs
	 *//*
	public static HashMap<String, String> queryForOrdersAndProducts(String query) {
		query = transformQuery(query);
		PreparedStatement stmt = null;
		HashMap<String, String> resultMap = null;
		logger.info("Query is: " + query);
		try {
			stmt = TestRunnerBase.oracleServerConnection.prepareStatement(query);
			ResultSet rs = stmt.executeQuery();
			resultMap = new HashMap<String, String>();
			while (rs.next()) {
				resultMap.put(rs.getObject(1).toString(), rs.getObject(2).toString());
			}
			rs.close();
			return resultMap;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return resultMap;
	}
	
	*//**
	 * Creates query string according parameters
	 * @param query - Query string for select
	 * @param condition - String for conditions defining
	 * @param environment - String for environment defining
	 *//*
	private static String getQueryStr(String query, String condition, String environment, boolean isInStock, String additionalInnerJoin, boolean isProduct) {
			return getQueryStr(query, condition, environment, isInStock, false, false, additionalInnerJoin, isProduct);
	}
	*//**
	 * Creates query string according parameters
	 * @param query - Query string for select
	 * @param condition - String for conditions defining
	 * @param environment - String for environment defining
	 * @param isInStock - true if need to get skus which are in stock
	 * @param isBIExclusive - true if sku should has flad BI exclusive
	 * @param isNotGetDefaultSku - true if query shouldn't revert just skus which are default for any product 
	 *//*
	private static String getQueryStr(String query, String condition, String environment, boolean isInStock, boolean isBIExclusive, boolean isNotGetDefaultSku, String additionalInnerJoin, boolean isProduct) {
		String str;
		if (isProduct) {
			str = query + PRODUCT_SQL_STRING + condition+ NOT_IN_STORE_ONLY_PRODUCT;
			if(isNotGetDefaultSku){
				str = str.replace(ADDITIONAL_DEFAULT_SKU_JOIN, " ");
			}
			else{
				str = str.replace(ADDITIONAL_ANY_SKU_JOIN, " ");
			}
			if(!isBIExclusive){
				str=str + NOT_BI_EXCLUSIVE_PRODUCT;
			}
			if (!isInStock) {
				str = str.replace("st.stock_level > 3", "st.stock_level = 0");
			}
			str = str.replace(ADDITIONAL_INNER_JOIN, " " + additionalInnerJoin);
			return replaceBaseParameters(environment, str);
		} else {
			str = query + EGC_SQL_STRING + condition;
			return transformQuery(str);
		}
	}
	
	public static String replaceBaseParameters(String environment, String strQuery) {
		if (environment.contains(PROD_ENV) || environment.contains(PREVIEW_ENV)) {
			return strQuery.replace("TEMPLATE_PARAM", PROD_TEMPLATE).replace("CA_PLIST_PARAM", PROD_CA_PLIST).replace("PLIST_PARAM", PROD_PLIST).replace("VCOLL_TEMPLATE", PROD_VCOLL_TEMPLATE).replace("COLL_TEMPLATE", PROD_COLL_TEMPLATE).replaceAll("SCHEMA_PARAM_CATA", SCHEMA_PARAM_CATA_PROD).replaceAll("SCHEMA_PARAM_CORE", SCHEMA_PARAM_CORE_PROD).replace(DATA_BASE_IN_QUERY, PROD_DB_NAME);
		} else if (environment.contains(DEV_ENV)) {
			return strQuery.replace("TEMPLATE_PARAM", QA_TEMPLATE).replace("CA_PLIST_PARAM", QA_CA_PLIST).replace("PLIST_PARAM", QA_PLIST).replace("VCOLL_TEMPLATE", QA_VCOLL_TEMPLATE).replace("COLL_TEMPLATE", QA_COLL_TEMPLATE).replaceAll("SCHEMA_PARAM_CATA", SCHEMA_PARAM_CATA_DEV).replaceAll("SCHEMA_PARAM_CORE", SCHEMA_PARAM_CORE_DEV).replace(DATA_BASE_IN_QUERY, DEV_DB_NAME);
		} else if (environment.contains(EBF_ENV)) {
			return strQuery.replace("TEMPLATE_PARAM", QA_TEMPLATE).replace("CA_PLIST_PARAM", QA_CA_PLIST).replace("PLIST_PARAM", QA_PLIST).replace("VCOLL_TEMPLATE", QA_VCOLL_TEMPLATE).replace("COLL_TEMPLATE", QA_COLL_TEMPLATE).replaceAll("SCHEMA_PARAM_CATA", SCHEMA_PARAM_CATA_EBF).replaceAll("SCHEMA_PARAM_CORE", SCHEMA_PARAM_CORE_EBF);
		} else {
			return strQuery.replace("TEMPLATE_PARAM", QA_TEMPLATE).replace("CA_PLIST_PARAM", QA_CA_PLIST).replace("PLIST_PARAM", QA_PLIST).replace("VCOLL_TEMPLATE", QA_VCOLL_TEMPLATE).replace("COLL_TEMPLATE", QA_COLL_TEMPLATE).replaceAll("SCHEMA_PARAM_CATA", SCHEMA_PARAM_CATA_QA).replaceAll("SCHEMA_PARAM_CORE", SCHEMA_PARAM_CORE_QA);
		}
	}
	
	private static String transformQuery(String query) {
		String environment = FrameworkServicesBase.frameworkThread().properties().environmentName();
		if (environment.contains(PROD_ENV) || environment.contains(PREVIEW_ENV)) {
			return query.replaceAll("SCHEMA_PARAM_CORE", SCHEMA_PARAM_CORE_PROD).replace(DATA_BASE_IN_QUERY, PROD_DB_NAME).replaceAll("SCHEMA_PARAM_CATA", SCHEMA_PARAM_CATA_PROD);
		} else if (environment.contains(DEV_ENV)) {
			return query.replaceAll("SCHEMA_PARAM_CORE", SCHEMA_PARAM_CORE_DEV).replace(DATA_BASE_IN_QUERY, DEV_DB_NAME).replaceAll("SCHEMA_PARAM_CATA", SCHEMA_PARAM_CATA_DEV);
		} else if (environment.contains(EBF_ENV)) {
			return query.replaceAll("SCHEMA_PARAM_CORE", SCHEMA_PARAM_CORE_EBF).replace(DATA_BASE_IN_QUERY, EBF_DB_NAME).replaceAll("SCHEMA_PARAM_CATA", SCHEMA_PARAM_CATA_EBF);
		} else if (environment.contains(QTS_ENV)) {
			return query.replaceAll("SCHEMA_PARAM_CORE", SCHEMA_PARAM_CORE_QA).replace(DATA_BASE_IN_QUERY, EBF_DB_NAME).replaceAll("SCHEMA_PARAM_CATA", SCHEMA_PARAM_CATA_QA);
		} else if (environment.contains(QA2_ENV)) {
			return query.replaceAll("SCHEMA_PARAM_CORE", SCHEMA_PARAM_CORE_QA).replace(DATA_BASE_IN_QUERY, QA2_DB_NAME).replaceAll("SCHEMA_PARAM_CATA", SCHEMA_PARAM_CATA_QA);
		} else {
			return query.replaceAll("SCHEMA_PARAM_CORE", SCHEMA_PARAM_CORE_QA).replace(DATA_BASE_IN_QUERY, QA_DB_NAME).replaceAll("SCHEMA_PARAM_CATA", SCHEMA_PARAM_CATA_QA);
		}
	}*/
	/**
	 * get profile database name
	 * @return String
	 */
	/*public static String getProfileDataBaseName(){
		if(FrameworkServicesBase.frameworkThread().properties().environmentName().contains(DEV_ENV)){
			return DEV_PROFILE_DB_NAME;
		} else {
			return QA_PROFILE_DB_NAME;
		}
	}*/
}