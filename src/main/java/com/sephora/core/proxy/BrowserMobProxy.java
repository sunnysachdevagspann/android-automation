package com.sephora.core.proxy;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import net.lightbody.bmp.core.har.Har;
import net.lightbody.bmp.core.har.HarEntry;
import net.lightbody.bmp.core.har.HarLog;
import net.lightbody.bmp.core.har.HarNameValuePair;
import net.lightbody.bmp.proxy.ProxyServer;
import org.openqa.selenium.Proxy;

public class BrowserMobProxy {
	private ProxyServer server;
	private Har har ;
	private HarNameValuePair harPair;
	private static Properties properties;
	private final int PROXY = 4461;
	private Proxy proxy = new Proxy();

	public void proxyStart() {
		server = new ProxyServer(PROXY);
		proxy.setSslProxy("trustAllSSLCertificates");
		server.start();
//		server.setCaptureHeaders(true);
//		server.setCaptureContent(true);
		server.newHar("xxx");
		while(!server.isStarted()) {
			System.out.println("Not started yet");
		}
	}
	
	public void stopProxy() {
		server.stop();
	}
	
	public Map<String, String> getAnalyticsKeyValue() throws Exception{
		Map<String, String> keyValue = new HashMap<String, String>();
		har = server.getHar();
		String strFilePath = "Selenium_test.har";
		FileOutputStream fos = new FileOutputStream(strFilePath, false);
		har.writeTo(fos);
		HarLog log = har.getLog();
		List<HarEntry> entries = log.getEntries();
		for (int i = entries.size()-1; i >= 0; i--) {
			//keyValue=new HashMap<String, String>();
			HarEntry entry = entries.get(i);
			if (entry.getRequest().getUrl().contains("metrics")) {
				for (int j = 0; j < entry.getRequest().getQueryString().size(); j++) {

					keyValue.put(entry.getRequest().getQueryString().get(j).getName().trim(), entry.getRequest().getQueryString().get(j).getValue().trim());
					System.out.println(entry.getRequest().getQueryString().get(j).getName()+"---"+ entry.getRequest().getQueryString().get(j).getValue());

				}
				break;
			}
			}

		return keyValue;
	}
	
	public Map<String, String> getSecondLastAnalyticsKeyValue() throws Exception{
		  List<Map<String, String>> sl = new ArrayList<Map<String, String>>();
		        Map<String, String> keyValue = new HashMap<String, String>();
		        int count=0;
		        har = server.getHar();
		        String strFilePath = "Selenium_test.har";
		        FileOutputStream fos = new FileOutputStream(strFilePath, false);
		        har.writeTo(fos);
		        HarLog log = har.getLog();
		        List<HarEntry> entries = log.getEntries();
		        for (int i = entries.size()-1; i >= 0; i--) {
		               HarEntry entry = entries.get(i);
		               //keyValue = new HashMap<String, String>();
		               if (entry.getRequest().getUrl().contains("metrics")) {
		                     for (int j = 0; j < entry.getRequest().getQueryString().size(); j++) {
		                            keyValue.put(entry.getRequest().getQueryString().get(j).getName(), entry.getRequest().getQueryString().get(j).getValue());
		                            System.out.println(entry.getRequest().getQueryString().get(j).getName()+"---"+ entry.getRequest().getQueryString().get(j).getValue());
		                            count=count+1;
		                     }
		                    // sl.add(keyValue);
		               }
		               if(count==2)break;
		        }
		        
		        return keyValue;
		 }

}
