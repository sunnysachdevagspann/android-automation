package com.sephora.test.androidApp.Base;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.sephora.core.driver.mobile.SephoraMobileDriver;
import com.sephora.core.utils.Config;
import com.sephora.core.utils.HardAssert;
import com.sephora.core.utils.HtmlLogger;
import com.sephora.core.utils.SoftAssert;
import com.sephora.pages.AndroidApp.Basket.BasketPage;
import com.sephora.pages.AndroidApp.Checkout.CheckoutPage;
import com.sephora.pages.AndroidApp.Home.HomePage;
import com.sephora.pages.AndroidApp.MyAccount.MyAccountPage;
import com.sephora.pages.AndroidApp.Navigation.LeftNavigationDrawerPage;
import com.thoughtworks.selenium.webdriven.commands.IsElementPresent;

/**
 * 
 * SephoraMobileBaseTest is the super class for all
 * Mobile Test classes Initializes the driver is used for execution.
 *
 */
public class SephoraAndroidAppBaseTest extends SephoraBaseTest{
	
	public SephoraAndroidAppBaseTest() {
		homePage = new HomePage(driver);
		basketPage = new BasketPage(driver);
		leftNavigationDrawerPage = new LeftNavigationDrawerPage(driver);
		myAccountPage = new MyAccountPage(driver);
		checkoutPage = new CheckoutPage(driver);
	}
	
	protected HomePage homePage;
	protected BasketPage basketPage;
    protected LeftNavigationDrawerPage leftNavigationDrawerPage;
    protected MyAccountPage myAccountPage;
    protected CheckoutPage checkoutPage;
	public String deviceName = null;
	public String defaultProps = "defaultenv.properties";
	protected Config config = new Config();
	static DesiredCapabilities capabilities = new DesiredCapabilities();
	protected SephoraMobileDriver driver = new SephoraMobileDriver(config);
	private static final Logger logger = LogManager
			.getLogger(SephoraAndroidAppBaseTest.class.getName());
	
	public static final By REMOVE_PRODUCT_BUTTON_LOC= By.xpath("//android.widget.RelativeLayout[@index='0']/android.widget.TextView[@text='Remove']");
	public static final By PRODUCT_QUANTITY_LIST_LOC = By.xpath("//android.support.v7.widget.RecyclerView/android.widget.RelativeLayout");
	public static final By SIGNIN_LINK = By.xpath("//android.widget.TextView[contains(@text,'SIGN IN')]");
	public static final By LOGOUT_BUTTON = By.id("com.sephora:id/main_logout_button");
	
	
	/**
	 * @throws Exception
	 *             setup function loads the driver and environment and baseUrl
	 *             for the tests
	 */
	@BeforeSuite(alwaysRun=true)
	public void setUp() throws Exception {
		((SephoraMobileDriver) driver).loadApplication();
	}

	/**
	 * @throws Exception
	 */
	@AfterSuite(alwaysRun = true)
	public void tearDown() throws Exception {
		new HtmlLogger().createHtmlLogFile();
		driver.closeApp();
	}
	
	
	public void assertTrue(String message, boolean condition) {

		if (!condition) {
			logger.info("[FUNCTIONAL FAILURE - ASSERTION ERROR ----------- "
					+ message + "]");
			Assert.fail(message);
		}
	}

	public void assertTrue(boolean condition, String message) {

		if (!condition) {
			logger.info("[FUNCTIONAL FAILURE - ASSERTION ERROR ----------- "
					+ message + "]");
			Assert.fail(message);
		}
	}

	public void assertEquals(Object obj1, Object obj2, String message) {

		if (!obj1.equals(obj2)) {
			logger.info("[FUNCTIONAL FAILURE - ASSERTION ERROR ----------- "
					+ message + "]");
			Assert.fail(message);
		}
	}

	public void assertEquals(String message, int num1,int num2) {

		if (!(num1==num2)) {
			logger.info("[FUNCTIONAL FAILURE - ASSERTION ERROR ----------- "
					+ message + "]");
			Assert.fail(message);
		}
	}

	public void assertFalse(boolean condition, String message) {

		if (condition) {
			logger.info("[FUNCTIONAL FAILURE - ASSERTION ERROR ----------- "
					+ message + "]");
			Assert.fail(message);
		}
	}

	public void assertFalse(String message, boolean condition) {

		if (condition) {
			logger.info("[FUNCTIONAL FAILURE - ASSERTION ERROR ----------- "
					+ message + "]");
			Assert.fail(message);
		}
	}


	public void assertEquals(String message, float num1,float num2) {

		if (!(num1==num2)) {
			logger.info("[FUNCTIONAL FAILURE - ASSERTION ERROR ----------- "
					+ message + "]");
			Assert.fail(message);
		}
	}
	
	
	
	@BeforeMethod(alwaysRun=true)
	public void beforeMethod(Method m) throws Exception{
		Test t;
		t=m.getAnnotation(Test.class);
		logger.debug("<<<<<<<<<<<<<<<<<<<<<<<<<Before Test Method Starts:: "+m.getName()+">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		test= extent.startTest(m.getName(),t.description());
		s_Assert= new SoftAssert(driver,test);
		h_Assert = new HardAssert(driver,test);
		
		logger.info("<<<<<<<<<<<<<<<<<<<<<<<<<Before Test Method Ends:: "+m.getName()+">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
	}
	
	@AfterMethod
	public void afterMethod(Method m){
		logger.debug("<<<<<<<<<<<<<<<<<<<<<<<<<After Test Method Starts>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		homePage.navigateToHomePage();
		 homePage.WaitforHomePagetoLoad();
			homePage.clickBasketButton();
			logger.debug("Going to remove item from basket if any");
			if(!driver.isElementPresent(PRODUCT_QUANTITY_LIST_LOC)){
				logger.info("No product to remove");
				driver.navigateBack();
			}
			if(driver.isElementPresent(PRODUCT_QUANTITY_LIST_LOC)){
				int n;
				for(n=0; n<3; n++){
			List<WebElement> product_List=  driver.findElements(PRODUCT_QUANTITY_LIST_LOC);
			int size = product_List.size();
			System.out.println(product_List.size());
			int i;
			for (i = 0; i < size+1; i++) {
				if(driver.isElementPresent(REMOVE_PRODUCT_BUTTON_LOC)){
				basketPage.removeProductFromBasketforPreCondition(0);
				logger.debug("item #: " + (i+1) + " is removed from basket");
				}
			}
			logger.debug("All the items were removed from the basket");
			if (!driver.isElementPresent(REMOVE_PRODUCT_BUTTON_LOC)) break;
		}
			
			driver.navigateBack();
			if(driver.IsLocatorEnable(LOGOUT_BUTTON)){
				driver.click(LOGOUT_BUTTON);
			 }

		if(!driver.isElementPresent(SIGNIN_LINK)){
		leftNavigationDrawerPage.clickLeftNavigationButton().clickMyAccoyntButton();
		 myAccountPage.clickSignOutButtton().WaitforHomePagetoLoad();
		
		}
		 if(driver.isElementPresent(SIGNIN_LINK)){
			 logger.debug("User is successfully logged out");				 
	 }
		}
			extent.endTest(test);
		logger.debug("<<<<<<<<<<<<<<<<<<<<<<<<<After Test Method Ends>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
	}
	
	
}
