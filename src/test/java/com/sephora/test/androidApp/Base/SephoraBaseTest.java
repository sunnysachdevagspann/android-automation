package com.sephora.test.androidApp.Base;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;

import java.lang.reflect.Method;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.ITestContext;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.sephora.core.driver.mobile.SephoraMobileDriver;
import com.sephora.core.mobile.constants.TestConstantsI;
import com.sephora.core.utils.Config;
import com.sephora.core.utils.ExcelReader;
import com.sephora.core.utils.ExtentManager;
import com.sephora.core.utils.HardAssert;
import com.sephora.core.utils.SoftAssert;
@Listeners({ com.sephora.core.listeners.TestListner.class })


public class SephoraBaseTest implements TestConstantsI{
	public static AndroidDriver driver;
	public static String Appium_Host;
	public static String browserName;
	public static String baseUrl;
	public static String deviceName;
	public static String platformName;
	public static String platformVersion;
	public static String testRailRunID;
	public static String device;
	public static String appPath;
	public static String Package;
	public static String AppActivity;
	protected ExtentReports extent;
	protected ExtentTest test;
	protected SoftAssert s_Assert;
	protected HardAssert h_Assert;

	
	
	
	//protected Config config = new Config();
	private static final Logger logger = LogManager
			.getLogger(SephoraBaseTest.class.getName());
	
	
	@BeforeSuite(alwaysRun = true)
	public void setupBeforeSuite(ITestContext context) {
	
		Appium_Host = context.getCurrentXmlTest().getParameter("host");
	    deviceName = context.getCurrentXmlTest().getParameter("deviceName");
	    device = context.getCurrentXmlTest().getParameter("device");
	    platformName = context.getCurrentXmlTest().getParameter("platformName");
	    platformVersion = context.getCurrentXmlTest().getParameter("platformVersion");
	    testRailRunID = context.getCurrentXmlTest().getParameter("testRailRunID");
	    appPath = context.getCurrentXmlTest().getParameter("app");
	    Package = context.getCurrentXmlTest().getParameter("package");
        AppActivity= context.getCurrentXmlTest().getParameter("app-activity");
    	extent=ExtentManager.getInstance();
	}
	
	/**
	 * @throws Exception
	 */
	@DataProvider(name = "sephoraTestData")
	public Object[][] sephoraDataProvider(Method testMethod) throws Exception {
		String sheetName = testMethod.getName();
		String filePath = "src/test/resources/"
				+ testMethod
						.getDeclaringClass()
						.getName()
						.replace(DOT, FORWARD_SLASH)
				+ ".xlsx";
		logger.debug("Test data is loaded from file " + filePath
				+ " and the sheet is " + sheetName);
		Object[][] testObjArray = ExcelReader.getTableArray(filePath,
				testMethod.getName());

		return (testObjArray);

	}

	public SoftAssert getSoftAssert() {
		return s_Assert;
	}
	
}
