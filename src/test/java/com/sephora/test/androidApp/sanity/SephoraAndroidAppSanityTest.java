package com.sephora.test.androidApp.sanity;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.testng.annotations.Test;
import com.relevantcodes.extentreports.LogStatus;
import com.sephora.core.annotations.TestRailID;


public class SephoraAndroidAppSanityTest extends SephoraAndroidAppSanityBase {

	 private static final Logger logger = LogManager.getLogger(SephoraAndroidAppSanityTest.class.getName());
	 
	 @Test(enabled=true, description = "Order-Non BI, HG/HM, Non-Standard Shipping, %off, AMEX+Gift Card")
	 @TestRailID(id = { "C185118" })
	 
	  public void AndroidAppSanity_Test_C185118() throws Exception{
	      String Address[] = DUMMY_ADDRESS_ONE;
	      String NewUser[]=  NEW_USER_REGISTER;
	      String CC[]=CC_DETAILS;
	      String cc[]=DUMMY_CREDIT_CARD_FOUR;
	      String CanadaAddress[]=CANADA_DUMMY_ADDRESS;
	      String GC[]=GIFT_CARD_TWO;
	      test.log(LogStatus.INFO, "User is successfully registered and signed in Successfully");
		  homePage.WaitforHomePagetoLoad();
		  homePage.clickSignInLink();
		 
		  signInPage.WaitforSignInPagetoLoad().clickCreateAccountButton();
		  createAccountPage.typeNewUserEmail().clickDoneButton();
		  createAccountPage.registerNewUserNonBI(NewUser[0],NewUser[1], NewUser[2], NewUser[2], SSIStatusOFF);
		  assertTrue(homePage.isSignInSuccessful(NewUser[0]), "User is not Registered successfully");
		  logger.info("User is successfully registered and signed in Successfully");
		  
		  
		  homePage.clickCategoriesSection();
		  categoryPage.selectCategory(2, "MAKEUP").clickSubCategory(0).clickThirdLevelCategory(2);
		  productPage.selectMultipleproduct(3);
		  assertTrue(productPage.isProductSearchResultPage("Tinted Moisturizer"), "Product Search Result Page is not opened");
		  logger.info("Product Search Result Page is opened");
		  
		  productDetailPage.clickBasketButton().waitForBasketPageToLoad();
		  assertTrue(basketPage.isProductAddedToBasket(), "product is not added to basket");
		  logger.info("Product added to basket successfully");
		  
		  basketPage.clickCheckoutButton().waitforCheckOutPageToLoad();
		  checkoutPage.clickAddShippingAddressAccordion().fillShippingAddressData(Address[0], Address[1], Address[2], Address[3]).clickDoneButton();
		  assertTrue(checkoutPage.isShippingAddressAdded(Address[0]), "Shipping Address is not added successfully");
		  logger.info("Shipping Address is added successfully");
		  
		  checkoutPage.clickShipToAccordion().editShipToAddress().typeAddressFirstLine(DUMMY_ADDRESS_THREE[0]).clickDoneButton();
		  assertTrue(checkoutPage.isShippingAddressAdded(DUMMY_ADDRESS_THREE[0]), "Shipping Address is not updated successfully");
		  logger.info("Shipping Address is updated successfully");
		  checkoutPage.clickDeliveryAndGiftOptionAccordion().selectDeliveryOption(1, DELIVERY_METHODS[1]);
		  assertTrue(checkoutPage.isDeliveryOptionUpdated(DELIVERY_METHODS[1]), "Delivery method is not updated correctly");
		  logger.info("Delivery Method updated correctly");
		  
		 
		  
		  checkoutPage.clickAddPaymentAccordion().clickAddCreditCardOverlay().fillCCDetails(cc[0], CC[1], CC[2], cc[1], cc[2], cc[3]);
		  paymentPage.clickMakeMyDefaultCCSwitchSwitch("OFF").clickSaveToMyAccountSwitch("ON").checkBillingAddressCheckbox("ON").clickDoneButton();
		  checkoutPage.waitforCheckOutPageToLoad();
		  assertTrue(checkoutPage.isCreditCardAdded(cc[0]), "credit card: "+cc[0]+ " is not added");
		  logger.info("credit card: "+cc[0]+ " is added successfully");
		  
		  checkoutPage.clickEditPaymentAccordion().clickEditCreditCardOverlay().typeCCSecurityCode(cc[1]).clickMakeMyDefaultCCSwitchSwitch("ON");
		  paymentPage.selectCountry(CanadaAddress[0]).TypePostalCode(CanadaAddress[2]).selectState(CanadaAddress[3]);
		  shippingAddressPage.typeAddressFirstLine(CanadaAddress[1]).typeCity(CanadaAddress[4]).typePhone(CanadaAddress[5]).clickDoneButton();
		  assertTrue(checkoutPage.isCreditCardAdded(cc[0]), "credit card: "+cc[0]+ " is not updated");
		  logger.info("credit card: "+cc[0]+ " is updated successfully");
		  
		  checkoutPage.clickEditPaymentAccordion().clickAddGiftCardOverlay();
		  giftCardPage.typeGCNumber(GC[0]).typeGCPin(GC[1]).clickDoneButton();
		  assertTrue(checkoutPage.isGiftCardAdded(GC[0]), "Gift card: "+GC[0]+ " is not added");
		  logger.info("Gift card: "+GC[0]+ " is Added and displayed correctly");
		  
		  checkoutPage.addPromotion(PROMO_CODE_10_OFF);
		  Assert.assertTrue(checkoutPage.isPromoAdded(PROMO_CODE_10_OFF));
		  
		  checkoutPage.pressDeviceBackButton();
		  basketPage.waitForBasketPageToLoad();
		  Assert.assertTrue(basketPage.isPromoAdded(PROMO_CODE_10_OFF));
		  
		  basketPage.clickCheckoutButton().waitforCheckOutPageToLoad();
		  Assert.assertTrue( checkoutPage.isTotalsSectionDisplayCorrectInfoforSanity());
		  
		  checkoutPage.clickPlaceOrderButton();
		  Assert.assertTrue(orderConfirmationPage.isOrderSuccessfullyPlaced());
		  
	 }
	 
	 @Test(enabled=true, description = "Order - BI, HG, Standard Shipping, GWP, Visa + Store Credit")
	 @TestRailID(id = { "C185120" })
	  public void AndroidAppSanity_Test_C185120() throws Exception{
		 
		 String cc[]=DUMMY_CREDIT_CARD_FOUR;
		 String CC[]=CC_DETAILS;
		 String address[] = DUMMY_ADDRESS_FIVE;
		 String SearchText[]=  SEARCH_KEYWORD;
	      String cc1[]=DUMMY_CREDIT_CARD_THREE;
	      
	      
		  homePage.WaitforHomePagetoLoad();
		  homePage.clickSignInLink();
		  
		  signInPage.WaitforSignInPagetoLoad().signIn(NEW_BI_USER[0], NEW_BI_USER[1], "ON");
		  assertTrue(homePage.isSignInSuccessful(NEW_BI_USER[2]), "User is not Signed In successfully as BI User");
		  logger.info("User is signed in Successfully as BI User");
		  
		  homePage.clickSearchIcon();
		  searchPage.searchKeywordbyIndex(SearchText[0], 0);
		  
		  productPage.selectMultipleproduct(3);
		  productDetailPage.clickBasketButton().waitForBasketPageToLoad();
		  Assert.assertTrue(basketPage.isProductAddedToBasket());
		  
		  int itemscountbeforecountupdateofProduct=basketPage.getNumberOfItemsFromBasketOrderTotal();
		  double merchandisetotalbeforecountupdateofProduct=basketPage.getMerchandiseTotalFromBasketOrderTotal();
		  basketPage.updateItemCountOfProductInBasket(0, "4");
		  int itemscountaftercountupdateofProduct=basketPage.getNumberOfItemsFromBasketOrderTotal(); 
		  double merchandisetotalaftercountupdateofProduct=basketPage.getMerchandiseTotalFromBasketOrderTotal();
		  
		  assertTrue(itemscountbeforecountupdateofProduct<itemscountaftercountupdateofProduct, "The quantity of product in basket after increasing count of product is not updated successfully");
		  logger.info("Quantity of product after increasing count of product is updated successfully");
		  
		  assertTrue(merchandisetotalbeforecountupdateofProduct<merchandisetotalaftercountupdateofProduct, "merchandise total after increasing count of product is not updated successfully");
		  logger.info("merchandise total after increasing count of product is updated successfully");
		  
		  basketPage.scrollPageDown(1);
		  basketPage.removeProductFromBasket(0, 2);
		  int itemsafterRemovalofProduct=basketPage.getNumberOfItemsFromBasketOrderTotal(); 
		  double merchandisetotalafterRemovalofProduct=basketPage.getMerchandiseTotalFromBasketOrderTotal();
		  
		  assertTrue(itemscountaftercountupdateofProduct>itemsafterRemovalofProduct, "The quantity of product in basket after removal is not updated successfully");
		  logger.info("Quantity of product after removal of product is updated successfully");
		  
		  assertTrue(merchandisetotalaftercountupdateofProduct>merchandisetotalafterRemovalofProduct, "merchandise total after removal of product is not updated successfully");
		  logger.info("merchandise total after removal of product is updated successfully");
		  
		  
		  basketPage.clickCheckoutButton().waitforCheckOutPageToLoad();
		  Assert.assertTrue(checkoutPage.isShippingAddressPrefilled());
		  
		  Assert.assertTrue(checkoutPage.isDeliveryMethodPrefilled(DELIVERY_METHODS[0]));
		 
		 //Assert.assertTrue(checkoutPage.isCCPrefilledInPaymentSection());
		  
		  // Workaround step
		  checkoutPage.clickAddPaymentAccordion().clickAddCreditCardOverlay().fillCCDetails(cc[0], CC[1], CC[2], cc[1], cc[2], cc[3]);
		  //checkoutPage.clickEditPaymentAccordion().clickAddCreditCardOverlay().fillCCDetails(cc[0], CC[1], CC[2], cc[1], cc[2], cc[3]);
		  paymentPage.clickMakeMyDefaultCCSwitchSwitch("ON").clickSaveToMyAccountSwitch("ON").checkBillingAddressCheckbox("ON").clickDoneButton();
		  checkoutPage.waitforCheckOutPageToLoad();
		  assertTrue(checkoutPage.isCreditCardAdded(cc[0]), "credit card: "+cc[0]+ " is not added");
		  logger.info("credit card: "+cc[0]+ " is added successfully");
		  
		  checkoutPage.addPromotion(PROMO_CODE_MOBILE);
		  Assert.assertTrue(checkoutPage.isPromoAdded(PROMO_CODE_MOBILE));
		  
		  Assert.assertTrue( checkoutPage.isTotalsSectionDisplayCorrectInfoforSanity());
		  
		  checkoutPage.clickPlaceOrderButton();
		  Assert.assertTrue(orderConfirmationPage.isOrderSuccessfullyPlaced());
		  
	 }
	 
	 @Test(enabled=true, description = "Order-BI, HG/Gift Card, Standard Shipping, satchel, MSG, JCP")
	 @TestRailID(id = { "" })
	 public void testOrderPlacementForBIUserJCP() throws Exception {
		 
		 String cc[]=DUMMY_CREDIT_CARD_FOUR;
		 String CC[]=CC_DETAILS;
		 String address[]=DUMMY_ADDRESS_FOUR;
		 
		 String SearchText[]=  SEARCH_KEYWORD;
		 
		  homePage.WaitforHomePagetoLoad();
		  homePage.clickSignInLink();
		  signInPage.WaitforSignInPagetoLoad().signIn(NEW_BI_USER2[0], NEW_BI_USER2[1], "ON");
		  assertTrue(homePage.isSignInSuccessful(NEW_BI_USER2[2]), "User is not Signed In successfully as BI User");
		  logger.info("User is signed in Successfully as BI User");
		  
		 homePage.clickCategoriesSection();
		 homePage.scrollPageDown(4);

		 categoryPage.selectCategory(4, "SALE");
		 productPage.selectMultipleproduct(3);
		 assertTrue(productPage.isProductSearchResultPage("sale"), "Product Search Result Page is not opened");
		 logger.info("Product Search Result Page is opened");
		 
		 homePage.navigateToHomePage().clickPPageGC();
		 productDetailPage.clickAddToBasketButton();
		 
//		 TODO GiftCard Search ADD functionality
		 
	     /*homePage.clickSearchLink();
		 searchPage.isSearhPage();
		 searchPage.searchKeywordbyIndex(SearchText[7],0);*/
		 
		 productDetailPage.clickBasketButton().waitForBasketPageToLoad();
		 assertTrue(basketPage.isProductAddedToBasket(), "product is not added to basket");
		 logger.info("Product added to basket successfully");
		 
		 basketPage.clickCheckoutButton();
//		 TO DO BI Register/Login functionality
		 
		 signInPage.WaitforSignInPagetoLoad().signIn(NEW_BI_USER2[0], NEW_BI_USER2[1], "ON");
		 
		 basketPage.clickProductMergeOKButton().clickCheckoutButton().waitforCheckOutPageToLoad();
		 
		  Assert.assertTrue(checkoutPage.isShippingAddressPrefilled());
		  
		  Assert.assertTrue(checkoutPage.isDeliveryMethodPrefilled(DELIVERY_METHODS[0]));
		  
		  checkoutPage.clickDeliveryAndGiftOptionAccordion();
		  deliveryAndGiftOptionPage.ckeckGiftBox().selectGiftWrapOptionAndAddMessage(1, GIFTWRAP_OPTION[1], GIFT_WRAP_MESSAGE);
		  Assert.assertTrue(checkoutPage.isGiftWrapOptionAdded(GIFTWRAP_OPTION[1], GIFT_WRAP_MESSAGE));
		  
		  
		  checkoutPage.addGCShippingAndDelivery();
		  giftCardShippingPage.typeMessageGC(GIFT_WRAP_MESSAGE).clickAddNewAddressGC();
		  
		  shippingAddressPage.waitForShippingAddressPageToLoad().fillShippingAddressData(address[0], address[1], address[2], address[3])
		  .clickNextButtonOnHeaderGC();
		  
		  deliveryAndGiftOptionPage.selectDeliveryOption(0, "FREE - USPS First Class");
		  
		  checkoutPage.waitforCheckOutPageToLoad();
		  
		  Assert.assertTrue(checkoutPage.isGCAddressAddedSuccssfully(address[0]) && checkoutPage.isGCDeliveryMethodAddedSuccssfully("FREE - USPS First Class")
				  && checkoutPage.isGCMessageAddedSuccssfully(GIFT_WRAP_MESSAGE));
		  
		  checkoutPage.clickAddPaymentAccordion().clickAddCreditCardOverlay().fillJCPDetails(JCCARDTYPE, CC[1], CC[2]);
		  paymentPage.clickMakeMyDefaultCCSwitchSwitch("ON").clickSaveToMyAccountSwitch("ON").checkBillingAddressCheckbox("ON").clickDoneButton();
		  checkoutPage.waitforCheckOutPageToLoad();
		  assertTrue(checkoutPage.isCreditCardAdded(JCCARDTYPE), "credit card: "+JCCARDTYPE+ " is not added");
		  logger.info("credit card: "+JCCARDTYPE+ " is added successfully");
		  
		  checkoutPage.addPromotion(PROMO_CODE_MULTIPLE_ITEM);
		  promotionPage.selectPromotion(0);
		  Assert.assertTrue(checkoutPage.isPromoAdded(PROMO_CODE_MULTIPLE_ITEM));
		  
          Assert.assertTrue( checkoutPage.isTotalsSectionDisplayCorrectInfoforSanity());
          
		  checkoutPage.clickPlaceOrderButton();
		  Assert.assertTrue(orderConfirmationPage.isOrderSuccessfullyPlaced());
	 }
	 
}