package com.sephora.test.androidApp.data.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import com.sephora.test.androidApp.data.beanmapping.Product;


/**
 * DataService class creates connection string
 * @param <E>
 *
 */
public class DataService<E> {
	
	private  Connection con;
	private  final String driver = "oracle.jdbc.driver.OracleDriver";
	private  final String connectionString = "jdbc:oracle:thin:@10.105.36.139:1525:SEPQA01";
	private  final String user = "dv_vtomar";
	private  final String pwd = "change!now";

	public DataService() {
		try {
			Class.forName(driver);
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * 
	 * Purpose: To Make connection to DB
	 * @author: Sunny Sachdeva
	 * @date:13-Feb-2015
	 * @returnType: Connection
	 * @return
	 * @throws SQLException
	 */
	public  Connection initDatabaseProvider() throws SQLException
	{
		if(null==con || con.isClosed())
		{
			con= DriverManager.getConnection(connectionString,user,pwd);
		}
		return con;
	}

	/**
	 * 
	 * Purpose: Checks the state of Connection
	 * @author: Sunny Sachdeva
	 * @date:13-Feb-2015
	 * @returnType: boolean
	 */
	public  boolean isConnectedToDB() throws SQLException
	{
		boolean IsConncted=false;
		if(!con.isClosed()){
			IsConncted=true;
		}
		return IsConncted;
	}

	/**
	 * 
	 * Purpose: Get ResultSet from DB 
	 * @author: Sunny Sachdeva
	 * @date:13-Feb-2015
	 * @returnType: ResultSet
	 */
	public  ResultSet performSelectQuery(String sQuery) throws SQLException
	{
		Connection con = initDatabaseProvider();
		ResultSet rs;
		PreparedStatement st = con.prepareStatement(sQuery);
		rs = st.executeQuery();
		return rs;
	}

	/**
	 * 
	 * Purpose: Perform Update context in DB
	 * @author: Sunny Sachdeva
	 * @date:13-Feb-2015
	 * @returnType: void
	 */
	public  void performUpdateQuery(String query) throws SQLException {
		Connection con = initDatabaseProvider();
		//ResultSet rs;
		PreparedStatement st = con.prepareStatement(query);
		st.executeUpdate();
	}

	/**
	 * 
	 * Purpose: Close the Connection
	 * @author: Sunny Sachdeva
	 * @date:13-Feb-2015
	 * @returnType: void
	 */
	public  void closeConnection() 
	{
		if(con!=null)
		{
			try {
				con.close();
			} catch (SQLException e) {

			}
		}
	}
	
	public void dbHandler() throws SQLException{
		DbUtils.loadDriver(driver);
		Connection conn=initDatabaseProvider();
		System.out.println("Is Connection closed : "+conn.isClosed());
		QueryRunner runner= new QueryRunner();
		List<Product> products=null;
		products= (List<Product>) runner.query(conn,CommonDBQueries.GET_EXPENSIVE_PRODUCT, new BeanListHandler<Product>(Product.class));
		
		for(Product oProduct: products){
			System.out.println(oProduct.getSku_id());
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public <T> List<E> getBeanFromSql(Class<T> clazz,String query) throws Exception{
		DbUtils.loadDriver(driver);
		Connection conn=initDatabaseProvider();
		List<E> list=null;
		System.out.println("Is Connection closed : "+conn.isClosed());
		if(!conn.isClosed()){
			QueryRunner runner= new QueryRunner();
			list=(List<E>) runner.query(conn, query,new BeanListHandler<T>(clazz));
		}else{
			throw new Exception("Connection State is closed. Please check the credentials");
		}
		
		return list;
	}


}
