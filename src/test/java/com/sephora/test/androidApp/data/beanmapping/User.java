package com.sephora.test.androidApp.data.beanmapping;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="User")
public class User {

	
	private String emailId;
	private String firstName;
	private String lastName;
	private String password;
	private String securityQuestion;
	private String securityAnswer;
	private String securityCheck;
	private String month;
	private String day;
	private String year;

	public String getEmailId() {
		return emailId;
	}
	
	@XmlElement
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	@XmlElement
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	@XmlElement
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getPassword() {
		return password;
	}
	
	@XmlElement
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getSecurityQuestion() {
		return securityQuestion;
	}
	
	@XmlElement
	public void setSecurityQuestion(String securityQuestion) {
		this.securityQuestion = securityQuestion;
	}
	
	public String getSecurityAnswer() {
		return securityAnswer;
	}
	
	@XmlElement
	public void setSecurityAnswer(String securityAnswer) {
		this.securityAnswer = securityAnswer;
	}
	
	public String getSecurityCheck() {
		return securityCheck;
	}
	
	@XmlElement
	public void setSecurityCheck(String securityCheck) {
		this.securityCheck = securityCheck;
	}
	
	public String getMonth() {
		return month;
	}
	
	@XmlElement
	public void setMonth(String month) {
		this.month = month;
	}
	
	public String getDay() {
		return day;
	}
	
	@XmlElement
	public void setDay(String day) {
		this.day = day;
	}
	
	public String getYear() {
		return year;
	}
	
	@XmlElement
	public void setYear(String year) {
		this.year = year;
	}
	
}
