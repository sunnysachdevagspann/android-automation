package com.sephora.test.androidApp.data.sql;

import java.io.File;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.testng.annotations.Test;

import com.sephora.test.androidApp.data.beanmapping.Product;
import com.sephora.test.androidApp.data.beanmapping.User;


/**
 * @author Admin
 *
 */
public class TestDB {
	
	
	@Test
	public void testDBStuff() throws Exception{
		DataService<Product> ds = new DataService<Product>();
		List<Product> products=ds.getBeanFromSql(Product.class, CommonDBQueries.GET_EXPENSIVE_PRODUCT);
		System.out.println(products.size());
		for(Product o:products){
			System.out.println(o.getSku_id());
		}
	}
	
	
	@Test
	public void testXMLStuff() throws JAXBException{
		File file = new File("src/test/resources/staticdata/User.xml");
		System.out.println(file.exists());
		JAXBContext jaxbContext = JAXBContext.newInstance(User.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		User newUser= (User) jaxbUnmarshaller.unmarshal(file);
		System.out.println(newUser.getDay());
	}
}
