package com.sephora.test.androidApp.data.sql;

/**
 * CommonDBQueries provides queries to be used during execution
 */
public class CommonDBQueries {
	
	
	public static String GET_EXPENSIVE_PRODUCT =" SELECT DS.sku_id FROM ATGCATA.DCS_SKU DS INNER JOIN ATGCATA.SEPH_SKU_MCS B ON DS.SKU_ID = B.SKU_ID INNER JOIN ATGCORE.DCS_INVENTORY ST ON DS.SKU_ID = ST.CATALOG_REF_ID INNER JOIN ATGCATA.SEPH_SKU P_SKU ON DS.SKU_ID = P_SKU.SKU_ID INNER JOIN ATGCATA.SEPH_PRODUCT PR ON  pr.DEF_SKU_ID = p_sku.sku_id INNER JOIN ATGCATA.SEPH_PRODUCT PR ON  pr.DEF_SKU_ID = p_sku.sku_id INNER JOIN ATGCATA.SEPH_SKU_FLAGS F ON DS.SKU_ID = F.SKU_ID  inner join ATGCATA.dcs_product dp on p_sku.primary_product_id = dp.product_id   inner join ATGCATA.seph_brand sb on PR.BRAND_ID = sb.brand_id WHERE PR.is_enabled = 1 and F.is_enabled = 1  and (ds.end_date is null or ds.end_date >= sysdate)  and p_sku.temp_deactivation is null  and st.on_hold = 0 and st.stock_level > 3  and st.avail_status = 1004  and p_sku.primary_product_id is not null  and pr.temporary_deactive_dt is null  and (dp.end_date is null or dp.end_date >= sysdate)  and sb.enabled = 1  and (sb.end_date is null or sb.end_date >= sysdate)  AND P_SKU.sku_type = 0 AND P_SKU.bi_type= 2 AND f.is_hazmat < > 1 and pr.sephora_product_template = '200006' AND DS.SKU_ID in (select sku.sku_id from ATGCATA.seph_sku sku inner join ATGCORE.dcs_price price on price.sku_id=sku.sku_id where price_list='plist32120019' and list_price between 260 and 300)  and not exists( select 1 from ATGCATA.SEPH_SKU_COUNTRYCODE sku_restr_country where sku_restr_country.country_code_id in ('10001', '10002') and sku_restr_country.sku_id = ds.sku_id ) and not exists( select 1 from ATGCATA.SEPH_BRAND_COUNTRYCODE brand_restr_country where brand_restr_country.country_code_id in ('10001', '10002') and brand_restr_country.brand_id = pr.brand_id ) and f.is_store_only = '0' and f.is_bi_only = 0";
	public static String GET_BI_Exclusive_Product=" SELECT DS.sku_id FROM ATGCATA.DCS_SKU DS INNER JOIN ATGCATA.SEPH_SKU_MCS B ON DS.SKU_ID = B.SKU_ID INNER JOIN ATGCORE.DCS_INVENTORY ST ON DS.SKU_ID = ST.CATALOG_REF_ID INNER JOIN ATGCATA.SEPH_SKU P_SKU ON DS.SKU_ID = P_SKU.SKU_ID INNER JOIN ATGCATA.SEPH_PRODUCT PR ON  pr.DEF_SKU_ID = p_sku.sku_id INNER JOIN ATGCATA.SEPH_PRODUCT PR ON  pr.DEF_SKU_ID = p_sku.sku_id INNER JOIN ATGCATA.SEPH_SKU_FLAGS F ON DS.SKU_ID = F.SKU_ID  inner join ATGCATA.dcs_product dp on p_sku.primary_product_id = dp.product_id   inner join ATGCATA.seph_brand sb on PR.BRAND_ID = sb.brand_id WHERE PR.is_enabled = 1 and F.is_enabled = 1  and (ds.end_date is null or ds.end_date >= sysdate)  and p_sku.temp_deactivation is null  and st.on_hold = 0 and st.stock_level > 3  and st.avail_status = 1004  and p_sku.primary_product_id is not null  and pr.temporary_deactive_dt is null  and (dp.end_date is null or dp.end_date >= sysdate)  and sb.enabled = 1  and (sb.end_date is null or sb.end_date >= sysdate)  AND P_SKU.sku_type = 0 AND P_SKU.bi_type= 2 AND f.is_hazmat <> 1 and pr.sephora_product_template = '200006' AND DS.SKU_ID in (select sku.sku_id from ATGCATA.seph_sku sku inner join ATGCORE.dcs_price price on price.sku_id=sku.sku_id where price_list='plist32120019' and list_price between 151 and 174) and f.is_store_only = '0' and f.is_bi_only = 0";
//	public static String
//	public static String
//	public static String
//	public static String
//	public static String
//	public static String
//	public static String
//	public static String
//	public static String
//	public static String
//	public static String
//	public static String
//	public static String
	

}
