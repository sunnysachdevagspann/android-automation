package com.sephora.test.androidApp.data.beanmapping;

public class Product {
	private String sku_id;

	public String getSku_id() {
		return sku_id;
	}

	public void setSku_id(String sku_id) {
		this.sku_id = sku_id;
	}

}
