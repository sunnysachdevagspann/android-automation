package com.sephora.test.androidApp.criticalPath;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sephora.core.utils.DBUtil;
import com.sephora.pages.AndroidApp.Basket.BasketPage;
import com.sephora.pages.AndroidApp.Category.CategoryPage;
import com.sephora.pages.AndroidApp.Checkout.CheckoutPage;
import com.sephora.pages.AndroidApp.Checkout.DeliveryAndGiftOptionPage;
import com.sephora.pages.AndroidApp.Checkout.GiftCardPage;
import com.sephora.pages.AndroidApp.Checkout.OrderConfirmationPage;
import com.sephora.pages.AndroidApp.Checkout.PaymentPage;
import com.sephora.pages.AndroidApp.Checkout.ShippingAddressPage;
import com.sephora.pages.AndroidApp.Home.HomePage;
import com.sephora.pages.AndroidApp.MyAccount.MyAccountPage;
import com.sephora.pages.AndroidApp.Navigation.LeftNavigationDrawerPage;
import com.sephora.pages.AndroidApp.Product.ProductDetailPage;
import com.sephora.pages.AndroidApp.Product.ProductPage;
import com.sephora.pages.AndroidApp.Promotion.PromotionPage;
import com.sephora.pages.AndroidApp.Search.SearchPage;
import com.sephora.pages.AndroidApp.SignIn.CreateAccountPage;
import com.sephora.pages.AndroidApp.SignIn.SignInPage;
import com.sephora.test.androidApp.Base.SephoraAndroidAppBaseTest;

public class BasketCPBase extends SephoraAndroidAppBaseTest{
	
	protected HomePage homePage;
	protected SearchPage searchPage;
	protected SignInPage signInPage;
	protected CreateAccountPage createAccountPage;
	protected CategoryPage categoryPage;
	protected ProductPage productPage;
	protected ProductDetailPage productDetailPage;
	protected BasketPage basketPage;
	protected CheckoutPage checkoutPage;
	protected PaymentPage paymentPage;
	protected ShippingAddressPage shippingAddressPage;
	protected GiftCardPage giftCardPage;
	protected OrderConfirmationPage orderConfirmationPage;
	protected LeftNavigationDrawerPage leftNavigationDrawerPage;
	protected MyAccountPage myAccountPage;
	protected DeliveryAndGiftOptionPage deliveryAndGiftOptionPage;
	protected PromotionPage promotionPage;
	public DBUtil dbutil = new DBUtil();
	
	
	
	private final Logger logger = LogManager.getLogger(this.getClass().getName());
	
	public BasketCPBase() {
		homePage = new HomePage(driver);
		searchPage = new SearchPage(driver);
		signInPage = new SignInPage(driver);
		createAccountPage= new CreateAccountPage(driver);
		categoryPage = new CategoryPage(driver);
		productPage = new ProductPage(driver);
		productDetailPage = new ProductDetailPage(driver);
		basketPage = new BasketPage(driver);
		checkoutPage = new CheckoutPage(driver);
		paymentPage = new PaymentPage(driver);
		shippingAddressPage = new ShippingAddressPage(driver);
		giftCardPage = new GiftCardPage(driver);
		orderConfirmationPage = new OrderConfirmationPage(driver);
		leftNavigationDrawerPage = new LeftNavigationDrawerPage(driver);
		myAccountPage = new MyAccountPage(driver);
		deliveryAndGiftOptionPage = new DeliveryAndGiftOptionPage(driver);
		promotionPage = new PromotionPage(driver);
		
	}

}

