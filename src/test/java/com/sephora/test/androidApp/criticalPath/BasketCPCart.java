package com.sephora.test.androidApp.criticalPath;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;
import com.sephora.core.annotations.TestRailID;
import com.sephora.core.utils.SoftAssert;

public class BasketCPCart extends BasketCPBase{
	
 private static final Logger logger = LogManager.getLogger(BasketCPCart.class.getName());
	 
	 @Test(enabled=true, description = "Cart/remove+move to loves")
	 @TestRailID(id = { "C137673" })
	 
	  public void cart_remove_move_to_loves_test_C137673() throws Exception{
		 
		 String SearchText[]=  SEARCH_KEYWORD;
		 homePage.WaitforHomePagetoLoad();
		 
		  homePage.clickSearchIcon();
		  searchPage.searchKeywordbyIndex(SearchText[0], 0);
		  
		  productPage.selectMultipleproduct(3);
		  productDetailPage.clickBasketButton().waitForBasketPageToLoad();
		  Assert.assertTrue(basketPage.isProductAddedToBasket());
		  basketPage.isProductInfoCorrectWithNoTrucation(3);
		  basketPage.scrollPageUP(3);
		  basketPage.removeProductFromBasket(0, 1);
		  
		  int itemsAfterRemovalOfProduct=basketPage.getNumberOfItemsFromBasketOrderTotal(); 
		  logger.info("Quantity of product after removing 1st product: "+itemsAfterRemovalOfProduct);
		  
		  assertTrue(basketPage.isProductInfoCorrectWithNoTrucation(2), "The quantity of product in basket after removal is not updated successfully");
		  logger.info("Sku line item is removed");
		  basketPage.scrollPageUP(3);
		  basketPage.clickMovesToLoves(0, 1);
		  int itemsAfterMovesToLovesOfProduct=basketPage.getNumberOfItemsFromBasketOrderTotal(); 
		  logger.info("items in basket after moving the product to Loves is: "+itemsAfterMovesToLovesOfProduct);
		  assertTrue(basketPage.isProductInfoCorrectWithNoTrucation(1), "The quantity of product in basket after moving product to loves is not updated successfully");
		  logger.info("Sku item is removed from cart to Loves List");
		  
		  basketPage.clickCheckoutButton();
		  signInPage.WaitforSignInPagetoLoad().signIn(NEW_BI_USER[0], NEW_BI_USER[1], "ON");
		  basketPage.clickProductMergeOKButton().waitForBasketPageToLoad().clickCheckoutButton().waitforCheckOutPageToLoad().scrollToExact("Merchandise Subtotal");
		  checkoutPage.clickItemTotalDropList().scrollPageDownForBasket(1);
		  assertTrue(checkoutPage.isCheckoutProductInfoCorrect(1), "Items section on checkout is not updated");
		  logger.info("Items section on checkout is updated, removed items isn't displayed in Checkout");
	 }
		
	 

	 
	 @Test(enabled=true, description = "Cart/hardgood layout")
	 @TestRailID(id = { "C137672" })
	 
	  public void cart_hardgood_layout_test_C137672() throws Exception{
		
		 homePage.WaitforHomePagetoLoad();
		 homePage.clickPPageColor();
		 productDetailPage.clickAddToBasketButton().clickAddToBasketButton().navigateBack();
		 homePage.clickPPageGC();
		 productDetailPage.clickAddToBasketButton().navigateBack();
		 homePage.clickPPageSize();
		 productDetailPage.clickAddToBasketButton().navigateBack();
		 homePage.clickPPageType();
		 productDetailPage.clickAddToBasketButton().clickBasketButton();
		 basketPage.waitForBasketPageToLoad();
		 assertTrue(basketPage.isProductInfoCorrectWithNoTrucation(4), "product info is not correct and is not displayed in full (no truncation)");
		 logger.info("product info is correct and is displayed in full (no truncation)");
		 assertTrue(basketPage.getNumberOfItemsFromBasketOrderTotal()==5, "product added to basket is incorrect in number");
		 logger.info("product displayed on basket page is correct in number");
		 basketPage.clickCheckoutButton();
		 signInPage.WaitforSignInPagetoLoad().signIn(NEW_BI_USER[0], NEW_BI_USER[1], "ON");
		 basketPage.clickProductMergeOKButton().waitForBasketPageToLoad().clickCheckoutButton().waitforCheckOutPageToLoad().scrollToExact("Merchandise Subtotal");
		 checkoutPage.clickItemTotalDropList();
		 assertTrue(checkoutPage.getNumberOfItemsFromCheckoutOrderTotal()==5, "product displayed on checkout page is incorrect in number");
		 logger.info("product displayed on checkout page is correct in number");
         checkoutPage.scrollPageDownForBasket(1);	
         assertTrue(checkoutPage.isCheckoutProductInfoCorrect(4), "product on checkout page fails to display correct info");
         logger.info("product on checkout page display correct info");
         assertTrue(checkoutPage.checkoutItemDetails.toString().contentEquals(basketPage.basketItemDetails.toString()),"product info on checkout page and Basket page fails to match");
         logger.info("product info on checkout page and Basket page match successfully");
	 }	 
}
