package com.sephora.test.androidApp.criticalPath;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.sephora.core.annotations.TestRailID;

public class BasketCPPromo extends BasketCPBase{
	
 private static final Logger logger = LogManager.getLogger(BasketCPPromo.class.getName());
	 
	 @Test(enabled=true, description = "Promo/gwp")
	 @TestRailID(id = { "C137678" })
	  public void promo_gwp_test_C137678() throws Exception{
		 
		 homePage.WaitforHomePagetoLoad();
		 homePage.clickSignInLink();
		 signInPage.WaitforSignInPagetoLoad().signIn(NEW_BI_USER[0], NEW_BI_USER[1], "ON");
		 homePage.clickPPageColor();
		 productDetailPage.clickAddToBasketButton().clickAddToBasketButton().navigateBack();
		/* homePage.clickPPageSingleSKU();
		 productDetailPage.clickAddToBasketButton().navigateBack();*/
		 homePage.clickPPageSize();
		 productDetailPage.clickAddToBasketButton().clickBasketButton();
		 basketPage.waitForBasketPageToLoad();
		 assertTrue(basketPage.isPromotionFieldValid(), "promotion section fails to display valid fields");
		 int numberOfItemsBefore = basketPage.getNumberOfItemsFromBasketOrderTotal();
		 logger.info("the number of products before adding promo in basket is: " + numberOfItemsBefore);
		 basketPage.addPromotion("GWP");
		 int numberOfItemsAfter = basketPage.getNumberOfItemsFromBasketOrderTotal();
		 logger.info("the number of products after adding promo in basket is: " + numberOfItemsAfter);
		 assertTrue(basketPage.isPromoAdded(PROMO_CODE_GWP), "promotion is not added successfully");
		 Assert.assertTrue(basketPage.isremovePromotionButtonPresent());
		 assertTrue(numberOfItemsAfter==(numberOfItemsBefore+1), "Quantity of item is not updated successfully");
		 logger.info("Quantity of item is successfully updated");
		 basketPage.scrollPageUP(5);
		 assertTrue(basketPage.isProductInfoCorrectWithNoTrucation(2), "product on Basket page fails to display correct info");
		 logger.info("product on basket page display correct info");
		 basketPage.clickCheckoutButton().waitforCheckOutPageToLoad().scrollToExact("Merchandise Subtotal");
		 checkoutPage.clickItemTotalDropList().scrollPageDownForCheckout(1);	
         assertTrue(checkoutPage.isCheckoutProductInfoCorrect(3), "product on checkout page fails to display correct info");
         logger.info("product on checkout page display correct info");
         checkoutPage.navigateBack();
         basketPage.waitForBasketPageToLoad();
         int itemsBeforeRemovingPromo = basketPage.getNumberOfItemsFromBasketOrderTotal();
         basketPage.removePromotion();
         int itemsAfterRemovingPromo = basketPage.getNumberOfItemsFromBasketOrderTotal();
         assertFalse(basketPage.isremovePromotionButtonPresent(), "promo is not removed");
         logger.info("promo is removed successfully");
         Assert.assertTrue(basketPage.isPromotionFieldValid(), "promotion section fails to display valid fields");
         assertTrue(itemsBeforeRemovingPromo == (itemsAfterRemovingPromo+1), "Item number is not decreased in Order Total.");
         logger.info("Item number is decreased in Order Total.");
         basketPage.clickCheckoutButton().waitforCheckOutPageToLoad().scrollPageDown(4);
         int itemsOnCheckout = checkoutPage.getNumberOfItemsFromCheckoutOrderTotal();
         assertFalse(basketPage.isremovePromotionButtonPresent() && itemsAfterRemovingPromo==itemsOnCheckout, " Promo item not updated on Checkout");
         logger.info("Items is updated and Promo item is removed from Checkout");
		 // TO Do verification of Step 5
	 }	 
		 
	 
	 @Test(enabled=true, description = "Promo10%off")
	 @TestRailID(id = { "C137679" })
	  public void promo__10_percent_off_test_C137679() throws Exception{
		 
		 homePage.WaitforHomePagetoLoad();
		 homePage.clickSignInLink();
		 signInPage.WaitforSignInPagetoLoad().signIn(NEW_BI_USER[0], NEW_BI_USER[1], "ON");
		 homePage.clickPPageColor();
		 productDetailPage.clickAddToBasketButton().navigateBack();
		 homePage.clickPPageGC();
		 productDetailPage.clickAddToBasketButton().navigateBack();
		 homePage.clickPPageSize();
		 productDetailPage.clickAddToBasketButton().clickBasketButton();
		 basketPage.waitForBasketPageToLoad();
		 int numberOfItemsBefore = basketPage.getNumberOfItemsFromBasketOrderTotal();
		 logger.info("the number of products before adding promo in basket is: " + numberOfItemsBefore);
		 double merchTotalBeforeAddingPromo = basketPage.getMerchandiseTotalFromBasketOrderTotal();
		 basketPage.addPromotion(PROMO_CODE_10_OFF);
		 assertTrue(basketPage.isPromoAdded(PROMO_CODE_10_OFF), "promotion is not added successfully");
		 double discountTotalAfterAddingPromo = basketPage.getDiscountTotalFromBasketOrderTotal();
		 double merchTotalAfterAddingPromo = basketPage.getMerchandiseTotalFromBasketOrderTotal();
		/* int numberOfItemsAfter = basketPage.getNumberOfItemsFromBasketOrderTotal();
		logger.info("the number of products after adding promo in basket is: " + numberOfItemsAfter); */
		 Assert.assertTrue(basketPage.isremovePromotionButtonPresent());
		 Assert.assertTrue(basketPage.isPromoOfPercentTypeReflectsCorrectDiscount(10.00), "The promo applied fails to reflect correct discount value");
		 Assert.assertTrue(basketPage.isDiscountPromoUpdatesSubtotal(merchTotalBeforeAddingPromo, discountTotalAfterAddingPromo, merchTotalAfterAddingPromo), "Discount promo fails to update subtotal in price summary");
		 Assert.assertTrue(basketPage.isPromoApplyOnMerchOnly(10.00), "The promo is not applied only on merch");
         basketPage.removePromotion();
         assertFalse(basketPage.isremovePromotionButtonPresent(), "promo is not removed");
         double merchTotalAfterRemovingPromo = basketPage.getMerchandiseTotalFromBasketOrderTotal();
         Assert.assertTrue(basketPage.isPromotionFieldValid(), "promotion section fails to display valid fields");
         Assert.assertFalse(basketPage.isDiscountLineDisplayed(), "Discount line is displayed");
         Assert.assertTrue(basketPage.isOrderPriceUpdated(merchTotalAfterAddingPromo, discountTotalAfterAddingPromo, merchTotalAfterRemovingPromo), "Order price is not updated.");
	 }
	 
	 @Test(enabled=true, description = "Promo/$off")
	 @TestRailID(id = { "C137680" })
	  public void promo_$_OFF_test_C137680() throws Exception{
		 homePage.WaitforHomePagetoLoad();
		 homePage.clickSignInLink();
		 signInPage.WaitforSignInPagetoLoad().signIn(NEW_BI_USER[0], NEW_BI_USER[1], "ON");
		 homePage.clickPPageColor();
		 productDetailPage.clickAddToBasketButton().navigateBack();
		 homePage.clickPPageType();
		 productDetailPage.clickAddToBasketButton().clickBasketButton();
		 basketPage.waitForBasketPageToLoad();
		 double orderTotal = basketPage.getMerchandiseTotalFromBasketOrderTotal();
		 logger.info("Oder total is: " + orderTotal);
		 basketPage.addPromotion(PROMOCODE_15_OFF);
		 Assert.assertTrue(basketPage.isPromoApplyErrorMessageValid(PROMO_ERROR_MESSAGE_FOR_LESS_THAN_100$_CART), "promo error message is invalid");
		 homePage.navigateToHomePage().clickPPageSize();
		 productDetailPage.clickAddToBasketButton().clickBasketButton();
		 basketPage.waitForBasketPageToLoad();
		 Assert.assertTrue(basketPage.isProductAddedMoreThanXAmount(100.00), "The order total is basket is not more than $100");
		 double merchTotalBeforeAddingPromo = basketPage.getMerchandiseTotalFromBasketOrderTotal();
		 basketPage.addPromotion(PROMOCODE_15_OFF);
		 assertTrue(basketPage.isPromoAdded(PROMOCODE_15_OFF), "promotion is not added successfully");
		 double discountTotalAfterAddingPromo = basketPage.getDiscountTotalFromBasketOrderTotal();
		 double merchTotalAfterAddingPromo = basketPage.getMerchandiseTotalFromBasketOrderTotal();
		 Assert.assertTrue(basketPage.isremovePromotionButtonPresent());
		 Assert.assertTrue(basketPage.isPromoOf$TypeReflectsCorrectDiscount(15.00), "The promo applied fails to reflect correct discount value");
		 Assert.assertTrue(basketPage.isDiscountPromoUpdatesSubtotal(merchTotalBeforeAddingPromo, discountTotalAfterAddingPromo, merchTotalAfterAddingPromo), "Discount promo fails to update subtotal in price summary");
         basketPage.removePromotion();
         assertFalse(basketPage.isremovePromotionButtonPresent(), "promo is not removed");
         double merchTotalAfterRemovingPromo = basketPage.getMerchandiseTotalFromBasketOrderTotal();
         Assert.assertTrue(basketPage.isPromotionFieldValid(), "promotion section fails to display valid fields");
         Assert.assertFalse(basketPage.isDiscountLineDisplayed(), "Discount line is displayed");
         Assert.assertTrue(basketPage.isOrderPriceUpdated(merchTotalAfterAddingPromo, discountTotalAfterAddingPromo, merchTotalAfterRemovingPromo), "Order price is not updated.");
	 }
	 
	 @Test(enabled=true, description = "Promo/msg select")
	 @TestRailID(id = { "C137684" })
	  public void promo_msg_select_test_C137684() throws Exception{
		 homePage.WaitforHomePagetoLoad();
		 homePage.clickSignInLink();
		 signInPage.WaitforSignInPagetoLoad().signIn(NEW_BI_USER[0], NEW_BI_USER[1], "ON");
		 homePage.clickPPageColor();
		 productDetailPage.clickAddToBasketButton().navigateBack();
		 homePage.clickPPageType();
		 productDetailPage.clickAddToBasketButton().clickBasketButton();
		 basketPage.waitForBasketPageToLoad();
		 double orderTotal = basketPage.getMerchandiseTotalFromBasketOrderTotal();
		 logger.info("Oder total is: " + orderTotal);
		 basketPage.addPromotion(PROMO_CODE_YOUR_GIFT);
		 promotionPage.selectPromotion(0);
		 Assert.assertTrue(checkoutPage.isPromoAdded(PROMO_CODE_YOUR_GIFT));
	 }
		 
}
